<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
    private $salt;

    public function __construct()
    {
        parent::__construct();

        // Password generation using crypt and salt
        $this->salt = '$2a$07$R.ABb2QcZ.FMk4phJ1y2CN$';

//        $password = crypt($password, $salt);
    }


    /****************************************************************
     *
     *      EULA - End User License Agreement CRUD operations
     *
     *
     *
     ****************************************************************/

    public function getAgreement($id = null, $company = null)
    {
        $this->db->from('agreement');
        if (!is_null($id)) {
            $this->db->where('id', $id);
        } elseif (!is_null($company)) {
            $this->db->where('company', $company);
        }
        $this->db->order_by('agreedon', "desc");
        $result = $this->db->get();
        return $result;
    }

    public function validAgreement($company = null)
    {
        if (ENVIRONMENT == 'development') {  // currently only enabled in dev environment

            if (is_null($company))
                $company = $this->session->userdata('company');

            $this->db->from('agreement');
            $this->db->where('company', $company);
            $this->db->order_by('agreedon', "desc");

            $result = $this->db->get();
            if ($result->num_rows() <= 0)
                return false;
            else {
                $row = $result->row();
                $eula = $this->getEula($row->license, true)->row();
                $version = $eula->version;
                $latest = $this->getCurrentEulaVersion();
                if (floor($version) == floor($latest))  // main version is the same ie 5.xx
                    return true;
            }
            return false;
        }
        else
            return true;
    }


    public function eulaAgreed($company = null, $license = null)
    {
        // license id is linked to the version, the status and the content.
        if (is_null($company) || is_null($license))
            return false;
        $data = array(
            'company' => $company,
            'license' => $license,
            'agreedon' => Date('Y-m-d h:i:s', time()),
            'agreedby' => $this->session->userdata('id'),
            'agreedfrom' => $this->input->ip_address()
        );
        $result = $this->db->insert('agreement', $data);
        return $this->db->insert_id();
    }

    public function addEula($version = null, $status = 0, $content = null)
    {
        if (is_null($version) || is_null($content))
            return false;

        $data = array(
            'version' => $version,
            'edited' => date('Y-m-d h:i:s', time()),
            'editedby' => $this->session->userdata('id'),
            'status' => $status,
            'content' => $content
        );

        $result = $this->db->insert('license', $data);
        return $this->db->insert_id();
    }

    public function editEula($id = null, $data = null)
    {
        if (is_null($id))
            return false;

        $data['edited'] = date('Y-m-d h:i:s', time());
        $data['editedby'] = $this->session->userdata('id');
        $this->db->where('id', $id);
        $result = $this->db->update('license', $data);
        return $result;
    }

    public function getEula($id = null, $final = false)
    {
        $this->db->from('license');
        if (!is_null($id)) {
            $this->db->where('id', $id);
        } elseif ($final) {
            $this->db->where('status', 2);
        }
        $this->db->order_by('version', "desc");
        $result = $this->db->get();
        return $result;
    }

    public function getCurrentEulaVersion()
    {

        $result = $this->getEula(null, true);
        $eula = $result->row();
        return $eula->version;
    }

    public function resetApprovedEula(){

        $data = array('status' => 4);
        $this->db->where('status', "2");
        $result = $this->db->update('license', $data);
        return $result;
    }

    /****************************************************************
     *
     *      COMPANY CRUD operations
     *
     *
     *
     ****************************************************************/

    public function updateCompany($id = null, $data = null)
    {
        if (is_null($data))
            return false;

        if (is_null($id) || $id == "") {
            $result = $this->db->insert('company', $data);
        } else {
            $this->db->where('id', $id);;
            $result = $this->db->update('company', $data);
        }
        return $result;
    }

    public function getVIP($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('company');
        $this->db->where('id', $id);
        $this->db->from('company');
        $query = $this->db->get();
        return $query->row()->vip;
//        $sql = 'SELECT id, name, world, status, score FROM teams WHERE world = '.$world.' AND status = 1 Order By score desc limit 5';

    }// END public function updateUser()

    public function getCompany($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('company');

        $this->db->where('id', $id);
        $this->db->from('company');
        $query = $this->db->get();
        return $query->row();
    }// END public function updateUser()

    public function getCompanyByVIP($vip = null)
    {
        if ($vip == null)
            return false;

        $this->db->where('vip', $vip);
        $this->db->from('company');
        $query = $this->db->get();
        return $query;
    }// END public function updateUser()

    public function getApprovers($company = null)
    {
        if ($company == null)
            $company = $this->session->userdata('company');
        if (!isset($company)) return false;
        $this->db->where('company', $company);
        $this->db->where('status', 2);
        $this->db->where('usertype <=', 3);
        $this->db->from('users');
        $this->db->order_by('name', 'asc');
        $query = $this->db->get();
        return $query;

    }


    /****************************************************************
     *
     *      USER CRUD operations
     *
     *
     *
     ****************************************************************/


    public function isUnique($email = null, $company = 0)
    {
        if (!is_null($email)) {
            $this->db->where('email', $email);
            $this->db->where('company', $company);
            $this->db->where('status !=', 5);
            $result = $this->db->count_all_results('users');
            return $result;
        } else
            return false;

    }

    public function getUserType($id = null)
    {
        if ($id != null)
            $this->db->where('id', $id);

        $this->db->from('usertype');
        $query = $this->db->get();
        return $query;
    }

    public function updateUser($id = null, $data = null)
    {
        if ($id == null)
            return false;
        if ($data == null)
            return false;

        $this->db->where('id', $id);;
        $result = $this->db->update('users', $data);
        return $result;
    }

    public function updateLastLogin($id = null)
    {
        if ($id == null)
            return false;

        $data = array('lastlogin' => date('Y-m-d h:i:s', time()));

        $this->db->where('id', $id);;
        $result = $this->db->update('users', $data);
        return $result;
    }

    public function getUser($userID = null)
    {
        if (is_null($userID))
            $userID = $this->session->userdata('id');

        $this->db->where('id', $userID);

        $this->db->from('users');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query;
        else
            return false;
    }// END public function updateUser()

    public function getUserBy($data = null)
    {
        if ($data == null)
            return false;

        $this->db->where($data);
//        $this->db->where('status !=', 5);
        $this->db->from('users');
        $query = $this->db->get();
        return $query;
    }// END public function updateUser()

//
// may need a different function for active 2 or inactive 5 users
//
    public function getUserByEmail($email = null, $company = null, $active = true)
    {
        if ($email == null)
            return false;

        $this->db->select('users.*');
        $this->db->from('users');
        $this->db->join('company', 'users.company = company.id');
        if ($company != null)
            $this->db->where('users.company', $company);
        $this->db->where('users.email', $email);
        if($active)
            $this->db->where('users.status !=', 5);
        $this->db->where('company.status', 2);

        $query = $this->db->get();
        return $query;
    }// END public function updateUser()



    public function getUserByAOID($attacheonlineid = null,$company = null)
    {
        if ($attacheonlineid == null || $company == null)
            return false;

        $this->db->where('attacheonlineid', $attacheonlineid);
        $this->db->where('company', $company);
//        $this->db->where('status !=', 5);  if found by unique Id, then they are found even if deleted
        $this->db->from('users');
        $query = $this->db->get();
        if ($query->num_rows() == 1)
            return $query->row();
        else
            return false;
    }// END public function updateUser()
    //

    public function getUserByEmpID($location = null, $empID = null, $company = null)
    {
        if ($empID == null || $company == null)
            return false;

        $this->db->where('employeeID', $empID);

        if($location != null)
            $this->db->where('location', $location);

        $this->db->where('company', $company);
        $this->db->where('status !=', 5);
        $this->db->from('users');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query;
        else
            return false;
    }// END public function updateUser()


    public function getUsers($company = null)
    {
        if ($company == null)
            $company = $this->session->userdata('company');

        $this->db->where('company', $company);
        $this->db->where('status !=', 5);
        $this->db->from('users');
        $this->db->order_by('name', 'asc');
        $query = $this->db->get();
        return $query;


    }


    public function getAllUsers($company = null)
    {
        if ($company == null)
            $company = $this->session->userdata('company');

        $this->db->where('company', $company);
        $this->db->from('users');
        $this->db->order_by('name', 'asc');
        $query = $this->db->get();
        return $query;


    }


    public function getLeaveLiability($company = null, $threshold = 150)
    {
        if ($company == null)
            $company = $this->session->userdata('company');

        $this->db->where('company', $company);
//        $this->db->where('leavebalance >=', $threshold);
        $this->db->where('status !=', 5);
        $this->db->from('users');
        $this->db->order_by('leavebalance', 'desc');
        $query = $this->db->get();
        return $query;


    }


    /****************************************************************
     *
     *      REGION CRUD operations
     *
     *
     *
     ****************************************************************/

    public function getCountryByRegion($state = null)
    {
        if ($state == null)
            return false;

        $this->db->where('id', $state);
        $this->db->from('regions');
        $query = $this->db->get();
        return $query->row();

    }


    public function getCountry($countryID = null)
    {
        if (!is_null($countryID))
            $this->db->where('id', $countryID);

        $this->db->from('countries');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query;
        else
            return false;
    }// END public function updateUser()

    public function removeRegion($region)
    {
        if (!is_null($region)) {
            $this->db->where('id', $region);
            $this->db->delete('regions');
            return true;
        }
        return false;
    }

    public function getRegionsByCountry($countryID = null)
    {
        if ($countryID == null)
            return false;

        $this->db->where('country', $countryID);
        $this->db->from('regions');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query;
        else
            return false;
    }// END public function updateUser()

    public function addcountry($data = null)
    {
        if (!is_null($data)) {
            $this->db->insert('countries', $data);
            return $this->db->insert_id();
        } else
            return false;
    }

    public function deletecountry($id = null)
    {
        if (!is_null($id)) {
            $this->db->where('id', $id);
            $this->db->delete('countries');
            return true;
        } else
            return false;
    }

    public function deletecompany($id = null)
    {
        if (!is_null($id)) {
            $this->db->where('id', $id);
            $this->db->delete('company');

            $this->db->where('company', $id);
            $this->db->delete('users');


            $this->db->where('company', $id);
            $this->db->delete('leavetype');

            return true;
        } else
            return false;
    }


    public function getRegionByName($name = null)
    {
        if ($name == null)
            return false;

        $this->db->where('name', $name);
        $this->db->or_where('region', $name);
        $this->db->from('regions');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query;
        else
            return false;
    }// END public function updateUser()

    public function getRegion($id = null)
    {
        if ($id == null)
            return false;

        $this->db->where('id', $id);
        $this->db->from('regions');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query;
        else
            return false;
    }// END public function updateUser()

    public function createRegion($data = null)
    {
        if (!is_null($data)) {
            $result = $this->db->insert('regions', $data);
            return $this->db->insert_id();
        } else
            return false;
    }

    /****************************************************************
     *
     *      LEAVE PROCESSING CRUD operations
     *
     *
     *
     ****************************************************************/

    public function createBatch($data = null)
    {
        if (!is_null($data)) {
            $result = $this->db->insert('processedleave', $data);
            return $this->db->insert_id();
        } else
            return false;
    }

    public function getBatches($company = null)
    {
        if (!is_null($company)) {
            $this->db->where('company', $company);
            $this->db->from("processedleave");
            $query = $this->db->get();
            return $query;
        } else
            return false;
    }

    public function getBatch($id = null)
    {
        if (!is_null($id)) {
            $this->db->where('id', $id);
            $this->db->from("processedleave");
            $query = $this->db->get();
            if ($query->num_rows() == 1)
                return $query->row();
            else
                return false;
        } else
            return false;
    }

    public function updateBatch($id = null, $data = null)
    {
        if (!is_null($id) && !is_null($data)) {
            $this->db->where('id', $id);
            $result = $this->db->update('processedleave', $data);
            return $result;
        } else
            return false;
    }


    public function rollbackLeave($batchID = null)
    {
        if (!is_null($batchID)) {
            $this->db->where('batch', $batchID);
            $updates = $this->db->update('leave', array('processed' => false));
            return $updates;
        } else
            return false;

    }

    public function rollbackItem($leaveID = null)
    {
        if (!is_null($leaveID)) {
            $this->db->where('id', $leaveID);
            $updates = $this->db->update('leave', array('processed' => false));
            return $updates;
        } else
            return false;

    }


    /****************************************************************
     *
     *      LEAVE CRUD operations
     *
     *
     *
     ****************************************************************/

    public function addLeave($data = null)
    {
        if (!is_null($data)) {
            $result = $this->db->insert('leave', $data);
            return $this->db->insert_id();
        } else
            return false;
    }

    public function addTimesheet($data = null)
    {
        if (!is_null($data)) {
            $result = $this->db->insert('timesheet', $data);
            return $this->db->insert_id();
        } else
            return false;
    }


    public function addTimeEntry($data = null)
    {
        if (!is_null($data)) {
            $result = $this->db->insert('timeentry', $data);
            return $this->db->insert_id();
        } else
            return false;
    }

    public function updateTimeEntry($id = null, $data = null)
    {
        if (!is_null($id) && !is_null($data)) {
            $this->db->where('id', $id);
            $result = $this->db->update('timeentry', $data);
            return true;
        } else
            return false;
    }


    public function updateTimeEntryStatus($timesheet = null, $type= null, $status = null)
    {
        if (!is_null($timesheet) && !is_null($type) && !is_null($status)) {
            $this->db->where('timesheet', $timesheet);
            $this->db->where('type', $type);
            $result = $this->db->update('timeentry', array('status' => $status));
            return true;
        } else
            return false;
    }


    public function getTimeEntry($id = null)
    {
        if (!is_null($id)) {
            $this->db->where('id', $id);
            $this->db->from('timeentry');
            $result = $this->db->get();
            if($result->num_rows() <=1)
                return false;
            else
                return $result->row();
        } else
            return false;
    }

    public function getTimeEntryByDay($timesheet = null, $day = null)
    {
        if (!is_null($timesheet) && !is_null($day) ) {
            $this->db->where('timesheet', $timesheet);
            $this->db->where('day', $day);
            $this->db->from('timeentry');
            $result = $this->db->get();
            if($result->num_rows() != 1)
                return false;
            else
                return $result->row();
        } else
            return false;
    }

    public function cleanupTimeEntries($timesheetID = null){
        if (is_null($timesheetID)) return false;
        $this->db->where('timesheet', $timesheetID);
        $this->db->where('status !=', 2);
        $this->db->from('leave');
        $result = $this->db->delete();
        return $result;
    }

    public function updateLeaveByTimesheet($timesheetID = null, $data = null){
        if (is_null($timesheetID) or is_null($data)) return false;
//        if($data->status !=0) {
            $this->db->where('timesheet', $timesheetID);
            $this->db->update('leave', $data);
//        }
        $this->db->where('timesheet', $timesheetID);
        $this->db->update('timeentry', $data);

        return true;
    }

    public function getLeaveStatus($id = null)
    {
        if ($id != null)
            $this->db->where('id', $id);
        $this->db->from('leavestatus');
        $query = $this->db->get();
        return $query;
    }

    public function getLocations($company = null){
        if (is_null($company))
            $company = $this->session->userdata('company');
        $this->db->select('location');
        $this->db->distinct();
        $this->db->where('company', $company);
        $this->db->where('location !=', '');
        $this->db->from('users');
        $query = $this->db->get();
        return $query;
    }

    public function getLeave($id = null)
    {
        if ($id == null)
            return false;

        $this->db->where('id', $id);
        $this->db->from('leave');
        $query = $this->db->get();
        return $query;
    }


    public function getLeaveWhere($query = null)
    {
        if ($query == null)
            return false;

        $this->db->select('leave.*');
        $this->db->where($query);
        $this->db->from('leave');
        $this->db->join('users', 'leave.user = users.id');

        $query = $this->db->get();
        return $query;
    }



    public function getBillingstatus($id = null)
    {
        if (!is_null($id))
            $this->db->where('id', $id);

        $this->db->from('billingstatus');
        $query = $this->db->get();
        return $query;
    }

    public function editBillingstatus($id = null, $data = null)
    {
        if (!is_null($id)) {
            $this->db->where('id', $id);
            $this->db->update('billingstatus', $data);
        }
        else{
            $this->db->insert('billingstatus', $data);
        }
        return true;
    }

    public function getTimesheet($id = null)
    {
        if ($id == null)
            return false;

        $this->db->where('id', $id);
        $this->db->from('timesheet');
        $query = $this->db->get();
        return $query;
    }

    public function getTimesheetsByUser($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('id');
        $this->db->where('user', $id);
        $this->db->from('timesheet');
        $query = $this->db->get();
        return $query;
    }


    public function getTimesheetByWeek($userID = null, $week = null, $year = null)
    {
        if (is_null($userID) || is_null($week) || is_null($year))
            return false;

        $this->db->where('user', $userID);
        $this->db->where('week', $week);
        $this->db->where('year', $year);
        $this->db->from('timesheet');
        $query = $this->db->get();
        return $query;
    }

    public function timesheetExists($userID = null, $week = null, $year = null)
    {
        if (is_null($userID) || is_null($week) || is_null($year))
            return false;

        $this->db->where('user', $userID);
        $this->db->where('week', $week);
        $this->db->where('year', $year);
        $this->db->from('timesheet');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return true;
        else
            return false;
    }


    public function getTimesheetDetails($id = null)
    {
        if ($id == null)
            return false;
        $this->db->select('timesheet.week as week, timesheet.year as year, timeentry.*');
        $this->db->from('timeentry');
        $this->db->join('timesheet', 'timeentry.timesheet = timesheet.id');
        $this->db->where('timesheet', $id);
        $this->db->order_by('timeentry.type');
        $this->db->order_by('timeentry.costcentre');

        $query = $this->db->get();
        return $query;
    }

    public function clearTimeEntries($id = null)
    {
        if ($id == null)
            return false;
        $this->db->where('timesheet', $id);
        $this->db->delete('timeentry');
        return true;
    }




    public function getLeaveByUser($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('id');

        $this->db->select('leave.*');
        $this->db->where('user', $id);
        $this->db->from('leave');
        $this->db->join('leavetype', 'leavetype.id = leave.leavetype');
        $this->db->where('leave.user', $id);
        $this->db->where('leavetype.istime !=', 1);

        $this->db->order_by('leave.created desc');
        $query = $this->db->get();
        return $query;
    }


    public function getTimeSummaryByUser($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('id');

        $this->db->select('leave.*');
        $this->db->from('leave');
        $this->db->join('leavetype', 'leavetype.id = leave.leavetype');
        $this->db->where('leave.user', $id);
        $this->db->where('leavetype.istime', 1);
        $this->db->order_by('leave.created desc');

        $query = $this->db->get();
        return $query;
    }


    public function getLeaveByTimesheet($id = null)
    {
        if (is_null($id))
            return false;

        $this->db->from('leave');
        $this->db->where('timesheet', $id);
//        $this->db->order_by('timesheet');

        $query = $this->db->get();
        return $query;
    }


    public function getUnapprovedTimesheetsByApprover($approver = null)
    {
        if (is_null($approver))
            $approver = $this->session->userdata('id');

//        $this->db->distinct('timesheet');
        $this->db->where('timesheet !=', null);
        $this->db->where('approver', $approver);
        $this->db->where('status', 1);
        $this->db->from('leave');
        $this->db->group_by('timesheet');

        $query = $this->db->get();
        return $query;
    }

    public function getLeaveByTimesheetAndType($timesheet = null, $type = null)
    {
        if (is_null($timesheet) || is_null($type))
            return false;

        $this->db->from('leave');
        $this->db->where('timesheet', $timesheet);
        $this->db->where('leavetype', $type);
        $query = $this->db->get();
        return $query;
    }

    public function getLeaveByBatch($id = null)
    {
        if (!is_null($id)) {
            $this->db->where('batch', $id);
            $this->db->from('leave');
            $result = $this->db->get();
            return $result;
        } else
            return false;
    }

    public function getLeaveByDate($userid = null, $fromdate, $todate)
    {
        if ($userid == null)
            $userid = $this->session->userdata('id');

        $this->db->where('user', $userid);
        $this->db->where('status <', 3);
        $this->db->where('timesheet IS NULL');
        $this->db->where('fromdate <=', $todate);
        $this->db->where('todate >=', $fromdate);
        $this->db->order_by('leavetype, fromdate asc');
        $this->db->from('leave');

        $query = $this->db->get();

        return $query;
    }

    public function checkAway($id = null, $fromdate, $todate)
    {
        if ($id == null)
            $id = $this->session->userdata('id');

        $this->db->where('user', $id);
        $this->db->where('status <', 3);
        $this->db->where('timesheet IS NULL');
        $this->db->from('leave');
        $query = $this->db->get();

        $format1 = "d/m/Y";
        $format2 = "Y-m-d";
        $tofilter = DateTime::createFromFormat($format1, $todate);
        $fromfilter = DateTime::createFromFormat($format1, $fromdate);

//        $tofilter = strtotime($todate);
//        $fromfilter = strtotime($fromdate);

        $onleave = 0;
        if (isset($query)) {
            foreach ($query->result() as $leave) {
                $todate = DateTime::createFromFormat($format2, $leave->todate);
                $fromdate = DateTime::createFromFormat($format2, $leave->fromdate);

                if ($todate >= $fromfilter && $fromdate <= $tofilter) {
                    $onleave += $leave->hours;
                }
            }
        }
        return $onleave;
    }

    public function getLeaveByApprover($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('id');
        $this->db->select('leave.*');
        $this->db->from('leave');
        $this->db->join('leavetype', 'leavetype.id = leave.leavetype');
        $this->db->where('leavetype.istime !=', 1);
        $this->db->where("(approver ='" . $id . "' or approvedby = '" . $id . "' )");
//        $this->db->where('approver', $id);
//        $this->db->or_where('approvedby', $id);
        $this->db->order_by('todate', 'desc');
        $query = $this->db->get();
        return $query;
    }


    public function getPendingLeaveByApprover($approverid = null)
    {
        if ($approverid == null)
            $approverid = $this->session->userdata('id');
        $this->db->select('leave.*');
        $this->db->from('leave');
        $this->db->where('leave.status', 1);
        $this->db->join('leavetype', 'leavetype.id = leave.leavetype');
        $this->db->where('leavetype.istime !=', 1);
        $this->db->where("approver", $approverid);
//        $this->db->where('approver', $id);
//        $this->db->or_where('approvedby', $id);
//        $this->db->order_by('todate', 'desc');
        $query = $this->db->get();
        return $query->num_rows();
    }


    public function getPendingTimesheetsByApprover($approverid = null)
    {
        if ($approverid == null)
            $approverid = $this->session->userdata('id');
        $this->db->distinct();
        $this->db->select('leave.timesheet');
        $this->db->from('leave');
        $this->db->where('leave.status', 1);
        $this->db->join('leavetype', 'leavetype.id = leave.leavetype');
        $this->db->where('leavetype.istime', 1);
        $this->db->where("approver", $approverid);
//        $this->db->where('approver', $id);
//        $this->db->or_where('approvedby', $id);
//        $this->db->order_by('todate', 'desc');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getLeaveAndTimeByCompany($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('company');
        $this->db->select('leave.*');
        $this->db->join('users', 'leave.user = users.id');
        $this->db->where('users.company', $id);
        $this->db->from('leave');
        $this->db->order_by('todate', 'desc');
        $query = $this->db->get();
        return $query;
    }

    public function getLeaveByCompany($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('company');
        $this->db->select('leave.*');
        $this->db->join('users', 'leave.user = users.id');
        $this->db->where('users.company', $id);
        $this->db->from('leave');
        $this->db->join('leavetype', 'leavetype.id = leave.leavetype');
        $this->db->where('leavetype.istime !=', 1);
        $this->db->order_by('todate', 'desc');
        $query = $this->db->get();
        return $query;
    }

    public function getTimeByCompany($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('company');

        $this->db->select('leave.*');
        $this->db->join('users', 'leave.user = users.id');
        $this->db->where('users.company', $id);
        $this->db->from('leave');
        $this->db->join('leavetype', 'leavetype.id = leave.leavetype');
        $this->db->where('leavetype.istime', 1);
        $this->db->order_by('todate', 'desc');
        $query = $this->db->get();
        return $query;
    }

    public function getTimesheetsByCompany($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('company');
        $this->db->select('timesheet.*');
        $this->db->from('timesheet');
        $this->db->join('users', 'timesheet.user = users.id');
        $this->db->where('users.company', $id);
        $query = $this->db->get();
        return $query;
    }

    public function getTimesheetsByApprover($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('id');

//        $this->db->where('approver', $id);
//        $this->db->or_where('approvedby', $id);
        $this->db->select('timesheet.*');
        $this->db->distinct();
        $this->db->from('timesheet');
        $this->db->join('leave', 'leave.timesheet = timesheet.id');
        $this->db->where("(leave.approver ='" . $id . "' or leave.approvedby = '" . $id . "' )");
        $query = $this->db->get();
        return $query;
    }


    public function getTimeByApprover($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('id');

//        $this->db->where('approver', $id);
//        $this->db->or_where('approvedby', $id);
        $this->db->select('leave.*');
        $this->db->from('leave');
        $this->db->join('leavetype', 'leavetype.id = leave.leavetype');
        $this->db->where('leavetype.istime', 1);
        $this->db->where("(approver ='" . $id . "' or approvedby = '" . $id . "' )");
        $this->db->order_by('timesheet', 'asc');
        $this->db->order_by('todate', 'desc');
        $query = $this->db->get();
        return $query;
    }

    public function getLeaveAudit($companyid = null)
    {
        if ($companyid == null)
            $companyid = $this->session->userdata('company');
        $this->db->select('leave.*');
        $this->db->join('users', 'leave.user = users.id');
        $this->db->where('users.company', $companyid);
        $this->db->where('users.defaultapprover != leave.approvedby');
        $this->db->from('leave');
        $this->db->order_by('todate', 'desc');
        $query = $this->db->get();
        return $query;
    }

    public function getApprovedLeaveByCompany($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('company');
        $this->db->select('leave.*');
        $this->db->join('users', 'leave.user = users.id');
        $this->db->where('users.company', $id);
        $this->db->where('leave.status >', 1);
        $this->db->from('leave');
        $this->db->order_by('todate', 'desc');
        $query = $this->db->get();
        return $query;
    }

    public function updateleave($id = null, $data = null)
    {
        if (is_null($id) || $id == '')
            return false;
        if (is_null($data) || $data == '')
            return false;

        $this->db->where('id', $id);
        $result = $this->db->update('leave', $data);

        return $result;

    }

    public function isleave($id = null)
    {
        if ($id == null)
            return false;

        $this->db->select('leave.*');
        $this->db->from('leave');
        $this->db->join('leavetype', 'leavetype.id = leave.leavetype');
        $this->db->where('leavetype.istime', 1);
        $this->db->where('id', $id);;
        $result = $this->db->get();
        if ($result->num_rows() == 1)
            return true;
        else
            return false;
    }

    public function istimesheet($id = null)
    {
        return !$this->isleave($id);
    }


    /****************************************************************
     *
     *      HELP CRUD operations
     *
     *
     *
     ****************************************************************/

    public function getHelp($id = null)
    {
        if (!is_null($id)) {
            $this->db->where("id", $id);
        }
        $this->db->from("help");
        $this->db->order_by('order', 'asc');
        $query = $this->db->get();
        return $query;
    }

    public function addHelp($data = null)
    {
        if (!is_null($data)) {
            $result = $this->db->insert('help', $data);
            return $result;
        } else
            return false;
    }

    public function editHelp($id = null, $data = null)
    {
        if (!is_null($id) && !is_null($data)) {
            $this->db->where("id", $id);
            $result = $this->db->update('help', $data);
            return $result;
        } else
            return false;
    }


    /****************************************************************
     *
     *      EXPENSE CRUD operations
     *
     *
     *
     ****************************************************************/

    public function addExpenseClaim($data = null)
    {
        if (!is_null($data)) {
            $result = $this->db->insert('expenseclaim', $data);
            return $this->db->insert_id();
        } else
            return false;
    }

    public function updateExpenseClaim($id = null, $data = null)
    {
        if ($id == null)
            return false;
        if ($data == null)
            return false;

        $this->db->where('id', $id);
        $result = $this->db->update('expenseclaim', $data);
        return $result;
    }

    public function addExpenseItem($data = null)
    {
        if (!is_null($data)) {
            $result = $this->db->insert('expenseitem', $data);
            return $this->db->insert_id();
        } else
            return false;
    }

    public function addExpenseAttachment($data = null){
            // license id is linked to the version, the status and the content.
            if (!is_null($data)) {
                $result = $this->db->insert('expenseattachments', $data);
                return $this->db->insert_id();
            } else{
                return false;
            }

    }
    public function updateAttachments($olditemid = null, $newitemid = null){
        if ($olditemid == null)
            return false;
        if ($newitemid == null)
            return false;
        $data['itemid'] = $newitemid;
        $this->db->where('itemid', $olditemid);
        $result = $this->db->update('expenseattachments', $data);
        return $result;
    }

    public function getAttachments($itemid = null){
        if ($itemid == null)
            $itemid = 0;

        $this->db->where('itemid', $itemid);
        $this->db->from('expenseattachments');
        $result = $this->db->get();
        return $result;
    }

    public function updateExpenseItem($id = null, $data = null)
    {
        if ($id == null)
            return false;
        if ($data == null)
            return false;

        $this->db->where('id', $id);
        $result = $this->db->update('expenseitem', $data);
        return $result;
    }

    public function getExpenseClaim($id = null)
    {
        if ($id != null){
            $this->db->where('id', $id);
            $this->db->from('expenseclaim');
            $result = $this->db->get();

            if($result->num_rows() <1)
                    return false;
                else
                    return $result->row();
        } else
            return false;
    }

    public function getExpenseClaimsByUser($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('id');

        $this->db->where('user', $id);
        $this->db->from('expenseclaim');
        $query = $this->db->get();
        return $query;
    }

    public function getExpenseClaimsByApprover($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('id');

        $this->db->where('approver', $id);
        $this->db->where('status', 1);
        $this->db->from('expenseclaim');
        $query = $this->db->get();
        return $query;
    }


    public function getExpenseHistoryByApprover($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('id');

        $this->db->where('status >', 1);
        $this->db->where("(approver ='" . $id . "' or approvedby = '" . $id . "' )");

        $this->db->from('expenseclaim');
        $query = $this->db->get();
        return $query;
    }


    public function getPendingExpenseClaimsByApprover($approverid = null)
    {
        if ($approverid == null)
            $approverid = $this->session->userdata('id');

        $this->db->from('expenseclaim');
        $this->db->where('status', 1);
        $this->db->where("approver", $approverid);
//        $this->db->where('approver', $id);
//        $this->db->or_where('approvedby', $id);
//        $this->db->order_by('todate', 'desc');
        $query = $this->db->get();
        return $query->num_rows();
    }


    public function getExpenseClaimsByCompany($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('company');

        $this->db->where('company', $id);
        $this->db->from('expenseclaim');
        $query = $this->db->get();
        return $query;
    }

    public function getExpenseItemsByClaim($id = null)
    {
        if ($id == null)
            $id = 0;

        $this->db->where('claimid', $id);
        $this->db->from('expenseitem');
        $query = $this->db->get();
        return $query;
    }
    public function cleanupitems($claimid = null, $itemlist = null){
        if ($claimid == null)
            return false;
        if ($itemlist == null)
            return false;

        $this->db->where('claimid', $claimid);
        $this->db->where_not_in('id', $itemlist);
        $this->db->delete('expenseitem');

        return true;
    }

    public function getLeaveComments($id){
        return $this->getComments($id, 1);
    }

    public function getTimesheetComments($id){
        return $this->getComments($id, 2);
    }

    public function getExpenseComments($id){
        return $this->getComments($id, 3);
    }

    public function getComments($id, $type){
        $this->db->where('type', $type);
        $this->db->where('parentid', $id);
        $this->db->order_by('created');
        $this->db->from('comments');
        $query = $this->db->get();
        return $query;
    }

    public function addLeaveComment($id, $comment){
        return $this->addComment($id, 1, $comment);
    }

    public function addTimesheetComment($id, $comment){
        return $this->addComment($id, 2, $comment);
    }

    public function addExpenseComment($id, $comment){
        return $this->addComment($id, 3, $comment);
    }

    public function addComment($parentid, $type, $comment){
        $data = array(
            'parentid' => $parentid,
            'comment' => $comment,
            'madeby' => $this->session->userdata('id'),
            'type' => $type
        );
        $this->db->insert('comments', $data);
        return true;
    }


    /****************************************************************
     *
     *      HOLIDAY CRUD operations
     *
     *
     *
     ****************************************************************/


    public function getHolidaysByRegion($state = null)
    {
        if ($state == null)
            $state = $this->session->userdata('state');
        $country = $this->getCountryByRegion($state)->country;

        $where = array('country' => $country, 'national' => true);
        $this->db->where($where);
        $this->db->or_where('state', $state);
        $this->db->from('holidays');
        $query = $this->db->get();
        return $query;
    }

    public function addHoliday($data = null)
    {
        if (is_null($data))
            return false;
        $this->db->insert('holidays', $data);
        return $this->db->insert_id();
    }

    public function editHoliday($id = null, $data = null)
    {
        if (is_null($id) || is_null($data)) return false;
        $this->db->where('id', $id);
        $this->db->update('holidays', $data);
        return true;
    }

    public function deleteHoliday($id = null)
    {
        $this->db->where('id', $id);
        $this->db->delete('holidays');
        return true;
    }

    public function getNationalHolidays($country = null)
    {
        if ($country == null)
            return false;
        $this->db->where('country', $country);
        $this->db->where('national', true);
        $this->db->from('holidays');
        $query = $this->db->get();
        return $query;
    }

    public function getHolidaysByCountry($country = null)
    {
        if ($country == null)
            return false;

        $this->db->select('holidays.id as id, holidays.date as date, holidays.description as description, countries.name as country, regions.name as state, holidays.national as national');
        $this->db->join('regions', 'holidays.state = regions.id');
        $this->db->join('countries', 'holidays.country = countries.id');
        $this->db->where('holidays.country', $country);
        $this->db->from('holidays');
        $query = $this->db->get();
        return $query;
    }

    public function getAllHolidays()
    {

        $this->db->select('holidays.id as id, holidays.country as cid, holidays.state as sid, holidays.date as date, holidays.description as description, countries.name as country, regions.name as state, holidays.national as national');
        $this->db->join('regions', 'holidays.state = regions.id');
        $this->db->join('countries', 'holidays.country = countries.id');
        $this->db->from('holidays');

        $query = $this->db->get();
        return $query;
    }

    public function getWorkingDays($startDate, $endDate, $userID)
    {

        $user = $this->user_model->getUser($userID)->row();
        $c = $this->user_model->getCountryByRegion($user->state);
        $country = $c->country;
//        $holidays = $this->user_model->getHolidaysByCountry($country);
        $holidays = $this->user_model->getHolidaysByRegion($user->state);
//        $national = $this->user_model->getNationalHolidays($user->country);


        // do strtotime calculations just once
        if (is_string($startDate))
            $startDate = strtotime(str_replace('/', '-', $startDate));
        elseif (is_object($startDate))
            $startDate = $startDate->getTimestamp();
        if (is_string($endDate))
            $endDate = strtotime(str_replace('/', '-', $endDate));
        elseif (is_object($endDate))
            $endDate = $endDate->getTimestamp();

        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;

        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);

        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);

        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        } else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)

            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;

                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            } else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }

        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
        //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
        $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0) {
            $workingDays += $no_remaining_days;
        }

        //We subtract the holidays
        foreach ($holidays->result() as $holiday) {
            //check the holiday is national, or if its part ot the users region
            if ($holiday->national || $holiday->state == $user->state) {
                $time_stamp = strtotime($holiday->date);
                //If the holiday doesn't fall in weekend
                if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N", $time_stamp) != 6 && date("N", $time_stamp) != 7)
                    $workingDays--;
            }
        }
        $workingDays = round($workingDays, 1);
        $hours = $workingDays * $user->hours;
        $result = array('days'=> $workingDays, 'hours' => $hours);

        return $result;
    }


    /****************************************************************
     *
     *      LEAVE TYPE CRUD operations
     *
     *
     *
     ****************************************************************/

    public function getLeaveTypes($company = null, $istime = 0)
    {
        if ($company == null)
            $company = $this->session->userdata('company');
        $this->db->where('company', $company);
        if(!is_null($istime))
            $this->db->where('istime', $istime);
        $this->db->from('leavetype');
        $this->db->order_by('description', 'asc');
        $query = $this->db->get();
        return $query;
    }

    public function deleteLeaveType($id = null)
    {
        $this->db->where('id', $id);
        $this->db->update('leavetype', array('company' => 0));
        return true;
    }

    public function getLeaveType($id = null)
    {
        if ($id != null)
            $this->db->where('id', $id);
        $this->db->from('leavetype');
        $query = $this->db->get();
        return $query;
    }


    public function getLeaveTypeByValue($value = null, $company = null)
    {
        if(is_null($value))
            return false;

        if (is_null($company))
            $company = $this->session->userdata('company');

        $this->db->where('value', $value);
        $this->db->where('company', $company);
        $this->db->from('leavetype');
        $query = $this->db->get();
        return $query;
    }

    public function updateLeaveTypes($data = null, $id = null)
    {
        if ($data == null)
            return false;

        if ($id != null) {   //then its an update
            $this->db->where('id', $id);;
            $result = $this->db->update('leavetype', $data);
        } else {              //then its an add
            $result = $this->db->insert('leavetype', $data);
        }
        return $result;
    }

    public function removeLeaveType($company = null, $id = null)
    {
        $this->db->where('id', $id);
        $this->db->where('company', $company);
        $result = $this->db->delete('leavetype');
        return $result;
    }

    public function getLeaveMapping($id = null)
    {
        if ($id != null)
            $this->db->where('id', $id);
        $this->db->from('leavebalance');
        $query = $this->db->get();
        return $query;
    }


    /****************************************************************
     *
     *      STATUS CRUD operations
     *
     *
     *
     ****************************************************************/


    public function getStatusTypes()
    {
        $this->db->from('status');
        $query = $this->db->get();
        return $query;
    }


    public function getStatus($id)
    {
        $this->db->from('status');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    /****************************************************************
     *
     *      ACTIVE USER LOGGING CRUD operations
     *
     *
     *
     ****************************************************************/


    public function logUser()
    {
//        session_start();
        $session = session_id();
        $time = time();
        $time_check = $time - 300; //We Have Set Time 5 Minutes

        $this->db->where('session', $session);
        $this->db->from('online_users');
        $result = $this->db->get();

        $count = $result->num_rows();

//If count is 0 , then enter the values
        if ($count == "0") {
            $data = array('session' => $session, 'time' => $time);
            $crawlers_agents = strtolower('Bloglines subscriber|Dumbot|Sosoimagespider|QihooBot|FAST-WebCrawler|Superdownloads Spiderman|LinkWalker|msnbot|ASPSeek|WebAlta Crawler|Lycos|FeedFetcher-Google|Yahoo|YoudaoBot|AdsBot-Google|Googlebot|Scooter|Gigabot|Charlotte|eStyle|AcioRobot|GeonaBot|msnbot-media|Baidu|CocoCrawler|Google|Charlotte t|Yahoo! Slurp China|Sogou web spider|YodaoBot|MSRBOT|AbachoBOT|Sogou head spider|AltaVista|IDBot|Sosospider|Yahoo! Slurp|Java VM|DotBot|LiteFinder|Yeti|Rambler|Scrubby|Baiduspider|accoona');
            $crawlers = explode("|", $crawlers_agents);
            if (is_array($crawlers)) {
                foreach ($crawlers as $crawler) {
                    if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), trim($crawler)) !== false) {
                        $data['bot'] = true;
                    }
                }
            }
            $this->db->insert('online_users', $data);
        } else { // else update the values
            $data = array('time' => $time);
            $this->db->where('session', $session);
            $this->db->update('online_users', $data);
        }

        // after 5 minutes, session will be deleted
        $this->db->where('time <', $time_check);
        $this->db->delete('online_users');

    }

    public function getOnlineUsers()
    {
        $this->db->where('bots', false);
        $this->db->from('online_users');
        $result = $this->db->get();
        return $result->num_rows();
    }

    public function getOnlineBots()
    {
        $this->db->where('bots', true);
        $this->db->from('online_users');
        $result = $this->db->get();
        return $result->num_rows();
    }


    /****************************************************************
     *
     *      SECURITY RELATED operations
     *
     *
     *
     ****************************************************************/

    public function login()
    {

//        $token = $this->input->post('token');

//        $this->db->where('token', $token);
//        $this->db->from('company');
//        $query1 = $this->db->get();

//        if ($query1->num_rows() == 1)
//        {


        // Getting post data from login form
        $email = $this->input->post('email');

        $password = crypt($this->input->post('password'), $this->salt);
//	    $password = str_replace('/','',$password);
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $this->db->where('status !=', 5);
//            $this->db->where('company', $companyid);
        $this->db->from('users');
        $query2 = $this->db->get();

        if ($query2->num_rows() > 0) {
            $count = 0;
//foreach
            foreach ($query2->result() as $user) {
//                $user = $query2->row();
                $status = $user->status;

                if ($status != 2) {
                    $data = array(
                        'message' => 'Error: Your account is currently "' . $this->getStatus($status)->description . '". Contact your HR Manager.',
                        'logged_in' => false
                    );
                    $this->session->set_userdata($data);
                    continue;
//                    redirect(base_url());
//                    return FALSE;
                }
                $company = $this->getCompany($user->company);
                if ($company->status != 2) {
                    $data = array(
                        'message' => 'Error: login failed as your Organisations Account has been locked',
                        'logged_in' => false
                    );
                    $this->session->set_userdata($data);
                    continue;
//                    redirect(base_url());
//                    return FALSE;
                }


                $data = array(
                    'message' => '',
                    'company' => $user->company,
                    'companyname' => $company->name,
                    'companyhours' => $company->hours,
                    'companystatus' => $company->status,
                    'companyapikey' => $company->apikey,
                    'syncdataset' => $company->syncdataset,
                    'id' => $user->id,
                    'name' => $user->name,
                    'username' => $user->name,
                    'email' => $user->email,
                    'employeeid' => $user->employeeid,
                    'hours' => $user->hours,
                    'location' => $user->location,
                    'usertype' => $user->usertype,
                    'defaultapprover' => $user->defaultapprover,
                    'logged_in' => true
                );

                $this->updateLastLogin($user->id);
                $this->session->set_userdata($data);

                return TRUE;
            }
// break if status != 2
// if all fail, show msg
//
            redirect(base_url());
            return FALSE;
        } else {
            $data = array('message' => 'Error: login failed due to incorrect email / password ');
            $this->session->set_userdata($data);
            return FALSE;
        }
//        }
//        else{
//            $data   = array('message'    => 'Error: login failed due to incorrect company token ');
//            $this->session->set_userdata($data);
//            return FALSE;
//        }
    }

    public function getToken($id = null)
    {
        if ($id == null)
            $id = $this->session->userdata('company');
        $this->db->where('id', $id);
        $this->db->from('company');
        $query = $this->db->get();
        return $query->row()->vip;
//        $sql = 'SELECT id, name, world, status, score FROM teams WHERE world = '.$world.' AND status = 1 Order By score desc limit 5';

    }// END public function updateUser()

    public function cryptStr($email = null)
    {
//	$newStr = urlencode($email);//crypt($email, $this->salt);
        return $email; //$newStr;
    }

    public function validatelink($token = null, $pwd = null, $email = null)
    {
        $this->db->where('vip', $token);
        $this->db->from('company');
        $query = $this->db->get();
        $company = $query->row();


        if (isset($company)) {
            $users = $this->getUserBy(array('email' => $email, 'company' => $company->id, 'status !=' => 5));
            foreach ($users->result() as $user) {
                $password = str_replace('/', '', $user->password);

                if ($password == $pwd) {
                    $data = array(
                        'company' => $company->id,
                        'companyname' => $company->name,
                        'companyhours' => $company->hours,
                        'companystatus' => $company->status,
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                        'hours' => $user->hours,
                        'employeeid' => $user->employeeid,
                        'usertype' => $user->usertype,
                        'defaultapprover' => $user->defaultapprover,
                        'location' => $user->location,
                        'logged_in' => false
                    );

                    $this->updateLastLogin($user->id);
                    $this->session->set_userdata($data);
                    return true;
                }// end if
            }// end for
        }// end

        return false;

    }


    /****************************************************************
     *
     *      EMAIL operations
     *
     *
     *
     ****************************************************************/


    public function sendwelcome($email = null, $userID = null, $company = null)
    {
        if (!is_null($email)) {
            if (is_null($userID)) {
                $user = $this->getUserByEmail($email)->row();
            } else {
                $user = $this->getUser($userID)->row();
            }
//          if(is_null($company)){
            $company = $this->getCompany($user->company);
//          }
            $subject = "Welcome to Express Leave";
            $password = str_replace('/', '', $user->password);
            $message = array(
                "Dear " . $user->name . ", ",
                $this->session->userdata('name') . " has registered you with Attaché Express Leave.",
                "Please <a href='" . base_url('user/confirm/' . urlencode($this->getToken($user->company)) . '/' . urlencode($password) . '/' . urlencode($this->cryptStr($email))) . "'>create your password</a> so you can manage leave requests online.<br>",
                "If you cannot click the link, paste the following into your browser:<br>",
                "" . base_url('user/confirm/' . urlencode($this->getToken($user->company)) . '/' . urlencode($password) . '/' . urlencode($this->cryptStr($email)))
            );
            // Always set content-type when sending HTML email

            if($this->session->userdata('isadmin')){
                $this->sendgeneric($user, $subject, $message, null, null, $this->session->userdata('email'));
            }
            else {
                $this->sendgeneric($user, $subject, $message, $this->session->userdata('email'));
            }
        }
    }

    public function sendassigned($leaveID, $newApprover)
    {
        if (!is_null($leaveID)) {
            $leave = $this->getLeave($leaveID)->row();
            $user = $this->getUser($leave->user)->row();
            $approver = $this->getUser($newApprover)->row();

            $format = 'D jS \of M y';
            $todate = date_create($leave->todate);
            $fromdate = date_create($leave->fromdate);

            $subject = "Leave request";
            $duration = "";

            if ($leave->hours < $user->hours) {
                $duration = number_format($leave->hours, '1') . " hour(s)";
            } elseif ($leave->days == 1) {
                $duration = "1 day";
            } else {
                $duration = $leave->days . " days / " . number_format($leave->hours, '1') . " hours";
            }


            $message = array(
                "Dear " . $approver->name . ", ",
                "" . $user->name . " has submitted a leave request for " . $duration . " of " . $this->user_model->getLeaveType($leave->leavetype)->row()->description . ", from " . date_format($fromdate, $format) . " to " . date_format($todate, $format) . ".",
                "Please <a href='" . base_url() . "user/approval'>log in to Express Leave</a> to action this request."
            );
            // Always set content-type when sending HTML email
            $this->sendgeneric($approver, $subject, $message, null);
        }
    }


    public function sendassignedTime($timesheetID, $newApprover)
    {
        if (!is_null($timesheetID)) {
            $leave = $this->getTimesheet($timesheetID)->row();
            $user = $this->getUser($leave->user)->row();
            $approver = $this->getUser($newApprover)->row();

            $subject = "Timesheet submission";

            $message = array(
                "Dear " . $approver->name . ", ",
                "" . $user->name . " has submitted a timesheet for you to review and approve. ",
                "Please <a href='" . base_url() . "user/timeapproval'>log in to Express Leave</a> to action this timesheet."
            );
            // Always set content-type when sending HTML email
            $this->sendgeneric($approver, $subject, $message, null);
        }
    }

    public function sendactioned($leaveID, $action)
    {
        if (!is_null($leaveID)) {
            $leave = $this->getLeave($leaveID)->row();
            $user = $this->getUser($leave->user)->row();
            $approver = $this->getUser($leave->approver)->row();
            $actioner = $this->getUser($this->session->userdata('id'))->row();

            $format = 'D jS \of M y';
            $todate = date_create($leave->todate);
            $fromdate = date_create($leave->fromdate);

            $ccaddress = $approver->email;
            if($approver->id != $actioner->id)
                $ccaddress .= ','.$actioner->email;

            if($actioner->id == $user->id){
                // if the user actioned their own leave (cancelled)
                $name = 'yourself';
            }
            else{
                $name = $actioner->name;
            }

            $subject = "Leave Request " . $action;
            $duration = "";

            $leaveType = $this->user_model->getLeaveType($leave->leavetype)->row()->description;

            if ($leave->hours < $user->hours) {
                $duration = number_format($leave->hours, '1') . " hour(s)";
                $str3 = ", on " . date_format($fromdate, $format) . " has been " . $action . " by " . $name . ".";
            } elseif ($leave->days == 1) {
                $duration = "1 day";
                $str3 = ", on " . date_format($fromdate, $format) . " has been " . $action . " by " . $name . ".";
            } else {
                $duration = $leave->days . " days / " . number_format($leave->hours, '1') . " hours";
                $str3 = ", from " . date_format($fromdate, $format) . " to " . date_format($todate, $format) . " has been " . $action . " by " . $name . ".";

            }
            $str2 = "Your request for " . $duration . " of " . $leaveType . $str3;

            $message = array(
                "Dear " . $user->name . ", ",
                $str2
            );
            // Always set content-type when sending HTML email


            $attachment = null;
            if($action == 'Approved'){
            // write the ical
            $this->load->helper('file');
            $file_content = 'BEGIN:VCALENDAR
VERSION:1.0
BEGIN:VEVENT
DTSTART:'.  date_format($fromdate, "Ymd").'T080000 
DTEND:'. date_format($todate, "Ymd").'T180000
SUMMARY:On Leave ('.$duration.')
X-MICROSOFT-CDO-BUSYSTATUS:OOF
LOCATION: 
CLASS:PRIVATE
DESCRIPTION:'.$duration.' of '.$leaveType.'
PRIORITY:3
X-MICROSOFT-CDO-BUSYSTATUS:OOF
END:VEVENT
END:VCALENDAR';

//            $file_location = FCPATH . 'uploads/' . $this->session->userdata('id') . '/calendar.ics';
            $file_location = '/var/www/uploads/' . $user->id . '/leave_calendar_entry.ics';
                if(write_file($file_location, $file_content))
                    $attachment = $file_location;
            }

            $this->sendgeneric($user, $subject, $message, $ccaddress, $attachment);
        }
    }
    public function sendactionedTimesheet($timesheetID, $action)
    {
        if (!is_null($timesheetID)) {
            $leave = $this->getLeaveByTimesheet($timesheetID)->row();
            $user = $this->getUser($leave->user)->row();
            $approver = $this->getUser($leave->approver)->row();
            $actioner = $this->getUser($this->session->userdata('id'))->row();

            $format = 'D jS \of M y';
            $todate = date_create($leave->todate);
            $fromdate = date_create($leave->fromdate);

            $subject = "Timesheet " . $action;
            $duration = "";

            $ccaddress = $approver->email;
            if($approver->id != $actioner->id)
                $ccaddress .= ','.$actioner->email;

            if($actioner->id == $user->id){
                // if the user actioned their own leave (cancelled)
                $name = 'yourself';
            }
            else{
                $name = $actioner->name;
            }


            $message = array(
                "Dear " . $user->name . ", ",
                "Your timesheet for " . date_format($fromdate, $format) . " to " . date_format($todate, $format) . " has been " . $action . " by " . $name . "."
            );

            if(strcasecmp($action, 'Approved') == 0 ){
                $days = array(
                    0 => "Sunday",
                    1 => "Monday",
                    2 => "Tuesday",
                    3 => "Wednesday",
                    4 => "Thursday",
                    5 => "Friday",
                    6 => "Saturday",
                    7 => "Sunday"
                );
                $timesheet = $this->getTimesheetDetails($timesheetID);
                $timestring = '';

                $startday = date_format($fromdate, 'N'); // get day of week

                for ($i = 0; $i < 7; $i++){
                    $first = true;
                    $index = $startday + $i;
                    if($index != 7)
                    $index = $index % 7;
                    $timestring .= $days[$index].'<br> ';
                    foreach ($timesheet->result() as $timeentry){
                        if($index == $timeentry->day) {
                            if(!$first)
                                $timestring .= ', and <br>';

                            $timestring .= '' . $timeentry->hours . ' hour';
                            if($timeentry->hours > 1) $timestring .= 's';
                            if($timeentry->minutes > 0) $timestring .= ' '.$timeentry->minutes . ' minutes';
                            $timestring .=  ' of ' . $this->getLeaveType($timeentry->type)->row()->description;

                            $first = false;
                        }
                    }
                    if($first)
                        $timestring .= 'No Entries';

                    $timestring .= '<br><br>';
                }

                $message[] = $timestring;
            }

            // Always set content-type when sending HTML email
            $this->sendgeneric($user, $subject, $message, $ccaddress, null);
        }
    }

    public function sendreset($email = null)
    {
        $users = $this->getUserByEmail($email);
        if ($users->num_rows() > 0)
            $user = $users->row();
        else {
            return false;
        }
        $subject = "Reset your password";
        $message = array("Hi there" . ",", "Uh Oh! We had an issue trying to reset your password. Please try again.", "" . base_url());
        if (!is_null($user) && isset($user)) {
            $password = str_replace('/', '', $user->password);
            $message = array(
                "Dear " . $user->name . ", ",
                "A password reset was requested for the Attaché Express Leave account associated with " . $user->email . ".",
                "Click here to <a href='" . base_url('user/confirm/' . urlencode($this->getToken($user->company)) . '/' . urlencode($password) . '/' . urlencode($this->cryptStr($email))) . "'>reset your password</a>.",
                "If you cannot click the link, paste the following into your browser:<br>",
                "" . base_url('user/confirm/' . urlencode($this->getToken($user->company)) . '/' . urlencode($password) . '/' . urlencode($this->cryptStr($email)))
            );
        }
        $this->sendgeneric($user, $subject, $message, null);
    }

    public function sendparking($user = null, $spot = 0, $when = null)
    {

        if(is_null($user) || !isset($user))
            return false;
        if(is_null($when) || $spot == 0)
            return false;

        $subject = "You just got a Parking Spot!";

        $message = array(
            "Dear " . $user->name . ", ",
            "You were after a parking spot and guess what, ... you got one!",
            "You have been allocated <b>Space# ".$spot."</b> for <b>".$when."</b><br>"
        );


        $this->sendgeneric($user, $subject, $message, null);
    }


    public function sendgeneric($toUser = null, $subject = null, $content = null, $ccaddress = null, $attachment = null, $bccaddress = null)
    {

        $to = $toUser->email;

        $middle = '<table>';

        foreach ($content as $item) {
            $middle = $middle . '
                                            <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                                                <tr>
                                                     <td width="20" class="mobileHide">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" width="600" class="flexibleContainerCell">
                                                     <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top" class="textContent">
                                                            <span style="font-family:Roboto, Verdana;font-size:14px; color:#333333;">';
            $middle = $middle . $item;
            $middle = $middle . '</span>
                                                                </td>
                                                                <td width="20" class="mobileHide">&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                        </td>
                                                </tr>
                                                <tr>
                                                     <td width="20" class="mobileHide">&nbsp;</td>
                                                </tr>
                                            </table>';
        }

        $middle = $middle . "</table>";

        $beginning = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="format-detection" content="telephone=no" /> <!-- disable auto telephone linking in iOS -->
    <title>Activate your account</title>
    <style type="text/css">
            /* RESET STYLES */
            html {
            background-color: #f5f6f7;
            margin: 0;
            padding: 0;
        }

        body, #bodyTable, #bodyCell, #bodyCell {
            height: 100% !important;
            margin: 0;
            padding: 0;
            width: 100% !important;
            font-family: Helvetica, Arial, "Lucida Grande", sans-serif;
        }

table {
border-collapse: collapse;
}

table[id=bodyTable] {
    width: 100% !important;
    margin: auto;
    max-width: 500px !important;
                color: #7A7A7A;
                font-weight: normal;
            }

        img, a img {
    border: 0;
    outline: none;
    text-decoration: none;
            height: auto;
            line-height: 100%;
        }

        h1, h2, h3, h4, h5, h6 {
    color: #5F5F5F;
    font-weight: normal;
            font-family: Helvetica;
            font-size: 20px;
            line-height: 125%;
            text-align: Left;
            letter-spacing: normal;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            padding-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
        }

        /* CLIENT-SPECIFIC STYLES */

        .emailLinkWrap a {
    text-decoration: none;
            color: #ffffff !important;
        }

        .ReadMsgBody {
    width: 100%;
}

        .ExternalClass {
    width: 100%;
}
            /* Force Hotmail/Outlook.com to display emails at full width. */
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%;
            }
        /* Force Hotmail/Outlook.com to display line heights normally. */
        table, td {
    mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }
        /* Remove spacing between tables in Outlook 2007 and up. */
        #outlook a {
            padding: 0;
        }
        /* Force Outlook 2007 and up to provide a "view in browser" message. */
        img {
    -ms-interpolation-mode: bicubic;
            display: block;
            outline: none;
            text-decoration: none;
        }
        /* Force IE to smoothly render resized images. */
        body, table, td, p, a, li, blockquote {
    -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
        .ExternalClass td[class="ecxflexibleContainerBox"] h3 {
    padding-top: 10px !important;
        }
        /* Force hotmail to push 2-grid sub headers down */

        /* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */

        /* ========== Page Styles ========== */
        h1 {
    display: block;
    font-size: 26px;
            font-style: normal;
            font-weight: normal;
            line-height: 100%;
        }

        h2 {
    display: block;
    font-size: 20px;
            font-style: normal;
            font-weight: normal;
            line-height: 120%;
        }

        h3 {
    display: block;
    font-size: 17px;
            font-style: normal;
            font-weight: normal;
            line-height: 110%;
        }

        h4 {
    display: block;
    font-size: 18px;
            font-style: italic;
            font-weight: normal;
            line-height: 100%;
        }

        .flexibleImage {
    height: auto;
}

        .linkRemoveBorder {
    border-bottom: 0 !important;
        }

        table[class=flexibleContainerCellDivider] {
    padding-bottom: 0 !important;
            padding-top: 0 !important;
        }

        body, #bodyTable {
            background-color: #f5f6f7;
        }

        #emailHeader {
            background-color: #f5f6f7;
        }

        .emailBody {
    background-color: #FFFFFF;
        }

        #emailFooter {
            background-color: #f5f6f7;
        }

        .nestedContainer {
    background-color: #F8F8F8;
            border: 1px solid #CCCCCC;
        }

        .emailButton {
    background-color: #205478;
            border-collapse: separate;
        }

        .buttonContent {
    color: #FFFFFF;
    font-family: Helvetica;
            font-size: 18px;
            font-weight: bold;
            line-height: 100%;
            padding: 15px;
            text-align: center;
        }

            .buttonContent a {
    color: #FFFFFF;
    display: block;
    text-decoration: none !important;
                border: 0 !important;
            }

        .emailCalendar {
    background-color: #FFFFFF;
            border: 1px solid #CCCCCC;
        }

        .emailCalendarMonth {
    background-color: #205478;
            color: #FFFFFF;
            font-family: Helvetica, Arial, sans-serif;
            font-size: 16px;
            font-weight: bold;
            padding-top: 10px;
            padding-bottom: 10px;
            text-align: center;
        }

        .emailCalendarDay {
    color: #205478;
    font-family: Helvetica, Arial, sans-serif;
            font-size: 60px;
            font-weight: bold;
            line-height: 100%;
            padding-top: 20px;
            padding-bottom: 20px;
            text-align: center;
        }

        .imageContentText {
    margin-top: 10px;
            line-height: 0;
        }

            .imageContentText a {
    line-height: 0;
            }

        #invisibleIntroduction {
            display: none !important;
        }
        /* Removing the introduction text from the view */

        /*FRAMEWORK HACKS & OVERRIDES */
        span[class=ios-color-hack] a {
    color: #275100 !important;
    text-decoration: none !important;
        }
        /* Remove all link colors in IOS (below are duplicates based on the color preference) */
        span[class=ios-color-hack2] a {
    color: #205478 !important;
    text-decoration: none !important;
        }

        span[class=ios-color-hack3] a {
    color: #8B8B8B !important;
    text-decoration: none !important;
        }
        /* A nice and clean way to target phone numbers you want clickable and avoid a mobile phone from linking other numbers that look like, but are not phone numbers.  Use these two blocks of code to "unstyle" any numbers that may be linked.  The second block gives you a class to apply with a span tag to the numbers you would like linked and styled.
            Inspired by Campaign Monitors article on using phone numbers in email: http://www.campaignmonitor.com/blog/post/3571/using-phone-numbers-in-html-email/.
            */
        .a[href^="tel"], a[href^="sms"] {
    text-decoration: none !important;
            color: #606060 !important;
            pointer-events: none !important;
            cursor: default !important;
        }

        .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
    text-decoration: none !important;
            color: #606060 !important;
            pointer-events: auto !important;
            cursor: default !important;
        }


        /* MOBILE STYLES */
        @media only screen and (max-width: 620px) {
    /*////// CLIENT-SPECIFIC STYLES //////*/
    body {
        width: 100% !important;
        min-width: 100% !important;
            }

            .mobileHide{
        display:none;
    }

            center {
        padding: 0 10px;
            }

            /* Force iOS Mail to render the email at full width. */

            /* FRAMEWORK STYLES */
            /*
                CSS selectors are written in attribute
                selector format to prevent Yahoo Mail
                from rendering media query styles on
                desktop.
                */
            /*td[class="textContent"], td[class="flexibleContainerCell"] { width: 100%; padding-left: 10px !important; padding-right: 10px !important; }*/
            table[id="emailHeader"],
            table[class="emailBody"],
            table[id="emailFooter"],
            table[class="flexibleContainer"],
            td[class="flexibleContainerCell"] {
        width: 100% !important;
    }

            td[class="flexibleContainerBox"], td[class="flexibleContainerBox"] table {
        display: block;
        width: 100%;
        text-align: left;
            }

            td[class="mobileLessPadding"] {
        padding: 20px !important;
            }

            /*
                The following style rule makes any
                image classed with flexibleImage
                fluid when the query activates.
                Make sure you add an inline max-width
                to those images to prevent them
                from blowing out.
                */
            td[class="imageContent"] img {
        height: auto !important;
                width: 100% !important;
                max-width: 100% !important;
            }

            img[class="flexibleImage"] {
        height: auto !important;
                width: 100% !important;
                max-width: 100% !important;
            }

            img[class="flexibleImageSmall"] {
        height: auto !important;
                width: auto !important;
            }


            /*
                Create top space for every second element in a block
                */
            table[class="flexibleContainerBoxNext"] {
        padding-top: 10px !important;
            }

            /*
                Make buttons in the email span the
                full width of their container, allowing
                for left- or right-handed ease of use.
                */
            table[class="emailButton"] {
        width: 100% !important;
    }

            td[class="buttonContent"] {
        padding: 0 !important;
            }

                td[class="buttonContent"] a {
        padding: 15px !important;
                }

            td[class="buttonInner"] {
        width: 80vw !important;
            }

                td[class="buttonInner"] td {
        width: 80vw !important;
                }

                td[class="buttonInner"] span {
        width: 100% !important;
        float: left !important;
                }

            a[class="roundedCornerButton"] {
        width: 100% !important;
    }

            span[class="mobileOnly"] {
        display: block !important;
                font-size: 12px !important;
                height: 10px !important;
            }
        }

        /*  CONDITIONS FOR ANDROID DEVICES ONLY
            *   http://developer.android.com/guide/webapps/targeting.html
            *   http://pugetworks.com/2011/04/css-media-queries-for-targeting-different-mobile-devices/ ;
            =====================================================*/

        @media only screen and (-webkit-device-pixel-ratio:.75) {
    /* Put CSS for low density (ldpi) Android layouts in here */
}

        @media only screen and (-webkit-device-pixel-ratio:1) {
    /* Put CSS for medium density (mdpi) Android layouts in here */
}

        @media only screen and (-webkit-device-pixel-ratio:1.5) {
    /* Put CSS for high density (hdpi) Android layouts in here */
}
        /* end Android targeting */

        /* CONDITIONS FOR IOS DEVICES ONLY
            =====================================================*/
        @media only screen and (min-device-width : 320px) and (max-device-width:568px) {
}
        /* end IOS targeting */
    </style>
    <!--
Outlook Conditional CSS

        These two style blocks target Outlook 2007 & 2010 specifically, forcing
        columns into a single vertical stack as on mobile clients. This is
        primarily done to avoid the page break bug and is optional.

More information here:
        http://templates.mailchimp.com/development/css/outlook-conditional-css
    -->
    <!--[if mso 12]>
        <style type="text/css">
            .flexibleContainer{display:block !important; width:100% !important;}
        </style>
    <![endif]-->
    <!--[if mso 14]>
        <style type="text/css">
            .flexibleContainer{display:block !important; width:100% !important;}
        </style>
    <![endif]-->
</head>
<body bgcolor="#F5F6F7" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

    <!-- CENTER THE EMAIL // -->
    <!--
    1.  The center tag should normally put all the
        content in the middle of the email page.
        I added "table-layout: fixed;" style to force
        yahoomail which by default put the content left.

    2.  For hotmail and yahoomail, the contents of
        the email starts from this center, so we try to
        apply necessary styling e.g. background-color.
    -->
    <center style="background-color:#F5F6F7;">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
            <tr>
                <td align="center" valign="top" id="bodyCell">

                    <!-- EMAIL HEADER // -->
                    <!--
                        The table "emailBody" is the emails container.
        Its width can be set to 100% for a color band
        that spans the width of the page.
        -->
                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="emailHeader">

                        <!-- HEADER ROW // -->
        <tr>
                            <td align="center" valign="top">
                                <!-- CENTERING TABLE // -->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- FLEXIBLE CONTAINER // -->
        <table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
                                                <tr>
                                                    <td valign="top" width="600" class="flexibleContainerCell">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                        <tr>
                                                                            <td align="left" class="textContent">
                                                                                <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
        </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- // FLEXIBLE CONTAINER -->
                                        </td>
                                    </tr>
                                </table>
                                <!-- // CENTERING TABLE -->
                            </td>
                        </tr>
                        <!-- // END -->

                    </table>
                    <!-- // END -->
                    <!-- EMAIL BODY // -->
        <!--
        The table "emailBody" is the emails container.
                        Its width can be set to 100% for a color band
                        that spans the width of the page.
                    -->
                    <table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600" class="emailBody">

                        <!-- MODULE ROW // -->
                        <!--
                            To move or duplicate any of the design patterns
                            in this email, simply move or copy the entire
                            MODULE ROW section for each content block.
                        -->
                        <tr>
                            <td align="center" valign="top">
                                <!-- CENTERING TABLE // -->
                                <!--
                                    The centering table keeps the content
                                    tables centered in the emailBody table,
                                    in case its width is set to 100%.
                                -->
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#1d92ef">
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- FLEXIBLE CONTAINER // -->
                                            <!--
                                                The flexible container has a set width
                                                that gets overridden by the media query.
                                                Most content tables within can then be
                                                given 100% widths.
                                            -->
                                            <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                                                <tr>
                                                    <td align="center" valign="top" width="600" class="flexibleContainerCell">

                                                        <!-- CONTENT TABLE // -->
                                                        <!--
                                                        The content table is the first element
                                                            thats entirely separate from the structural
                                                            framework of the email.
        -->
                                                        <table border="0" cellpadding="15" cellspacing="0" width="100%" style="border-bottom:1px solid #8DC8F7;">
                                                            <tr>
                                                                <td align="center" valign="top" class="textContent">
                                                                    <a href="' . base_url() . '" style="text-decoration:none;"><img src="' . base_url() . 'images/emaillogo.png" class="logo" style="display:block; max-width:153px!important" alt="logo" /></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!-- // CONTENT TABLE -->

                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- // FLEXIBLE CONTAINER -->
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#1d92ef">
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- FLEXIBLE CONTAINER // -->
        <!--
        The flexible container has a set width
                                                that gets overridden by the media query.
        Most content tables within can then be
                                                given 100% widths.
        -->
                                            <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                                                <tr>
                                                    <td align="center" valign="top" width="600" class="flexibleContainerCell">

                                                        <!-- CONTENT TABLE // -->
        <!--
        The content table is the first element
                                                            thats entirely separate from the structural
                                                            framework of the email.
                                                        -->
                                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td width="20" class="mobileHide">&nbsp;</td>
                                                                <td align="center" valign="top" class="textContent">

                                                                    <span style="text-align:center;font-weight:lighter;font-family:Roboto, Verdana;font-size:28px;margin-bottom:10px;color:#FFFFFF;">' . $subject . '</span>
                                                                </td>
                                                                <td width="20" class="mobileHide">&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                        <!-- // CONTENT TABLE -->

                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- // FLEXIBLE CONTAINER -->
                                        </td>
                                    </tr>
                                </table>
                                <!-- // CENTERING TABLE -->
                            </td>
                        </tr>
                        <!-- // MODULE ROW -->
                        <!-- MODULE ROW // -->
                        <!--  The "mc:hideable" is a feature for MailChimp which allows
                            you to disable certain row. It works perfectly for our row structure.
                            http://kb.mailchimp.com/article/template-language-creating-editable-content-areas/
                        -->
                        <!-- MODULE ROW // -->
                        <tr>
                            <td align="center" valign="top">
                                <!-- CENTERING TABLE // -->
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- FLEXIBLE CONTAINER // -->';

        $end = '
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td valign="top" class="textContent">
										<br />
                                                                                <span style="font-family:Roboto, Verdana;font-size:14px; color:#333333;">
                                                                                    Regards,
                                                                                </span>
										<br />
                                                                                <span style="font-family:Roboto, Verdana;font-weight:bold; font-size:14px; color:#333333;">The Attaché Team</span>
                                                                                <br />
                                                                                <span style="font-family:Roboto, Verdana;font-size:14px; color:#333333;">
                                                                                    <a href="' . base_url() . '" style="font-family:Roboto, Verdana; text-decoration:underline; color:#187BCC;">' . base_url() . '</a>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- // FLEXIBLE CONTAINER -->
                                        </td>
                                    </tr>
                                </table>
                                <!-- // CENTERING TABLE -->
                            </td>
                        </tr>
                        <!-- // MODULE ROW -->

                    </table>
                    <!-- // END -->


                    <table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600" class="emailBody">
                        <!-- MODULE ROW // -->
        <tr>
                            <td align="center" valign="top">
                                <!-- CENTERING TABLE // -->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- FLEXIBLE CONTAINER // -->
        <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                                                <tr>
                                                    <td align="center" valign="top" width="600" class="flexibleContainerCell">

                                                        <!-- CONTENT TABLE // -->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td valign="top" class="imageContent">
                                                                    <img src="' . base_url() . 'images/bottom-border.jpg" width="600" class="flexibleImage" style="max-width:600px;width:100%;display:block;" alt="Attaché Online" title="Attaché Online" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!-- // CONTENT TABLE -->

                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- // FLEXIBLE CONTAINER -->
                                        </td>
                                    </tr>
                                </table>
                                <!-- // CENTERING TABLE -->
                            </td>
                        </tr>
                        <!-- // MODULE ROW -->
                    </table>


                    <!-- EMAIL FOOTER // -->
        <!--
        The table "emailBody" is the emails container.
                        Its width can be set to 100% for a color band
                        that spans the width of the page.
                    -->
                    <table bgcolor="#F5F6F7" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter" style="margin-top:10px">

                        <!-- FOOTER ROW // -->
                        <!--
                            To move or duplicate any of the design patterns
                            in this email, simply move or copy the entire
                            MODULE ROW section for each content block.
                        -->
                        <tr>
                            <td align="center" valign="top">
                                <!-- CENTERING TABLE // -->
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- FLEXIBLE CONTAINER // -->
                                            <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                                <tr>
                                                    <td align="center" valign="top" width="600" height="200" class="flexibleContainerCell">
                                                        <!-- CONTENT TABLE // -->
                                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="center" valign="top" bgcolor="#F5F6F7">
                                                                    <a href="' . base_url() . '" style="text-decoration:none;">
                                                                        <img height="15" width="75" class="footer_logo" style="display:block;" src="' . base_url() . 'images/footer_logo.jpg" alt="logo" />
                                                                    </a>
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;" height="10">
                                                                        <tr>
                                                                            <td align="left" height="10"></td>
                                                                        </tr>
                                                                    </table>
                                                                    <span style="font-family: Roboto, Verdana;font-size:14px;color:#9FA0A0;text-align:center;">
                                                                        Attaché Software
                                                                    </span>

                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;" height="5">
                                                                        <tr>
                                                                            <td align="left" height="5"></td>
                                                                        </tr>
                                                                    </table>


                                                                    <!-- // CONTENT TABLE -->

                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!-- // FLEXIBLE CONTAINER -->
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- // CENTERING TABLE -->
                                        </td>
                                    </tr>

                                </table>
                                <!-- // END -->
                            </td>
                        </tr>
                    </table>
                    <!-- // END -->

                </td>

            </tr>

        </table></td>
        </tr>
        </table>
    </center>
</body>
</html>
';

        ////////////////////////////////////////////////////////////////////////////////////

        $message = $beginning . $middle . $end;


        ///////////////////////////////////////////////////////////////////////////////////


        //load email helper
        $this->load->helper('email');
        //load email library
        $this->load->library('email');

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'localhost';
        $config['smtp_port'] = '25';
        $config['smtp_timeout'] = '30';
//            $config['smtp_user']='your mail id';
//            $config['smtp_pass']='your password';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['crlf'] = "\n";
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

//        $subject = 'Express Leave Email';
        // compose email
        $this->email->from('noreply@attacheonline.com', 'Attaché Express Leave', 'noreply@attacheonline.com');
        $this->email->reply_to('noreply@attacheonline.com', 'Attaché Express Leave');
        $this->email->to($to);

        if(!is_null($ccaddress))
            $this->email->cc($ccaddress);


        if(!is_null($bccaddress))
            $this->email->bcc($bccaddress);

        $this->email->subject($subject);

        if(!is_null($attachment))
            $this->email->attach($attachment);

        $this->email->message($message);

//            $this->email->set_alt_message($str)
//            $this->email->set_header($header, $value)
        $this->email->newline = "\r\n";
        $this->email->crlf = "\n";

        // try send mail ant if not able print debug
        if (!$this->email->send()) {
            echo "Email not sent \r\n" . $this->email->print_debugger();
            // $this->load->view('header');
            // $this->load->view('message',$data);
            // $this->load->view('footer');

        }
        // successfull message
//        echo "Email was successfully sent to $to";



        // $this->load->view('header');
        // $this->load->view('message',$data);
        // $this->load->view('footer');


        ///////////////////////////////////////////////////////////////////////////////////
    }



    public function getUserWorkDays($id = null){
        if(is_null($id))
            $id = $this->session->userdata('id');
        $this->db->where('user', $id);
        $this->db->from('userworkdays');
        $result = $this->db->get();
        if ($result->num_rows() == 1)
            return $result->row();
        else
            return false; // if nothing set, return false

    }

    public function setUserWorkDays($data){
        if(is_null($data)) return false;

        $this->db->where('user', $data['user']);
        $this->db->from('userworkdays');
        $result1 = $this->db->get();
        if($result1->num_rows() > 0){
            $this->db->where('user', $data['user']);
            $result = $this->db->update('userworkdays', $data);
        }
        else{
            $result = $this->db->insert('userworkdays', $data);
        }
        return $result;
    }

    public function getWorkingHours($startDate, $endDate, $userID)
    {

        $user = $this->user_model->getUser($userID)->row();
        $c = $this->user_model->getCountryByRegion($user->state);
        $country = $c->country;
//        $holidays = $this->user_model->getHolidaysByCountry($country);
        $holidays = $this->user_model->getHolidaysByRegion($user->state);
//        $national = $this->user_model->getNationalHolidays($user->country);

        $startDate = new DateTime($startDate);
        $endDate = new DateTime($endDate);
        $endDate = $endDate->modify('+1 day');


        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($startDate, $interval, $endDate);
        $str = '';
        $hours = 0;
        $days = 0;
        $empWorkingDays = $this->getUserWorkDays($userID);
        if($empWorkingDays === false) $empWorkingDays = $this->getUserWorkDays(0); // use default template

        foreach ($daterange as $date) {
            // check if this employee works on public holidays
            if($empWorkingDays->applypublicholidays) {
                // check to see if this date is a holiday for this particular user
                if(!$this->isHoliday($user, $date)){
                    // get the day of the week
                    $d = 'd' . date_format($date, 'w');  // 0 = sunday, 6 = saturday
                    if($empWorkingDays->$d > 0) {
                        $hours += $empWorkingDays->$d;
                        $days++;
                    }
                }
            }
            else{
                // get the day of the week
                $d = 'd' . date_format($date, 'w');  // 0 = sunday, 6 = saturday
                if($empWorkingDays->$d > 0) {
                    $hours += $empWorkingDays->$d;
                    $days++;
                }            }
            // add the number of hours to the total

        }

        $result = array('days'=> $days, 'hours' => $hours);
        return $result;

    }


    public function isHoliday($user = null, $date = null){
        $holidays = $this->user_model->getHolidaysByRegion($user->state);
        foreach ($holidays->result() as $holiday) {
            if ($holiday->national || $holiday->state == $user->state) {
                if(date_create($holiday->date) == $date){
                    return true;
                }
            }
        }
        return false;
    }

    public function getTemplate($id = null){
        if(is_null($id)){
            // get all for teh company
            $id = $this->session->userdata('company');
            $this->db->where('company', $id);
        }
        else{
            // get the specific
            $this->db->where('id', $id);
        }

        $this->db->from('template');
        $result = $this->db->get();
        return $result;
    }

    public function setTemplate($id = null, $data = null){
        if(is_null($id)){
            // then its an insert
            $this->db->insert('template', $data);
        }
        else{
            // its an update
            $this->db->where('id', $id);
            $this->db->update('template', $data);
        }
        return true;
    }

    public function getdatasetconfig($user = null, $company = null){
        if(is_null($user))
            $user = $this->session->userdata('id');
        if(is_null($company))
            $company = $this->session->userdata('company');

//        $this->db->where('user', $user);
//        $this->db->where('company', $company);
        $this->db->from('dataset');
        $result = $this->db->get();

        return $result;
    }

    public function availableSpots($day){
        $this->db->where('user', 0);
        $this->db->where('carparkdate', $day);
        $this->db->from('carparkallocation');
        $result = $this->db->get();

        return $result->num_rows();
    }

    public function getAllSpots(){
        $this->db->where('status', 1);   // one means available - not deleted
        $this->db->from('carpark');
        $result = $this->db->get();

        return $result;
    }


    public function getAllSpotsForRotation(){
        $this->db->where('status', 1);   // one means available - not deleted
        $this->db->where('permanentowner', 0);   // one means available - not deleted
        $this->db->from('carpark');
        $result = $this->db->get();

        return $result;
    }


    public function  getallocations($day)
    {

        $this->db->where('carparkdate', $day);
        $this->db->from('carparkallocation');
        $this->db->join('user', 'carparkallocation.user=user.userid', 'left');
        $this->db->order_by("fullname", "asc");
        $this->db->order_by("carpark", "asc");
        $result = $this->db->get();

        return $result;

    }

    public function  getAvailableSpots($day)
    {
        $this->db->where('carparkdate', $day);
        $this->db->where('user', 0);
        $this->db->from('carparkallocation');
        $this->db->order_by("carpark", "asc");
        $result = $this->db->get();

        return $result;

    }

    public function  getRosterByDate($start, $end)
    {
        $this->db->where('carparkdate >=', $start);
        $this->db->where('carparkdate <=', $end);
        $this->db->from('carparkallocation');
        $result = $this->db->get();

        return $result;

    }

    public function  getRosterBySpace($start, $end, $carpark)
    {
        $this->db->where('carparkdate >=', $start);
        $this->db->where('carparkdate <=', $end);
    	$this->db->where('carpark', $carpark);
        $this->db->from('carparkallocation');
        $result = $this->db->get();

        return $result;

    }

    public function doesUserHaveSpot($day){
        $this->db->where('carparkdate', $day);
        $this->db->where('user', $this->session->userdata('id'));
        $this->db->from('carparkallocation');
        $result = $this->db->get();
        if ($result->num_rows() > 0)
            return $result->row();
        else
            return false;

    }
    public function doesUserHavePerm(){
        $this->db->where('PermanentOwner', $this->session->userdata('id'));
        $this->db->from('carpark');
        $result = $this->db->get();
        if ($result->num_rows() > 0)
            return $result->row();
        else
            return false;

    }


    public function allocateSpace($day, $space, $user){
        // check date exists
        // check space is available
        if(is_null($day) || is_null($space))
            return "3";
    
        if(is_null($user))
        	$user = $this->session->userdata('id');

		if($user!=0)
        	$this->db->where('user', 0);
        $this->db->where('carparkdate', $day);
        $this->db->where('carpark', $space);
        $this->db->from('carparkallocation');
        $result = $this->db->get();
        if($result->num_rows() != 1)
            return "2";
    
    	if($user!=0) {
    		$this->db->where('user', $user);
        	$this->db->where('carparkdate', $day);
        	$this->db->from('carparkallocation');
        	$result = $this->db->get();
        	if($result->num_rows() > 0)
            	return "4";
        }



        // update space record
        if($user!=0)
        	$this->db->where('user', 0);
        $this->db->where('carparkdate', $day);
        $this->db->where('carpark', $space);

        $data = array(
            'user' => $user
        );
        $this->db->update('carparkallocation', $data);

        if($this->db->affected_rows() == 1)
            return "0";
        else
            return "1";

    }


    public function releaseSpace($day){
        // check date exists
        // check space is available
        if(is_null($day))
            return "1";

        $staff = 0; // unallocated

        $this->db->where('carparkdate', $day);
        $this->db->where('user', $this->session->userdata('id'));
        $this->db->from('carparkallocation');
        $result = $this->db->get();
        if(isset($result) && $result->num_rows() > 0) {
            $space = $result->row()->carpark;
        }

        $data = array(
            'user' => $staff
        );
        $this->db->where('carparkdate', $day);
        $this->db->where('user', $this->session->userdata('id'));
        $this->db->update('carparkallocation', $data);

        // assuming all went well,
        // check waitlist. give it to someone else.
        $waitlist = $this->getWaitingList($day);
        if($waitlist === false) {
            // do nothing}
        }
        else{
            $this->autoAllocateSpace($day, $space);
        }

        return "0";


    }


    public  function getUsersOnRotation(){
        $this->db->select('idlikeaspot.*');
        $this->db->from('idlikeaspot');
        $this->db->join('users', 'idlikeaspot.user = users.id', 'left');
        $this->db->order_by('users.name', 'asc');
        $result = $this->db->get();
        if(isset($result) && $result->num_rows() > 0)
            return $result;
        else
            return false;
    }


    public  function isUserOnRotation($user = null){
        if(is_null($user))
            $user = $this->session->userdata('id');
        $this->db->where('user', $user);
        $this->db->from('idlikeaspot');
        $result = $this->db->get();
        if(isset($result) && $result->num_rows() > 0)
            return $result;
        else
            return false;
    }

	public function addUserToRotation($user = null){
    	if(is_null($user))
            $user = $this->session->userdata('id');
    
    	$data = array(
            'user' => $user
        );
    
    	$this->db->insert('idlikeaspot', $data);
    	if($this->db->affected_rows() == 1)
            return "0";
        else
            return "1";
    
    }

	public function removeFromRotation($user = null){
    	if(is_null($user))
            $user = $this->session->userdata('id');
    
    	$this->db->where('user', $user);
        $this->db->delete('idlikeaspot');
    
        if($this->db->affected_rows() > 0)
            return "0";
        else
            return "1";
    }

    public function removeFromWaitingList($day){
        $this->db->where('carparkdate', $day);
        $this->db->where('user', $this->session->userdata('id'));
        $this->db->delete('carparkwaitlist');
    
        if($this->db->affected_rows() == 1)
            return "0";
        else
            return "1";
    }


    public function goOnWaitingList($day){
        // check date exists
        // check space is available
        if(is_null($day))
            return "3";

		//Check that the user isn't already on the waiting list
        $this->db->where('user', $this->session->userdata('id'));
        $this->db->where('carparkdate', $day);
        $this->db->from('carparkwaitlist');
        $result = $this->db->get();
        if($result->num_rows() > 0)
            return "2";



        // new waiting list record
        $data = array(
            'user' => $this->session->userdata('id'),
        	'carparkdate' => $day
        );
    
        $this->db->insert('carparkwaitlist', $data);

        if($this->db->affected_rows() == 1)
            return "0";
        else
            return "1";

    }

    public function isUserOnWaitingList($day){
        $this->db->where('carparkdate', $day);
        $this->db->where('user', $this->session->userdata('id'));
        $this->db->from('carparkwaitlist');
        $result = $this->db->get();
        if ($result->num_rows() > 0)
            return true;
        else
            return false;

    }

    public function getWaitingList($day){
        $this->db->where('carparkdate', $day);
        $this->db->from('carparkwaitlist');
        $result = $this->db->get();
        if ($result->num_rows() > 0)
            return $result;
        else
            return false;

    }
    public function getWaitingListWinner($day){
        $this->db->where('carparkdate', $day);
        $this->db->from('carparkwaitlist');
        $this->db->order_by('RAND()');
        $this->db->limit(1);
        $result = $this->db->get();
        if ($result->num_rows() == 1)
            return $result->row();
        else
            return false;

    }
    public function autoAllocateSpace($day, $space){
        // check date exists
        // check space is available
        if(is_null($day) || is_null($space))
            return "3";


        $this->db->where('user', 0);
        $this->db->where('carparkdate', $day);
        $this->db->where('carpark', $space);
        $this->db->from('carparkallocation');
        $result = $this->db->get();
        if($result->num_rows() != 1)
            return "2";

        //Get Users on Waiting List
//        $this->db->where('carparkdate',$day);
//        $this->db->where('carpark', $space);
//        $this->db->from('carparkwaitlist');

        $winner = $this->getWaitingListWinner($day);

//        $randomIndex=array_rand($result);

        // update space record
        $this->db->where('user', 0);
        $this->db->where('carparkdate', $day);
        $this->db->where('carpark', $space);

        $data = array(
            'user' => $winner->user
        );
        $this->db->update('carparkallocation', $data);

        if($this->db->affected_rows() == 1) {
            //remove user from waiting list
            $this->db->where('carparkwaitlistid',$winner->carparkwaitlistid);
            $this->db->from('carparkwaitlist');
            $this->db->delete('carparkwaitlist');

            // send the lucky person an email
            $thewinner = $this->getUser($winner->user)->row();
            $daytext = date_format(date_create($day),'l jS F Y');
            $this->sendparking($thewinner, $space, $daytext);


            $contact = $this->getcarparkcontact($winner->user);
            if($contact === false) {
                $mobile = 0;
            }
            else{
                $mobile = $contact->mobile;
            }

            if($mobile != 0 && ENVIRONMENT != 'development'){
                $this->load->library('aws');
                error_reporting(E_ALL);
                ini_set("display_errors", 1);

                $params = array(
                    'credentials' => array(
                        'key' => 'AKIAJXTAIRENQ2HSW5FA',
                        'secret' => 'E2Ci5mKoYvSuvrTomFNerYxetvCjsoRxZ5CgtCAj',
                    ),
                    'region' => 'ap-southeast-2', // < your aws from SNS Topic region
                    'version' => 'latest'
                );
                $sns = new \Aws\Sns\SnsClient($params);

                $args = array(
                    "MessageAttributes" => [
                        'AWS.SNS.SMS.SenderID' => [
                            'DataType' => 'String',
                            'StringValue' => 'AttCarpark'
                        ],
                        'AWS.SNS.SMS.SMSType' => [
                            'DataType' => 'String',
                            'StringValue' => 'Promotional'
                        ]
                    ],
                    "Message" => "You have been allocated Space# ".$space." for ".$daytext,
                    "PhoneNumber" => $mobile
                );

                $result = $sns->publish($args);
                // +1 to count for contact

                $this->db->where('id' , $contact->id);
                $data = array(
                    'sent' => $contact->sent +1
                );
                $this->db->update('carparkcontact ', $data);
            }

            return "0";
        }
        else
            return "1";

    }

    public function permAllocateSpace($staff, $space){
        if(is_null($staff) || is_null($space))
            return "1";

        $this->db->where('number', $space);

        $data = array(
            'PermanentOwner' => $staff
        );
        $this->db->update('carpark', $data);
        return "0";

    }

    public function generateRoster($monthindex = null){
        if(is_null($monthindex))
            return "MEGA FAIL!";

        $carparks = $this->getAllSpots();

        $this->db->where('id', $monthindex);
        $this->db->from('carparkrostermonths');
        $result = $this->db->get();
        $monthrecord = $result->row();

        $days = date_format(date_create($monthrecord->monthstart), "t");

        foreach ($carparks->result() as $carpark) {
            // allocate it to the permanent owner
            $currentDay = $monthrecord->monthstart;
            $allocation = $carpark->PermanentOwner;
            $email = false;

            if ($allocation == 0) {
                // if no permanent owner, check to see if it was allocated to someone else.
                $allocation = $this->input->post($carpark->number);
                if ($allocation != 0) {
                    $email = true;
                }
            }

            for ($i = 0; $i < $days; $i++) {
                $dayOfWeek = date("w", strtotime($currentDay));
                if ($dayOfWeek > 0 && $dayOfWeek < 6) {
                    $data = array(
                        'carparkdate' => $currentDay,
                        'carpark' => $carpark->number,
                        'user' => $allocation
                    );
                    $this->db->insert('carparkallocation', $data);
                }
                $currentDay = date('Y-m-d', strtotime($currentDay . '+1 day'));
            }

            if ($email) {
                // send the lucky person an email
                $thewinner = $this->getUser($allocation)->row();
                $monthtext = date_format(date_create($monthrecord->monthstart), 'F Y');
                $this->sendparking($thewinner, $carpark->number, $monthtext);
            }
        }
        // set the rostermonth to done, so it cant be double rostered
        $this->db->where('id', $monthindex);
        $data = array(
            'rostered' => true
        );
        $this->db->update('carparkrostermonths', $data);


        return "Finished";

    }


    public function getRosterMonths($status = false){
        // true status means months that have been rostered.
        // false status means roster hasnt been generated yet
        $this->db->where('rostered', $status);
        $this->db->from('carparkrostermonths');
        $result = $this->db->get();
        if ($result->num_rows() > 0)
            return $result;
        else
            return false;
    }

    public function getSpaceOccupant($day,$carpark){
        $this->db->where('carparkdate',$day);
        $this->db->where('carpark',$carpark);
        $this->db->from('carparkallocation');
        $result = $this->db->get();
        if ($result->num_rows() == 1)
            return $result;
        else
            return false;
    }

    public function getcarparkcontact($userid = null){
        if(is_null($userid)){
            return false;
        }
        $this->db->where('user', $userid);
        $this->db->from('carparkcontact');
        $result = $this->db->get();
        if($result->num_rows() > 0){
            return $result->row();
        }
        else
            return false;
    }

	public function doesUserHaveCarparkSMS($userid=null){
        if(is_null($userid)){
            $userid = $this->session->userdata('id');
        }
        $this->db->where('user', $userid);
        $this->db->from('carparkcontact');
        $result = $this->db->get();
        if($result->num_rows() > 0){
            $row = $result->row();
        	$enabled = $row->enabled;
        	if($enabled=="true")
            	return true;
        }
        return false;
    
    }

	    public function addcarparkcontact($mobile,$userid = null,$enable){
        if(is_null($userid)){
            $userid = $this->session->userdata('id');
        }
        if(is_null($enable)){
        	return false;
        }
        $this->db->where('user', $userid);
        $this->db->from('carparkcontact');
        $result = $this->db->get();
        if($result->num_rows() > 0){
            //modify existing
            $this->db->where('user', $userid);
        	$data = array(
            	'mobile' => $mobile,
            	'enabled' => $enable
        	);
        	$this->db->update('carparkcontact',$data);
        	if($this->db->affected_rows() >= 0)
            	return true;
        	else
            	return false;
        }
        else {
        	$data = array(
            	'mobile' => $mobile,
        		'user' => $userid,
            	'enabled' => $enable
        	);
        	$this->db->insert('carparkcontact',$data);
        	if($this->db->affected_rows() == 1)
            	return true;
        	else
            	return false;
        }
        	
    }
}// END class User_model

