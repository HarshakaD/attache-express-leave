<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {

    private $salt;

    public function __construct() {
        parent::__construct();

        // Password generation using crypt and salt
        $this->salt = '$2a$07$R.ABb2QcZ.FMk4phJ1y2CN$';

    }

    public function createHelp($data){
        $this->db->from('help');
        $total = $this->db->count_all_results();

        $data['order'] = $total;
        $this->db->insert('help',$data);  // name, token, status, logo
        return true;
    }

    public function editHelp($id = null, $data = null) {
        if ($id == null)
            return false;
        if ($data == null)
            return false;

        $this->db->where('id', $id);;
        $result = $this->db->update('help',$data);
        return $result;
    }

    public function deleteHelp($id = null) {
        if ($id == null)
            return false;
        $this->db->where('id', $id);;
        $this->db->from('help');
        $query = $this->db->get()->row();
        $gap = $query->order;
        $this->db->where('id', $id);;
        $this->db->delete('help');

        $this->db->from('help');
        $total = $this->db->count_all_results();

        for($i = $gap+1; $i <= $total; $i++){
            $this->db->set('order', $i-1);
            $this->db->where('order', $i);
            $this->db->update('help');
        }

        return true;
    }


    public function reorderhelp($from, $to) {
        $this->db->where('order', $from);
        $this->db->update('help', array('order'=>'-1'));
        if ($from < $to){
            for($i = $from; $i <= $to; $i++){
                $this->db->set('order', $i-1);
                $this->db->where('order', $i);
                $this->db->update('help');
            }
        }else{
            for($i = $from; $i > $to; $i--){
                $this->db->set('order', $i);
                $this->db->where('order', $i-1);
                $this->db->update('help');
            }
        }
        $this->db->where('order', '-1');
        $this->db->update('help', array('order'=>$to));
        return true;
    }

//note, should also create default leave types
    public function createCompany($data){

            $result = $this->db->insert('company',$data);  // name, token, status, logo
            $id = $this->db->insert_id();
            $default = array(
                    'company' => $id,
                    'description' => 'Annual Leave',
                    'value' => 'AL'
                );
            $this->user_model->updateLeaveTypes($default, null);

            return $id;
    }

    public function getCompanies($deleted = false) {
        $this->db->from('company');
        if(!$deleted)
            $this->db->where('status !=', 5);
        $this->db->order_by('name');
        $query = $this->db->get();
        return $query;
//        $sql = 'SELECT id, name, world, status, score FROM teams WHERE world = '.$world.' AND status = 1 Order By score desc limit 5';

    }// END pub

    public function getCompanyByVIP($vip=null) {
        $this->db->where('vip', $vip);
        $this->db->from('company');
        $query = $this->db->get();

        return $query->row();
//        $sql = 'SELECT id, name, world, status, score FROM teams WHERE world = '.$world.' AND status = 1 Order By score desc limit 5';

    }

    public function getCompanyCount() {
        return $this->db->count_all('company');
    }

    public function getUserCount($company = null) {
        if (!is_null($company))
            $this->db->where("company", $company);
        $this->db->where("status !=", 5);
        $this->db->from('users');
        return $this->db->count_all_results();
    }

    public function getLeaveCount($company = null) {
        $this->db->select('leave.*');
        if (!is_null($company)){
            $this->db->join('users', 'leave.user = users.id');
            $this->db->where('users.company', $company);
        }
        $this->db->from('leave');
        return $this->db->count_all_results();
    }

    public function getAllCompanies() {
        $this->db->from('company');
        $this->db->order_by('name');
        $query = $this->db->get();
        return $query;
    }// END public function updateUser()


    public function updateCompany($id = null, $data = null) {
        if ($id == null)
            return false;
        if ($data == null)
            return false;

        $this->db->where('id', $id);;
        $result = $this->db->update('company',$data);
        return $result;
    }

    public function createUser($data = null, $id = null){
        if ($data == null)
            return false;

        if ($id != null){   //then its an update
            $this->db->where('id', $id);;
            $result = $this->db->update('users',$data);
            return $id;
        }else{              //then its an add
            $password = crypt("".date("m/dd-Y:i-s"), $this->salt);
            $data['password'] = $password;
            if(!isset($data['status']))
                $data['status'] = 2;
            $result = $this->db->insert('users',$data);
            return $this->db->insert_id();
        }

    }

    public function changepassword($user = null, $password = null){
        if ($user == null || $password == null)
            return false;

        $this->db->where('id', $user);
        $password = crypt($password, $this->salt);
        $data = array(
            'password' => $password
        );
        $result = $this->db->update('adminusers',$data);
        return $result;
    }

    public function createAdmin($data = null, $id = null){
        if ($data == null)
            return false;

        if ($id != null){   //then its an update
            $this->db->where('id', $id);;
            $result = $this->db->update('adminusers',$data);
            return $id;
        }else{              //then its an add
            $password = crypt("".date("m/dd-Y:i-s"), $this->salt);
            $data['password'] = $password;
            $result = $this->db->insert('adminusers',$data);
            return $this->db->insert_id();
        }

    }



    public function deleteAdmin($id = null) {
        if ($id == null)
            return false;
        $this->db->where('id', $id);;
        $this->db->delete('adminusers');

        return true;
    }

    public function getAdminType($id = null) {
        if ($id != null)
            $this->db->where('id', $id);

        $this->db->from('admintypes');
        $query = $this->db->get();
        return $query;
    }

    public function getAdmins($id = null) {
        if (!is_null($id))
            $this->db->where('id', $id);

        $this->db->from('adminusers');
        $this->db->order_by('name');
        $query = $this->db->get();
        return $query;
    }// END pub


    public function updateUser($id = null, $data = null) {
        if ($id == null)
            return false;
        if ($data == null)
            return false;

        $this->db->where('id', $id);;
        $result = $this->db->update('users',$data);
        return $result;
    }


    public function login() {
        // Getting post data from login form
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $password = crypt($password, $this->salt);
//PASSWORD ENCRYPTION CURRENTLY TURNED OFF

        $this->db->where('username', $email);
        $this->db->where('password', $password);
        $this->db->from('adminusers');
        $query = $this->db->get();

        // If user exist in the DB, then create the session data
        if ($query->num_rows() == 1)
        {
            $result = $query->row();
            if ($password == $result->password)
            {
                $data   = array (
                    'id'        => $result->id,
                    'username'	    => $result->username,
                    'name'	    => "Your Attache representative",
                    'email'	    => $result->email,
                    'role'	    => $result->role,
                    'logged_in' => true,
                    'isadmin' => true
                );

                $this->session->set_userdata($data);
                return TRUE;
            }
            else{
                $data   = array('message'    => 'Sorry, try again '.$email);
                $this->session->set_userdata($data);
                return FALSE;
            }
        }
        else{
            $data   = array('message'    => 'Sorry, try again '.$email);
            $this->session->set_userdata($data);
            return FALSE;
        }
    }// END function login()


    public function updateField($data, $where, $table)
    {
        $this->db->where($where);

        $result = $this->db->update($table, $data);
        return $result;

    }


    public function getTables(){
        $tables = $this->db->list_tables();
        return $tables;
    }

    public function getTableColumns($table){
//        $fields = $this->db->list_fields($table);
        $fields = $this->db->field_data($table);
        return $fields;
    }

    public function getTableData($table, $limit = null, $offset = 0){
        $this->db->from($table);
        if (!is_null($limit))
            $this->db->limit($limit, $offset);
        $results = $this->db->get();
        return $results;
    }

    public function getSize($table){
        return $this->db->count_all_results($table);
    }

    public function getProcs(){
        $sql = 'SHOW PROCEDURE STATUS';
        $query = $this->db->query($sql);
        return $query;
    }
    public function execute($sql){
        $query = $this->db->query($sql);
        return $query;
    }

    public function getProcDetail($proc){
        $sql = 'SHOW CREATE PROCEDURE '.$proc;
//        $sql = 'SELECT ROUTINE_DEFINITION FROM information_schema.ROUTINES WHERE SPECIFIC_NAME="'.$proc.'"';
//$sql = "Select * from information_schema.routines";
        $query = $this->db->query($sql);
        return $query;
    }

    public function getFuncs(){
        $sql = 'SHOW FUNCTION STATUS';
        $query = $this->db->query($sql);
        return $query;
    }

    public function getFuncDetail($proc){
        $sql = 'show create function '.$proc;
        $query = $this->db->query($sql);
        return $query;
    }

    public function getStatus($id = null){
        if (is_null($id)) return false;
        $this->db->where("id", $id );
        $this->db->from("status");
        $query = $this->db->get()->row();
        return $query->description;
    }

}// END class User_model 
