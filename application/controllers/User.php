<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
    private $salt;

    public function __construct()
    {
        parent::__construct();
        $this->load->model("user_model");
        $this->load->model("admin_model");
        $this->salt = '$2a$07$R.ABb2QcZ.FMk4phJ1y2CN$';

    }

    public function index($data = null)
    {
        if (!$this->session->userdata('logged_in')) {

            if ($this->session->userdata('id')) { $this->session->unset_userdata('id'); }
            if ($this->session->userdata('firstname')) { $this->session->unset_userdata('firstname'); }
            if ($this->session->userdata('surname')) { $this->session->unset_userdata('surname'); }
            if ($this->session->userdata('email')) { $this->session->unset_userdata('email'); }
            if ($this->session->userdata('logged_in')) { $this->session->unset_userdata('logged_in'); }
            if ($this->session->userdata('userData')) { $this->session->unset_userdata('userData'); }

            session_destroy();

            $this->load->view('login');
        } elseif (!$this->user_model->validAgreement() && $this->session->userdata('usertype') <= '2') {
            redirect(base_url() . 'user/eula');
        } else
            $this->load->view('dashboard');
    }

    public function login()
    {
        if (!$this->session->userdata('logged_in')) {
            if ($this->user_model->login()) {
                if($this->session->userdata('syncdataset') && !is_null($this->session->userdata('companyapikey')) && $this->session->userdata('companyapikey') != "" && $this->session->userdata('usertype') <= '2') {
//                    $this->aosync();
                    $this->session->set_userdata('aosync' , true);
                }
                redirect(base_url() . $this->uri->segment(3) . '/' . $this->uri->segment(4));
//		redirect(base_url().'user/dashboard');
            } else {

                if ($this->session->userdata('id')) { $this->session->unset_userdata('id'); }
                if ($this->session->userdata('firstname')) { $this->session->unset_userdata('firstname'); }
                if ($this->session->userdata('surname')) { $this->session->unset_userdata('surname'); }
                if ($this->session->userdata('email')) { $this->session->unset_userdata('email'); }
                if ($this->session->userdata('logged_in')) { $this->session->unset_userdata('logged_in'); }
                if ($this->session->userdata('userData')) { $this->session->unset_userdata('userData'); }

                session_destroy();

                $this->load->view('login');
            }
        } else
            redirect(base_url() . 'user/dashboard');
    }// END function login

    public function dashboard()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url() . 'login');
//        else if ($this->session->userdata('isadmin')){
//            $this->load->view('admin/dashboard');
//        }
        elseif (!$this->user_model->validAgreement() && $this->session->userdata('usertype') <= '2') {
            redirect(base_url() . 'user/eula');
        } else
            $this->load->view('dashboard');
    }


    public function audit()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else if ($this->session->userdata('usertype') <= '2') {
            $this->load->view('audit');
        } else {
            redirect(base_url());
        }
    }

    public function report()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else if ($this->session->userdata('usertype') <= '2') {
            $this->load->view('report');
        } else {
            redirect(base_url());
        }
    }


    public function calendar()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else {
            $company = $this->user_model->getCompany($this->session->userdata('company'));
            if ($this->session->userdata('usertype') <= $company->calendarAccessLevel) {
                $this->load->view('calendar');
            }
            else
                redirect(base_url());
        }
    }


    public function timecalendar()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else
            $this->load->view('timecalendar');
    }

    public function request()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url() . 'login');
        else
            $this->load->view('request');
    }

    public function templates()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url() . 'login');
        else {
            if(!is_null($this->input->post('save'))){
                $id = $this->input->post('id');
                if($id == '') $id = null;
                $data['name'] = $this->input->post('name');
                $data['company'] = $this->session->userdata('company');
                $data['d0'] = $this->input->post('d0');
                $data['d1'] = $this->input->post('d1');
                $data['d2'] = $this->input->post('d2');
                $data['d3'] = $this->input->post('d3');
                $data['d4'] = $this->input->post('d4');
                $data['d5'] = $this->input->post('d5');
                $data['d6'] = $this->input->post('d6');
                $data['applypublicholidays'] = $this->input->post('applypublicholidays');
                $this->user_model->setTemplate($id, $data);
            }

            $this->load->view('hourtemplate');
        }
    }


    public function edit()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url() . 'login');
        else {
            $id = $this->input->post('id');

            if (!is_null($this->input->post('edit'))) {
                $data['leaveid'] = $id;
                $this->load->view('request', $data);
            } elseif (!is_null($this->input->post('cancel'))) {
                $data['status'] = 4;
                $action = "Cancelled";
                $data['approvedby'] = $this->session->userdata('id');
                $data['approvedon'] = date('Y/m/d h:i:s', time());
                $this->user_model->updateleave($id, $data);
                $this->user_model->sendactioned($id, $action);
                redirect(base_url() . 'user/dashboard');
            } else {
                redirect(base_url() . 'user/dashboard');
            }
        }

    }


    public function edithours()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url() . 'login');
        else {
            $id = $this->input->post('id');

            if (!is_null($this->input->post('save'))) {
                $data['user'] = $id;
                $data['d0'] = $this->input->post('d0');
                $data['d1'] = $this->input->post('d1');
                $data['d2'] = $this->input->post('d2');
                $data['d3'] = $this->input->post('d3');
                $data['d4'] = $this->input->post('d4');
                $data['d5'] = $this->input->post('d5');
                $data['d6'] = $this->input->post('d6');
                $data['applypublicholidays'] = $this->input->post('applypublicholidays');
                $this->user_model->setUserWorkDays($data);

                redirect(base_url().'user/users');
            } else {
                $data['userid'] = $id;
                $this->load->view('edithours', $data);
            }

//                if (!is_null($this->input->post('cancel'))) {
//                $data['status'] = 4;
//                $action = "Cancelled";
//                $data['approvedby'] = $this->session->userdata('id');
//                $data['approvedon'] = date('Y/m/d h:i:s', time());
//                $this->user_model->updateleave($id, $data);
//                $this->user_model->sendactioned($id, $action);
//                redirect(base_url() . 'user/dashboard');
//            } else {
//                $this->load->view('request', $data);
//            }
        }

    }

    public function edittimesheet()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url() . 'login');
        else {
            $id = $this->input->post('id');

            if (!is_null($this->input->post('edit'))) {
                $data['timesheetid'] = $id;
                $data['readonly'] = false;
                $this->load->view('timesheet', $data);
            }elseif (!is_null($this->input->post('view'))) {
                $data['timesheetid'] = $id;
                $data['readonly'] = true;
                $this->load->view('timesheet', $data);
            }
            elseif (!is_null($this->input->post('cancel'))) {
                $data['status'] = 4;
                $action = "Cancelled";
                $data['approvedby'] = $this->session->userdata('id');
                $data['approvedon'] = date('Y/m/d h:i:s', time());
                $this->user_model->updateLeaveByTimesheet($id, $data);
                $this->user_model->sendactionedTimesheet($id, $action);
                redirect(base_url() . 'user/mytimesheets');
            } else {
                redirect(base_url() . 'user/mytimesheets');
            }
        }

    }


    public function changepassword()
    {
        $reset = $this->input->post('reset');
        if (!is_null($reset) && $reset != "") {
            $this->user_model->sendreset($reset);
            $data = array('message' => 'Password Reset: We just sent you an email. <br>');
            $this->session->set_userdata($data);
            redirect(base_url() . 'login');
        } else {
            $token = $this->input->post('r1');
            $password = $this->input->post('r2');
            if (is_null($token) || is_null($password))
                redirect(base_url());

            $newpwd = $this->input->post('password');
            if ($this->user_model->validatelink($token, $password, $this->session->userdata('email'))) {

                $newpwd = crypt($newpwd, $this->salt);

                $data = array(
//                    'status' =>  2 , //should now set to active
                    'password' => $newpwd
                );
                $this->user_model->updateUser($this->session->userdata('id'), $data);
                $data1 = array('logged_in' => true);
                $data1['message'] = 'Your password has been changed';
                $this->session->set_userdata($data1);
                $this->load->view('dashboard');
            } else {
                $data = array('message' => 'Error: login failed due to incorrect token data ' . $token . ": " . $password . ": " . $this->session->userdata('email'));
                $this->session->set_userdata($data);
                redirect(base_url() . 'logout');
            }
        }
    }

    public function confirm($token = null, $pwd = null, $email = null)
    {

        $data = array(
            'token' => $token,
            'password' => $pwd
        );
        if (!is_null($token) && !is_null($pwd) && !is_null($email)) {
            $token = urldecode($token);
            $pwd = urldecode($pwd);
            $email = urldecode(urldecode($email));
            $data = array(
                'token' => $token,
                'password' => $pwd,
                'userlist' => $this->user_model->getUserByEmail(urldecode($email))
            );
            if ($this->user_model->validatelink($token, $pwd, $email)) {
                $this->load->view('resetpassword', $data);
            } else {
                $data = array('message' => 'Error: You have probably already used this link');
                $this->session->set_userdata($data);
                redirect(base_url());
            }

        } elseif ($this->session->userdata('logged_in')) {
            $company = $this->user_model->getCompany($this->session->userdata('company'));
            $user = $this->user_model->getUser($this->session->userdata('id'))->row();
            $data = array(
                'token' => $company->vip,
                'password' => str_replace('/', '', $user->password),
                'userlist' => $this->user_model->getUserByEmail($this->session->userdata('email'))
            );
            $this->load->view('resetpassword', $data);
        } else {
            $data = array('message' => 'Error: no data provided ');
            $this->session->set_userdata($data);
            redirect(base_url());
        }
    }

    public function confirm1($token = null, $pwd = null)
    {
        if (!is_null($token) && !is_null($pwd)) {
            $token = urldecode($token);
            $pwd = urldecode($pwd);
            $data = array(
                'token' => $token,
                'password' => $pwd
            );

            if ($this->user_model->validatelink($token, $pwd)) {
                $this->load->view('resetpassword', $data);
            } else {
                $data = array('message' => 'Error: You have probably already used this link');
                $this->session->set_userdata($data);
                redirect(base_url() . 'login');
            }
        } else {
            $data = array('message' => 'Error: no data provided ');
            $this->session->set_userdata($data);
            redirect(base_url() . 'login');
        }
    }


    public function myexpenses(){
        if (!$this->session->userdata('logged_in'))
            redirect(base_url() . 'login');
        else
            $this->load->view('myexpenses');
    }

    public function approveexpenses(){
        if (!$this->session->userdata('logged_in'))
            redirect(base_url() . 'login');
        else
            $this->load->view('approveexpenses');
    }

    public function newexpenseclaim()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url() . 'login');
        else {
            $data = array();
            $data['readonly'] = false;
            $data['claimID'] = 0;
            $data['new'] = true;
            $this->load->view('expenseclaim', $data);
        }
    }

    public function expense(){
        if (!$this->session->userdata('logged_in'))
            redirect(base_url() . 'login');
        else {
            $data = array();
            $id = $this->input->post('claimid');
            if (!is_null($this->input->post('edit'))) {
                $data['claimID'] = $id;
                $data['readonly'] = false;
                $this->load->view('expenseclaim', $data);
            }elseif (!is_null($this->input->post('view'))) {
                $data['claimID'] = $id;
                $data['readonly'] = true;
                $this->load->view('expenseclaim', $data);
            }else{
                $data['approvedby'] = $this->session->userdata('id');
                $data['approvedon'] = date('Y/m/d h:i:s', time());
                if (!is_null($this->input->post('approve'))) {
                    $data['status'] = 3;
                    $action = "Approved";
                    $redirect = 'user/approveexpenses'; // approver
                }
                elseif (!is_null($this->input->post('reject'))) {
                    $data['status'] = 3;
                    $action = "Rejected";
                    $redirect = 'user/approveexpenses'; // approver
                }
                elseif (!is_null($this->input->post('cancel'))) {
                    $data['status'] = 4;
                    $action = "Cancelled";
                    $redirect = 'user/myexpenses'; // requester
                }
                $this->user_model->updateExpenseClaim($id, $data);
                $comment = $this->input->post('comments');
                if(trim($comment) != '')
                    $this->user_model->addExpenseComment($id, $comment);
//                $this->user_model->sendactionedExpense($id, $action);
                redirect(base_url() . $redirect);
            }
        }



    }



    // this isnt right
    public function addexpense()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url() . 'login');
        else{
            $status = 0;
            if (!is_null($this->input->post('btnSave'))) {
                $status = 0;
            }
            elseif (!is_null($this->input->post('btnSubmit'))) {
                $status = 1;
            }
            $redirect = 'user/myexpenses';
            $total = 0;
            $data = array(
                'name' => $this->input->post('claimname'),
                'approver' => $this->input->post('approver'),
                'status' => $status
            );
            $claimid = $this->input->post('claimid');
            if($claimid == 0) {
                // then its a new claim
                $data['total'] = $total;
                $data['user'] = $this->session->userdata('id');
                $data['created'] = date('Y/m/d h:i:s', time());
                $data['companyid'] = $this->session->userdata('company');
                $claimid = $this->user_model->addExpenseClaim($data);
            }
            else{
                // its an update

                if (!is_null($this->input->post('approve'))) {
                    $data['status'] = 2;
                    $action = "Approved";
                    $data['approvedby'] = $this->session->userdata('id');
                    $data['approvedon'] = date('Y/m/d h:i:s', time());
                    $redirect = 'user/approveexpenses'; // approver
                }
                elseif (!is_null($this->input->post('reject'))) {
                    $data['status'] = 3;
                    $action = "Rejected";
                    $data['approvedby'] = $this->session->userdata('id');
                    $data['approvedon'] = date('Y/m/d h:i:s', time());
                    $redirect = 'user/approveexpenses'; // approver
                }
                elseif (!is_null($this->input->post('cancel'))) {
                    $data['status'] = 4;
                    $action = "Cancelled";
                    $data['approvedby'] = $this->session->userdata('id');
                    $data['approvedon'] = date('Y/m/d h:i:s', time());
                    $redirect = 'user/myexpenses'; // requester
                }

                $this->user_model->updateExpenseClaim($claimid, $data);
            }


            $numitems = $this->input->post('items')*1;
            $itemlist = array();
           for($r = 1; $r <= $numitems; $r++) {
                $amount = $this->input->post('expamount'.$r)*1;
                $total += $amount*1;
                $itemdata = array(
                    'transactiondate' => $this->input->post('expdate'.$r),
                    'description' => $this->input->post('expdesc'.$r),
                    'amount' => $amount,
                    'claimid' => $claimid
                );
                $itemid = $this->input->post('item'.$r)*1;

                if($itemid < 0){
                    // its a new one
                    $newitemid = $this->user_model->addExpenseItem($itemdata);
                    $itemlist[] = $newitemid;
                    $this->user_model->updateAttachments($itemid, $newitemid);
                }else{
                    $this->user_model->updateExpenseItem($itemid, $itemdata);
                    $itemlist[] = $itemid;
                }
            }
            // in case some items were deleted, having already been saved,
            // delete from items where claimid = $claimid and itemid not in $itemlist
            $this->user_model->cleanupitems($claimid, $itemlist);

            $expdata['total'] = $total;
            $this->user_model->updateExpenseClaim($claimid, $expdata);

            $comment = $this->input->post('comments');
            if(trim($comment) != '')
                $this->user_model->addExpenseComment($claimid, $comment);
        }
//                $this->user_model->sendactionedExpense($id, $action);
        redirect(base_url() . $redirect);
    }

    public function processleave()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url() . 'login');
        } else {
            $data = array();
            $id = $this->input->post('id');
            $leave = $this->user_model->getLeave($id)->row();

            $action = "";
            if (!is_null($this->input->post('approve'))) {
                $data['status'] = 2;
                $action = "Approved";
            } elseif (!is_null($this->input->post('reject'))) {
                $data['status'] = 3;
                $action = "Rejected";
            } elseif (!is_null($this->input->post('cancel'))) {
                $data['status'] = 4;
                $action = "Cancelled";
            }
            $data['approvedby'] = $this->session->userdata('id');
            $data['approvedon'] = date('Y/m/d h:i:s', time());
            if ($id != '') {
                if ($this->user_model->updateleave($id, $data) && is_null($leave->timesheet))
                    $this->user_model->sendactioned($id, $action);
            } else
                redirect(base_url());
// could be improved to only send if not the users own item.


            if (!is_null($leave->timesheet) && $leave->timesheet != '') {
                // need to update the status of the time entries
                $this->user_model->updateTimeEntryStatus($leave->timesheet, $leave->leavetype, $data['status']);
                redirect(base_url() . 'user/timeapproval');
            } else if ($data['status'] == 4)
                redirect(base_url() . 'user/dashboard');
            else
                redirect(base_url() . 'user/approval');
        }
    }


    public function processtime()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url() . 'login');
        } else {
            $data = array();
            $id = $this->input->post('id');
//            $leave = $this->user_model->getLeave($id)->row();

            $action = "";
            if (!is_null($this->input->post('save'))) {
                // need the show the timesheet detail page
                $data['status'] = 0;
                $action = "Saved";
            } elseif (!is_null($this->input->post('edit'))) {
                // need the show the timesheet detail page
                $data['timesheetid'] = $id;
                $data['isapprover'] = true;
                $this->load->view('timesheet', $data);
                return;
            } elseif (!is_null($this->input->post('approve'))) {
                $data['status'] = 2;
                $action = "Approved";
            } elseif (!is_null($this->input->post('reject'))) {
                $data['status'] = 3;
                $action = "Rejected";
            } elseif (!is_null($this->input->post('cancel'))) {
                $data['status'] = 4;
                $action = "Cancelled";
            }
            $data['approvedby'] = $this->session->userdata('id');
            $data['approvedon'] = date('Y-m-d h:i:s', time());
            if ($id != '') {
                $this->user_model->updateLeaveByTimesheet($id, $data);
                if($data['status'] == 2) { // if approveed
                    $this->user_model->sendactionedTimesheet($id, $action);
                    redirect(base_url() . 'user/timeapproval');
                }
                else
                    redirect(base_url().'user/mytimesheets');
            } else
                redirect(base_url());


        }
    }

    ////////////////////////////////////////
    ////////////////////////////////////////
    ////////////////////////////////////////
    ////////////////////////////////////////


    public function updatecompany()
    {
        $data = array();

        $name = $this->clean($this->input->post('name'));
        $country = $this->clean($this->input->post('country'));
        $state = $this->clean($this->input->post('state'));
        $hours = $this->clean($this->input->post('hours'));
        $timesheetstart = $this->clean($this->input->post('timesheetstart'));
        $decimal = $this->clean($this->input->post('decimal'));
        $apikey = $this->clean($this->input->post('apikey'));
        $calendarAccessLevel = $this->clean($this->input->post('calendarAccessLevel'));

        if (!is_null($name) && $name != "")
            $data['name'] = $name;
        if (!is_null($country) && $country != "")
            $data['country'] = $country;
        if (!is_null($state) && $state != "")
            $data['state'] = $state;
        if (!is_null($hours) && $hours != "")
            $data['hours'] = $hours;
        //if (!is_null($apikey) && $apikey != "")
            $data['apikey'] = $apikey;
        if (!is_null($timesheetstart) && $timesheetstart != "")
            $data['timesheetstart'] = $timesheetstart;
        if ($decimal != "1") {
            $decimal = 0;
        }
        $data['decimal'] = $decimal;
        $data['calendarAccessLevel'] = $calendarAccessLevel;

        if ($this->session->userdata('isadmin')) {
            $status = $this->clean($this->input->post('status'));
            $vip = $this->clean($this->input->post('vip'));

            if (!is_null($vip) && $vip != "")
                $data['vip'] = $vip;
            if (!is_null($status) && $status != "")
                $data['status'] = $status;
        } else {
            $data1 = array(
                'companyname' => $name,
                'companyhours' => $hours
            );
            $this->session->set_userdata($data1);
        }

        $id = $this->input->post('company');

        $result = $this->user_model->updateCompany($id, $data);
//        echo $token;
        if ($this->session->userdata('isadmin')) {
            redirect(base_url() . "admin/dashboard");
        } else
            redirect(base_url() . "user/company");
    }


    public function createuser()
    {
        $self = false;
        $approver = $this->input->post('approver');
        $data = array(
            'company' => $this->clean($this->input->post('company')),
            'name' => $this->clean($this->input->post('name')),
            'email' => $this->input->post('email'),
            'employeeid' => $this->clean($this->input->post('empid')),
            'hours' => $this->input->post('hours'),
            'location' => $this->clean($this->input->post('location')),
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'usertype' => $this->input->post('usertype'),
            'defaultapprover' => $approver
//	        'status' => 2
        );

        $timesheets = $this->input->post('timesheets');
        if ($timesheets == "" || is_null($timesheets)) {
            $timesheets = 0;
        }
        $data["hastimesheets"] = $timesheets;

        if (!isset($approver) || $approver == 'self' || $approver == '') {
            $self = true;
            $data['defaultapprover'] = 0;
        }

        $id = $this->input->post('id');
        if ($id == "" || is_null($id)) {
            $id = null;
            $data['status'] = 2;
            $newID = $this->admin_model->createUser($data, $id);
            if ($self) {
                $newdata = array('defaultapprover' => $newID);
                $this->user_model->updateUser($newID, $newdata);
            }
            $company = $this->user_model->getCompany($this->input->post('company'));
            $this->user_model->sendwelcome($this->input->post('email'), $newID, $company);
        } else // its an update
            $this->admin_model->createUser($data, $id);

        if ($this->session->userdata('isadmin'))
            redirect(base_url() . "admin/users");
        else
            redirect(base_url() . "user/users");
    }

    public function removeleavetype()
    {
        $company = $this->input->post('company');
        $id = $this->input->post('id');
        $this->user_model->removeLeaveType($company, $id);
        redirect(base_url() . "user/managetypes");
    }


    public function restorebatch()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('login');
        else if ($this->session->userdata('usertype') <= 2) {
            $data = array('page' => 'Batch');
            $this->load->view('restorebatch', $data);
        } else {
            redirect(base_url());
        }
    }


    public function restoreitem($batchID = null)
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('login');
        else if ($this->session->userdata('usertype') <= 2) {
            if (is_null($batchID)) {
                $data = array('page' => 'Item');
                $this->load->view('restorebatch', $data);
            } else {
                $data = array('batchID' => $batchID);
                $this->load->view('restoreitem', $data);
            }
        } else {
            redirect(base_url());
        }
    }


    public function publicholidays()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('login');
        else {
            $this->load->view('publicholidays');
        }

    }

    public function admin()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $this->load->view('admin/admin');
        } else {
            redirect(base_url());
        }
    }

    public function company()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else if ($this->session->userdata('usertype') == '1') {
            $this->load->view('company');
        } else {
            redirect(base_url());
        }
    }

    public function package()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else if ($this->session->userdata('usertype') == '1') {
            $this->load->view('admin/package');
        } else {
            redirect(base_url());
        }
    }

    public function users()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else if ($this->session->userdata('usertype') == '1') {
            $this->load->view('users');
        } else {
            redirect(base_url());
        }
    }


    public function summary()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else if ($this->session->userdata('usertype') <= '2') {
            $this->load->view('summary');
        } else {
            redirect(base_url());
        }
    }


    public function proxy()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else if ($this->session->userdata('usertype') <= '3') {
            $this->load->view('proxy');
        } else {
            redirect(base_url());
        }
    }

    public function liability()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else if ($this->session->userdata('usertype') <= '2') {
            $this->load->view('liability');
        } else {
            redirect(base_url());
        }
    }

    public function download()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else if ($this->session->userdata('usertype') <= '2') {
            if (!is_null($this->input->post('download')))
                $this->load->view('generateimp');
            else
                $this->load->view('download');
        } else {
            redirect(base_url());
        }
    }

    public function generateimp()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else if ($this->session->userdata('usertype') <= '2') {
            $this->load->view('generateimp');
        } else {
            redirect(base_url());
        }
    }

    public function managetypes()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else if ($this->session->userdata('usertype') <= '2') {
            $this->load->view('managetypes');
        } else {
            redirect(base_url());
        }
    }

    public function manageusers()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else if ($this->session->userdata('usertype') <= '2') {
            $this->load->view('users');
        } else {
            redirect(base_url());
        }
    }


    public function approval()
    {
        if (!$this->session->userdata('logged_in')) {
            $this->load->view('login');
//            redirect(base_url());
        } else if ($this->session->userdata('usertype') <= '3') {
            $this->load->view('approval');
        } else {
            redirect(base_url());
        }
    }

    public function history()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else if ($this->session->userdata('usertype') <= '3') {
            $this->load->view('history');
        } else {
            redirect(base_url());
        }
    }

    public function upload()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else if ($this->session->userdata('usertype') <= '3') {
            $this->load->view('upload');
        } else {
            redirect(base_url());
        }
    }

    public function addleavetype()
    {
        $data = array(
            'company' => $this->input->post('company'),
            'description' => $this->clean($this->input->post('description')),
            'value' => $this->clean(strtoupper($this->input->post('value'))),
            'notes' => $this->clean($this->input->post('notes')),
            'balancemap' => $this->clean($this->input->post('leavemap')),
            'visibleto' => $this->clean($this->input->post('usertype'))
        );

        $id = $this->input->post('id');
        if ($id == "" || is_null($id)) $id = null;
        $this->user_model->updateLeaveTypes($data, $id);

        redirect(base_url() . "user/managetypes");
    }


    public function bulkimport()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());

        $colcount = $this->input->post("cols");
        $rowcount = $this->input->post("rows");

        $data = array();

        $IDcol = "C";
        // get each column
        // assign column value to array

        // loop through each row and assign values to array
        for ($row = 1; $row <= $rowcount; ++$row) {

            // look for empID
            // if found - perform update.
            // if not found - perform an insert.

        }

        $id = $this->input->post($IDcol . $row);

        $this->admin_model->updateUser($id, $data);

    }


    public function addleave()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else {

            $fd = explode('/', $this->input->post('fromdate'));
            $fromdate = $fd[2] . "-" . $fd[1] . "-" . $fd[0];
            $td = explode('/', $this->input->post('todate'));
            $todate = $td[2] . "-" . $td[1] . "-" . $td[0];

            $approver = $this->input->post('approver');
            $days = $this->input->post('duration');
            $userid = $this->input->post('user');
            $hours = $this->input->post('hours');
//            $half = $this->input->post('half');
            $onedayhours = $this->input->post('onedayhours');
            if ($days == 1 && isset($onedayhours) ){//} && isset($half) && $half == "half") {
                //$hours = $hours / 2;
                $hours = $this->input->post('onedayhours');
            }

            $data = array(
                'fromdate' => $fromdate,
                'todate' => $todate,
                'approver' => $approver,
                'days' => $days,
                'hours' => $hours,
                'createdby' => $this->session->userdata('id'),
                'status' => '1', //default to submitted
                'leavetype' => $this->input->post('type'),
                'comments' => $this->input->post('comments')

            );

            $config['upload_path'] = FCPATH . 'uploads/' . $userid . "/";
            $config['allowed_types'] = 'gif|jpg|png|pdf|bmp';
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, TRUE);
            }

            $this->load->helper(array('form'));

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {
                // probably no file
            } else {
                $upload = array('upload_data' => $this->upload->data());
                $attachment = $upload['upload_data']['file_name'];
                $data['attachment'] = "uploads/" . $userid . "/" . $attachment;
            }
            $data['user'] = $userid;
            $id = $this->input->post('leaveid');
            $company = $this->user_model->getCompany($this->session->userdata('company'));

            if (is_null($id) || $id == "") {
                $data['created'] = date('Y/m/d h:i:s', time());
                $leavehours = $this->user_model->checkAway($userid, $this->input->post('fromdate'), $this->input->post('todate'));
                if ($leavehours == 0) {  // easy, they are not on leave.
                    $leaveID = $this->user_model->addLeave($data);
                    if($company->timesheets){
                        //add to timesheet
//                        foreach ($fromdate to the $todate) ...
//                        $timesheets = $this->user_model->getTimesheetByWeek($userid, $week, $year);
                        //get each day
                        //get working hours for each day
                        //check if timesheet id exists for that day
                        //create if not exist
                        //add / edit timesheet entry for the leave day.
                    }
                    $this->user_model->sendassigned($leaveID, $approver);

                    if ($userid == $this->session->userdata('id')) // then its not a proxy
                        redirect(base_url() . 'user/dashboard');
                    else
                        redirect(base_url() . 'user/approval');
                } else {
                    $data = array('message' => 'This request overlaps with an existing request');
                    $this->session->set_userdata($data);
                    if ($userid == $this->session->userdata('id')) // then its not a proxy
                        redirect(base_url() . 'user/request');
                    else
                        redirect(base_url() . 'user/proxy');
                }
            } else {
                // its an update
                $this->user_model->updateleave($id, $data);
                if($company->timesheets){
                    //add to timesheet
                }
                $this->user_model->sendassigned($id, $approver);


                if ($userid == $this->session->userdata('id')) // then its not a proxy
                    redirect(base_url() . 'user/dashboard');
                else
                    redirect(base_url() . 'user/approval');

            }

        }

    }

    private function clean($str = "")
    {
        $clean = str_replace("'", "", $str);
        return $clean;
    }

    public function editimport()
    {
        // get the batch id
        $batch = $this->input->post('batch');

        $sl = $this->input->post('sl');
        if (isset($sl) && $sl == 'D') {
            $statement = 'Update users SET personalbalance = personalbalance * hours WHERE batch = "' . $batch . '"';
            $this->db->query($statement);
        }

        $al = $this->input->post('al');
        if (isset($al) && $al == 'D') {
            $statement = 'Update users SET leavebalance = leavebalance * hours WHERE batch = "' . $batch . '"';
            $this->db->query($statement);
        }

        $lsl = $this->input->post('lsl');
        if (isset($lsl) && $lsl == 'D') {
            $statement = 'Update users SET longservicebalance = longservicebalance * hours WHERE batch = "' . $batch . '"';
            $this->db->query($statement);
        }

        $rdo = $this->input->post('rdo');
        if (isset($rdo) && $rdo == 'D') {
            $statement = 'Update users SET timeoffbalance = timeoffbalance * hours WHERE batch = "' . $batch . '"';
            $this->db->query($statement);
        }

        $rdo = $this->input->post('flx');
        if (isset($rdo) && $rdo == 'D') {
            $statement = 'Update users SET flexbalance = flexbalance * hours WHERE batch = "' . $batch . '"';
            $this->db->query($statement);
        }

        $st = $this->input->post('st');
        if (isset($st) && $st == 'D') {
            $statement = 'Update users SET studybalance = studybalance * hours WHERE batch = "' . $batch . '"';
            $this->db->query($statement);
        }

        $c1 = $this->input->post('c1');
        if (isset($c1) && $c1 == 'D') {
            $statement = 'Update users SET otherbalance = otherbalance * hours WHERE batch = "' . $batch . '"';
            $this->db->query($statement);
        }

        $c2 = $this->input->post('c2');
        if (isset($c2) && $c2 == 'D') {
            $statement = 'Update users SET otherbalance2 = otherbalance2 * hours WHERE batch = "' . $batch . '"';
            $this->db->query($statement);
        }

        redirect(base_url() . 'user/users');
    }


    public function timesheet()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url().'logout');
        else {
            $this->load->view('timesheet');  // change to be timesheet
        }
    }

    public function mytimesheets()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url().'logout');
        else {
            $this->load->view('mytimesheets');  // change to be timesheet
        }
    }

    public function timeapproval()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url().'logout');
        else {
            $this->load->view('timeapproval');  // change to be timesheet
        }
    }

    public function timehistory()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url().'logout');
        else {
            $this->load->view('timehistory');  // change to be timesheet
        }
    }

    public function managetimes()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else {
            $this->load->view('managetimes');  // change to be timesheet
        }
    }

    public function addtimetype()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        else {
            $data = array(
                'company' => $this->input->post('company'),
                'description' => $this->clean($this->input->post('description')),
                'value' => $this->clean(strtoupper($this->input->post('value'))),
                'notes' => $this->clean($this->input->post('notes')),
                'istime' => '1'
            );

            $id = $this->input->post('id');
            if ($id == "" || is_null($id)) $id = null;
            $this->user_model->updateLeaveTypes($data, $id);
            $this->load->view('managetimes');  // change to be timesheet
        }
    }


    public function addtime()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url().'logout');
        else {
            //  get the week number that the timesheet relates to
            //  date_format($d, "W')  returns the number of te week - stats on a monday
            //
            //  for each day of the week get the hours, minutes and type.
            //  write the data to the timesheet table
            //  and keep track of the total for each category / type
            //
            //  write the totals to the leave table
            //
            //  fromdate can be determined from the week id
            //
            //  to date is from date + 7 days
            $update = false;
            $timesheetID = $this->input->post('timesheetid');  // used if its an update
            if (!is_null($timesheetID) && $timesheetID != '') {
                $update = true;
                $leaveitems = $this->user_model->getLeaveByTimesheet($timesheetID);
                $leaveitem = $leaveitems->row();    // get one of the leave entries in case we need to keep the attachment

                // delete the current entries from timeentry table
                $this->user_model->clearTimeEntries($timesheetID);
                // removes from the Leave table
                $this->user_model->cleanupTimeEntries($timesheetID);
            }

            $userid = $this->input->post('user');
            $company = $this->user_model->getCompany($this->session->userdata('company'));

            $status = 0;
            $btnSave = $this->input->post('btnSave');
            $btnSubmit = $this->input->post('btnSubmit');
            $btnApprove = $this->input->post('btnApprove');

            if (isset($btnSave) )
                $status = 0;
            elseif (isset($btnSubmit) )
                $status = 1;
            elseif (isset($btnApprove) )
                $status = 2;

            $year = $this->input->post('year');
            $week = $this->input->post('weekid');
            $timesheetdata = array(
                'week' => $week,
                'year' => $year,
                'user' => $userid
            );
            if (!$update)
                $timesheetID = $this->user_model->addTimesheet($timesheetdata);

            $begin = new DateTime(date("l, M jS, Y", strtotime($year . "W" . $week . $company->timesheetstart))); // First day of week $$ CHANGE TO COMPANY START DAY
            $end = new DateTime(date("l, M jS, Y", strtotime($year . "W" . $week . $company->timesheetstart)));;
            $end = $end->modify('+6 days');    // need to add one more day

            $fromdate = date("Y/m/d h:i:s", $begin->getTimestamp()); // First day of week
            $todate = date("Y/m/d h:i:s", $end->getTimestamp()); // Last day of week

            if($status == 2) // btnApproved
                $approver = $this->session->userdata('id');
            else
                $approver = $this->input->post('approver');

            $types = $this->user_model->getLeaveTypes(null, 1);  // timesheet types
            $leave = array();

            foreach ($types->result() as $type) {
                $leave[$type->id] = 0;                              // hours
            }


            $rowcount = $this->input->post('rows');
            $rows = array();
            //  loop through the days
            //  for each category, write total to leave
            for ($x = 0; $x < $rowcount; $x++) {
                $rowtotal = 0;

                $rtype = $this->input->post('t' . $x);
            	$ctype = $this->input->post('c' . $x);

                for ($day = 1; $day <= 7; $day++) {
                    $input = $this->input->post('r' . $x . 'd' . $day);
                    if (!is_null($input) && $input != '') {
                        // then its got some data entered
                        $rmins = 0;
                        if($company->decimal){
                            $rhours = floor($input);
                            $rmins = ($input - $rhours)*60;
                        }
                        else{
                            if (strpos($input, ':') > 0) {
                                // then it has hours and minutes
                                $time = explode(':', $input);
                                $rhours = $time[0];

                                if (count($time) > 1)
                                    $rmins = $time[1];
                            } else {
                                // it just has hours
                                $rhours = $input;
                            }
                        }

                        $thisrow = array(
                            'day' => $day,
                            'hours' => $rhours,
                            'minutes' => $rmins,
                            'type' => $rtype,
                            'timesheet' => $timesheetID,
                            'status' => $status,
                        	'costcentre' => $ctype
                        );
                        if($status == 2){
                            $thisrow['approvedby'] = $approver;
                            $thisrow['approvedon'] = date("Y-m-d H:i:s", time());
                        }
       //                 $thisrow['status'] = 1; // pending

                        // write leave entry to the db
                        $timeentryID = $this->user_model->addTimeEntry($thisrow);
                        $rowtotal += $rhours + $rmins / 60;

//                        if (!$update) {
//                        } else {
//                            $timeentry = $this->user_model->getTimeEntryByDay($timesheetID, $day);
//                            if ($timeentry->status != 2) { // if not already approved
//                                $leave[$rtype] += $rhours + $rmins / 60;
//                                // $this->user_model->updateTimeEntry($timeentry->id, $thisrow);
//                            }
//                            // else ... nothing.  cant update an approved time entry.
//                        }
//                        $rows[$x] = $thisrow;
                    }
                }
                if ($rowtotal > 0){
                    // write to leave table
                    $data = array(
                        'fromdate' => $fromdate,
                        'todate' => $todate,
                        'approver' => $approver,
                        'days' => 7,
                        'hours' => $rowtotal,
                        'status' => $status, //default to submitted
                        'leavetype' => $rtype
                    );
                    $comments = $this->input->post('comments');
                    if (!is_null($comments) && $comments != '')
                        $data['comments'] = $comments;

//                    $attachment = $this->input->post('attachment');
//                    if (!is_null($attachment) && $attachment != '')
//                    $data['attachment'] = $attachment;


                    $config['upload_path'] = FCPATH . 'uploads/' . $userid . "/";
                    $config['allowed_types'] = 'gif|jpg|png|pdf|bmp';
                    if (!is_dir($config['upload_path'])) {
                        mkdir($config['upload_path'], 0777, TRUE);
                    }
                    $this->load->helper(array('form'));

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload()) {
                        // probably no file
                        if ($update)
                            $data['attachment'] = $leaveitem->attachment;
                    } else {
                        $upload = array('upload_data' => $this->upload->data());
                        $attachment = $upload['upload_data']['file_name'];
                        $data['attachment'] = "uploads/" . $userid . "/" . $attachment;
                    }


                    if($status == 2){
                        $data['approvedby'] = $approver;
                        $data['approvedon'] = date("Y-m-d H:i:s", time());
                    }

                    $data['user'] = $userid;

                    $data['created'] = date('Y/m/d h:i:s', time());
                    $data['timesheet'] = $timesheetID;
                    $leaveID = $this->user_model->addLeave($data);
                }

                // when added to db, needs the leave id to be added to it.
            }

            // if btnSubmit (or status = 1) then let the approver know timesheet has been assigned to them.
            // if btnApprove (or Status = 2) then let hte person know their timesheet has been approved.
            if($status ==  1){ // submitted
                $this->user_model->sendassignedTime($timesheetID, $approver);
                redirect(base_url() . 'user/mytimesheets');
            }
            else if($status == 2) { // approved{
                $this->user_model->sendactionedTimesheet($timesheetID, "Approved");
                redirect(base_url().'user/timeapproval');
            }
            else
                redirect(base_url() . 'user/mytimesheets');
        }//  if logged in
        redirect(base_url());

    }// end of function


    public function eula()
    {
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        elseif ($this->input->post('agree') == 'agree') {
            // user has agreed, record in db then redirect
            $company = $this->session->userdata('company');
            $license = $this->input->post('eula');
            $this->user_model->eulaAgreed($company, $license);

            redirect(base_url());
        } else {
            $this->load->view('eula');
        }
    }

    public function sync(){
        if (!$this->session->userdata('logged_in'))
            redirect(base_url().'logout');
        $this->load->view('sync');
    }

    public function aosync(){
        if (!$this->session->userdata('logged_in'))
            redirect(base_url().'logout');


        $this->session->set_userdata('aosync', false);


        //  $company->apikey
        $orgkey = $this->session->userdata('companyapikey');


        $host = 'https://api.';
        if (ENVIRONMENT == 'development'){
            // $host .= 'gold.aws.attachecloud.com';
            echo "Currently disabled";
            exit();
        }
        else
            $host .= 'attacheonline.com';


        $host .= '/public.api/v1/employees';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $host,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "appkey: gvINBSjn1EWkGR8qc6+cYw==",
                "cache-control: no-cache",
                "orgkey: ".$orgkey,
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
//** WOULD BE GOOD TO ADD THIS TO A LOG SOMWHERE  */
            // add the error to "error messages" and redirect the user back to ssync screen
            if ($this->session->userdata('usertype') == '1')
                echo "Data Sync Error from Attache Online: " . $err;
//            $this->load->view('sync');
        } else {
            $batchid = md5(date_format(date_create(), 'imHdi'));

            $jsonArray = json_decode($response);

            $company = $this->session->userdata('company');
            $companyData = $this->user_model->getCompany($company);


            for($i = 0; $i<count($jsonArray); $i++){
                $rowdata = Array(); // new clear array

                // set some of the defaults
//                $rowdata['company'] = $company;
                $rowdata['batch'] = $batchid;


                // add the data from the API call
                $rowdata['employeeid'] = $jsonArray[$i]->Code;
                $rowdata['location'] = $jsonArray[$i]->Location;
                $rowdata['email'] = $jsonArray[$i]->EmailAddress;


                if(is_null($jsonArray[$i]->EmailAddress)) continue;


                $hours = $jsonArray[$i]->HoursPerDay; // to bse used later
                $rowdata['hours'] = $hours;
                $rowdata['leaveupdated'] = $jsonArray[$i]->LeaveAccruedUpTo;

                $result = $this->user_model->getRegionByName($jsonArray[$i]->State);
                if ($result != false) {
                    $state = $result->row();
                    $regionID = $state->id;
                    if ($regionID < 1) $regionID = 1;
                    $rowdata['state'] = $regionID;
                    $country = $this->user_model->getCountryByRegion($regionID)->country;
                    $rowdata['country'] = $country;
                }
                // if Days, convert to hours
                for ($j = 0; $j < count($jsonArray[$i]->LeaveBalances); $j++){
                    $balance = $jsonArray[$i]->LeaveBalances[$j]->Balance;
                    if($jsonArray[$i]->LeaveBalances[$j]->Units == "Days")
                        $balance = $balance * $hours;
                    switch ($jsonArray[$i]->LeaveBalances[$j]->LeaveType){
                        case 'Sick':
                            $rowdata['personalbalance'] = $balance;
                            break;
                        case 'Annual':
                            $rowdata['leavebalance'] = $balance;
                            break;
                        case 'Long Service':
                            $rowdata['longservicebalance'] = $balance;
                            break;
                        case 'RDO':
                            $rowdata['timeoffbalance'] = $balance;
                            break;
                        case 'Study':
                            $rowdata['studybalance'] = $balance;
                            break;
                        case 'Income Category 1':
                            $rowdata['otherbalance'] = $balance;
                            break;
                        case 'Income Category 2':
                            $rowdata['otherbalance2'] = $balance;
                            break;

                        /*
                         case 'Flex':
                            $rowdata['flexbalance'] = $balance;
                            break;
                          */
                    }
                }

                $newuser = true;
                //  look for employee by unique AO ID
                $user = $this->user_model->getUserByAOID($jsonArray[$i]->EmployeeId, $company);

                if($user != false){
                    // user was found by unique id so stop looking and flag as update
                    $newuser = false;
                }
                else {
                    // unique AO ID doesnt exist, so needs to be included in add / update
                    $rowdata['attacheonlineid'] = $jsonArray[$i]->EmployeeId;

                    //  look for employee by email address

                    // first exclude deleted users to find an active one
                    $userlist = $this->user_model->getUserByEmail($rowdata['email'], $company, true);
                    if ($userlist->num_rows() >= 1) { // found using email address
                        // how do we know its the right user given more than one has been found?
                        $user = $userlist->row();
                        $newuser = false;
                    }
                    else {
                        // include deleted users - maybe they were deleted for a reason, so dont create them.
                        $userlist = $this->user_model->getUserByEmail($rowdata['email'], $company, false);
                        if ($userlist->num_rows() >= 1) { // found using email address
                            // how do we know its the right user given more than one has been found?
                            // lets try the same search but exclude
                            $user = $userlist->row();
                            $newuser = false;
                        } else {
                            // still not found (by email address)
                            // last try - use employee id and location as the 'employeecode'
                            $userlist = $this->user_model->getUserByEmpID($rowdata['location'], $rowdata['employeeid'], $company);  // if not found by ID, or email, try by employee ID
                            if ($userlist != false) {
                                $user = $userlist->row();
                                $newuser = false;
                            }
                        }
                    }

                }

                if ($newuser){   // else, insert new user.
                    $rowdata['name'] = $jsonArray[$i]->FirstName." ".$jsonArray[$i]->LastName;
                    $rowdata['company']         = $company;
                    if(!array_key_exists('country', $rowdata))
                        $rowdata['country']         = $companyData->country;    // use company default
                    if(!array_key_exists('state', $rowdata))
                        $rowdata['state']           = $companyData->state;      // use company default
                    if(!array_key_exists('defaultapprover', $rowdata))
                        $rowdata['defaultapprover'] = 0;//$this->session->userdata('id');
                    $rowdata['status']          = 1;  // set status to pending. use pending to indicate no welcome email sent.
                    if (!isset($rowdata['usertype']))
                        $rowdata['usertype']        = 4;
                    $id = null;
                    $newID = $this->admin_model->createUser($rowdata);
                }
                else{
                    $this->admin_model->updateUser($user->id, $rowdata);
                }
            }

            if ($this->session->userdata('usertype') == '1')
                echo count($jsonArray)." employees updated from Attache Online.";

        }
        $this->session->set_userdata('aosync', false);

    }

    public function park($page = 'index'){
        if (!$this->session->userdata('logged_in'))
            redirect(base_url().'logout');
        if($page == 'generateroster'){
            if(!is_null($this->input->post('month'))) {
                $monthindex = $this->input->post('month');
                $result = $this->user_model->generateRoster($monthindex);

                if ($result === false) {
                    echo $result;
                    $page = 'roster';
                }
                else
                    $page = "index";
            }
        }
        $this->load->view('park/'.$page);
    }

    public function updatecarparkmobile()
    {
        $data = array();
        $data['result'] = 'error';

        $mobile = '+614' . $this->clean($this->input->post('mobilenumber'));
    
    	$enable = $this->clean($this->input->post('enablesms'));
    	if($enable!="true")
        	$enable="false";

        $result = $this->user_model->addcarparkcontact($mobile,$this->session->userdata('id'),$enable);


    	if($result)
            $data['result'] = 'success';

        $this->load->view('park/addmobile', $data);

    }

    public function auroratest(){
        echo "aurora test started<br>";
        $this->load->database(ENVIRONMENT, TRUE);

//        echo 'Current PHP version: ' . phpversion();
//
        $this->db->from('organisation');
        $result = $this->db->get();
        foreach (($result->result()) as $row){
            echo "Company: ". $row->Name . "<br>";
        }
        echo "aurora test ended<br>";
    }

}
