<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        // log the user out, close the session, redirect to login page

        if ($this->session->userdata('id')) { $this->session->unset_userdata('id'); }
        if ($this->session->userdata('firstname')) { $this->session->unset_userdata('firstname'); }
        if ($this->session->userdata('surname')) { $this->session->unset_userdata('surname'); }
        if ($this->session->userdata('email')) { $this->session->unset_userdata('email'); }
        if ($this->session->userdata('logged_in')) { $this->session->unset_userdata('logged_in'); }
        if ($this->session->userdata('userData')) { $this->session->unset_userdata('userData'); }

        session_destroy();
        redirect(base_url());
//		$this->load->view('login');
	}
}
