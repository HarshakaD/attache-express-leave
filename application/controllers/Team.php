<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->model("user_model");
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    public function index()
    {
            if(! $this->session->userdata('logged_in'))
            $this->load->view('login');
        else
            $this->load->view('dashboard');
    }
    public function login() {
//        $this->form_validation->set_rules('email','email required','trim|required|valid_email|xss_clean');
//        $this->form_validation->set_rules('password','password required','trim|required|xss_clean');
        // If alreay NOT logged in
        if(!$this->session->userdata('logged_in'))
        {
            //To be check login form validation
            if (1==2)//$this->form_validation->run() == FALSE)
            {
                $data["current_page"]="login";
                $this->load->view('login.php',$data);
            }
            else
            {
                //No errors found and allow the user to login to the system
                if ($this->user_model->login())// && ($this->session->userdata('logged_in')) )//&& $this->session->userdata('user_type_id') == 0) //user (v admin)
                {
                    redirect(base_url().'user/dashboard');//
                }
//                elseif ($this->user_model->login() && ($this->session->userdata('logged_in')) && $this->session->userdata('user_type_id') == 1)
//                {
//                    redirect(base_url().'admin/dashboard');
//                }
                else
                {
                    //No user found in the user table
                    $error_message['login_error'] = 'Incorrect email address or password';
                    $this->load->view('login',$error_message);
                }
            }
        }
        else
        {
            // If already logged in
            redirect(base_url().'user/dashboard');
        }
    }// END function login

    public function dashboard()
    {
        if(! $this->session->userdata('logged_in'))
            $this->load->view('login');
        else{

// load up the world and team info

            $this->load->view('history');
        }
    }
    public function profile()
    {
        if(! $this->session->userdata('logged_in'))
            $this->load->view('login');
        else
            $this->load->view('profile');
    }

    public function contacts()
    {
        if(! $this->session->userdata('logged_in'))
            $this->load->view('login');
        else
            $this->load->view('contacts');
    }


    public function submissions()
    {
        if(! $this->session->userdata('logged_in'))
            $this->load->view('login');
        else
            $this->load->view('submissions');
    }


    public function history()
    {
        if(! $this->session->userdata('logged_in'))
            $this->load->view('login');
        else
            $this->load->view('history');
    }

    public function incomestatement()
    {
        if(! $this->session->userdata('logged_in'))
            $this->load->view('login');
        else
            $this->load->view('incomestatement');
    }
    public function cashflow()
    {
        if(! $this->session->userdata('logged_in'))
            $this->load->view('login');
        else
            $this->load->view('cashflow');
    }
    public function balancesheet()
    {
        if(! $this->session->userdata('logged_in'))
            $this->load->view('login');
        else
            $this->load->view('balancesheet');
    }

}
