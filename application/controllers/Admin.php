<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("user_model");
        $this->load->model("admin_model");
        $this->load->library('OnlineUsers');
    }


    public function index()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        elseif ($this->session->userdata('isadmin'))
            $this->load->view('admin/dashboard');
        else
            redirect(base_url());
    }

    public function login()
    {
        if (!$this->session->userdata('logged_in')) {
            if ($this->admin_model->login()) {
                redirect(base_url() . 'admin/dashboard');//
            } else {
                $error_message['login_error'] = 'Incorrect email address or password';
                $this->load->view('admin/login', $error_message);
            }
        } else if ($this->session->userdata('isadmin')) {
            $this->load->view('admin/dashboard');
        } else {
            redirect(base_url() . 'admin');
        }

    }

    public function dashboard()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $this->load->view('admin/dashboard');
        } else {
            redirect(base_url());
        }
    }

    public function restorebatch()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $data = array('page' => 'Batch');
            $this->load->view('admin/restorebatch', $data);
        } else {
            redirect(base_url());
        }
    }


    public function restoreitem($batchID = null)
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            if (is_null($batchID)) {
                $data = array('page' => 'Item');
                $this->load->view('admin/restorebatch', $data);
            } else {
                $data = array('batchID' => $batchID);
                $this->load->view('admin/restoreitem', $data);
            }
        } else {
            redirect(base_url());
        }
    }

    public function createcompany()
    {
        $vip = $this->input->post('vip');
        if (!isset($vip)) {
            $data['vip'] = null;
            $this->load->view('admin/company', $data);
        } else {
            $data = array(
                'name' => $this->input->post('name'),
                'country' => $this->input->post('country'),
                'state' => $this->input->post('state'),
                'status' => $this->input->post('status'),
                'hours' => $this->input->post('hours'),
                'vip' => $vip,
                'created' => date('Y-m-d')
            );
            if(!is_null($this->input->post('timesheets')) && $this->input->post('timesheets') != '')
                $data['timesheets'] = $this->input->post('timesheets');

            if(!is_null($this->input->post('syncdataset')) && $this->input->post('syncdataset') != '')
                $data['syncdataset'] = $this->input->post('syncdataset');

            if ($this->session->userdata('role') <= '1'){
                if(!is_null($this->input->post('billingstatus')) && $this->input->post('billingstatus') != '')
                    $data['billingstatus'] = $this->input->post('billingstatus');
            }
            $token = $this->admin_model->createCompany($data);
            redirect(base_url() . "admin/company/" . urlencode($vip));
        }
    }

    public function editcompany()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'status' => $this->input->post('status'),
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'hours' => $this->input->post('hours'),
            'timesheets' => $this->input->post('timesheets'),
            'expenses' => $this->input->post('expenses'),
            'sevendayleave' => $this->input->post('sevendayleave'),
            'syncdataset' => $this->input->post('syncdataset'),
            'vip' => $this->input->post('vip')
        );
        if($this->session->userdata('role') <= '1')
            $data['billingstatus'] = $this->input->post('billingstatus');

        if(!is_null($this->input->post('syncdataset')) && $this->input->post('syncdataset') != '')
            $data['syncdataset'] = $this->input->post('syncdataset');

        $id = $this->input->post('company');

        $result = $this->user_model->updateCompany($id, $data);
//        echo $token;
        redirect(base_url() . "admin");
    }

    public function help()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $this->load->view('admin/help');
        } else {
            redirect(base_url());
        }
    }

    public function billingplans()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            if (!is_null($this->input->post('description'))) {
                $id = $this->input->post('id');
                if($id == '') $id = null;
                $data = array(
                    'description' => $this->input->post('description'),
                    'notes' => $this->input->post('notes'),
                    'formula' => $this->input->post('formula')
                );
                $this->user_model->editBillingstatus($id, $data);
            }
            $this->load->view('admin/billingplans');
        } else {
            redirect(base_url());
        }
    }

    public function billingreport()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $this->load->view('admin/billingreport');
        } else {
            redirect(base_url());
        }
    }

    public function eula()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $this->load->view('admin/eula');
        } else {
            redirect(base_url());
        }
    }

    public function editeula()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $id = $this->input->post('id');
            $version = $this->input->post('version');
            $status = $this->input->post('status');
            $data = array(
                'version' => $version,
                'status' => $status
            );
            // if a new file was uploaded, update content otherwise leave as is.
            $config['upload_path'] = FCPATH . 'license/';
            $config['allowed_types'] = 'gif|jpg|png|pdf|bmp';
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, TRUE);
            }
            $this->load->helper(array('form'));
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {
//                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload = array('upload_data' => $this->upload->data());
                $attachment = $upload['upload_data']['file_name'];
                $data['content'] = $attachment;
            }

            if($status == 2){
                // approved version
                // mark all other approved versions as cancelled
                $this->user_model->resetApprovedEula();
            }

            if ($id > 0) // edit the existing one
                $this->user_model->editeula($id, $data);
            else   // add a new one
                $this->user_model->addEula($data);


            $this->load->view('admin/eula');
        } else {
            redirect(base_url());
        }
    }

    public function eulareport()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $this->load->view('admin/eulareport');
        } else {
            redirect(base_url());
        }
    }

    public function countries()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $this->load->view('admin/countries');
        } else {
            redirect(base_url());
        }
    }


    public function createcountry()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $id = $this->input->post('id');
            if (is_null($id) || $id == "")
                $id = $this->input->post('newid');
            $data = array(
                'id' => $id,
                'name' => $this->input->post('name')
            );
            $this->user_model->addCountry($data);
            $this->load->view('admin/countries');
        } else {
            redirect(base_url());
        }
    }

    public function edithelp()
    {
        $data = array(
            'summary' => $this->input->post('summary'),
            'detail' => $this->input->post('detail'),
            'visibleto' => $this->input->post('usertype')
        );
        $id = $this->input->post('id');
        if (!is_null($id) && $id != "")
            $result = $this->admin_model->editHelp($id, $data);
        else
            $result = $this->admin_model->createHelp($data);

        redirect(base_url() . "admin/help");
    }

    public function holidays()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $this->load->view('admin/holidays');
        } else {
            redirect(base_url());
        }
    }

    public function admins()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $this->load->view('admin/admin');
        } else {
            redirect(base_url());
        }
    }


    public function companies()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $this->load->view('admin/companies');
        } else {
            redirect(base_url());
        }
    }

    public function company($vip = null)
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $data['vip'] = urldecode($vip);
            $this->load->view('admin/company', $data);
        } else {
            redirect(base_url());
        }
    }

    public function users()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $this->load->view('admin/companies');
        } else {
            redirect(base_url());
        }
    }

    public function createholiday()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        else if ($this->session->userdata('isadmin')) {
            $national = false;
            $id = $this->input->post('id');
            $state = $this->input->post('state');
            $day = explode("/", $this->input->post('day'));
            $date = $day[2] . '-' . $day[1] . '-' . $day[0];

            if ($state == 0)
                $national = true;
            $data = array(
                'country' => $this->input->post('country'),
                'state' => $state,
                'date' => $date,
                'description' => $this->input->post('name'),
                'national' => $national
            );
            if ($id > 0) // edit the existing one
                $this->user_model->editHoliday($id, $data);
            else   // add a new one
                $this->user_model->addHoliday($data);
            $this->load->view('admin/holidays');
        } else {
            redirect(base_url());
        }
    }

    public function createuser()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        elseif ($this->session->userdata('isadmin')) {
            $company = $this->input->post('company');
            if (!isset($company)) {
                $this->load->view('admin/companies');
            } else {
                $approver = $this->input->post('approver');
                $data = array(
                    'company' => $this->input->post('company'),
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'employeeid' => $this->input->post('empid'),
                    'hours' => $this->input->post('hours'),
                    'location' => $this->input->post('location'),
                    'state' => $this->input->post('state'),
                    'country' => $this->input->post('country'),
                    'usertype' => $this->input->post('usertype')
                );
                $id = $this->input->post('id');
                if (!isset($id)) {//} == "" || is_null($id)){
                    $id = null;
                    if (isset($approver)) {
                        $data['defaultapprover'] = $approver;
                    } else {
                        $data['defaultapprover'] = 0;
                    }
                    $newID = $this->admin_model->createUser($data, $id);
                    $this->admin_model->createUser(array('defaultapprover' => $newID), $id);
                    $company = $this->user_model->getCompany($this->input->post('company'));
                    $this->user_model->sendwelcome($data['email'], $newID, $company);
                } else
                    $this->admin_model->createUser($data, $id);

                $this->load->view('admin/companies');
            }
        } else {
            redirect(base_url());
        }
    }

    public function edituser()
    {
    }

    public function createadmin()
    {
        $data = array(
            'username' => $this->input->post('username'),
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'role' => $this->input->post('role')
        );
        $id = $this->input->post('id');
        if ($id == "" || is_null($id)) {
            $id = null;
            $newID = $this->admin_model->createAdmin($data, $id);
        } else
            $this->admin_model->createAdmin($data, $id);


        redirect(base_url() . "admin");


    }


    public function changepassword()
    {
        if (!$this->session->userdata('logged_in'))
            $this->load->view('admin/login');
        elseif ($this->session->userdata('isadmin')) {
            $for = $this->input->post('for');
            $user = $this->input->post('user');

            if (!is_null($for) || $for != "") {
                $newpwd = $this->input->post('password');
                $this->admin_model->changepassword($for, $newpwd);
                redirect(base_url() . 'admin');
            } elseif (is_null($user) || $user == "") {
                $data['id'] = $this->session->userdata('id');
                $this->load->view('admin/changepassword', $data);
            } else {
                $data['id'] = $user;
                $this->load->view('admin/changepassword', $data);
            }
        } else
            redirect(base_url() . 'admin');
    }


    public function db($page = null)
    {
        $data = array();
        $this->load->helper(array('datagrid', 'url'));

//        $this->Datagrid = new Datagrid('NS_GLOBAL_MASTER','NS_GM_WRLD_RID');

        if (!$this->session->userdata('logged_in'))
            redirect(base_url() . 'logout');
        elseif ($this->session->userdata('isadmin')) {
            if (is_null($page) || $page == 'tables')
                $this->load->view('admin/managedb', $data);
            elseif ($page == "procs")
                $this->load->view('admin/manageprocs', $data);
            elseif ($page == "funcs")
                $this->load->view('admin/managefuncs', $data);
            elseif ($page == "sql") {
                $this->load->view('admin/managesql');
            }
        } else {
            redirect(base_url());
        }
    }

}
