<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("user_model");
        $this->load->model("admin_model");
        $this->load->helper(array('form'));
    }

    public function index()
    {
        redirect(base_url());
    }

    private function validate()
    {
        if (!$this->session->userdata('logged_in')) {
            echo "Error: Not Logged In";
            exit();
        }
    }

    public function checkemail()
    {
        $this->validate();

        $result = $this->user_model->isUnique($_POST['email']);
        if ($result != 0) echo 'FALSE';
        else echo 'TRUE';
    }// END function login

    public function approve()
    {
        $this->validate();

        $data = array();
        $id = $this->input->post('leave');
        $action = "Approved";
        $data['status'] = 2; // approved
        $data['approvedby'] = $this->session->userdata('id');
        $data['approvedon'] = date('Y/m/d h:i:s', time());
        $this->user_model->updateleave($id, $data);
        // could be improved to only send if not the users own item.
        $this->user_model->sendactioned($id, $action);
        echo "Approved";

    }// END function login


    public function approveTime()
    {
        $this->validate();


        $data = array();
        $id = $this->input->post('time');
        $action = "Approved";
        $data['status'] = 2; // approved
        $data['approvedby'] = $this->session->userdata('id');
        $data['approvedon'] = date('Y/m/d h:i:s', time());
        $this->user_model->updateLeaveByTimesheet($id, $data);
        $this->user_model->sendactionedTimesheet($id, $action);
        echo "Approved";

    }// END function login

    public function cancel()
    {
        $this->validate();

        $data = array();
        $id = $this->input->post('leave');
        $action = "Cancelled";
        $data['status'] = 4; // cancelled
        $data['approvedby'] = $this->session->userdata('id');
        $data['approvedon'] = date('Y/m/d h:i:s', time());
        $this->user_model->updateleave($id, $data);
        // could be improved to only send if not the users own item.
        $this->user_model->sendactioned($id, $action);
        echo "Cancelled";

    }// END function login

    public function cancelTime()
    {
        $this->validate();

        $data = array();
        $id = $this->input->post('time');
        $action = "Cancelled";
        $data['status'] = 4; // cancelled
        $data['approvedby'] = $this->session->userdata('id');
        $data['approvedon'] = date('Y/m/d h:i:s', time());
        $this->user_model->updateLeaveByTimesheet($id, $data);
        $this->user_model->sendactionedTimesheet($id, $action);
//            $this->user_model->updateleave($id, $data);
//            // could be improved to only send if not the users own item.
//            $this->user_model->sendactioned($id, $action);
        echo "Cancelled";

    }// END function login

    public function reject()
    {
        $this->validate();

        $data = array();
        $id = $this->input->post('leave');
        $action = "Rejected";
        $data['status'] = 3; // rejected
        $data['approvedby'] = $this->session->userdata('id');
        $data['approvedon'] = date('Y/m/d h:i:s', time());
        $this->user_model->updateleave($id, $data);
        // could be improved to only send if not the users own item.
        $this->user_model->sendactioned($id, $action);
        echo "Rejected";
    }// END function login


    public function rejectTime()
    {
        $this->validate();

        $data = array();
        $id = $this->input->post('time');
        $action = "Rejected";
        $data['status'] = 3; // cancelled
        $data['approvedby'] = $this->session->userdata('id');
        $data['approvedon'] = date('Y/m/d h:i:s', time());
        $this->user_model->updateLeaveByTimesheet($id, $data);
        $this->user_model->sendactionedTimesheet($id, $action);
//            $this->user_model->updateleave($id, $data);
//            // could be improved to only send if not the users own item.
//            $this->user_model->sendactioned($id, $action);
        echo "Rejected";

    }// END function login

    public function reassign()
    {
        $this->validate();

        $leaveID = $_POST['leave'];
        $newApprover = $_POST['approver'];
        $actionedBy = $this->session->userdata('id');

        $data = array(
            'approver' => $newApprover,
            'status' => 1,
            'approvedby' => $actionedBy,
            'approvedon' => date('Y/m/d h:i:s', time())
        );
        $result = $this->user_model->updateleave($leaveID, $data);
        if ($result == 1) {
            // send email
            $this->user_model->sendassigned($leaveID, $newApprover);
            echo "<br>Successfully reassigned to " . $this->user_model->getUser($newApprover)->row()->name . " and they have been emailed";
        } else echo "Something went wrong. It was not reassigned.";
    }// END function login


    public function reassigntime()
    {
        $this->validate();

        $id = $_POST['time'];
        $newApprover = $_POST['approver'];
        $actionedBy = $this->session->userdata('id');
        $action = 'Reassigned';
        $data = array(
            'approver' => $newApprover,
            'status' => 1,
            'approvedby' => $actionedBy,
            'approvedon' => date('Y/m/d h:i:s', time())
        );
//        $this->user_model->updateLeaveByTimesheet($id, $data);
//        $this->user_model->sendactionedTimesheet($id, $action);
    }// END function login

    public function getWorkingDays()
    {
        $this->validate();

//        $userID =  $this->session->userdata('id');
        $userID = $this->input->post('employee');
        if (is_null($userID) | $userID == "")
            $userID = $this->session->userdata('id');

        $user = $this->user_model->getUser($userID)->row();
        $holidays = $this->user_model->getHolidaysByRegion($user->state);

        if ($_POST['datef'] == "" || $_POST['datet'] == "") {
            echo "";
            return;
        }

        $fd = explode('/', $_POST['datet']);
        $endDate = $fd[2] . "-" . $fd[1] . "-" . $fd[0];

        $td = explode('/', $_POST['datef']);
        $startDate = $td[2] . "-" . $td[1] . "-" . $td[0];

        $company = $this->user_model->getCompany($user->company);
        if ($company->sevendayleave) {
            $workingDays = $this->user_model->getWorkingHours($startDate, $endDate, $userID);
        } else {
            $workingDays = $this->user_model->getWorkingDays($startDate, $endDate, $userID);
        }

        echo json_encode($workingDays);
    }


    public function getTable($page = 0)
    {
        $this->validate();


        if ($this->session->userdata('isadmin')) {
            $table = $this->input->post('table');
            $limit = $this->input->post('limit');
            $offset = $this->input->post('offset');

            if (is_null($limit) || $limit == "")
                $limit = 25;
            if (is_null($page) || $page == "")
                $page = 1;
            if (is_null($offset) || $offset == "")
                $offset = ($page - 1) * $limit;


            $per_page = $limit;         // number of results to show per page
            $total_results = $this->admin_model->getSize($table);
            $total_pages = ceil($total_results / $per_page);//total pages we going

            if ($total_pages > 1) {
                $this->load->library('pagination');

                $config['base_url'] = "javascript:getTable('" . $table . "', '";
                $config['total_rows'] = $total_results;
                $config['per_page'] = $limit;
                $config['num_links'] = 3;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';
                $config['cur_tag_open'] = '<li class="active">replacemestart';
                $config['next_link'] = 'Next &gt;';
                $config['prev_link'] = '&lt; Prev';

                $config['uri_segment'] = 3;

                $config['cur_tag_close'] = 'replacemeend</li>';

                $config['use_page_numbers'] = true;

                $this->pagination->initialize($config);

                $tmp = $this->pagination->create_links();
                $pagination = str_replace("<a", "<li><a", $tmp);
                $pagination = str_replace("</a>", "</a></li>", $pagination);
                $pagination = str_replace("replacemestart", "<a>", $pagination);
                $pagination = str_replace("replacemeend", "</a>", $pagination);
                $pagination = str_replace("data-ci-", "');\" data-ci-", $pagination);
                $pagination = str_replace("'/", "'", $pagination);
                $pagination = str_replace("\" ');", "');", $pagination);

            } else
                $pagination = "";

            $headings = $this->admin_model->getTableColumns($table);
            $faq = $this->admin_model->getTableData($table, $limit, $offset);

            $columns = array();
            $hcounter = 0;
            echo '<table class="tbl-qa table table-bordered" style="margin: 0;padding: 0">';
            echo '<thead><tr class="table-row">';
            foreach ($headings as $h) {
                echo '<th >' . $h->name . '</th>';
                $columns[$hcounter] = $h;
                if ($h->primary_key)
                    $pk = $h->name;
                $hcounter++;
            }
            echo '</tr></thead><tbody>';
            foreach ($faq->result() as $row) {
                echo '<tr class="table-row">';
                $hcounter = 0;
                $id = null;
                foreach ($row as $key => $item) {

                    if ($key == $pk) {
                        $id = $item;
                        $editable = '';
                    } else {
                        $editable = 'contenteditable="TRUE" onfocus="showEdit(this);" onblur="leaveEdit(this);" onkeyup="saveToDatabase(this, \'' . $pk . '\', \'' . $columns[$hcounter]->name . '\',\'' . $id . '\')" ';
                    }
                    echo '<td ' . $editable . ' >' . $item . '</td>';
                    $hcounter++;
                }
                echo '</tr>';
            }
            echo '</tbody></table>' . $pagination;
        }
//        echo $str;
    }

    public function updatedb()
    {
        $this->validate();

        //later, check logged in and admin
        $table = $this->input->post('table');
        $pk = $this->input->post('pk');
        $column = $this->input->post('column');
        $newval = $this->input->post('newval');
        $pkid = $this->input->post('pkid');

        //($data, $where,$table

        $success = $this->admin_model->updateField(array($column => $newval), array($pk => $pkid), $table);
        echo $success;

    }

    private function execute()
    {
        $this->validate();

        //later, check logged in and admin
        $sql = $this->input->post('sql');
        $success = $this->admin_model->execute($sql);
        echo '<table class="tbl-qa table table-bordered" style="margin: 0;padding: 0">';
        try {
            if (is_object($success)) {
                foreach ($success->result() as $row) {
                    echo '<tr class="table-row">';
                    if (is_object($row)) {
                        foreach ((array)$row as $key => $item) {
                            echo '<td >' . $item . '</td>';
                        }
                    } else echo print_r($row, true);
                    echo '</tr>';
                }

            } else echo print_r($success, true);
        } catch (Exception $e) {
            echo '<tr class="table-row"><td>';
            echo print_r($success, true);
            echo '</td></tr>';
            echo '<tr class="table-row"><td>';
            echo $e->getMessage();
            echo '</td></tr>';
        }
        echo '</table>';
    }


    public function deleteLeave()
    {
        $this->validate();

        $this->user_model->deleteLeaveType($this->input->post('leavetype'));
        return "Deleted";
    }

    public function deleteEmp()
    {
        $this->validate();

        // cehck that the user does not have depentancts (ie, employees whose default approer is this user).
        $dependants = $this->user_model->getUserBy(array('defaultapprover' => $this->input->post('emp'), 'status !=' => 5));
        if (!is_null($dependants) && $dependants->num_rows()) {
            $str = 'Cannot Delete. The following Employees have this Employee set as their default approver: <br>';
            foreach ($dependants->result() as $emp) {
                $str .= $emp->name . '<br>';
            }
            $str .= 'Please remove all default approver references then delete.';
            echo $str;
        } else {
            $this->admin_model->updateUser($this->input->post('emp'), array('status' => 5));
            echo "Deleted";
        }
    }

    public function deleteAdmin()
    {
        // check user is logged in
        // check they are admin
        // check they are supervisor or system admin
        $this->validate();

        if ($this->session->userdata('isadmin')) {
            if ($this->admin_model->deleteAdmin($this->input->post('adminid'))) {
                echo "Delete confirmed";
            } else {
                echo "The delete may not have worked";
            }
        }

    }

    public function addexpattachment()
    {
        $this->validate();

        $config['upload_path'] = FCPATH . 'uploads/' . $this->session->userdata('id') . "/expenses/";
        $config['allowed_types'] = 'pdf|jpg|jpeg|png|tiff';
        if (!is_dir($config['upload_path'])) {
            mkdir($config['upload_path'], 0777, TRUE);
        }
        $this->load->helper(array('form'));

        $this->load->library('upload', $config);
        $itemid = $this->input->post('attitemid');

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            echo print_r($error, true);
        } else {
            $upload = array('upload_data' => $this->upload->data());
            $attachment = $upload['upload_data']['file_name'];
            $data['attachment'] = "uploads/" . $this->session->userdata('id') . "/expenses/" . $attachment;

            $attdata = array(
                'location' => $data['attachment'],
                'itemid' => $itemid
            );

            $this->user_model->addExpenseAttachment($attdata);
            echo base_url() . $data['attachment'];
        }
    }

    public function importbi()
    {
        $this->validate();

        $config['upload_path'] = FCPATH . 'uploads/' . $this->session->userdata('id') . "/";
        $config['allowed_types'] = 'csv|xls|xlsx|CSV|XLS';
        if (!is_dir($config['upload_path'])) {
            mkdir($config['upload_path'], 0777, TRUE);
        }

        $this->load->helper(array('form'));

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            echo print_r($error, true);
        } else {
            $upload = array('upload_data' => $this->upload->data());
            $attachment = $upload['upload_data']['file_name'];
            $data['attachment'] = "uploads/" . $this->session->userdata('id') . "/" . $attachment;

            //load the excel library
            $this->load->library('excel');
            //read file from path
            $objPHPExcel = PHPExcel_IOFactory::load($data['attachment']);
            //get only the Cell Collection`1
//            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
            $batchid = md5(date_format(date_create(), 'imHdi'));
            echo '<div class="x_panel">';
            echo '<div class="x_title">';
            echo '   <h2>File Data - Imported </h2>';
            echo '       <div class="clearfix"></div>';
            echo '       </div>';
            echo '       <div class="x_content" >';
            echo '       <br />';
            echo '       <div >';

            $objWorksheet = $objPHPExcel->getActiveSheet();
            $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
            $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'

            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
            if ($highestColumnIndex < 2) {
                echo 'ERROR: Columns not found. Please check your document is in the correct MS Excel format.';
            }

            echo '<form action="' . base_url() . 'user/editimport" method="post">';
            echo '  <input type="hidden" name="batch" value="' . $batchid . '">';
            echo '<table class="tbl-qa table table-bordered jambo_table" style="margin: 0;padding: 0">';

            $fieldKey = array(
                "EmployeeCode" => null,
                "Location" => null,
                "FullName" => null,
                "State" => null,
//                "Country"                   => null,
                "HoursPerDay" => null,
                "HourPerDay" => null,
                "SLEntitleHourAcc" => null,
                "SLProRataHourAcc" => null,
                "ALEntitleHourAcc" => null,
                "ALProRataHourAcc" => null,
                "LSLEntitleHourAcc" => null,
                "LSLProRataHourAcc" => null,
                "RDOEntitleHourAcc" => null,
                "RDOProRataHourAcc" => null,
                "STLEntitleHourAcc" => null,
                "STLProRataHourAcc" => null,
                "FlexHourAccrue" => null,
                "UDL1EntitleHourAcc" => null,
                "UDL1ProRataHourAcc" => null,
                "UDL2EntitleHourAcc" => null,
                "UDL2ProRataHourAcc" => null,
                "WorkEmail" => null,
                "AOEmail" => null,
                "LeaveAccrueTo" => null,
                "usertype" => null,
// accounts
                "Employee Location" => null,
                "Employee Code" => null,
                "Full Name" => null,
                "Hours per Day" => null,
                "New Address - State" => null,
//                "New Address - Country"	    => null,
                "Leave Accrued to/Last Pay Date" => null,
                "Acc-SL-Entitlement Hours" => null,
                "Acc-SL-Pro-rata Hours" => null,
                "Acc-AL-Entitlement Hours" => null,
                "Acc-AL-Pro-rata Hours" => null,
                "Acc-LSL-Entitlement Hours" => null,
                "Acc-LSL-Pro-rata Hours" => null,
                "Acc-RDO-Entitlement Hours" => null,
                "Acc-RDO-Pro-rata Hours" => null,
                "Acc-STL-Entitlement Hours" => null,
                "Acc-STL-Pro-rata Hours" => null,
                "Acc-U1-Entitlement Hours" => null,
                "Acc-U1-Pro-rata Hours" => null,
                "Acc-U2-Entitlement Hours" => null,
                "Acc-U2-Pro-rata Hours" => null,
                "Approver" => null,
                "DefaultApprover" => null

            );
            $fieldMap = array(
                "EmployeeCode" => "employeeid",
                "Location" => "location",
                "FullName" => "name",
                "State" => "state",
                "Country" => "country",
                "HoursPerDay" => "hours",
                "HourPerDay" => "hours",
                "SLEntitleHourAcc" => "personalbalance",
                "SLProRataHourAcc" => "personalbalance",
                "ALEntitleHourAcc" => "leavebalance",
                "ALProRataHourAcc" => "leavebalance",
                "LSLEntitleHourAcc" => "longservicebalance",
                "LSLProRataHourAcc" => "longservicebalance",
                "RDOEntitleHourAcc" => "timeoffbalance",
                "RDOProRataHourAcc" => "timeoffbalance",
                "STLEntitleHourAcc" => "studybalance",
                "STLProRataHourAcc" => "studybalance",
                "FlexHourAccrue" => "flexbalance",
                "UDL1EntitleHourAcc" => "otherbalance",
                "UDL1ProRataHourAcc" => "otherbalance",
                "UDL2EntitleHourAcc" => "otherbalance2",
                "UDL2ProRataHourAcc" => "otherbalance2",
                "WorkEmail" => "email",
                "AOEmail" => "email",
                "LeaveAccrueTo" => "leaveupdated",
                "usertype" => "usertype",
// accounts
                "Employee Location" => "location",
                "Employee Code" => "employeeid",
                "Full Name" => "name",
                "Hours per Day" => "hours",
                "New Address - State" => "state",
                "New Address - Country" => "country",
                "Leave Accrued to/Last Pay Date" => "leaveupdated",
                "Acc-SL-Entitlement Hours" => "personalbalance",
                "Acc-SL-Pro-rata Hours" => "personalbalance",
                "Acc-AL-Entitlement Hours" => "leavebalance",
                "Acc-AL-Pro-rata Hours" => "leavebalance",
                "Acc-LSL-Entitlement Hours" => "longservicebalance",
                "Acc-LSL-Pro-rata Hours" => "longservicebalance",
                "Acc-RDO-Entitlement Hours" => "timeoffbalance",
                "Acc-RDO-Pro-rata Hours" => "timeoffbalance",
                "Acc-STL-Entitlement Hours" => "studybalance",
                "Acc-STL-Pro-rata Hours" => "studybalance",
                "Acc-U1-Entitlement Hours" => "otherbalance",
                "Acc-U1-Pro-rata Hours" => "otherbalance",
                "Acc-U2-Entitlement Hours" => "otherbalance2",
                "Acc-U2-Pro-rata Hours" => "otherbalance2",
//other
                "Approver" => "defaultapprover",
                "DefaultApprover" => "defaultapprover",
                "Default Approver" => "defaultapprover"
            );

//            "FlexHourAccrue"        ,

            $excludelist = array(
                "SLEntitleHourAcc",
                "SLProRataHourAcc",
                "ALEntitleHourAcc",
                "ALProRataHourAcc",
                "LSLEntitleHourAcc",
                "LSLProRataHourAcc",
                "RDOEntitleHourAcc",
                "RDOProRataHourAcc",
                "FlexHourAccrue",
                "STLEntitleHourAcc",
                "STLProRataHourAcc",
                "UDL1EntitleHourAcc",
                "UDL1ProRataHourAcc",
                "UDL2EntitleHourAcc",
                "UDL2ProRataHourAcc",

                "Acc-SL-Entitlement Hours",
                "Acc-SL-Pro-rata Hours",
                "Acc-AL-Entitlement Hours",
                "Acc-AL-Pro-rata Hours",
                "Acc-LSL-Entitlement Hours",
                "Acc-LSL-Pro-rata Hours",
                "Acc-RDO-Entitlement Hours",
                "Acc-RDO-Pro-rata Hours",
                "Acc-STL-Entitlement Hours",
                "Acc-STL-Pro-rata Hours",
                "Acc-U1-Entitlement Hours",
                "Acc-U1-Pro-rata Hours",
                "Acc-U2-Entitlement Hours",
                "Acc-U2-Pro-rata Hours"
            );

            $userTypes = $this->user_model->getUserType();

            $headRow = null;
            $containsApprovers = false;
            $company = $this->session->userdata('company');
            $companyData = $this->user_model->getCompany($company);
            $colcount = 0;
            $showupdate = false;
            for ($row = 0; $row <= $highestRow; ++$row) {
                $rowdata = array();

                $rowStr = "";

                if (is_null($headRow)) {
                    for ($col = 0; $col <= $highestColumnIndex - 1; ++$col) {
                        $cellval = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();

                        foreach ($fieldKey as $key => $val) {  // find all the column indexes
                            if (strcasecmp($cellval, $key) == 0) {
                                $fieldKey[$key] = $col;
                                $headRow = $row;
                            }
                        }
                    }
                }
                if (!is_null($headRow)) {
                    if ($row == $headRow) {
                        $rowStr .= '<thead><tr><th>Action</th>';
                        foreach ($fieldKey as $key => $val) {
                            if (!is_null($val) && !in_array($key, $excludelist)) {
                                $rowStr .= "<th>" . $key . "</th>";
                                $colcount++;
                                if (strcasecmp($key, "Approver") == 0 || strcasecmp($key, "DefaultApprover") == 0 || strcasecmp($key, "Default Approver") == 0) {
                                    $containsApprovers = true;
                                }
                            }
                        }
                        //Personal Leave
                        $options = "<option selected value='H'>Hours</option><option value='D'>Days</option>";
                        if ((!is_null($fieldKey['SLEntitleHourAcc']) || !is_null($fieldKey['SLProRataHourAcc']))
                            || (!is_null($fieldKey['Acc-SL-Entitlement Hours']) || !is_null($fieldKey['Acc-SL-Pro-rata Hours']))) {
                            $rowStr .= "<th>Sick/Personal Leave<br><select name='sl' id='sl' style='color: red !important; width: 100%'>" . $options . "</select></th>";
                            $showupdate = true;
                            $colcount++;
                        }
                        //Annual Leave
                        if ((!is_null($fieldKey['ALEntitleHourAcc']) || !is_null($fieldKey['ALProRataHourAcc']))
                            || (!is_null($fieldKey['Acc-AL-Entitlement Hours']) || !is_null($fieldKey['Acc-AL-Pro-rata Hours']))) {
                            $rowStr .= "<th>Total Annual Leave<br><select name='al' id='al' style='color: red !important; width: 100%'>" . $options . "</select></th>";
                            $showupdate = true;
                            $colcount++;
                        }
                        //Long Service Leave
                        if ((!is_null($fieldKey['LSLEntitleHourAcc']) || !is_null($fieldKey['LSLProRataHourAcc']))
                            || (!is_null($fieldKey['Acc-LSL-Entitlement Hours']) || !is_null($fieldKey['Acc-LSL-Pro-rata Hours']))) {
                            $rowStr .= "<th>Long Service Leave<br><select name='lsl' id='lsl' style='color: red !important; width: 100%'>" . $options . "</select></th>";
                            $showupdate = true;
                            $colcount++;
                        }
                        // RDO Leave
                        if ((!is_null($fieldKey['RDOEntitleHourAcc']) || !is_null($fieldKey['RDOProRataHourAcc']))
                            || (!is_null($fieldKey['Acc-RDO-Entitlement Hours']) || !is_null($fieldKey['Acc-RDO-Pro-rata Hours']))) {
                            $rowStr .= "<th>RDO Leave<br><select name='rdo' id='rdo' style='color: red !important; width: 100%'>" . $options . "</select></th>";
                            $showupdate = true;
                            $colcount++;
                        }
                        // Flex Leave
                        if ((!is_null($fieldKey['FlexHourAccrue']))) {
                            $rowStr .= "<th>Flex-Time Leave"; //<br><select name='flx' id='flx' style='color: red !important; width: 100%'>".$options."</select>
                            $rowStr .= "</th>";
//                            $showupdate = true;
                            $colcount++;
                        }
                        // Study Leave
                        if ((!is_null($fieldKey['STLEntitleHourAcc']) || !is_null($fieldKey['STLProRataHourAcc']))
                            || (!is_null($fieldKey['Acc-STL-Entitlement Hours']) || !is_null($fieldKey['Acc-STL-Pro-rata Hours']))) {
                            $rowStr .= "<th>Study Leave<br><select name='st' id='st' style='color: red !important; width: 100%'>" . $options . "</select></th>";
                            $showupdate = true;
                            $colcount++;
                        }
                        // Income Category 1
                        if ((!is_null($fieldKey['UDL1EntitleHourAcc']) || !is_null($fieldKey['UDL1ProRataHourAcc']))
                            || (!is_null($fieldKey['Acc-U1-Entitlement Hours']) || !is_null($fieldKey['Acc-U1-Pro-rata Hours']))) {
                            $rowStr .= "<th>Income Category 1 Leave<br><select name='c1' id='c1' style='color: red !important; width: 100%'>" . $options . "</select></th>";
                            $showupdate = true;
                            $colcount++;
                        }
                        // Income Category 2
                        if ((!is_null($fieldKey['UDL2EntitleHourAcc']) || !is_null($fieldKey['UDL2ProRataHourAcc']))
                            || (!is_null($fieldKey['Acc-U2-Entitlement Hours']) || !is_null($fieldKey['Acc-U2-Pro-rata Hours']))) {
                            $rowStr .= "<th>Income Category 2 Leave<br><select name='c2' id='c2' style='color: red !important; width: 100%'>" . $options . "</select></th>";
                            $showupdate = true;
                            $colcount++;
                        }

                        $rowStr .= '</tr></thead>';

                        if ($showupdate) {
                            // add a row explaing the hours / days, and include and "Save" button
                            $rowStr .= '<tr><td colspan="' . ($colcount + 1) . '" class="success">Please Note: Imported Leave Balances are assumed to be in Hours.
                                <b>Please specify any balances you want imported as Days.</b> Change as required, then click "Update Leave Balances".
                                <input type="submit" class="btn btn-sm btn-danger" id="updatehours" name="updatehours" value="Update Leave Balances" /></td></tr>';
                        }

                        echo $rowStr;
                        $rowStr = "";
                    } elseif ($row > $headRow) {
                        $warn = false;
                        foreach ($fieldKey as $key => $val) {
                            if (!is_null($val)) {
                                $msg = "";
                                $cv = $this->clean($objWorksheet->getCellByColumnAndRow($val, $row)->getValue());
                                if (!is_null($cv) && $cv != "") {
                                    if ($key == "usertype") {
                                        $rowdata[$fieldMap[$key]] = 4; // default to non-approver, but continue to check
                                        foreach ($userTypes->result() as $type) {
                                            if (strcasecmp($type->description, $cv) == 0)
                                                $rowdata[$fieldMap[$key]] = $type->id;
                                        }
                                    } elseif ($key == "State" || $key == "New Address - State") {
                                        $result = $this->user_model->getRegionByName($cv);
                                        if ($result != false) {
                                            $state = $result->row();
                                            $regionID = $state->id;
                                            if ($regionID < 1) $regionID = 1;
                                            $rowdata['state'] = $regionID;
                                            $country = $this->user_model->getCountryByRegion($regionID)->country;
                                            $rowdata['country'] = $country;
                                            $cv = $state->name . ", " . $country;
                                        }
                                    } elseif ($key == "LeaveAccrueTo" || $key == 'Leave Accrued to/Last Pay Date') {
                                        if ($cv != "") {
                                            if (strpos($cv, '/')) { // if reading from excel as a date  dd/mm/yyyy
                                                $fd = explode('/', $cv);
                                                $cv = $fd[2] . "-" . $fd[1] . "-" . $fd[0];
                                            } else {  // if reading from excel as a number
                                                $newdate = '1899-12-30';
                                                $cv = date('Y-m-d', strtotime($newdate . ' + ' . $cv . ' days'));
                                            }
                                        } else
                                            $cv = "";
                                        $rowdata[$fieldMap[$key]] = $cv;
                                    } elseif ($key == "Approver" || $key == 'DefaultApprover') {
                                        // need to
                                        $approverresult = $this->user_model->getUserByEmpID(null, $cv, $company);
                                        if ($approverresult != false) {
                                            $approver = $approverresult->row();
                                            //check that the new approver is set to approver or higher
                                            if ($approver->usertype > 3) {
                                                $this->user_model->updateUser($approver->id, array('usertype' => '3'));
                                                $msg = ' (Changed to Approver)';
                                                $warn = true;
                                            }
                                            $rowdata[$fieldMap[$key]] = $approver->id;
                                        } else {
                                            $rowdata[$fieldMap[$key]] = $this->session->userdata('id');
                                            $msg = ' Approver Not Found';
                                            $warn = true;
                                        }
                                    } else
                                        $rowdata[$fieldMap[$key]] = $cv;

                                    if (!in_array($key, $excludelist))
                                        $rowStr .= "<td>" . $cv . $msg . "</td>";

                                } elseif (!in_array($key, $excludelist))
                                    $rowStr .= "<td></td>";
                            }
                        }


                        // SL
                        if (!is_null($fieldKey['SLEntitleHourAcc']) || !is_null($fieldKey['SLProRataHourAcc']) || !is_null($fieldKey['Acc-SL-Entitlement Hours']) || !is_null($fieldKey['Acc-SL-Pro-rata Hours'])) {
                            $totalSL = 0;

                            if (!is_null($fieldKey['SLEntitleHourAcc'])) {
                                $totalSL += $objWorksheet->getCellByColumnAndRow($fieldKey['SLEntitleHourAcc'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['SLProRataHourAcc'])) {
                                $totalSL += $objWorksheet->getCellByColumnAndRow($fieldKey['SLProRataHourAcc'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['Acc-SL-Entitlement Hours'])) {
                                $totalSL += $objWorksheet->getCellByColumnAndRow($fieldKey['Acc-SL-Entitlement Hours'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['Acc-SL-Pro-rata Hours'])) {
                                $totalSL += $objWorksheet->getCellByColumnAndRow($fieldKey['Acc-SL-Pro-rata Hours'], $row)->getValue();
                            }

                            $rowStr .= "<td>" . number_format((float)$totalSL, 4, '.', '') . "</td>";
                            $rowdata['personalbalance'] = number_format((float)$totalSL, 4, '.', '');

                        }

                        // AL
                        if (!is_null($fieldKey['ALEntitleHourAcc']) || !is_null($fieldKey['ALProRataHourAcc']) || !is_null($fieldKey['Acc-AL-Entitlement Hours']) || !is_null($fieldKey['Acc-AL-Pro-rata Hours'])) {
                            $totalAL = 0;

                            if (!is_null($fieldKey['ALEntitleHourAcc'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['ALEntitleHourAcc'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['ALProRataHourAcc'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['ALProRataHourAcc'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['Acc-AL-Entitlement Hours'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['Acc-AL-Entitlement Hours'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['Acc-AL-Pro-rata Hours'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['Acc-AL-Pro-rata Hours'], $row)->getValue();
                            }

                            $rowStr .= "<td>" . number_format((float)$totalAL, 4, '.', '') . "</td>";
                            $rowdata['leavebalance'] = number_format((float)$totalAL, 4, '.', '');
                        }

                        // LSL
                        if (!is_null($fieldKey['LSLEntitleHourAcc']) || !is_null($fieldKey['LSLProRataHourAcc']) || !is_null($fieldKey['Acc-LSL-Entitlement Hours']) || !is_null($fieldKey['Acc-LSL-Pro-rata Hours'])) {
                            $totalAL = 0;

                            if (!is_null($fieldKey['LSLEntitleHourAcc'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['LSLEntitleHourAcc'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['LSLProRataHourAcc'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['LSLProRataHourAcc'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['Acc-LSL-Entitlement Hours'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['Acc-LSL-Entitlement Hours'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['Acc-LSL-Pro-rata Hours'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['Acc-LSL-Pro-rata Hours'], $row)->getValue();
                            }

                            $rowStr .= "<td>" . number_format((float)$totalAL, 4, '.', '') . "</td>";
                            $rowdata['longservicebalance'] = number_format((float)$totalAL, 4, '.', '');
                        }


                        // RDO
                        if (!is_null($fieldKey['RDOEntitleHourAcc']) || !is_null($fieldKey['RDOProRataHourAcc']) || !is_null($fieldKey['Acc-RDO-Entitlement Hours']) || !is_null($fieldKey['Acc-RDO-Pro-rata Hours'])) {
                            $totalAL = 0;

                            if (!is_null($fieldKey['RDOEntitleHourAcc'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['RDOEntitleHourAcc'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['RDOProRataHourAcc'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['RDOProRataHourAcc'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['Acc-RDO-Entitlement Hours'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['Acc-RDO-Entitlement Hours'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['Acc-RDO-Pro-rata Hours'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['Acc-RDO-Pro-rata Hours'], $row)->getValue();
                            }

                            $rowStr .= "<td>" . number_format((float)$totalAL, 4, '.', '') . "</td>";
                            $rowdata['timeoffbalance'] = number_format((float)$totalAL, 4, '.', '');
                        }

                        // Flex-Time
                        if (!is_null($fieldKey['FlexHourAccrue'])) {
                            $totalAL = 0;
                            $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['FlexHourAccrue'], $row)->getValue();

                            $rowStr .= "<td>" . number_format((float)$totalAL, 2, '.', '') . "</td>";
                            $rowdata['flexbalance'] = number_format((float)$totalAL, 2, '.', '');
                        }

                        // Study
                        if (!is_null($fieldKey['STLEntitleHourAcc']) || !is_null($fieldKey['STLProRataHourAcc']) || !is_null($fieldKey['Acc-STL-Entitlement Hours']) || !is_null($fieldKey['Acc-STL-Pro-rata Hours'])) {
                            $totalAL = 0;

                            if (!is_null($fieldKey['STLEntitleHourAcc'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['STLEntitleHourAcc'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['STLProRataHourAcc'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['STLProRataHourAcc'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['Acc-STL-Entitlement Hours'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['Acc-STL-Entitlement Hours'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['Acc-STL-Pro-rata Hours'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['Acc-STL-Pro-rata Hours'], $row)->getValue();
                            }

                            $rowStr .= "<td>" . number_format((float)$totalAL, 4, '.', '') . "</td>";
                            $rowdata['studybalance'] = number_format((float)$totalAL, 4, '.', '');
                        }


                        // income category 1
                        if (!is_null($fieldKey['UDL1EntitleHourAcc']) || !is_null($fieldKey['UDL1ProRataHourAcc']) || !is_null($fieldKey['Acc-U1-Entitlement Hours']) || !is_null($fieldKey['Acc-U1-Pro-rata Hours'])) {
                            $totalAL = 0;

                            if (!is_null($fieldKey['UDL1EntitleHourAcc'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['UDL1EntitleHourAcc'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['UDL1ProRataHourAcc'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['UDL1ProRataHourAcc'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['Acc-U1-Entitlement Hours'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['Acc-U1-Entitlement Hours'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['Acc-U1-Pro-rata Hours'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['Acc-U1-Pro-rata Hours'], $row)->getValue();
                            }

                            $rowStr .= "<td>" . number_format((float)$totalAL, 4, '.', '') . "</td>";
                            $rowdata['otherbalance'] = number_format((float)$totalAL, 4, '.', '');
                        }

                        // income category 2
                        if (!is_null($fieldKey['UDL2EntitleHourAcc']) || !is_null($fieldKey['UDL2ProRataHourAcc']) || !is_null($fieldKey['Acc-U2-Entitlement Hours']) || !is_null($fieldKey['Acc-U2-Pro-rata Hours'])) {
                            $totalAL = 0;

                            if (!is_null($fieldKey['UDL2EntitleHourAcc'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['UDL2EntitleHourAcc'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['UDL2ProRataHourAcc'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['UDL2ProRataHourAcc'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['Acc-U2-Entitlement Hours'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['Acc-U2-Entitlement Hours'], $row)->getValue();
                            }
                            if (!is_null($fieldKey['Acc-U2-Pro-rata Hours'])) {
                                $totalAL += $objWorksheet->getCellByColumnAndRow($fieldKey['Acc-U2-Pro-rata Hours'], $row)->getValue();
                            }

                            $rowStr .= "<td>" . number_format((float)$totalAL, 4, '.', '') . "</td>";
                            $rowdata['otherbalance2'] = number_format((float)$totalAL, 4, '.', '');
                        }


                        $rowStr .= '</tr>';

                        if (($showupdate) && is_null($fieldKey['LeaveAccrueTo'])) {
                            $rowdata['leaveupdated'] = date("Y-m-d");
                        }
                        $rowdata['batch'] = $batchid;
                        $newuser = true;
                        //  check if the employeeid exists
                        if (array_key_exists('employeeid', $rowdata) || array_key_exists('email', $rowdata)) {
                            if (array_key_exists('employeeid', $rowdata)) {
                                $user = $this->user_model->getUserByEmpID(null, $rowdata['employeeid'], $company);
                                if ($user != false) {
                                    $user = $user->row();
                                    if ($user->status != 5) {
                                        $this->admin_model->updateUser($user->id, $rowdata);
                                        if ($warn)
                                            $rowStr = "<tr class='warning'><td>Updated</td>" . $rowStr;
                                        else
                                            $rowStr = "<tr class='info'><td>Updated</td>" . $rowStr;
                                        $newuser = false;
                                    }
                                }
                            } elseif (array_key_exists('email', $rowdata)) {
                                $user = $this->user_model->getUserByEmail($rowdata['email'], $company);
                                if ($user != false) {
                                    $user = $user->row();
                                    if ($user->status != 5) {
                                        $this->admin_model->updateUser($user->id, $rowdata);
                                        if ($warn)
                                            $rowStr = "<tr class='warning'><td>Updated</td>" . $rowStr;
                                        else
                                            $rowStr = "<tr class='success'><td>Updated</td>" . $rowStr;
                                        $newuser = false;
                                    }
                                }
                            }
                            if ($newuser) {   // else, insert new user.
                                $rowdata['company'] = $company;
                                if (!array_key_exists('country', $rowdata))
                                    $rowdata['country'] = $companyData->country;    // use company default
                                if (!array_key_exists('state', $rowdata))
                                    $rowdata['state'] = $companyData->state;      // use company default
                                if (!array_key_exists('defaultapprover', $rowdata))
                                    $rowdata['defaultapprover'] = $this->session->userdata('id');
                                $rowdata['status'] = 1;  // set status to pending. use pending to indicate no welcome email sent.
                                if (!isset($rowdata['usertype']))
                                    $rowdata['usertype'] = 4;
                                $id = null;
                                $newID = $this->admin_model->createUser($rowdata);
                                $rowStr = "<tr class='success'><td>CREATED</td>" . $rowStr;
                            }
                        } else {
                            $rowStr = '<tr class="danger"><td colspan="' . ($colcount + 1) . '">ERROR: No EmployeeID value in this row</td></tr>';
                        }
                        echo $rowStr;
                    }
                }
            }
            echo '</table>' . "\n";
            echo '</form>';
            echo '       </div>';
            echo '   </div>';
            echo ' </div>';

        }


    }


    public function getApprover()
    {
        $this->validate();
        $user = $this->input->post('user');
        echo $this->user_model->getUser($user)->row()->defaultapprover;

    }

    public function getHours()
    {
        $this->validate();
        $user = $this->input->post('user');
        echo $this->user_model->getUser($user)->row()->hours;

    }

    public function deleteHelp()
    {
        $this->validate();
        $id = $this->input->post('helpid');
        $msg = $this->admin_model->deleteHelp($id);
        echo $msg;
    }


    public function deletecountry()
    {
        $this->validate();
        $id = $this->input->post('country');
        $msg = $this->user_model->deletecountry($id);
        echo $msg;
    }


    public function deletecompany()
    {
        $this->validate();
        $id = $this->input->post('company');
        $msg = $this->user_model->deletecompany($id);
        echo $msg;
    }

    public function deleteHoliday()
    {
        $this->validate();
        $id = $this->input->post('holiday');
        $msg = $this->user_model->deleteHoliday($id);
        echo $msg;
    }

    public function updateHolidays()
    {
        $this->validate();
        $url = 'http://www.data.gov.au/datastore/odata3.0/31eec35e-1de6-4f04-9703-9be1d43d405b?$format=json';
        $result = file_get_contents($url);

        $jIndex = ['Applicable To', 'Holiday Name', 'Date'];
        $count = 0;
        $data = json_decode($result, true);
        echo '<table class="tbl-qa table table-bordered" style="margin: 0;padding: 0">';
        echo '<thead><tr><th>Applicable To</th><th>Holiday Name</th><th>Date</th></tr></thead><tbody>';
        foreach ($data as $key => $val) {
            if ($count > 0) {
                foreach ($val as $k => $v) {
                    echo "<tr>";
                    for ($i = 0; $i < count($jIndex); $i++) {
                        echo "<td>" . $v[$jIndex[$i]] . "</td>";
                    }
                    echo "</tr>";
                }
            }
            $count++;
        }
        echo '</tbody></table>';
    }


    public function getRegionsByCountry()
    {
        $this->validate();

        $country = $this->input->post('country');
        if (!is_null($country) && $country != '') {
            $addnational = $this->input->post('addnational');
            $regions = $this->user_model->getRegionsByCountry($country);
            if ($addnational == 'YES')
                echo "<option value='0' >National Holiday</option>";
            foreach ($regions->result() as $region) {
                echo "<option value='" . $region->id . "' >" . $region->name . "</option>";
            }
        }
    }

    public function sendWelcome()
    {
        $this->validate();
        $employee = $this->input->post('employee');
        $employeeData = $this->user_model->getUser($employee)->row();
        $this->user_model->updateUser($employee, array('status' => 2));
        $this->user_model->sendwelcome($employeeData->email, $employee);
        return "done";
    }

    public function userinfo($set = false)
    {
        $this->validate();
        $userid = $this->input->post('employee');
        $user = $this->user_model->getUser($userid)->row();
        $set = $this->input->post('set');
        if ($set) {
            $company = $this->user_model->getCompany($user->company);
            $data = array(
                'company' => $user->company,
                'companyname' => $company->name,
                'companyhours' => $company->hours,
                'companystatus' => $company->status,
                'companyapikey' => $company->apikey,
                'syncdataset' => $company->syncdataset,
                'id' => $user->id,
                'name' => $user->name,
                'username' => $user->name,
                'email' => $user->email,
                'employeeid' => $user->employeeid,
                'hours' => $user->hours,
                'location' => $user->location,
                'usertype' => $user->usertype,
                'defaultapprover' => $user->defaultapprover,
                'aosync' => false
            );
            $this->session->set_userdata($data);
            if ($this->session->userdata('syncdataset') && !is_null($this->session->userdata('companyapikey')) && $this->session->userdata('companyapikey') != "" && $this->session->userdata('usertype') <= '2') {
                $this->session->set_userdata('aosync', true);
            }
            $data['r1'] = $company->vip;
            $data['r2'] = str_replace('/', '', $user->password);
            echo json_encode($data);
        } else
            echo json_encode($user);
    }

    public function admininfo()
    {
        $this->validate();
        $userid = $this->input->post('admin');
        $user = $this->admin_model->getAdmins($userid)->row();
        echo json_encode($user);
    }


    public function countryinfo($internal = null)
    {
        $this->validate();
        if (!is_null($internal))
            $country = $internal;
        else
            $country = $this->input->post('country');

        $regions = $this->user_model->getRegionsByCountry($country);
        $return = "<ul>";

        if (is_object($regions)) {
            foreach ($regions->result() as $region)
                $return .= "<li style='width:100%'><button type='button' class='btn-xs btn btn-danger' onclick='remregion(" . $region->id . ");'><i class='fa fa-trash'></i></button>" . $region->name . "</li>";
        }
        $return .= "</ul>";
        echo $return;
    }

    public function restorebatch()
    {
        $this->validate();
        $id = $this->input->post('batchid');

        // change all leave items with batch # ... to be processed = 0;
        $count = $this->user_model->rollbackLeave($id);

        // change status of batch to
        $batchdata = array(
            'status' => '5',
        );
        $this->user_model->updateBatch($id, $batchdata);
        echo "Complete " . $count;
    }


    public function completebatch()
    {
        $this->validate();
        $id = $this->input->post('batchid');

        // change status of batch to
        $batchdata = array(
            'status' => '4',
        );
        $this->user_model->updateBatch($id, $batchdata);
        echo "Leave processing complete. Status changed to inactive";
    }

    public function restoreitem()
    {
        $this->validate();
        $id = $this->input->post('leaveid');

        // change status of batch to
        $leavedata = array(
            'processed' => false,
        );
        $this->user_model->updateleave($id, $leavedata);
        echo "Complete";
    }

    public function orderHelp()
    {
        $this->validate();
        $from = $this->input->post('from');
        $to = $this->input->post('to');
        $this->admin_model->reorderhelp($from, $to);
        echo "OK";
    }

    public function removeregion()
    {
        $this->validate();
        $country = $this->input->post('countryid');
        $region = $this->input->post('regionid');
        $this->user_model->removeRegion($region);
        $this->countryinfo($country);
    }


    public function createregion()
    {
        $this->validate();
        $country = $this->input->post('countryid');
        $data = array(
            'country' => $this->input->post('country'),
            'region' => $this->input->post('short'),
            'name' => $this->input->post('name')
        );
        $this->user_model->createRegion($data);
        $this->countryinfo($country);
    }

    public function getHolidays()
    {
        $this->validate();
        $countries = $this->input->post('countries');
        if (is_null($countries) || $countries == '') return '';

        $countries = explode(':', $countries);
        $first = true;
        $source = '';//'{ events: [';
        foreach ($countries as $countryid) {
            $holidays = $this->user_model->getHolidaysByCountry($countryid);

            foreach ($holidays->result() as $hol) {
                if ($first) {
                    $first = false;
                    $source .= '{';
                } else
                    $source .= ',{';
                $fd = explode("-", $hol->date);

                if ($hol->state == 'National Holiday') {
                    $source .= '  "title":"' . $hol->country . ' - ' . str_replace('"', '', $hol->description);
                    $source .= '",';
                } else {
                    $source .= '  "title":"' . $hol->state . ' - ' . str_replace('"', '', $hol->description);
                    $source .= '",';
                }

                $source .= '  "start": "' . $fd[0] . '-' . $fd[1] . '-' . $fd[2] . '",';
                $source .= '  "end": "' . $fd[0] . '-' . $fd[1] . '-' . $fd[2] . '",';
                $source .= '  "allDay": true';
//                $source .= '}';
//            }            
//        $source .= '],';
                $source .= ',';
                if ($hol->country == 'Australia') {
                    $source .= ' "color": "darkgreen",';
                    $source .= ' "textColor": "gold",';
                    $source .= ' "borderColor": "red"';
                } elseif ($hol->country == 'New Zealand') {
                    $source .= ' "color": "black",';
                    $source .= ' "textColor": "white",';
                    $source .= ' "borderColor": "red"';
                } elseif ($hol->country == 'Papua New Guinea') {
                    $source .= ' "color": "red",';
                    $source .= ' "textColor": "black",';
                    $source .= ' "borderColor": "red"';
                } else {
                    $source .= ' "color": "white",';
                    $source .= ' "textColor": "black",';
                    $source .= ' "borderColor": "red"';
                }
                $source .= '}';
            }


        }


//        $source .= '}';
        echo $source;
    }

    public function checkvip()
    {
        $this->validate();
        $vip = $this->input->post('vip');
        $results = $this->user_model->getCompanyByVIP($vip);
        if ($results->num_rows() > 0)
            echo 'This VIP is already in use. Please enter a unique VIP.';
        else
            echo 'OK';
    }

    private function clean($str = "")
    {
        $clean = str_replace("'", "", $str);
        return $clean;
    }

    public function gettemplate()
    {
        $this->validate();
        $id = $this->input->post('id');
        $result = $this->user_model->getTemplate($id);
        if ($result->num_rows() > 0) {
            $template = $result->row();
            echo json_encode($template);

        } else
            echo 'Error';

    }

    public function checkAway()
    {
        $this->validate();
        $id = $this->input->post('employee');
        $fromdate = $this->input->post('datef');
        $todate = $this->input->post('datet');
        $leavehours = $this->user_model->checkaway($id, $fromdate, $todate);
        echo $leavehours;
//        if($leavehours > 0)
//            echo "There is already a leave request for this day for ".$leavehours." hours.";
//        else
//            echo "";

    }



//
//
//         not currently used
//      was used for dropzone js
//
    public function addfile()
    {
        $this->validate();
        $location = 'Nope - Some error ocurred';
        $config['upload_path'] = FCPATH . 'uploads/' . $this->session->userdata('id') . "/";
        $config['allowed_types'] = 'csv|xls|xlsx|CSV|XLS|jgp|jpeg|gif|png|tiff|pdf|bmp';
        if (!is_dir($config['upload_path'])) {
            mkdir($config['upload_path'], 0777, TRUE);
        }

        $this->load->helper(array('form'));

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            echo print_r($error, true);
        } else {
            $upload = array('upload_data' => $this->upload->data());
            $attachment = $upload['upload_data']['file_name'];
            $location = "uploads/" . $this->session->userdata('id') . "/" . $attachment;
        }
        echo $location;
    }

    public function getReportFilter()
    {
        $this->validate();
        $filter = $this->input->post('filter');
        $str = '';
        switch ($filter) {

            case "user":
                $results = $this->user_model->getUsers();
                foreach ($results->result() as $user) {
                    $str .= '<option value="' . $user->id . '">' . $user->name . '</option>';
                }
                break;
            case "approvedby":
                $results = $this->user_model->getApprovers();
                foreach ($results->result() as $user) {
                    $str .= '<option value="' . $user->id . '">' . $user->name . '</option>';
                }
                break;
            case "leavetype":
                $results = $this->user_model->getLeaveTypes();
                foreach ($results->result() as $type) {
                    $str .= '<option value="' . $type->id . '">' . $type->description . '</option>';
                }
                break;
            case "status":
                $results = $this->user_model->getLeaveStatus();
                foreach ($results->result() as $status) {
                    $str .= '<option value="' . $status->id . '">' . $status->description . '</option>';
                }
                break;
        }

        echo $str;

    }

    public function getReportData()
    {
        $this->validate();
        $criteria = $this->input->post('criteria');
        $data = json_decode($criteria);
        $str = '';
        $arrayand = array();
        $arrayand[] = '(company = "' . $this->session->userdata('company') . '") ';

        foreach ($data as $key => $value) {
            $arrayor = array();
            foreach ($value as $itemid => $itemvalue) {
                $arrayor[] = 'leave.' . $key . ' = "' . $itemid . '"';
            }
            $stringor = '(' . implode($arrayor, ' or ') . ')';
            $arrayand[] = $stringor;
        }
        $str = implode($arrayand, ' and ');

        $results = $this->user_model->getLeaveWhere($str);
        ?>
        <table class="table table-striped projects" id="example">
            <thead>
            <tr>
                <th>Employee</th>
                <th>Employee Code</th>
                <th>Approved By</th>
                <th>Default Approver</th>
                <th>Request</th>
                <th></th>
                <th>Period</th>
                <th></th>
                <th>Status</th>
                <th>Last Actioned</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $format = 'D j M Y';

            foreach ($results->result() as $leave) {
                echo '<tr>';
                $requestor = $this->user_model->getUser($leave->user)->row();
                echo '<td>' . $requestor->name . '</td>';
                echo '<td>' . $requestor->employeeid . '</td>';
                echo '<td>';
                if (!is_null($leave->approvedby))
                    echo $this->user_model->getUser($leave->approvedby)->row()->name;
                echo '</td>';
                echo '<td>' . $this->user_model->getUser($requestor->defaultapprover)->row()->name . '</td>';

                ?>
                <td class=" " style="white-space:nowrap">
                    <?php

                    $todate = date_create($leave->todate);
                    $fromdate = date_create($leave->fromdate);
                    $approveddate = date_create($leave->approvedon);

                    if ($leave->hours > $requestor->hours)
                        echo $leave->days . " days / " . number_format($leave->hours, 2) . " hours ";
                    elseif ($leave->hours == $requestor->hours)
                        echo "1 day / " . number_format($leave->hours, 2) . " hours ";
                    elseif ($leave->hours < $requestor->hours)
                        echo number_format($leave->hours, 2) . " hours ";

                    echo "of " . $this->user_model->getLeaveType($leave->leavetype)->row()->description;;

                    if (!is_null($leave->attachment) && $leave->attachment != "") {
                        ?>
                        <br>
                        <button type="button" class="btn btn-primary btn-xs"
                                data-toggle="modal" data-target=".bs-world-modal"
                                onclick="document.getElementById('modalsrc').src ='<?php echo base_url() . $leave->attachment ?>'">
                            <i class="fa fa-file"></i> View Attachment
                        </button>
                        <?php
                    }

                    if (!is_null($leave->comments) && $leave->comments != "") {
                        ?>
                        <br>
                        <button type="button" class="btn btn-default btn-xs"
                                data-toggle="tooltip" data-placement="top" title
                                data-original-title="<?php echo $leave->comments ?>"
                                onclick="alert('<?php echo $leave->comments ?>')"><i
                                    class="fa fa-comment"></i> View Comments
                        </button>
                        <?php
                    } ?>

                </td>
                <td><?php echo $this->user_model->getLeaveType($leave->leavetype)->row()->description ?></td>
                <td class=" " style="white-space:nowrap">
                    <?php echo date_format($fromdate, $format) ?>
                    to <br><?php echo date_format($todate, $format) ?>
                </td>

                <td class=" ">
                    <?php echo date_format($fromdate, "U") ?>
                </td>
                <?php

                echo '<td>' . $this->user_model->getLeaveStatus($leave->status)->row()->description . '</td>';
                echo '<td>' . date_format($approveddate, $format) . '</td>';
                echo '<td>' . date_format($approveddate, "U") . '</td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
        <?php
    }


    public function getReportDataJSON()
    {
        $this->validate();
        $criteria = $this->input->post('criteria');
        $data = json_decode($criteria);
        $str = '';
        $arrayand = array();
        $arrayand[] = '(company = "' . $this->session->userdata('company') . '") ';

        foreach ($data as $key => $value) {
            $arrayor = array();
            foreach ($value as $itemid => $itemvalue) {
                $arrayor[] = 'leave.' . $key . ' = "' . $itemid . '"';
            }
            $stringor = '(' . implode($arrayor, ' or ') . ')';
            $arrayand[] = $stringor;
        }
        $str = implode($arrayand, ' and ');

        $results = $this->user_model->getLeaveWhere($str);
        $resp = "";
        $resp .= '"Employee","Employee Code","Approved By","Default Approver","Request","","Period","","Status","Last Actioned"';
        $resp .= '\r\n';
        $resp .= $resp;


        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $query = $this->db->query("SELECT * FROM Users");
        $delimiter = ",";
        $newline = "\r\n";
        $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        force_download('CSV_Report.csv', $data);

        $format = 'D j M Y';
        if (false){
                foreach ($results->result() as $leave) {
                    $requestor = $this->user_model->getUser($leave->user)->row();
                    $resp .= '' . $requestor->name . ',';
                    $resp .= '' . $requestor->employeeid . ',';
                    $resp .= '';
                    if (!is_null($leave->approvedby))
                        $resp .= $this->user_model->getUser($leave->approvedby)->row()->name;
                    $resp .= ',';
                    $resp .= '' . $this->user_model->getUser($requestor->defaultapprover)->row()->name . ',';

                    $todate = date_create($leave->todate);
                    $fromdate = date_create($leave->fromdate);
                    $approveddate = date_create($leave->approvedon);

                    if ($leave->hours > $requestor->hours)
                        $resp .= $leave->days . " days - " . number_format($leave->hours, 2) . " hours ";
                    elseif ($leave->hours == $requestor->hours)
                        $resp .= "1 day - " . number_format($leave->hours, 2) . " hours ";
                    elseif ($leave->hours < $requestor->hours)
                        $resp .= number_format($leave->hours, 2) . " hours ";

                    $resp .= "of " . $this->user_model->getLeaveType($leave->leavetype)->row()->description . ",";

                    $resp .= $this->user_model->getLeaveType($leave->leavetype)->row()->description . ',';
                    $resp .= date_format($fromdate, $format) . ' to ' . date_format($todate, $format) . ',';
                    $resp .= date_format($fromdate, "U") . ',';
                    $resp .= '' . $this->user_model->getLeaveStatus($leave->status)->row()->description . ',';
                    $resp .= '' . date_format($approveddate, $format) . ',';
                    $resp .= '' . date_format($approveddate, "U") . ',';
                    $resp .= '\r\n';
                }
        }
        echo $resp;
    }


    public function getleavecount()
    {
        $this->validate();
        $id = $this->input->post('company');
        $count = $this->admin_model->getLeaveCount($id);
        echo $count;
    }

    public function allocateSpot(){
        $day = $this->input->post('date');
        $space= $this->input->post('carpark');
        $result = $this->user_model->allocateSpace($day, $space);
        echo $result;
    }

    public function manuallyAllocateSpot(){
        $day = $this->input->post('date');
        $space= $this->input->post('carpark');
    	$user= $this->input->post('staff');
        $result = $this->user_model->allocateSpace($day, $space, $user);
        echo $result;
    }

    public function releaseSpot(){
        $day = $this->input->post('date');
        $result = $this->user_model->releaseSpace($day);
        echo $result;
    }

    public function goOnWaitingList(){
        $day = $this->input->post('date');
        $result = $this->user_model->goOnWaitingList($day);
        echo $result;
    }

	public function removeFromWaitingList(){
		$day = $this->input->post('date');
    	$result = $this->user_model->removeFromWaitingList($day);
    	echo $result;
	}

    public function autoAllocateSpot(){
        $day = $this->input->post('date');
        $space= $this->input->post('carpark');
        $result = $this->user_model->autoAllocateSpace($day, $space);
        echo $result;
    }

    public function assignSpot(){
        $staff = $this->input->post('user');
        $space = $this->input->post('space');
        $result = $this->user_model->permAllocateSpace($staff, $space);
        echo $result;
    }

	public function addUserToRotation(){
    	$result = $this->user_model->addUserToRotation();
    	echo $result;
    }

	public function removeFromRotation(){
    	$result = $this->user_model->removeFromRotation();
    	echo $result;
    }

    public function reinstate(){
        $employee = $this->input->post('employee');
        $data = array(
            'status' => 1
        );
        $result = $this->user_model->updateUser($employee, $data);
        echo $result;
    }


}



