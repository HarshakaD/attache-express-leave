<?php
include "header.php";
$user = $this->user_model->getUser($this->session->userdata('id'))->row();
$edit = false;
if (isset($leaveid) && $leaveid != "") {
    $edit = true;
    $leave = $this->user_model->getLeave($leaveid)->row();

}
$today = date_format(date_create(), 'd/m/Y');

?>


<body class="nav-md">
<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Request Leave
                        <small>(<?php echo $this->session->userdata('name'); ?>)</small>
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>
                <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">X</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">View Attachment</h4>
                            </div>
                            <div class="modal-body ">
                                <iframe id="modalsrc" src="" width="100%"></iframe>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row">
                <!-- form date pickers -->
                <div class="col-md-9 col-xs-12">
                    <div class="x_panel" style="">
                        <!-- div class="x_title">
                            < h2>
                                <small>Available Annual Leave:</small> <?php echo $user->leavebalance ?> hours (<?php echo number_format($user->leavebalance / $user->hours, "1") ?> days) <small> as at: </small><?php echo date_format(date_create($user->leaveupdated), "d/m/Y") ?>
                            </h2>
                        </div -->
                        <div class="x_content">

                            <form class="form-horizontal form-label-left" novalidate=""
                                  action="<?php echo base_url() ?>user/addleave" method="post"
                                  enctype="multipart/form-data">
                                <div id="leaverow">
                                    <?php
                                    $message = $this->session->userdata('message');
                                    $this->session->set_userdata(array('message' => ""));
                                    echo '<fieldset>
                                                                    <div class="control-group col-sm-offset-3">
                                                                        <div class="controls"><h2 id="error_message" class="">' . $message . '</h2></div>
                                                                        <div class="controls"><h2 id="error_message1" class="">' . $message . '</h2></div>
                                                                    </div>
                                                              </fieldset>';
                                    //if(isset($message) && $message !="") echo "<div class='error_message'>".$message."</div>"
                                    ?>
                                    <input id="leaveid" name="leaveid" type="hidden" value="">
                                    <input name="user" type="hidden" value="<?php echo $user->id ?>">

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            Leave Period <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <fieldset>
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <div class="input-prepend input-group">
                                                            <span class="add-on input-group-addon"><i
                                                                    class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                            <input type="text" name="reservation" id="reservation"
                                                                   class="form-control active"
                                                                   placeholder="dd/mm/yyyy to dd/mm/yyyy"
                                                                   required="required"
                                                                   value="<?php echo $today . ' to ' . $today ?>">
                                                        </div>
                                                        <input type="hidden" name="fromdate"
                                                               value="<?php echo $today ?>" id="fromdate">
                                                        <input type="hidden" name="todate" value="<?php echo $today ?>"
                                                               id="todate">
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            Leave Type <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <?php $leaveTypes = $this->user_model->getLeaveTypes();
                                            if (isset($leaveTypes)) {
                                                ?>
                                                <select class="form-control" name="type" id="type" required="required">
                                                    <option value="" selected> Select Leave Type</option>
                                                    <?php
                                                    foreach ($leaveTypes->result() as $type) {
                                                        ?>
                                                        <option value="<?php echo $type->id ?>"
                                                                tag="<?php echo $type->notes ?>"><?php echo $type->description ?></option>

                                                    <?php } ?>
                                                </select>
                                            <?php } ?>
                                            <br>
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            Number of Work Days
                                        </label>
                                        <div class="item form-group col-sm-2 col-xs-6">
                                            <input id="duration" class="form-control col-xs-12" disabled
                                                   data-validate-length-range="1" value="" required="required"
                                                   type="number">
                                            <input id="durationd" type="hidden" name="duration" value="">
                                        </div>

                                        <div class="item form-group col-sm-7 col-xs-6">
                                        </div>
                                    </div>


                                    <div class="item form-group" id="fullday">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            Number of Hours
                                        </label>
                                        <div class="item form-group  col-md-6 col-sm-6 col-xs-12">
                                            <input id="hours" class="form-control col-xs-12"
                                                   data-validate-length-range="1" value="" required="required"
                                                   type="number" disabled>
                                            <input id="hoursh" type="hidden" name="hours" value="">
                                        </div>
                                    </div>

                                    <div class="item form-group" id="halfday" style="display: none">
                                        <label class="control-label col-sm-3 col-xs-6">
                                            Number of Hours
                                        </label>
                                        <div class="item form-group col-sm-2 col-xs-6">
                                            <input type="number" id="onedayhours" value="<?php // echo $user->hours ?>" class="form-control col-xs-12"
                                                   name="onedayhours" >
                                        </div>

                                        <div class="item form-group col-sm-7 col-xs-6">
                                        </div>


                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            Approver <span class="required">*</span>
                                        </label>
                                        <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                            <?php $approvers = $this->user_model->getApprovers();
                                            if (isset($approvers)) {
                                                ?>
                                                <select class="form-control" name="approver" id="approver">
                                                    <?php
                                                    foreach ($approvers->result() as $approver) {
                                                        if ($approver->id != $user->id) {
                                                            ?>
                                                            <option
                                                                value="<?php echo $approver->id ?>" <?php if ($approver->id == $user->defaultapprover) echo 'selected' ?>><?php echo $approver->name ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <?php
                                            } ?>
                                        </div>
                                    </div>


                                    <?php
                                    $changeattachment = false;
                                    if ($edit && isset($leave) && !is_null($leave->attachment) && $leave->attachment != '') {
                                        $changeattachment = true; ?>
                                        <div class="item form-group" >
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                Current Attachment
                                            </label>
                                            <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                                <button type="button" class="btn-link btn-primary btn-xs"
                                                        data-toggle="modal" data-target=".bs-world-modal"
                                                        onclick="document.getElementById('modalsrc').src ='<?php echo base_url() . $leave->attachment ?>'">
                                                    <i class="fa fa-file"></i> <?php echo $leave->attachment ?>
                                                </button>
                                            </div>
                                        </div>

                                    <?php }
                                    else {
                                        ?>
                                        <?php
                                    }
                                    ?>

                                    <div class="item form-group" >
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            <?php if ($changeattachment) echo 'Change ' ?>Attachment if required
                                        </label>
                                        <div class="item form-group col-md-6 col-sm-6 col-xs-12" >
                                            <input type="file" class="form-control" id="attachment" name="userfile"
                                                   style="border: 1px solid #DDE2E8; line-height: 0px"/>
                                        </div>
                                    </div>


                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            Comments
                                        </label>
                                        <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                            <textarea id="message" class="form-control" name="comments"><?php if($edit) echo $leave->comments ?></textarea>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-xs-12 col-md-offset-3">
                                            <P></P>
                                            <button id="send" type="submit" class="btn btn-primary">Submit</button>
                                            <a class="btn btn-default" href="<?php echo base_url()?>">Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
                <!-- /form datepicker -->
            </div>


        </div>

    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>
<script src="<?php echo base_url() ?>js/validator/validator.js"></script>


<!-- datepicker -->
<script type="text/javascript">

    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
        .on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

    // bind the validation to the form submit event


    $('form').submit(function (e) {
//            e.preventDefault();
        var submit = true;
        // evaluate the form using generic validating
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            return true;
        //this.submit();
        return false;
    });


    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);




    $(document).ready(function () {

        <?php
        if ($edit){
        $fromdate = date_format(date_create($leave->fromdate), "d/m/Y");
        $todate = date_format(date_create($leave->todate), "d/m/Y");
        ?>
        $("#leaveid").val('<?php echo $leave->id  ?>');
        $("#fromdate").val('<?php echo $fromdate ?>');
        $("#todate").val('<?php echo $todate ?>');
        $("#reservation").val('<?php echo $fromdate . ' to ' . $todate ?>');
        $("#approver").val('<?php echo $leave->approver  ?>');
        $("#type").val('<?php echo $leave->leavetype  ?>');
        $("#duration").val('<?php echo $leave->days  ?>');
        $("#durationd").val('<?php echo $leave->days  ?>');
        $("#hours").val('<?php echo $leave->hours  ?>');
        $("#hoursh").val('<?php echo $leave->hours  ?>');
        <?php
        }
        ?>

        var dateoptions = {
            separator: ' to ',
            startDate: '<?php if ($edit) echo $fromdate; else echo date_format(date_create(), "d/m/Y")?>',
            endDate: '<?php if ($edit) echo $todate; else echo date_format(date_create(), "d/m/Y")?>',
            dateLimit: {
                days: 60
            },
            showDropdowns: false,
            showWeekNumbers: true,
            autoApply: true,
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'DD/MM/YYYY'
        };


        applydates();
        <?php
        if ($edit){
        $fromdate = date_format(date_create($leave->fromdate), "d/m/Y");
        $todate = date_format(date_create($leave->todate), "d/m/Y");
        ?>
        $("#duration").val('<?php echo $leave->days  ?>');
        $("#durationd").val('<?php echo $leave->days  ?>');
        $("#hours").val('<?php echo $leave->hours  ?>');
        $("#onedayhours").val('<?php echo $leave->hours  ?>');
        $("#hoursh").val('<?php echo $leave->hours  ?>');
        <?php
        /*
                $("#half").prop('checked', <?php if ($leave->hours == $user->hours / 2) echo "true"; else echo "false";  ?>);

        */
        }
        ?>


        $('#reservation').daterangepicker(dateoptions, applydates);
        $('#reservation').on('apply.daterangepicker', applydates);


        var cb = function (start, end, label) {
            var drange;

            drange = document.getElementById('reservation').value.split(" to ");

            document.getElementById('fromdate').value = start;// drange[0];
            document.getElementById('todate').value = end;// drange[1];
            $.ajax({
                url: "<?php echo base_url()?>ajax/getWorkingDays",
                data: {
                    "employee": '<?php echo $user->id ?>',
                    "datef": start,
                    "datet": end
                },
                dataType: "html",
                type: "post",
                success: function (data) {
                    document.getElementById('duration').value = data;
                    document.getElementById('durationd').value = data;
                    if (data == 1) {
                        document.getElementById('halfday').style.display = 'block';
                        document.getElementById('fullday').style.display = 'none';
                    }
                    else {
                        document.getElementById('halfday').style.display = 'none';
                        document.getElementById('fullday').style.display = 'block';
                    }
                    document.getElementById('hours').value = Math.round(data *<?php echo $user->hours ?>* 100) / 100;
                    document.getElementById('hoursh').value = Math.round(data *<?php echo $user->hours ?>* 100) / 100;
                }
            });

        }

        var optionSet1 = {
//            startDate: moment().add(1, 'days'),
//            endDate: moment().add(8, 'days'),
            dateLimit: {
                days: 60
            },
            showDropdowns: false,
            showWeekNumbers: true,
            autoApply: true,

//            ranges: {
//                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
//                'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
//                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
//                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
//                'This Month': [moment().startOf('month'), moment().endOf('month')],
//                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
//            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'DD/MM/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Save',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To'
//                customRangeLabel: 'Custom',
//                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
//                monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
//                firstDay: 1
            }
        };
        $('#single_cal4').val(moment().add(1, 'days').format('DD/MM/YYYY') + ' to ' + moment().format('DD/MM/YYYY'));
        $('#single_cal4').daterangepicker(optionSet1, cb);
        $('#single_cal4').on('show.daterangepicker', function () {
            console.log("show event fired");
        });
        $('#single_cal4').on('apply.daterangepicker', function () {
            console.log("apply event fired");
        });
        $('#single_cal4').on('hide.daterangepicker', cb);

        $('#single_cal4').on('cancel.daterangepicker', function (ev, picker) {

        });
        $('#options1').click(function () {
            $('#single_cal4').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#single_cal4').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });
    });

    ///////////////////////////////////////////////

</script>
<!-- datepicker -->

<!-- /datepicker -->

<!-- /input mask -->
<!-- Datatables -->
<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>

<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>

<script>
    $(document).ready(function () {
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });


    });


    function setdata(id, fromdate, todate, duration, type, approver, hours, half) {
        document.getElementById('leaveid').value = id;
        document.getElementById('fromdate').value = fromdate;
        document.getElementById('todate').value = todate;
        document.getElementById('single_cal4').value = fromdate + " to " + todate;
        document.getElementById('duration').value = duration;
        document.getElementById('durationd').value = duration;
        document.getElementById('hours').value = hours;
        document.getElementById('hoursh').value = hours;
        document.getElementById('type').value = type;
        document.getElementById('approver').value = approver;
        document.getElementById('attachment').value = '';
        if (duration == 1) {
            document.getElementById('halfday').style.display = 'block';
            document.getElementById('fullday').style.display = 'none';
            document.getElementById('half').checked = half;
        }
        else {
            document.getElementById('halfday').style.display = 'none';
            document.getElementById('fullday').style.display = 'block';
        }
        return true;
    }

    $('.modal').on('show.bs.modal', function () {
        $('.modal-body').css('height', $(window).height() * 0.75);
        $('#modalsrc').css('height', $(window).height() * 0.75);

    });


    $('#type').change(function () {
        var notes = this.options[this.selectedIndex].getAttribute('Tag');
        if (notes != '')
            new PNotify({title: 'Leave Policy', text: notes, type: 'warning'});

    });

    var avaiable = <?php echo $user->hours ?>;  // not true for 7 day leave.

    $('#onedayhours').on('input', checkhours);


    function checkhours(){
        var input = $('#onedayhours').val();
        if (input < 0) $('#onedayhours').val(0);
        if (input > avaiable) $('#onedayhours').val(avaiable);

    }


    function applydates(start, end, label) {
        //console.log(start.toISOString(), end.toISOString(), label);
        var drange;
        drange = document.getElementById('reservation').value.split(" to ");
        start = drange[0];
        end = drange[1];
        document.getElementById('fromdate').value = start;
        document.getElementById('todate').value = end;
        $.ajax({
            url: "<?php echo base_url()?>ajax/getWorkingDays",
            data: {
                "employee": '<?php echo $user->id ?>',
                "datef": start,
                "datet": end
            },
            dataType: "html",
            type: "post",
            success: function (data) {
                var vals = jQuery.parseJSON(data);
                document.getElementById('duration').value = vals['days'];
                document.getElementById('durationd').value = vals['days'];
                if (vals['days'] == 1) {
                    avaiable = vals['hours'];
                    document.getElementById('onedayhours').value = vals['hours'];
                    document.getElementById('halfday').style.display = 'block';
                    document.getElementById('fullday').style.display = 'none';
                }
                else {
                    document.getElementById('halfday').style.display = 'none';
                    document.getElementById('fullday').style.display = 'block';
                }
                document.getElementById('hours').value = vals['hours'];
                document.getElementById('hoursh').value = vals['hours'];
            }
        });


<?php if(!$edit) { ?>
            $.ajax({
                url: "<?php echo base_url()?>ajax/checkAway",
                data: {
                    "employee": '<?php echo $user->id ?>',
                    "datef": start,
                    "datet": end
                },
                dataType: "html",
                type: "post",
                success: function (data) {
                    if (data > 0) {
                        var msg = "Leave of "+data+" hours already exists ";
                        if(start == end)
                            msg += "for this day.";
                        else
                            msg += "during this period.";
                        $('#error_message1').html(msg);
//                        avaiable = <?php //echo $user->hours ?>// -data;
//                        if (avaiable <= 0) {
//                            $('#error_message1').html("Leave exisis for this entire day.");
//                            $('#onedayhours').val(0);
//                        }
//                        else {
//                            $('#error_message1').html("Leave exisis for this day. <br>" + avaiable + " hours can still be requested.");
//                            $('#onedayhours').val(avaiable);
//                        }
                        $("#send").prop('disabled', true);   //Disable input
                    }
                    else {
                        $('#error_message1').html("");
                        $("#send").prop('disabled', false);   //Disable input
                    }

                }
            });
<?php } ?>

    }
    ;


</script>

<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>
</html>
