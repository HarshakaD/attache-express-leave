<?php
    header('Content-Type: application/download');
    header('Content-Disposition: attachment; filename="PAYTSHT.INP"');
//    header('Content-type: text/plain');


    $leavetypes = $this->user_model->getLeaveTypes();
//    $leaveresults = $this->user_model->getLeaveByCompany();
//    $hours = $this->session->userdata('hours');

    $format = 'd/m/Y';
    $datefilter = false;
    if (!is_null($this->input->post('todate'))){

        if (strpos($this->input->post('todate'), '/') > 1 ){
            $datefilter = true;

            $td = explode('/',$this->input->post('todate'));
            $tofilterd = $td[2]."-".$td[1]."-".$td[0];
            $tofilter = strtotime($tofilterd);

            $NewDate = new DateTime($tofilterd);
            $NewDate->add(new DateInterval('P1D'));
        }
    }


    $leaveresults = $this->user_model->getApprovedLeaveByCompany();
    $i = 1;

    if (isset($leaveresults)){
        foreach ($leaveresults->result() as $leave) {
            $show = false;
            $createChild = false;

            $todate = strtotime($leave->todate);
            $fromdate = strtotime($leave->fromdate);
            $days =  $leave->days;

            if($datefilter){
                $show = false;
                if(!$leave->processed && $fromdate <= $tofilter){
                    if($leave->status > 2){
                        $this->user_model->updateleave($leave->id, array('processed' => TRUE));
                    }
                    else{
                        $show = true;
                    }

                    if($todate > $tofilter){
                        $todate = $tofilter;
                        // split
                        $createChild = true;
                        $child = array(
                            "user" => $leave->user,
                            "approver" => $leave->approver,
                            "fromdate" => $NewDate->format('Y-m-d'),
                            "todate" => $leave->todate,
                            "created" => $leave->created,
                            "approvedby" => $leave->approvedby,
                            "approvedon" => $leave->approvedon,
                            "status" => $leave->status,
                            "attachment" => $leave->attachment,
                            "leavetype" => $leave->leavetype,
                            "comments" => $leave->comments,
                            "parent" => $leave->id
                        );
                    }

                }

            }
            if ($show){
                $days = $leave->days;
                $user = $this->user_model->getUser($leave->user)->row();
                if($days > 1){
                    $days = $this->user_model->getWorkingDays($fromdate, $todate, $leave->user);
                    $hours = $days*$user->hours;
                }
                else
                    $hours = $leave->hours;
                $leavecode = $this->user_model->getLeaveType($leave->leavetype)->row()->value;

                echo "T6,".$user->location.",".$user->employeeid.",,N,".$leavecode.",".$hours.",,,,".date($format, $fromdate).",".date($format, $todate).",,,,,,,,,,,,\r\n";

                $this->user_model->updateleave($leave->id, array('processed' => TRUE, 'dateprocessed' => date('Y-m-d', time())));
                // update leave rocrd to process = true;
                if ($createChild){
                    $child["days"] = $leave->days - $days;
                    $child["hours"] = $leave->hours - $hours;
                    $this->user_model->addLeave($child);
                    $this->user_model->updateleave($leave->id, array('todate' => $tofilterd, 'days' => $days, 'hours'=> $hours));
                }
                $createChild = false;

            }

        }
    }
?>





