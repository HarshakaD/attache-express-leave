<?php
include "header.php";

if(!is_null($vip) || $vip = ""){
    $new = false;
    $vip = urldecode($vip);
    $company = $this->admin_model->getCompanyByVIP($vip);
}
else
    $new = true;

?>

<body class="nav-md" xmlns="http://www.w3.org/1999/html">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Company Details</h3>
                    </div>
                    <div class="modal fade bs-user-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">User Management</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>user/createuser" method="post">

                                        <div class="item form-group">
                                            <input type="hidden" name="company" id="company" value="<?php if(!$new) echo $company->id ?>">
                                            <input type="hidden" name="id" id="id" value="">
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="name" class="form-control has-feedback-left"
                                                       id="name" placeholder="" required="required">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>


                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empid">Employee
                                                ID
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="empid" class="form-control has-feedback-left"
                                                       id="empid" placeholder="">
                                                <span class="fa fa-info-circle form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="location">Location
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="location" class="form-control has-feedback-left" id="location" value="">
                                                <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>


                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">Country <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="country" id="usercountry"
                                                        class="form-control has-feedback-left" required>
                                                    <option value="" selected="selected">Select Country</option>
                                                    <!--                                                        <optgroup label="Select Country">-->
                                                    <?php $countries = $this->user_model->getCountry();
                                                    foreach ($countries->result() as $country){
                                                        echo "<option value='".$country->id."' ";
                                                        if (!$new && $country->id == $company->country){
                                                            echo "selected";
                                                        }
                                                        echo ">".$country->name."</option>";
                                                    }
                                                    ?>
                                                </select>
                                                <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>

                                            </div>
                                        </div>


                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">State/Region <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select id="userstate" name="state"
                                                        class="form-control has-feedback-left" required disabled>
                                                    <option selected>Select Country First</option>
                                                </select>
                                                <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>

                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hours">Hours (standard day) <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="number" name="hours" class="form-control has-feedback-left"
                                                       id="hours" value="" required>
                                                <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="approver">Default Approver</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <?php
                                                ?>
                                                <select class="form-control has-feedback-left" name="approver" id="approver">
                                                    <optgroup label="Approvers">
                                                        <?php
                                                        if (!$new){
                                                            $approvers = $this->user_model->getApprovers($company->id);
                                                            if (isset($approvers)){
                                                                foreach($approvers->result() as $approver) {
                                                                    ?>
                                                                    <option value="<?php    echo $approver->id ?>"
                                                                        <?php //if($approver->id == $user->defaultapprover) echo 'selected' ?>
                                                                    ><?php echo $approver->name ?>
                                                                    </option>
                                                                <?php }
                                                            }
                                                        }
                                                        ?>
                                                    </optgroup>
                                                </select>
                                                <span class="fa fa-check form-control-feedback left" aria-hidden="true"></span>

                                                <?php
                                                ?>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="email" id="email" name="email" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
                                                <span class="fa fa-at form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                            <div id="invalid" style="display: none"><i class="fa fa-ban"></i> Already Registered</div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="confirm email">Confirm Email <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="email" id="email2" name="confirm_email" data-validate-linked="email" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
                                                <span class="fa fa-at form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usertype">User Type <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="usertype" id="usertype"
                                                        class="form-control has-feedback-left" required>
                                                    <?php $types = $this->user_model->getUserType();
                                                    foreach ($types->result() as $type)
                                                        echo "<option value=".$type->id.">".$type->description."</option>";
                                                    ?>
                                                </select>
                                                <span class="fa fa-suitcase form-control-feedback left" aria-hidden="true"></span>

                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                &nbsp;
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                   for="buttons"></label>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <button id="save" type="submit" class="btn btn-primary col-xs-5"> Save
                                                </button>
                                                <button type="button" class="btn btn-default col-xs-5"
                                                        style="float:right" data-dismiss="modal"> Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row top_tiles" >
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-info-circle"></i>
                            </div>
                            <div class="count"><?php if (!$new) echo $company->vip; else echo "-" ?></div>
                            <h3>VIP</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-users"></i>
                            </div>
                            <div class="count"><?php if (!$new) echo $this->admin_model->getUserCount($company->id); else echo "0" ?></div>

                            <h3>Employees</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-users"></i>
                            </div>
                            <div class="count"><?php if (!$new) echo $this->admin_model->getLeaveCount($company->id); else echo "0" ?></div>

                            <h3>Leave Requests</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-check-square-o"></i>
                            </div>
                            <div class="count"><?php if (!$new) echo $this->admin_model->getStatus($company->status); else echo "-" ?></div>

                            <h3>Account Status</h3>
                        </div>
                    </div>

                </div>

                <?php if (!$new) { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div >
                                <div class="panel">
                                    <div class="panel-body">
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="selectcompany">Select Company</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="form-control has-feedback-left" name="selectcompany" id="selectcompany">
                                                    <option value="" selected>Select Company</option>
                                                    <?php

                                                    $deleted = false;
                                                    if ($this->session->userdata('role') <= 2)
                                                        $deleted = true;
                                                    $companies = $this->admin_model->getCompanies($deleted);

                                                    foreach($companies->result() as $c){
                                                        ?>
                                                        <option
                                                            value="<?php echo rawurlencode($c->vip) ?>" <?php if ($c->id == $company->id) echo "selected" ?>><?php echo $c->name ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <span class="fa fa-check form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">

                        <div >

                            <div class="panel">


                                <div class="panel-body">
                                    <p>

                                    <form class="form-horizontal form-label-left" novalidate=""
                                          action="<?php if($new) echo base_url().'admin/createcompany'; else echo base_url().'admin/editcompany'?>" method="post">

                                        <input type="hidden" name="company" id="company"  value="<?php  if (!$new) echo $company->id?>">

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Company Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" name="name" value="<?php if (!$new) echo $company->name ?>" required="required" type="text">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vip">VIP <span class="required">*</span>
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <input type="text" id="vip" name="vip" required="required" placeholder="" class="form-control col-md-7 col-xs-12" value="<?php if (!$new) echo $company->vip?>" >
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status</span>
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <select class="form-control" name="status">
                                                    <?php
                                                    // for each team in get top teams

                                                    $statustypes = $this->user_model->getStatusTypes();
                                                    if (isset($statustypes))
                                                        foreach($statustypes->result() as $type) {

                                                            ?>
                                                            <option value="<?php    echo $type->id ?>" <?php
                                                            if (!$new){
                                                                if( $type->id == $company->status) echo 'selected';
                                                            }  ?>><?php echo $type->description ?>
                                                            </option>
                                                        <?php }?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vip">Timesheets<span class="required"></span>
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <select class="form-control" name="timesheets">
                                                    <option value="0" >Timesheets Disabled</option>
                                                    <option value="1" <?php if (!$new){
                                                        if( $company->timesheets == 1) echo 'selected';
                                                    }  ?>>Timesheets Enabled</option>
                                                </select>
                                            </div>
                                        </div>


                                        <?php
                                        if ($this->session->userdata('isadmin') && $this->session->userdata('role') <= '2') {
                                            ?>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vip">Work Week (for leave)<span class="required"></span>
                                                </label>
                                                <div class="col-md-4 col-sm-6 col-xs-12">
                                                    <select class="form-control" name="sevendayleave">
                                                        <option value="0" >5 Days (Mon to Fri)</option>
                                                        <option value="1" <?php if (!$new){
                                                            if( $company->sevendayleave == 1) echo 'selected';
                                                        }  ?>>7 Days (Mon to Sun)</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <?php
                                        }
                                        if ($this->session->userdata('isadmin') && $this->session->userdata('role') <= '1') {

                                            ?>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vip">Expense Claims<span class="required"></span>
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <select class="form-control" name="expenses">
                                                    <option value="0" >Expense Claims Disabled</option>
                                                    <option value="1" <?php if (!$new){
                                                        if( $company->expenses == 1) echo 'selected';
                                                    }  ?>>Expense Claims Enabled</option>
                                                </select>
                                            </div>
                                        </div>


                                        <?php
                                            $billingoptions = $this->user_model->getBillingstatus();
                                            ?>
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vip">Billing<span class="required"></span>
                                                </label>
                                                <div class="col-md-4 col-sm-6 col-xs-12">
                                                    <select class="form-control" name="billingstatus">
                                                        <?php
                                                        foreach ($billingoptions->result() as $option){
                                                            echo '<option value="'.$option->id.'"';
                                                            if(!$new && $company->billingstatus == $option->id) echo ' selected ';
                                                            echo '>'.$option->description.'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>


                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="note">Note:
                                            </label>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                Note: The following values will be used as defaults when creating new employees
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">Country <span class="required">*</span>
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <select name="country" id="country" class="form-control" required>
                                                    <option value="" selected>Select Country</option>
                                                    <?php $countries = $this->user_model->getCountry();
                                                    foreach ($countries->result() as $country){
                                                        echo "<option value='".$country->id."' ";
                                                        if (!$new && $country->id == $company->country){
                                                            echo "selected";
                                                        }
                                                        echo ">".$country->name."</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>


                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">Region <span class="required">*</span>
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <select id="state" name="state" class="form-control" required disabled>
                                                </select>
                                            </div>
                                        </div>



                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hours">Hours in a standard day <span class="required">*</span>
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <input type="number" id="hours" name="hours" required="" placeholder="" class="form-control col-md-7 col-xs-12" value="<?php if (!$new) echo $company->hours ?>" >
                                            </div>
                                        </div>


                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hours">Sync via odata API ?<span class="required"></span>
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <select class="form-control" name="syncdataset">
                                                    <option value="0" selected>Disabled</option>
                                                    <option value="1" <?php if ($company->syncdataset) echo 'selected'?>>Enabled</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3 col-xs-12">
                                                Currently set to:<?php if ($company->syncdataset) echo $company->apikey ?>
                                            </div>

                                        </div>

                                        <div class="item form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                &nbsp;
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-3">
                                                <button id="send" type="submit" class="btn btn-primary"><i class="fa fa-building"></i> <?php if (!$new) echo "Update Company Details"; else echo "Create Company"?></button>
                                            <?php if (!$new &&  $this->session->userdata('role') <= '1') {   // for admins only
                                                ?>
                                                    <button id="delete" name="delete" type="button" class="btn btn-danger"><i class="fa fa-trash"></i> Delete </button>
                                            <?php }
                                            ?>
                                            </div>

                                        </div>

                                        <div class="item form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                &nbsp;
                                            </div>
                                        </div>

                                    </form>
                                    <!-- start project list -->

                                    <!-- end project list -->

                                </div>

                            </div>
                            <?php if (!$new){ ?>
                                <div class="panel">

                                    <div >
                                        <div class="panel-body">
                                            <button onclick="document.getElementById('company').value='<?php echo $company->id?>';" type="button" class="btn btn-success newuser" data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-user"></i> Create New User </button>
                                            <p>

                                                <!-- start project list -->
                                            <table class="table table-striped projects" id="example">
                                                <thead>
                                                <tr>
                                                    <th style="width: 20%">Name</th>
                                                    <th>email</th>
                                                    <th>Location</th>
                                                    <th>State/Region</th>
                                                    <th>Permission Level</th>
                                                    <th>Default Approver</th>
                                                    <th>Standard Hours</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php
                                                // for each team in get top teams
                                                $staff = $this->user_model->getAllUsers($company->id);
                                                if (isset($staff))
                                                    foreach($staff->result() as $employee) {
                                                        ?>

                                                        <tr id="row<?php echo $employee->id ?>">
                                                            <td>
                                                                <a><?php echo $employee->name ?></a>
                                                                <br />
                                                                <small>EmployeeID: <?php echo $employee->employeeid ?></small>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                $email = '<span style="color: red">Warning: Email address is missing</span>';
                                                                if (!is_null($employee->email) && $employee->email != '')
                                                                    $email = substr($employee->email,0, 10).'...';
                                                                echo $email;
                                                                ?>
                                                            </td>
                                                            <?php if($employee->status == 5){ ?>
                                                                <td> <span style="color: darkgray">USER</span> </td>
                                                                <td> <span style="color: darkgray">DELETED</span> </td>
                                                                <td> <span style="color: darkgray">USER</span> </td>
                                                                <td> <span style="color: darkgray">DELETED</span> </td>
                                                                <td> <span style="color: darkgray">-</span> </td>
                                                                <td> <button id="r<?php echo $employee->id ?>" tag="<?php echo $employee->id ?>" type="button" class="btn btn-danger  btn-xs reinstate" style="width: 100%" ><i class="fa    fa-recycle"></i> Reinstate </button> </td>
                                                                <?php
                                                            }
                                                            else{
                                                                ?>
                                                                <td>
                                                                    <?php echo $employee->location ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $welcome = false;
                                                                    $region = '<span style="color: red">Warning: State & Country are missing</span>';
                                                                    if(!is_null($employee->state) && $employee->state != ''){
                                                                        $regions = $this->user_model->getRegion($employee->state);
                                                                        if (isset($regions) && $regions != false) {
                                                                            if(!is_null($employee->country) && $employee->country != ''){
                                                                                $usercountry = $this->user_model->getCountry($employee->country);
                                                                                if (isset($usercountry) && $usercountry != false){
                                                                                    $region = $regions->row()->name.',<br>';
                                                                                    $region .= $usercountry->row()->name;
                                                                                    $welcome = true;
                                                                                }

                                                                            }
                                                                        }
                                                                    }
                                                                    echo $region;
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php $types = $this->user_model->getUserType($employee->usertype);
                                                                    $type = $types->row();
                                                                    echo $type->description ?>
                                                                </td>
                                                                <td>
                                                                    <?php $types = $this->user_model->getUser($employee->defaultapprover);
                                                                    $type = $types->row();
                                                                    echo $type->name ?>
                                                                </td>
                                                                <td>
                                                                    <a><?php echo $employee->hours ?></a>
                                                                </td>
                                                                <td>
                                                                    <?php if ($welcome && $employee->status == 1 && !is_null($employee->email) && $employee->email != ''){?>
                                                                        <button  tag="<?php echo $employee->id ?>" type="button" style="width: 100%" class="btn btn-success btn-xs welcome" disabled><i class="fa fa-envelope-o"></i> Send Welcome Email </button>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <button  tag="<?php echo $employee->id ?>"  onclick="setmodal('<?php echo $employee->id?>')"
                                                                             type="button" class="btn btn-primary btn-xs"  data-toggle="modal" data-target=".bs-user-modal" style="width: 100%" ><i class="fa fa-pencil-square-o"></i> Edit </button>

                                                                </td>
                                                                <?php
                                                            }
                                                            ?>
                                                        </tr>

                                                        <?php
                                                    }
                                                ?>


                                                </tbody>
                                            </table>
                                            <!-- end project list -->
                                            <button onclick="document.getElementById('company').value='<?php echo $company->id?>';" type="button" class="btn btn-success newuser" data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-user"></i> Create New User </button>
                                        </div>

                                    </div>
                                </div>
                            <?php } ?>
                        </div>

                    </div>

                </div>
            </div>


        </div>


    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>

<!-- Datatables -->
<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>

<script src="<?php echo base_url()?>js/validator/validator.js"></script>
<script>
    $( document ).ready(function() {


        // initialize the validator function
        validator.message['date'] = 'not a real date';

        $('#delete').click(function () {
            var id = $('#company').val();
            $.ajax({
                url: '<?php echo base_url() ?>ajax/deletecompany',
                type: 'post',
                data: {
                    company: id
                },
                success: function (data) {
                    window.location = "<?php echo base_url()?>admin";
                }
            }).error(function() {
                //alert ('An error occurred');
            });

        });


        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

        // bind the validation to the form submit event
        //$('#send').click('submit');//.prop('disabled', true);

        $('form').submit(function (e) {
//            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                return true;
            //this.submit();
            return false;
        });

        /* FOR DEMO ONLY */
        $('#vfields').change(function () {
            $('form').toggleClass('mode2');
        }).prop('checked', false);

        $('#alerts').change(function () {
            validator.defaults.alerts = (this.checked) ? false : true;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);

        $('#selectcompany').change(function(){
            var vip = $('#selectcompany').val();
            if(vip != ""){
                window.location.href = '<?php echo base_url()?>admin/company/'+vip;
            }
        });

        $('#usercountry').change(function(){
            $('#userstate').prop('disabled', true);
            $('#userstate').html("<optgroup>Loading ... </optgroup>");
            $.ajax({
                url: '<?php echo base_url() ?>ajax/getRegionsByCountry',
                type: 'post',
                data: {
                    country: this.value,
                    addnational: 'NO'
                },
                success: function (data) {
                    $('#userstate').html(data);
                    $('#userstate').prop('disabled', false);
                }
            }).error(function() {
                alert ('An error occurred');
            });
        });

        <?php // if($new){ ?>
        $('#vip').change(function () {
            var vipstr = this.value;
            vipstr = vipstr.replace(/\s+/g, '');
            this.value = vipstr;
            $.ajax({
                url: '<?php echo base_url() ?>ajax/checkvip',
                type: 'post',
                data: {
                    vip: this.value
                },
                success: function (data) {
                    if (data != 'OK') {
                        $('#send').prop('disabled', true);
                        alert(data);
                    }
                    else{
                        $('#send').prop('disabled', false);
                    }
                }
            }).error(function () {
                alert('An error occurred');
            });
        });
        <?php  // }
        ?>
        $('#country').change(function(){
            $('#state').prop('disabled', true);
            $('#state').html("<optgroup>Loading ... </optgroup>");
            $.ajax({
                url: '<?php echo base_url() ?>ajax/getRegionsByCountry',
                type: 'post',
                data: {
                    country: this.value,
                    addnational: 'NO'
                },
                success: function (data) {
                    $('#state').html(data);
                    $('#state').prop('disabled', false);
                }
            }).error(function() {
                alert ('An error occurred');
            });
        });

        <?php if (!$new){ ?>
        $('#state').html("<option>Loading ... </option>");
        $.ajax({
            url: '<?php echo base_url() ?>ajax/getRegionsByCountry',
            type: 'post',
            data: {
                country: $('#country').val(),
                addnational: 'NO'
            },
            success: function (data) {
                $('#state').html(data);
                $('#state').val('<?php echo $company->state ?>');
                $('#state').prop('disabled', false);
            }
        }).error(function() {
            //alert ('An error occurred');
        });



        var oTable = $('.table').dataTable({
            "oLanguage": {
                "sSearch": "Search:"
            },
            "bStateSave": false
            ,
            "iDisplayLength": 25,
            "sPaginationType": "full_numbers" ,
            "dom": '<"clear">lfrtip'
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
        <?php } ?>





    });



    $('.newuser').click(function () {
        clearmodal();
    });



    $('.reinstate').click(function () {
        var id = this.getAttribute('tag');
        $.ajax({
            url : "<?php echo base_url()?>/ajax/reinstate",
            type: "POST",
            data : {
                employee: id
            },
            success: function(data){
                alert("User Reinstated. Please refresh this page");
            },
            error: function(data){
                alert("Something went wrong"+data);
                return false
            }
        });

        return true;
    });


    function clearmodal(){
        document.getElementById('id').value         = '';
        document.getElementById('name').value       = '';
        document.getElementById('empid').value      = '';
        document.getElementById('email').value      = '';
        document.getElementById('email2').value     = '';
        document.getElementById('location').value   = '';
        document.getElementById('approver').value   = '';
        document.getElementById('usercountry').value    = '';
        document.getElementById('userstate').value    = '';
        document.getElementById('hours').value      = '';
//                document.getElementById('approver').value   = vals['approver'];
        document.getElementById('usertype').value   = '';
        $('#userstate').prop('disabled', true);

    }



    function setmodal(id){
        $.ajax({
            url : "<?php echo base_url()?>/ajax/userinfo",
            type: "POST",
            data : {
                employee: id
            },
            success: function(data){
                var vals = jQuery.parseJSON(data);
                document.getElementById('id').value         = vals['id'];
                document.getElementById('name').value       = vals['name'];
                document.getElementById('empid').value      = vals['employeeid'];
                document.getElementById('email').value      = vals['email'];
                document.getElementById('email2').value     = vals['email'];
                document.getElementById('location').value   = vals['location'];
                document.getElementById('approver').value   = vals['defaultapprover'];
                document.getElementById('usercountry').value    = vals['country'];
                document.getElementById('hours').value      = vals['hours'];
//                document.getElementById('approver').value   = vals['approver'];
                document.getElementById('usertype').value   = vals['usertype'];

                $('#userstate').html("<option>Loading ... </option>");

                $.ajax({
                    url: '<?php echo base_url() ?>ajax/getRegionsByCountry',
                    type: 'post',
                    data: {
                        country: vals['country'],
                        addnational: 'NO'
                    },
                    success: function (data) {
                        $('#userstate').html(data);
                        $('#userstate').val(vals['state']);
                        $('#userstate').prop('disabled', false);
                    }
                }).error(function() {
                    //alert ('An error occurred');
                });

            },
            error: function(data){
                alert(data);
                return false
            }
        });

        return true;
    }

</script>

<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>
