<?php
include "header.php";


?>

<body class="nav-md" xmlns="http://www.w3.org/1999/html">

    <div class="container body">


        <div class="main_container">

        <?php
        include "navbar.php";
        ?>

            <!-- page content -->
            <div class="right_col" role="main">

                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Company Details</h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>


                    <div class="row">
                        <div class="col-md-12">

                            <div >

                                     <div class="panel">


                                        <div class="panel-body">

                                            <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>user/createuser" method="post">
                                            <input name="approver" type="hidden" value="self">

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company">Company</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select class="form-control has-feedback-left" name="company" id="company">
                                                        <?php
                                                        $companies = $this->admin_model->getCompanies();
                                                        foreach($companies->result() as $company){
                                                            ?>
                                                            <option value="<?php echo $company->id ?>"><?php echo $company->name ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <span class="fa fa-check form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                            </div>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" name="name" class="form-control has-feedback-left" id="name"  required="required">
                                                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                            </div>


                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empid">Employee ID <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" name="empid" class="form-control has-feedback-left" id="empid" required="required">
                                                    <span class="fa fa-info-circle form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                            </div>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="location">Location
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" name="location" class="form-control has-feedback-left" id="location" value="" required="required">
                                                    <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="state">State <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" name="state" class="form-control has-feedback-left" id="state" value="NSW" required="required">
                                                    <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                            </div>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hours">Hours (standard day) <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="number" name="hours" class="form-control has-feedback-left" id="hours" value="7.6" required="required">
                                                    <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                            </div>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="email" id="email" name="email" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
                                                    <span class="fa fa-at form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <div id="invalid" style="display: none"><i class="fa fa-ban"></i> Already Registered</div>
                                            </div>
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="confirm email">Confirm Email <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="email" id="email2" name="confirm_email" data-validate-linked="email" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
                                                    <span class="fa fa-at form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                            </div>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usertype">User Type <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select name="usertype" id="usertype" class="form-control has-feedback-left" required="required">
                                                        <?php $types = $this->user_model->getUserType();
                                                        foreach ($types->result() as $type)
                                                            echo "<option value=".$type->id.">".$type->description."</option>";
                                                        ?>
                                                    </select>
                                                    <span class="fa fa-suitcase form-control-feedback left" aria-hidden="true"></span>

                                                </div>
                                            </div>

                                            <div class="item form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    &nbsp;
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-3">
                                                    <button id="send" type="submit" class="btn btn-success"><i class="fa fa-user"></i> Click to Save</button>
                                                </div>
                                            </div>
                                        </form>

                                        </div>

                                    </div>
                            </div>

                        </div>

                    </div>
                </div>


            </div>


        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

    <script src="<?php echo base_url()?>js/custom.js"></script>

    <script src="<?php echo base_url()?>js/validator/validator.js"></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

        // bind the validation to the form submit event
        //$('#send').click('submit');//.prop('disabled', true);

        $('form').submit(function (e) {
//            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                return true;
                //this.submit();
            return false;
        });

        /* FOR DEMO ONLY */
        $('#vfields').change(function () {
            $('form').toggleClass('mode2');
        }).prop('checked', false);

        $('#alerts').change(function () {
            validator.defaults.alerts = (this.checked) ? false : true;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);


    </script>

    <!-- footer content -->
    <?php include "footer.php" ?>
    <!-- /footer content -->
</body>

</html>
