<?php
include "header.php";
?>

<body class="nav-md">

    <div class="container body">


        <div class="main_container">

        <?php
        include "navbar.php";
        ?>

            <!-- page content -->
            <div class="right_col" role="main">

                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Procedures</h3>
                        </div>

                                <!-- modals -->
                                <!-- world modal -->
                                <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">Create New World</h4>
                                            </div>
                                            <div class="modal-body">

                                                <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>admin/createworld" method="post">

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">World Name <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" name="name" placeholder="Enter New World name" required="required" type="text">
                                                        </div>
                                                    </div>

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">World Type</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <select class="form-control" name="type">
                                                                <option value="NULL">None</option>
                                                                <?php
                                                                // for each team in get top teams
                                                                $worldtypes = $this->admin_model->getWorldTypes();
                                                                if (isset($worldtypes))
                                                                foreach($worldtypes->result() as $type) {
                                                                    ?>
                                                                    <option value="<?php    echo $type->id ?>"><?php echo $type->name ?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">World Status <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="radio">
                                                                <label class="">
                                                                    <div class="iradio_flat-green " style="position: relative;">
                                                                        <input type="radio" class="flat" checked="" name="status" style="position: absolute; opacity: 0;" value="A">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);">
                                                                        </ins>
                                                                    </div>
                                                                    Active
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label class="">
                                                                    <div class="iradio_flat-green" style="position: relative;">
                                                                        <input type="radio" class="flat" name="status" style="position: absolute; opacity: 0;" value="I">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);">
                                                                        </ins>
                                                                    </div>
                                                                    Inactive
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            &nbsp;
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-3">
                                                            <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-ban"></i>Cancel & Close</button>
                                                            <button id="send" type="submit" class="btn btn-primary"><i class="fa fa-globe"></i> Create World</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="modal fade bs-team-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                </button>
                                                <h4 class="modal-title" id="myTeamModalLabel">Create New Team</h4>
                                            </div>
                                            <div class="modal-body">

                                                <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>admin/createteam" method="post">
                                                    <input type="hidden" id="modalteamid" name="id" value="">
                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Team Name <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input id="modalteamname" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" name="name" placeholder="New Team name" required="required" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="world">World</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <select class="form-control" name="world" id="modalteamworld">
                                                                <option value="NULL">None</option>
                                                                <?php
                                                                // for each team in get top teams
                                                                $worlds = $this->admin_model->getWorlds();
                                                                foreach($worlds->result() as $world) {
                                                                    ?>
                                                                    <option value="<?php echo $world->NS_GM_WRLD_RID ?>"><?php echo $world->NS_GM_WRLD_NM ?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Team Status <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="radio">
                                                                <label class="">
                                                                    <div class="iradio_flat-green " style="position: relative;">
                                                                        <input type="radio" class="flat" checked="" name="status" style="position: absolute; opacity: 0;" value="A">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);">
                                                                        </ins>
                                                                    </div>
                                                                    Active
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label class="">
                                                                    <div class="iradio_flat-green" style="position: relative;">
                                                                        <input type="radio" class="flat" name="status" style="position: absolute; opacity: 0;" value="I">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);">
                                                                        </ins>
                                                                    </div>
                                                                    Inactive
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            &nbsp;
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-3">
                                                            <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-ban"></i>Cancel & Close</button>
                                                            <button id="send" type="submit" class="btn btn-info"><i class="fa fa-users"></i> Save Team </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="modal fade bs-user-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">Create New User</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>admin/createuser" method="post">
                                                    <input type="hidden" id="modaluserid" value="" name="id">
                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="firstname">First Name <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input type="text" name="firstname" class="form-control has-feedback-left" id="modaluserfname" data-validate-length-range="2"   placeholder="First Name" required="required">
                                                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                        </div>
                                                    </div>

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="surname">Surname <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input type="text" name="surname" class="form-control has-feedback-left" id="modalusersname" data-validate-length-range="2"  placeholder="Surname" required="required">
                                                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                        </div>
                                                    </div>

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input type="email" id="email" name="email" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
                                                            <span class="fa fa-at form-control-feedback left" aria-hidden="true"></span>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="confirm email">Confirm Email <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input type="email" id="email2" name="confirm_email" data-validate-linked="email" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
                                                            <span class="fa fa-at form-control-feedback left" aria-hidden="true"></span>
                                                        </div>
                                                    </div>

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Team</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <select class="form-control" name="team" id="modaluserteam">
                                                                <option value="null">None</option>
                                                                <?php
                                                                // for each team in get top teams
                                                                $worlds = $this->admin_model->getWorlds();
                                                                foreach($worlds->result() as $world) {
                                                                ?>
                                                                    <optgroup label="<?php echo $world->NS_GM_WRLD_NM?>">"
                                                                        <?php
                                                                        $teams = $this->admin_model->getTeamsByWorld($world->NS_GM_WRLD_RID);
                                                                        if (isset($teams))
                                                                        foreach($teams->result() as $team) {
                                                                            ?>
                                                                            <option value="<?php echo $team->NS_TM_TEAM_RID ?>"><?php echo $team->NS_TM_TEAM_NM ?></option>
                                                                        <?php }?>
                                                                    </optgroup>
                                                                <?php }?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">User Status <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="radio">
                                                                <label class="">
                                                                    <div class="iradio_flat-green " style="position: relative;">
                                                                        <input type="radio" class="flat" checked="" name="status" style="position: absolute; opacity: 0;" value="A">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);">
                                                                        </ins>
                                                                    </div>
                                                                    Active
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label class="">
                                                                    <div class="iradio_flat-green" style="position: relative;">
                                                                        <input type="radio" class="flat" name="status" style="position: absolute; opacity: 0;" value="I">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);">
                                                                        </ins>
                                                                    </div>
                                                                    Inactive
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            &nbsp;
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-3">
                                                            <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-ban"></i>Cancel & Close</button>
                                                            <button id="send" type="submit" class="btn btn-success"><i class="fa fa-user"></i> Save User</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row" style="overflow: auto">
                        <div class="col-md-12">
<!--                            <button type="button"  class="btn btn-primary" data-toggle="modal" data-target=".bs-world-modal"><i class="fa fa-globe"></i> Create New World </button>
                            <button type="button" onclick="setmodal('','','')" class="btn btn-info" data-toggle="modal" data-target=".bs-team-modal"><i class="fa fa-users"></i> Create New Team </button>
                            <button type="button" onclick="setmodaluser('','','','','')" class="btn btn-success" data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-user"></i> Create New User </button>
-->
                            <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                                <table class="table table-striped projects">
                                    <tbody>

                                    <?php
                                    // for each team in get top teams
                                    $tables = $this->admin_model->getFuncs();
                                    foreach($tables->result() as $table){
                                        echo "<tr>";
                                        echo "<td>".$table->Name."</td>";
                                        echo "<td>".$table->Type."</td>";

                                        $proc = $this->admin_model->getFuncDetail($table->Name)->row();
                                        foreach($proc as $item)
                                            echo "<td>".print_r($item, true)."</td>";
                                        echo "</tr>";
                                    }
                                    ?>

                                    </tbody>
                                </table>

                            </div>
<!--
                            <button type="button"  class="btn btn-primary" data-toggle="modal" data-target=".bs-world-modal"><i class="fa fa-globe"></i> Create New World </button>
                            <button type="button" onclick="setmodal('','','')" class="btn btn-info" data-toggle="modal" data-target=".bs-team-modal"><i class="fa fa-users"></i> Create New Team </button>
                            <button type="button" onclick="setmodaluser('','','','','')" class="btn btn-success" data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-user"></i> Create New User </button>
-->                        </div>
                    </div>


                </div>

                <?php
                include "footer.php";
                ?>

            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

    <script src="<?php echo base_url()?>js/custom.js"></script>

    <script>
        $( document ).ready(function() {
            // Handler for .ready() called.

            $(".dd" ).on ({
                click:function( event,ui ) {
                    var tbl = ''+event.target.id;
                    tbl = tbl.substr(7);
                    alert(tbl);
                    $.ajax({
                        url: '<?php echo base_url() ?>ajax/getTable',
                        type: 'post',
                        data: {
                            table: tbl
                        },
                        success: function (data) {
                            $('#tabledata'+tbl).html(data);
//                            $('#loading').hide();
                        }
                    }).error(function() {
                            alert ('An error occurred');
                    });

                }
            });

            $("#send" ).on ({
                click: function( event,ui ) {
                    q = $('#NS_GM_WRLD_RID').val();
                    $('#loading').show();
                    $('#send').hide();
                    $.ajax({
                        url: '<?php echo base_url() ?>ajax/calculate',
                        type: 'post',
                        data: {
                            NS_GM_WRLD_RID: q
                        },
                        success: function (data) {
                            $('#NS_GM_CUR_SUB_PER').val(data);
                            $('#loading').hide();
                            $('#send').show();
                        }
                    }).error(function() {
                            alert ('An error occurred');
                            $('#loading').hide();
                            $('#send').show();
                        });
                }
            });

        });


    </script>
    <script>

        function setmodal(id, name, world){
            document.getElementById('modalteamid').value=id;
            document.getElementById('modalteamname').value=name;
            document.getElementById('modalteamworld').value=world;

            return true;
        }

        function setmodaluser(id, firstname, surname, email, team){
            document.getElementById('modaluserid').value=id;
            document.getElementById('modaluserfname').value=firstname;
            document.getElementById('modalusersname').value=surname;
            document.getElementById('email').value=email;
            document.getElementById('email2').value=email;
            document.getElementById('modaluserteam').value=team;

            return true;
        }
    </script>

</body>

</html>