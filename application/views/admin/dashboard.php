<?php 
  // maindashboard
  include "header.php";
?>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">

          <?php  
            include "navbar.php";
          ?>

            <!-- page content -->
            <div class="right_col" role="main">
                <br />
                <div class="">
                    <div class="row top_tiles" >
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-desktop"></i>
                                </div>
                                <div class="count"><?php echo $this->user_model->getOnlineUsers(); ?></div>

                                <h3>Users Online</h3>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-building-o"></i>
                                </div>
                                <div class="count"><?php echo $this->admin_model->getCompanyCount(); ?></div>

                                <h3>Companies</h3>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-users"></i>
                                </div>
                                <div class="count"><?php echo $this->admin_model->getUserCount(); ?></div>

                                <h3>Users</h3>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-calendar-o"></i>
                                </div>
                                <div class="count"><?php echo $this->admin_model->getLeaveCount(); ?></div>

                                <h3>Leave Entries</h3>
                            </div>
                        </div>

                    </div>




                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Express Leave Summary </h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <div class="row" style="border-bottom: 1px solid #E0E0E0; padding-bottom: 5px; margin-bottom: 5px;">
                                        <div class="col-xs-12" style="overflow:hidden;">
                                            <table id="example" class="table table-striped  responsive-utilities">
                                                <thead>
                                                    <tr class="headings">
                                                        <th>Company Name</th>
                                                        <th>VIP</th>
                                                        <th>Status</th>
                                                        <th>Work Week (leave)</th>
                                                        <th>Total Users</th>
                                                        <th># Leave Entries</th>
                                                        <th>Billing Status</th>
                                                        <th>Date Created</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php

                                                    $deleted = false;
                                                    if($this->session->userdata('role') <= 2)
                                                        $deleted = true;
                                                    $companies = $this->admin_model->getCompanies($deleted);

                                                    foreach($companies->result() as $company){
                                                        $usercount = $this->admin_model->getUserCount($company->id);
                                                        $leavecount = "Click to show";//$this->admin_model->getLeaveCount($company->id);
                                                        $status = $this->admin_model->getStatus($company->status);
                                                        ?>
                                                        <tr>
                                                            <td data-order="<?php echo $company->name ?>"><a href="<?php echo base_url().'admin/company/'.rawurlencode($company->vip) ?>"><?php echo $company->name ?></a></td>
                                                            <td data-order="<?php echo $company->vip ?>"><a href="<?php echo base_url().'admin/company/'.rawurlencode($company->vip) ?>"><?php echo $company->vip ?></a></td>
                                                            <td data-order="<?php echo $status ?>"><a href="<?php echo base_url().'admin/company/'.rawurlencode($company->vip) ?>"><?php echo $status ?></a></td>
                                                            <td data-order="<?php echo $company->sevendayleave ?>"><a href="<?php echo base_url().'admin/company/'.urlencode($company->vip) ?>"><?php if($company->sevendayleave) echo '7 Days'; else echo '5 Days' ?></a></td>
                                                            <td data-order="<?php echo $usercount ?>"><a href="<?php echo base_url().'admin/company/'.rawurlencode($company->vip) ?>"><?php echo $usercount ?></a></td>
                                                            <td class="leavecount" tag="<?php echo $company->id ?>"><?php echo $leavecount ?></a></td>
                                                            <td data-order="<?php echo $company->billingstatus ?>"><a href="<?php echo base_url().'admin/company/'.rawurlencode($company->vip) ?>"><?php echo $this->user_model->getBillingStatus($company->billingstatus)->row()->description ?></a></td>
                                                            <td data-order="<?php echo date_format(date_create($company->created), "U"); ?>"><a href="<?php echo base_url().'admin/company/'.rawurlencode($company->vip) ?>"><?php echo date_format(date_create($company->created), "d/m/Y"); ?></a></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>

               <?php 
               include "footer.php";
               ?>

            </div>
            <!-- /page content -->
        </div>


    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url() ?>js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>
    <!-- sparkline -->
    <script src="<?php echo base_url() ?>js/sparkline/jquery.sparkline.min.js"></script>

    <!-- Datatables -->
    <script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>
    
    <script src="<?php echo base_url() ?>js/custom.js"></script>

   <script>
       function setmodal(id, name, empid, email, usertype,approver, loc){
           document.getElementById('id').value=id;
           document.getElementById('name').value=name;
           document.getElementById('empid').value=empid;
           document.getElementById('email').value=email;
           document.getElementById('email2').value=email;
           document.getElementById('location').value=loc;
           document.getElementById('approver').value=approver;
           document.getElementById('usertype').value=usertype;
           return true;
       }


       $(document).ready(function () {


       var oTable = $('.table').dataTable({
           "oLanguage": {
               "sSearch": "Search:"
           },
           "bStateSave": false
           ,
           "iDisplayLength": 50,
           "sPaginationType": "full_numbers" ,
           "dom": '<"clear">lfrtip'
       });

       $("tfoot input").keyup(function () {
           /* Filter on the column based on the index of this element's parent <th> */
           oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
       });
       $("tfoot input").each(function (i) {
           asInitVals[i] = this.value;
       });
       $("tfoot input").focus(function () {
           if (this.className == "search_init") {
               this.className = "";
               this.value = "";
           }
       });
       $("tfoot input").blur(function (i) {
           if (this.value == "") {
               this.className = "search_init";
               this.value = asInitVals[$("tfoot input").index(this)];
           }
       });

           $('.leavecount').click(function () {
               var id = this.getAttribute('Tag');
               var obj = $(this);
               $.ajax({
                   url: '<?php echo base_url() ?>ajax/getleavecount',
                   type: 'post',
                   data: {
                       company: id
                   },
                   success: function (data) {
                       obj.html(data);
                   }
               }).error(function () {
                   alert('An error occurred');
               });
           });

       });



   </script>

    <!-- /datepicker -->
</body>

</html>