<?php
include "header.php";
?>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

        <?php
        include "navbar.php";
        ?>

            <!-- page content -->
            <div class="right_col" role="main">

                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Billing Plans</h3>
                        </div>
                    </div>
                    <div class="modal fade bs-user-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Billing Plans</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>admin/billingplans" method="post">

                                        <div class="item form-group">
                                            <input type="hidden" name="id" id="id" value="">
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="description" class="form-control has-feedback-left" id="desc" placeholder="Description" required="required">
                                                <span class="fa fa-font form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="notes">Notes
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea type="text" name="notes" class="form-control has-feedback-left" id="notes"  value=""></textarea>
                                                <span class="fa fa-newspaper-o form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>


                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usertype">Formula <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea type="text" name="formula" class="form-control has-feedback-left" id="formula"  value=""></textarea>
                                                <span class="fa fa-newspaper-o form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                &nbsp;
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="buttons"></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <button id="save" name="save" type="submit" class="btn btn-primary col-xs-5"> Save </button>
                                                <button type="button" class="btn btn-default col-xs-5" style="float:right" data-dismiss="modal"> Cancel </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>



                    <div class="row">
                            <div class="col-md-12">

                                     <div class="panel">

                                         <div >
                                             <div class="panel-body">
                                                 <button onclick="setmodal('')" type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-usd"></i> Add New Plan </button>

                                        <!-- start project list -->
                                        <table class="table table-striped projects">
                                        <thead>
                                        <tr>
                                            <th>Short Description</th>
                                            <th>Notes</th>
                                            <th>Formula</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php
                                        // for each team in get top teams
                                            $plans = $this->user_model->getBillingstatus();
                                            if (isset($plans)){
                                                ?>
                                                    <?php
                                                    foreach($plans->result() as $plan) {
                                                        ?>
                                                        <tr id="row<?php echo $plan->id ?>">
                                                        <td id="desc<?php echo $plan->id ?>"><?php echo $plan->description ?></td>
                                                        <td id="notes<?php echo $plan->id ?>"><?php echo $plan->notes ?></td>
                                                        <td id="formula<?php echo $plan->id ?>"><?php echo $plan->formula ?></td>
                                                        <td>
                                                            <button type="button"  onclick="setmodal(<?php echo "'".$plan->id."'"?>)"  class="btn btn-primary btn-xs" style="width:100%; max-width:150px" data-toggle="modal" data-target=".bs-user-modal">Edit</button>
								                            <br>
<!--                                                            <button type="button" class="btn btn-danger btn-xs" style="width:100%; max-width:150px" >Delete</button>-->
                                                        </td>
                                                        </tr>
                                                    <?php } ?>

                                            <?php }?>

                                        </tbody>
                                        </table>
                                        <!-- end project list -->
                                             </div>
                                        </div>
                                    </div>

                            </div>
                    </div>




                </div>


            </div>
            <!-- /page content -->
        </div>

    </div>



    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

    <script src="<?php echo base_url()?>js/custom.js"></script>

    <script src="<?php echo base_url()?>js/validator/validator.js"></script>
    <!-- PNotify -->
    <script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
    <script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
    <script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>

    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

        // bind the validation to the form submit event
        //$('#send').click('submit');//.prop('disabled', true);

        $('form').submit(function (e) {
//            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                return true;
                //this.submit();
            return false;
        });

        /* FOR DEMO ONLY */
        $('#vfields').change(function () {
            $('form').toggleClass('mode2');
        }).prop('checked', false);

        $('#alerts').change(function () {
            validator.defaults.alerts = (this.checked) ? false : true;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);

        function setmodal(id){
            document.getElementById('id').value = id;
            document.getElementById('desc').value = document.getElementById('desc'+id).innerText;
            document.getElementById('notes').value = document.getElementById('notes'+id).innerText;
            document.getElementById('formula').value = document.getElementById('formula'+id).innerText;
            return true;
        }

        function remtype(id){
//            document.getElementById('did').value=id;
if(confirm("Please confirm you wish to delete the leave type. This cannot be undone.")){
            $.ajax({
                url : "<?php echo base_url()?>/ajax/deleteLeave",
                type: "POST",
                data : {leavetype: id},
                success: function(data){
                    document.getElementById("row"+id).style.display = 'none';
                    new PNotify({ title: 'Delete Leave Type', text: data, type: 'success' });
                },
                error: function(data){
                    new PNotify({ title: 'Delete Leave Type', text: data, type: 'error' });
                }
            });
}
        }


    </script>

    <!-- footer content -->
    <?php include "footer.php" ?>
    <!-- /footer content -->
</body>

</html>
