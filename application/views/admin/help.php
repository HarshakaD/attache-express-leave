<?php
include "header.php";
?>
<link href="<?php echo base_url()?>fonts/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url()?>css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
<link href="<?php echo base_url()?>css/editor/index.css" rel="stylesheet">
<script src="<?php echo base_url()?>js/html.sortable.src.js"></script>

<!-- select2 -->
<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        if ($this->session->userdata("logged_in"))
            include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main" style="overflow: auto">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Express Leave - Help
                    </h3>
                </div>
                <div class="navbar-right">
                    <button type="button" class="btn btn-primary btn-new" data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-star"></i> Create Help Item</button>
                </div>

                <div class="modal fade bs-user-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Help Management</h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>admin/edithelp" method="post">

                                    <div class="item form-group">
                                        <input type="hidden" name="id" id="id" value="">
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="summary">Summary <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="summary" class="form-control" id="summary" placeholder="Summary" required="required">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <div id="alerts"></div>
                                        <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor">
                                            <div class="btn-group">
                                                <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                                                <ul class="dropdown-menu">
                                                </ul>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                                                <ul class="dropdown-menu">
                                                    <li><a data-edit="fontSize 5"><p style="font-size:17px">Huge</p></a>
                                                    </li>
                                                    <li><a data-edit="fontSize 3"><p style="font-size:14px">Normal</p></a>
                                                    </li>
                                                    <li><a data-edit="fontSize 1"><p style="font-size:11px">Small</p></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                                                <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                                                <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                                                <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                                                <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                                                <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-outdent"></i></a>
                                                <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                                                <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                                                <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                                                <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                                                <div class="dropdown-menu input-append">
                                                    <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
                                                    <button class="btn" type="button">Add</button>
                                                </div>
                                                <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>

                                            </div>

                                            <!-- div class="btn-group">
                                                <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="icon-picture"></i></a>
                                                <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                                            </div -->



                                            <div class="btn-group">
                                                <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                                                <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                                            </div>
                                        </div>

                                        <div id="editor">
                                        </div>
                                    </div>
                                    <textarea name="detail" id="detail" style="display:none;"></textarea>
                                    <br />


                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="summary">Visible To <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="usertype" id="usertype" class="form-control has-feedback-left">
                                                <option value="9" selected>Users Not Logged In</option>
                                                <?php $types = $this->user_model->getUserType();
                                                foreach ($types->result() as $type)
                                                    echo "<option value=".$type->id.">".$type->description."</option>";
                                                ?>
                                            </select>
                                            <span class="fa fa-suitcase form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            &nbsp;
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-ban"></i> Cancel & Close</button>
                                            <button id="send" type="submit" class="btn btn-success xcxc"><i class="fa fa-user"></i> Click to Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>


            <div class="row col-xs-12 col-lg-10">
                <div class="col-xs-12">
                    <div class="x_panel" style="">
                        <div class="x_title">
                            <h2>Contents</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <ol id="helplist" class="o-sortable">
                                <?php
                                $helpitems = $this->user_model->getHelp();

                                foreach($helpitems->result() as $help){
                                    echo '<li style="border: 1px solid #d3d3d3;position: relative; z-index: 10; background-color: lightgrey; color: #001f3f; padding: .5rem; margin-bottom: .5rem; " tag="'.$help->id.'">';
                                    echo '<i class="fa fa-arrow-up" style="padding-left:5px;"></i>';
                                    echo '<i class="fa fa-arrow-down" style="padding-right:10px"></i>'.$help->summary.'</li>';
                                }
                                ?>
                            </ol>
                        </div>

                    </div>
                </div>

                <?php
                foreach($helpitems->result() as $help){
                    ?>
                    <div class="col-xs-12" id="help<?php echo $help->id ?>">
                        <div class="x_panel" style="">
                            <div class="x_title">
                                <h2 id="summary<?php echo $help->id ?>"><?php echo $help->summary?></h2>

                                <div class="navbar-right">
                                    Visible to <?php
                                    if($help->visibleto == 9) echo "All - even if not logged in";
                                    else{
                                        $type = $this->user_model->getUserType($help->visibleto)->row();
                                        echo $type->description."'s";
                                    }
                                    ?>
                                    <button type="button" class="btn btn-primary btn-xs btn-edit" tag="<?php echo $help->id ?>" vis="<?php echo $help->visibleto ?>" data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-edit"></i> Edit</button>
                                    <button type="button" class="btn btn-danger btn-xs btn-delete" tag="<?php echo $help->id ?>"><i class="fa fa-remove"></i> Delete</button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content" id="detail<?php echo $help->id ?>" >
                                <?php echo $help->detail ?>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>

            </div>


        </div>
    </div>


</div>
<!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
<!-- richtext editor -->
<script src="<?php echo base_url() ?>js/editor/bootstrap-wysiwyg.js"></script>
<script src="<?php echo base_url() ?>js/editor/external/jquery.hotkeys.js"></script>
<script src="<?php echo base_url() ?>js/editor/external/google-code-prettify/prettify.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- PNotify -->
<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>

<script>

    $( document ).ready(function() {
        $(".btn-edit" ).on ({
            click: function () {
                var id = this.getAttribute('Tag');
                var type = this.getAttribute('vis');
                var idname1 = "summary"+id;
                var idname2 = "detail"+id;
                document.getElementById('summary').value = document.getElementById(idname1).innerText;
                $('#editor').html(document.getElementById(idname2).innerHTML);
                document.getElementById('id').value = id;
                document.getElementById('usertype').value = type;
                return true;
            }
        });

        $(".btn-new" ).on ({
            click: function () {
                document.getElementById('summary').value = " ";
                $('#editor').html(" ");
                document.getElementById('detail').value = " ";
                document.getElementById('id').value = "";
                return true;
            }
        });

        $(".btn-delete" ).on ({
            click: function( event,ui ) {
                var id = this.getAttribute('Tag');
                $.ajax({
                    url: '<?php echo base_url() ?>ajax/deleteHelp',
                    type: 'post',
                    data: {
                        helpid: id
                    },
                    success: function (data) {
                        $('#help'+id).hide();
                        new PNotify({ title: 'Help item Deleted', type: 'success' });
                    }
                }).error(function() {
                        alert ('An error occurred');
                    });
            }
        });

    });
</script>
<!-- editor -->
<script>
    $(document).ready(function () {
        $('.xcxc').click(function () {
            $('#detail').val($('#editor').html());
        });
    });

    $('.o-sortable').sortable({
        forcePlaceholderSize: true
    });
    $('.o-sortable').sortable().bind('sortupdate', function(e, ui) {
        var to = ui.index;
        var from = ui.oldindex;
        $.ajax({
            url: '<?php echo base_url() ?>ajax/orderHelp',
            type: 'post',
            data: {
                from: from,
                to: to
            },
            success: function (data) {
                new PNotify({ title: 'Help items reordered', type: 'success' });
            }
        }).error(function() {
                alert ('An error occurred');
            });


        /*

         This event is triggered when the user stopped sorting and the DOM position has changed.

         ui.item contains the current dragged element.
         ui.index contains the new index of the dragged element (considering only list items)
         ui.oldindex contains the old index of the dragged element (considering only list items)
         ui.elementIndex contains the new index of the dragged element (considering all items within sortable)
         ui.oldElementIndex contains the old index of the dragged element (considering all items within sortable)
         ui.startparent contains the element that the dragged item comes from
         ui.endparent contains the element that the dragged item was added to (new parent)

         */
    });

    $(function () {
        function initToolbarBootstrapBindings() {
            var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                    'Times New Roman', 'Verdana'],
                fontTarget = $('[title=Font]').siblings('.dropdown-menu');
            $.each(fonts, function (idx, fontName) {
                fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
            });
            $('a[title]').tooltip({
                container: 'body'
            });
            $('.dropdown-menu input').click(function () {
                return false;
            })
                .change(function () {
                    $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                })
                .keydown('esc', function () {
                    this.value = '';
                    $(this).change();
                });

            $('[data-role=magic-overlay]').each(function () {
                var overlay = $(this),
                    target = $(overlay.data('target'));
                overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
            });
            if ("onwebkitspeechchange" in document.createElement("input")) {
                var editorOffset = $('#editor').offset();
                $('#voiceBtn').css('position', 'absolute').offset({
                    top: editorOffset.top,
                    left: editorOffset.left + $('#editor').innerWidth() - 35
                });
            } else {
                $('#voiceBtn').hide();
            }
        };

        function showErrorAlert(reason, detail) {
            var msg = '';
            if (reason === 'unsupported-file-type') {
                msg = "Unsupported format " + detail;
            } else {
                console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        };
        initToolbarBootstrapBindings();
        $('#editor').wysiwyg({
            fileUploadError: showErrorAlert
        });
        window.prettyPrint && prettyPrint();
    });
</script>
<!-- /editor -->
<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>
