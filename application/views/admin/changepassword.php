<?php
include "header.php";
?>

<body class="nav-md">

<div class="container body">


<div class="main_container">

<?php
if ($this->session->userdata("logged_in"))
    include "navbar.php";

$user = $this->admin_model->getAdmins($id)->row();
?>

<!-- page content -->
<div class="right_col" role="main">

<div class="page-title">
    <div class="title_left">
        <h3>
            Change Password
        </h3>
    </div>
    <div class="title_right">
        <h3>

        </h3>
    </div>

</div>
<div class="clearfix"></div>


<div class="row">
    <!-- form date pickers -->
    <div class="col-xs-12 col-sm-9 ">
        <div class="x_panel" style="">
            <div class="x_title">
                <h2>Change Password<br>
                </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>admin/changepassword" method="post" >
                    <div id="leaverow">

                        <?php
                        $message = $this->session->userdata('message');
                        echo "<div class='has-error'>".$message."</div>";
                        //if(isset($message) && $message !="") echo "<div class='error_message'>".$message."</div>"
                        ?>

                        <div class="item form-group">
                            <input type="hidden" name="for" value="<?php echo $id ?>">

                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empid">Name
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="empid" class="form-control has-feedback-left" id="user" value="<?php echo $user->name ?>" disabled>
                                <span class="fa fa-info-circle form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empid">Email
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="empid" class="form-control has-feedback-left" id="user" value="<?php echo $user->email ?>" disabled>
                                <span class="fa fa-info-circle form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Enter a New Password
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="password" name="password" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
                                <span class="fa fa-lock form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="confirm email">Confirm The Password
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="password2" name="confirm_password" data-validate-linked="password" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
                                <span class="fa fa-lock form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button id="send" type="submit" class="btn btn-success"><i class="fa fa-check-square"></i> Save Password</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-9 ">
        <div class="x_panel" style="">
            <div class="x_title">
                <h2>Change Password For:<br>
                </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>admin/changepassword" method="post" >

                <div class="form-group">
                        <div class="col-xs-12">

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="confirm email">Or change password for:
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="user" class="item form-control">
                                <option>Select User</option>
                                <?php
                                $admins = $this->admin_model->getAdmins();
                                foreach($admins->result() as $admin){
                                    if($admin->role > $this->session->userdata('role') || $admin->role == 1)
                                    echo '<option value="'.$admin->id.'">'.$admin->name.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button id="send" type="submit" class="btn btn-primary"><i class="fa fa-recycle"></i> Select User</button>
                        </div>
                    </div>



                </form>
            </div>

        </div>
    </div>
    <!-- /form datepicker -->


</div>
</div>


</div>
<!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>


<script src="<?php echo base_url()?>js/validator/validator.js"></script>
<script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
        .on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
//            e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            return true;
        //this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);
</script>

<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>
