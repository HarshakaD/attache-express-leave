<?php

include "header.php";
?>
<style>
    body {
        background: transparent url("<?php echo base_url()?>images/bg-admin.jpg") no-repeat center center fixed;
        background-size: cover;
    }
    footer{
        bottom: 18px !important;
        left: 15px !important;
        width: 100% !important;
        position: absolute !important;
    }
</style>

<body xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html">

<div class="">

    <div id="wrapper">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Admin Area</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="min-height: 350px">
                    <div id="login" class="animate form">
                        <section class="login_content">
                            <form novalidate="" class="form-horizontal form-label-left"
                                  action="<?php echo base_url() ?>admin/login" method="post">
                                <?php
                                $message = $this->session->userdata('message');
                                $this->session->set_userdata(array('message' => ""));
                                echo "<div class='has-error'>" . $message . "</div>";
                                ?>
                                <div class="clearfix"></div>
                                <div class="item form-group">
                                    <div>
                                        <input name="email" class="form-control" type="text"
                                               placeholder="username" required="required"/>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <div>
                                        <input type="password" name="password" class="form-control"
                                               placeholder="Enter your password" required="required"/>
                                    </div>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-danger btn-md submit col-xs-12"><i class="fa fa-sign-in"></i>
                                        Click to Login
                                    </button>
                                </div>
                                <div class="clearfix"></div>

                                </div>

                            </form>
                            <!-- form -->
                        </section>

                    </div>

                </div>

            </div>

        </div>

    </div>
    <?php

    include "footer.php";
    ?>
</body>
<script src="<?php echo base_url() ?>js/validator/validator.js"></script>
<script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
        .on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
//            e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            return true;
        //this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);
</script>
</html>