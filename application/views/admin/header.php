<?php
if($this->session->userdata('logged_in'))
$this->user_model->logUser();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="<?php echo(base_url())?>images/favicon.ico"/>
    <title>
        Attache Leave Administration
        <?php 
        // **TODO company name 
        ?>
    </title>

    <!-- Bootstrap core CSS -->

    <link href="<?php echo base_url() ?>css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>css/animate.min.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>css/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="<?php echo base_url() ?>css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="<?php echo base_url() ?>css/icheck/flat/green.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>css/floatexamples.css" rel="stylesheet" />

    <script src="<?php echo base_url() ?>js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->


<!--    <style>-->
<!--        html,-->
<!--        .body {-->
<!--            margin:0;-->
<!--            padding:0;-->
<!--        }-->
<!--        #wrapper {-->
<!--            min-height:100%;-->
<!--            position:relative;-->
<!--        }-->
<!--        #header {-->
<!--            background:#ededed;-->
<!--            padding:10px;-->
<!--        }-->
<!--        .container {-->
<!--            padding-bottom:100px; /* Height of the footer element */-->
<!--        }-->
<!--        footer {-->
<!--            position:fixed;-->
<!--            width: 100% !important;-->
<!--            bottom:0;-->
<!--            left:0;-->
<!--            background-color: #2A3F54!important;-->
<!--            display: none;-->
<!--        }-->
<!--    </style>-->

    <script>
        window.onscroll = function(ev) {
            if ((window.innerHeight + window.scrollY + 50) >= document.body.offsetHeight) {
                $('footer').show();
                // you're at the bottom of the page
            }
            else{
                $('footer').hide();
            }
        };
        $(document).ready(function() {
            // Check if body height is higher than window height :)
            if ($("body").height() > $(window).height()+100) {
                $('footer').hide();
            }
            else{
                $('footer').show();
            }

            // Check if body width is higher than window width :)
//        if ($("body").width() > $(window).width()) {
//            alert("Horizontal Scrollbar! D:<");
//        }
        });
    </script>

</head>