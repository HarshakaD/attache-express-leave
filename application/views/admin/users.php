<?php 
  // maindashboard
  include "header.php";
?>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">

          <?php  
            include "navbar.php";
          ?>

            <!-- page content -->
            <div class="right_col" role="main">
                <br />
                <div class="">
                    <div class="row top_tiles" >
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-desktop"></i>
                                </div>
                                <div class="count"><?php echo $this->onlineusers->total_users(); ?></div>

                                <h3>Users Online</h3>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-building-o"></i>
                                </div>
                                <div class="count"><?php echo $this->admin_model->getCompanyCount(); ?></div>

                                <h3>Companies</h3>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-users"></i>
                                </div>
                                <div class="count"><?php echo $this->admin_model->getUserCount(); ?></div>

                                <h3>Users</h3>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-calendar-o"></i>
                                </div>
                                <div class="count"><?php echo $this->admin_model->getLeaveCount(); ?></div>

                                <h3>Leave Entries</h3>
                            </div>
                        </div>

                    </div>




                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>User Management</h2>
                                    <div class="clearfix"></div>

                                </div>
                                <div class="x_content">
                                    <button type="button" class="btn btn-success"><a href="<?php echo base_url()?>admin/createuser"><i class="fa fa-user"></i> Create New User </a></button>

                                    <div class="row" style="border-bottom: 1px solid #E0E0E0; padding-bottom: 5px; margin-bottom: 5px;">
                                        <div class="col-xs-12" style="overflow:hidden;">
                                            <table class="table table-striped projects" id="table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Status</th>
                                                        <th>Role</th>
                                                        <th>Company Name</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $companies = $this->admin_model->getCompanies();
                                                    foreach($companies->result() as $company){

                                                        $users = $this->user_model->getUsers($company->id);
                                                        foreach($users->result() as $user){
                                                        ?>
                                                        <tr>

                                                            <td><?php echo $user->name ?></td>
                                                            <td><?php echo $user->email ?></td>
                                                            <td><?php echo $this->user_model->getStatus($user->status)->description ?></td>
                                                            <td><?php echo $this->user_model->getUserType($user->usertype)->row()->description ?></td>
                                                            <td><a href="<?php echo base_url().'admin/company/'.urlencode($company->vip) ?>"><?php echo $company->name ?></a></td>
                                                            <td class="right">
                                                                <button type="button" class="btn btn-primary btn-xs btn-edit" tag="<?php echo $user->id ?>" data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-edit"></i> Edit</button>
                                                                <!-- button type="button" class="btn btn-danger btn-xs btn-delete" tag="<?php echo $user->id ?>"><i class="fa fa-remove"></i> Delete</button -->
                                                            </td>

                                                        </tr>
                                                        <?php
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>

               <?php 
               include "footer.php";
               ?>

            </div>
            <!-- /page content -->
        </div>


    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url() ?>js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>
    <!-- sparkline -->
    <script src="<?php echo base_url() ?>js/sparkline/jquery.sparkline.min.js"></script>

    <script src="<?php echo base_url() ?>js/custom.js"></script>

    <!-- Datatables -->
    <script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>
   <script>


       var asInitVals = new Array();
       $(document).ready(function () {
           var oTable = $('#table').dataTable({
               "oLanguage": {
                   "sSearch": "Search:"
               },
               'iDisplayLength': 10,
               "sPaginationType": "full_numbers"
               //   "dom": 'T<"clear">lfrtip',
           });

           $("tfoot input").keyup(function () {
               /* Filter on the column based on the index of this element's parent <th> */
               oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
           });
           $("tfoot input").each(function (i) {
               asInitVals[i] = this.value;
           });
           $("tfoot input").focus(function () {
               if (this.className == "search_init") {
                   this.className = "";
                   this.value = "";
               }
           });
           $("tfoot input").blur(function (i) {
               if (this.value == "") {
                   this.className = "search_init";
                   this.value = asInitVals[$("tfoot input").index(this)];
               }
           });
       });

       function setmodal(id, name, empid, email, usertype,approver, loc){
           document.getElementById('id').value=id;
           document.getElementById('name').value=name;
           document.getElementById('empid').value=empid;
           document.getElementById('email').value=email;
           document.getElementById('email2').value=email;
           document.getElementById('location').value=loc;
           document.getElementById('approver').value=approver;
           document.getElementById('usertype').value=usertype;
           return true;
       }
   </script>

    <!-- /datepicker -->
</body>

</html>
