<?php
include "header.php";
?>
<style>
    footer{
        bottom: 0px !important;
        left: 15px !important;
        width: 100% !important;
        position: relative ;
    }
</style>
<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Public Holidays
                    </h3>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-holiday-modal"><i class="fa fa-star"></i> Create New Public Holiday </button>
                </div>
                <div class="modal fade bs-holiday-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Create New Public Holidayr</h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>admin/createholiday" method="post">

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">Country <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="country" class="form-control">
                                                <?php $countries = $this->user_model->getCountry();
                                                foreach ($countries->result() as $country)
                                                    echo "<option value=".$country->id.">".$country->name."</option>";
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">Region <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="state" class="form-control">
                                                <option>Select Country</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Date <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="day" class="form-control has-feedback-left" id="day" placeholder="dd/mm/yyyy" required="required">
                                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Holiday Name <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="name" class="form-control has-feedback-left" id="name" placeholder="Some Holiday" required="required">
                                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            &nbsp;
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-ban"></i> Cancel & Close</button>
                                            <button id="send" type="submit" class="btn btn-success"><i class="fa fa-user"></i> Create Holiday</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>


            </div>
            <div class="clearfix"></div>


            <div class="row">
                <!-- form date pickers -->
                <!-- /form datepicker -->

                <!-- form input knob -->
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Australian Public Holidays</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table id="example" class="table table-striped responsive-utilities jambo_table">
                                <thead>
                                <tr class="headings">
                                    <th>Country</th>
                                    <th>State</th>
                                    <th>Date</th>
                                    <th>timestamp</th>
                                    <th class="last">Description</th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $format = 'D jS \of MM, Y';
                                $format = "l jS F Y";
                                $holidays = $this->user_model->getAllHolidays();
                                foreach ($holidays->result() as $holiday) {
                                    ?>
                                    <tr>
                                        <td><?php echo $holiday->country ?></td>
                                        <td><?php echo $holiday->state ?></td>
                                        <td><?php echo date_format(date_create($holiday->date), $format) ?></td>
                                        <td><?php echo date_format(date_create($holiday->date), "U") ?></td>
                                        <td><?php echo $holiday->description ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /form input knob -->

            </div>
        </div>

        <!-- footer content -->

        <!-- /footer content -->

    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>



<!-- Datatables -->
<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>
<script>
    $(document).ready(function () {
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        $('#country').change(function(){
            $('#state').html("<option>Loading ... </option>");
            $.ajax({
                url: '<?php echo base_url() ?>ajax/getRegionsByCountry',
                type: 'post',
                data: {
                    country: this.value
                },
                success: function (data) {
                    $('#state').html(data);
                }
            }).error(function() {
                    alert ('An error occurred');
                });
        });

    });

    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search:"
            },
            "bStateSave": false
            ,
            "aoColumnDefs": [
                {
                    targets: [ 2 ],
                    orderData: [ 3 ]
                },
                {
                    targets: [ 3 ],
                    visible: false
                }
            ],
            "iDisplayLength": 25,
            "sPaginationType": "full_numbers" ,
            "dom": '<"clear">lfrtip'
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });


</script>

<?php include "footer.php" ?>

</body>

</html>