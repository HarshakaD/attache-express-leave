<?php
include "header.php";
?>

<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Leave Approval History <small>(<?php echo $this->session->userdata('name'); ?>)</small>
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                        <?php echo $this->session->userdata('companyname'); ?>
                    </h3>
                </div>
                <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg" >
                        <div class="modal-content" >

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">View Attachment</h4>
                            </div>
                            <div class="modal-body ">
                                <iframe id="modalsrc" src="" width="100%"></iframe>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row">
                <!-- form date pickers -->
                <!-- /form datepicker -->

                <!-- form input knob -->
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Leave Approval History</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table id="example" class="table table-striped responsive-utilities jambo_table">
                                <thead>
                                <tr class="headings">
                                    <th>Staff Member</th>
                                    <th>Leave Type</th>
                                    <th>Dates</th>
                                    <th>Status</th>
                                    <th>Created On</th>
                                    <th class=" no-link last"><span class="nobr">Actioned By</span></th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php

                                $leaveresults = $this->user_model->getLeaveByApprover();
                                $i = 1;
                                if (isset($leaveresults)){
                                    foreach ($leaveresults->result() as $leave) {
                                        if (1 == 1){

                                            $r = "even ";
                                            if ($i & 1) {
//                                                $r = "odd ";
                                            }
                                            $s = "";
                                            if ($leave->status == 2)
                                                $s = "selected ";
                                            else if ($leave->status == 3)
                                                $s = "danger ";
                                            else if ($leave->status == 4)
                                                $s = "info  ";
                                            echo '<tr class="'.$r.' pointer '.$s.'">';

                                            ?>
                                            <td class=" "><?php echo $this->user_model->getUser($leave->user)->row()->name ?></td>
                                            <td class=" ">
                                                <?php
                                                echo $leave->days." days of ".$this->user_model->getLeaveType($leave->leavetype)->row()->description." (".$leave->hours." hours)" ;
                                                if (!is_null($leave->attachment) && $leave->attachment != ""){?>
                                                    <br><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target=".bs-world-modal" onclick="document.getElementById('modalsrc').src ='<?php echo base_url().$leave->attachment ?>'"><i class="fa fa-file"></i> View Attachment</button>
                                                <?php
                                                }
                                                ?>
                                            </td>
                                            <td class=" ">
                                                From: <?php echo $leave->fromdate ?><br>
                                                To&nbsp;&nbsp;: <?php echo $leave->todate ?>
                                            </td>
                                            <td class=" "><?php echo $this->user_model->getLeaveStatus($leave->status)->row()->description ?></td>
                                            <td class="a-right a-right "><?php echo $leave->created ?></td>

                                            <td class=" last">
                                                <?php if (!is_null($leave->approvedby)) echo $this->user_model->getUser($leave->approvedby)->row()->name ?>
                                            </td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                    }
                                }
                                else{ ?>
                                    <tr class="odd pointer selected">
                                        <td colspan="7">You have no leave approvals visible</td>
                                    </tr>
                                <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /form input knob -->

            </div>
        </div>


    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>


<!-- datepicker -->
<script type="text/javascript">
    $(document).ready(function () {

        $('#more').click(function () {
            var newNode = document.createElement('div');
            newNode.innerHTML = newrow(document.getElementById('rowcount').value);
            document.getElementById('leaverow').appendChild(newNode);

            document.getElementById('rowcount').value += 1;

        });

        var cb = function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange_right span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
        }

        var optionSet1 = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2015',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };

        $('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

        $('#reportrange_right').daterangepicker(optionSet1, cb);

        $('#reportrange_right').on('show.daterangepicker', function () {
            console.log("show event fired");
        });
        $('#reportrange_right').on('hide.daterangepicker', function () {
            console.log("hide event fired");
        });
        $('#reportrange_right').on('apply.daterangepicker', function (ev, picker) {
            console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange_right').on('cancel.daterangepicker', function (ev, picker) {
            console.log("cancel event fired");
        });

        $('#options1').click(function () {
            $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
        });

        $('#options2').click(function () {
            $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
        });

        $('#destroy').click(function () {
            $('#reportrange_right').data('daterangepicker').remove();
        });


    });
</script>
<!-- datepicker -->

<!-- /datepicker -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#single_cal4f').daterangepicker({
            singleDatePicker: true,
            format: 'DD/MM/YYYY',
            calender_style: "picker_4"
        }, function (start, end, label) {
            calcBusinessDays();//console.log(start.toISOString(), end.toISOString(), label);
        });
    });

    $(document).ready(function () {
        $('#single_cal4t').daterangepicker({
            singleDatePicker: true,
            format: 'DD/MM/YYYY',
            calender_style: "picker_4"
        }, function (start, end, label) {
            calcBusinessDays();////console.log(start.toISOString(), end.toISOString(), label);
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#reservation').daterangepicker(null, function (start, end, label) {
            calcBusinessDays();//console.log(start.toISOString(), end.toISOString(), label);
        });
    });
</script>
<!-- /datepicker -->
<!-- input_mask -->
<script>
    function calcBusinessDays() {         // input given as Date objects
        var d1 = document.getElementById('single_cal4f').value;
        var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
        var dDate1 = new Date(d1.replace(pattern,'$3-$2-$1'));

        var d2 = document.getElementById('single_cal4t').value;
        var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
        var dDate2 = new Date(d2.replace(pattern,'$3-$2-$1'));

//        var dDate1 =  new Date(document.getElementById('single_cal4f').value);
//        var dDate2 =  new Date(document.getElementById('single_cal4t').value);

        var iWeeks, iDateDiff, iAdjust = 0;

        if (dDate1 == '' || dDate2 == '') return -1;
        if (dDate2 < dDate1) return -1;                 // error code if dates transposed

        var iWeekday1 = dDate1.getDay();                // day of week
        var iWeekday2 = dDate2.getDay();

        iWeekday1 = (iWeekday1 == 0) ? 7 : iWeekday1;   // change Sunday from 0 to 7
        iWeekday2 = (iWeekday2 == 0) ? 7 : iWeekday2;

        if ((iWeekday1 > 5) && (iWeekday2 > 5)) iAdjust = 1;  // adjustment if both days on weekend

        iWeekday1 = (iWeekday1 > 5) ? 5 : iWeekday1;    // only count weekdays
        iWeekday2 = (iWeekday2 > 5) ? 5 : iWeekday2;

        // calculate differnece in weeks (1000mS * 60sec * 60min * 24hrs * 7 days = 604800000)
        iWeeks = Math.floor((dDate2.getTime() - dDate1.getTime()) / 604800000)

        if (iWeekday1 <= iWeekday2) {
            iDateDiff = (iWeeks * 5) + (iWeekday2 - iWeekday1)
        } else {
            iDateDiff = ((iWeeks + 1) * 5) - (iWeekday1 - iWeekday2)
        }

        iDateDiff -= iAdjust                            // take into account both days on weekend

        // return (iDateDiff + 1);                         // add 1 because dates are inclusive
        document.getElementById('duration').value = (iDateDiff + 1);
    }
</script>
<!-- /input mask -->

<!-- Datatables -->
<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>
<script>
    $(document).ready(function () {
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search:"
            },
            "bStateSave": true
            ,
            "aoColumnDefs": [
                {
                    "asSorting": ["desc", "asc"],
                    "aTargets": [2]
                }
            ],
            "iDisplayLength": 25,
            "sPaginationType": "full_numbers" ,
            "dom": '<"clear">lfrtip'
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });

    $('.modal').on('show.bs.modal', function () {
        $('.modal-body').css('height',$( window ).height()*0.75);
        $('#modalsrc').css('height',$( window ).height()*0.75);

    });
</script>

<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>