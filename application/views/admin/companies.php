<?php 
  // maindashboard
  include "header.php";
?>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">

          <?php  
            include "navbar.php";
          ?>

            <!-- page content -->
            <div class="right_col" role="main">
                <br />
                <div class="">
                    <div class="row top_tiles" >
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-desktop"></i>
                                </div>
                                <div class="count"><?php echo $this->onlineusers->total_users(); ?></div>

                                <h3>Users Online</h3>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-building-o"></i>
                                </div>
                                <div class="count"><?php echo $this->admin_model->getCompanyCount(); ?></div>

                                <h3>Companies</h3>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-users"></i>
                                </div>
                                <div class="count"><?php echo $this->admin_model->getUserCount(); ?></div>

                                <h3>Users</h3>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-calendar-o"></i>
                                </div>
                                <div class="count"><?php echo $this->admin_model->getLeaveCount(); ?></div>

                                <h3>Leave Entries</h3>
                            </div>
                        </div>

                    </div>




                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Company Management</h2>
                                    <div class="clearfix"></div>

                                </div>
                                <div class="x_content">

                                    <div class="row" style="border-bottom: 1px solid #E0E0E0; padding-bottom: 5px; margin-bottom: 5px;">
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="selectcompany">Select Company</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="form-control has-feedback-left" name="selectcompany" id="selectcompany">
                                                    <option value="" selected>Select Company</option>
                                                    <?php

                                                    $deleted = false;
                                                    if($this->session->userdata('role') <= 2)
                                                        $deleted = true;
                                                    $companies = $this->admin_model->getCompanies($deleted);

                                                    foreach($companies->result() as $c){
                                                        ?>
                                                        <option value="<?php echo rawurlencode($c->vip) ?>"><?php echo $c->name ?></option>
<!--                                                        <option value="--><?php //echo rawurlencode($c->vip) ?><!--" --><?php //if ($c->id == $company->id) echo "selected" ?><!-->--><?php //echo '['.$this->admin_model->getStatus($c->status).']  '.$c->name ?><!--</option>-->

                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                                <span class="fa fa-check form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>

               <?php 
               include "footer.php";
               ?>

            </div>
            <!-- /page content -->
        </div>


    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url() ?>js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>
    <!-- sparkline -->
    <script src="<?php echo base_url() ?>js/sparkline/jquery.sparkline.min.js"></script>

    <script src="<?php echo base_url() ?>js/custom.js"></script>

    <!-- Datatables -->
    <script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>
   <script>


       var asInitVals = new Array();
       $(document).ready(function () {
           var oTable = $('#table').dataTable({
               "oLanguage": {
                   "sSearch": "Search:"
               },
               'iDisplayLength': 10,
               "sPaginationType": "full_numbers"
               //   "dom": 'T<"clear">lfrtip',
           });

           $("tfoot input").keyup(function () {
               /* Filter on the column based on the index of this element's parent <th> */
               oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
           });
           $("tfoot input").each(function (i) {
               asInitVals[i] = this.value;
           });
           $("tfoot input").focus(function () {
               if (this.className == "search_init") {
                   this.className = "";
                   this.value = "";
               }
           });
           $("tfoot input").blur(function (i) {
               if (this.value == "") {
                   this.className = "search_init";
                   this.value = asInitVals[$("tfoot input").index(this)];
               }
           });


           $('#selectcompany').change(function(){
               var vip = $('#selectcompany').val();
               if(vip != ""){
                   window.location.href = '<?php echo base_url()?>admin/company/'+vip;
               }
           });


           $("#company" ).on ({
               change: function( event,ui ) {
                   $('.table').hide();
                   $('#table'+this.value).show();
               }
           });

       });

       function setmodal(id, name, empid, email, usertype,approver, loc){
           document.getElementById('id').value=id;
           document.getElementById('name').value=name;
           document.getElementById('empid').value=empid;
           document.getElementById('email').value=email;
           document.getElementById('email2').value=email;
           document.getElementById('location').value=loc;
           document.getElementById('approver').value=approver;
           document.getElementById('usertype').value=usertype;
           return true;
       }
   </script>

    <!-- /datepicker -->
</body>

</html>