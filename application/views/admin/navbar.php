<?php 
// Left Nav menu
?>
  <div class="col-md-3 left_col" >
                <div class="left_col scroll-view">

                    <div class="clearfix"></div>


                    <!-- menu prile quick info -->

                    <!-- /menu prile quick info -->


                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul style="list-style-type: none;"><li style="height: 1px !important;"><a>&nbsp;</a></li></ul>
                            <ul class="nav side-menu">

                <?php if ($this->session->userdata('isadmin')){ ?>

                                <li style="height: 8px !important; padding-top: 10px"><div style="width: 90%; margin: auto" class="ln_solid"></div></li>
                                <li style="height: 32px !important;"><a>Site Management</a></li>
                                <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/dashboard"><i class="fa fa-line-chart"></i><span> Summary</span></a></li>
                                <?php
                                if ($this->session->userdata('isadmin') && $this->session->userdata('role') <= '2'){ ?>
                                    <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/help"><i class="fa fa-question-circle"></i><span> Manage Help</span></a></li>
                                    <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/holidays"><i class="fa fa-plane"></i><span> Update Holidays</span></a></li>
                                    <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/countries"><i class="fa fa-globe"></i><span> Update Countries/Regions</span></a></li>
                                    <?php
                                } ?>
                                <li style="height: 8px !important; padding-top: 10px"><div style="width: 90%; margin: auto" class="ln_solid"></div></li>
                                <li style="height: 32px !important;"><a>Company Management</a></li>
                                <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/createcompany"><i class="fa fa-building"></i><span> Create Company</span></a></li>
                                <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/companies"><i class="fa fa-building-o"></i><span> Manage Companies</span></a></li>
                                <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/users"><i class="fa fa-users"></i><span> Manage Users</span></a></li>
                                <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/createuser"><i class="fa fa-user"></i><span> Create User</span></a></li>

                                <li style="height: 8px !important; padding-top: 10px"><div style="width: 90%; margin: auto" class="ln_solid"></div></li>
                                <li style="height: 32px !important;"><a>Rollback / Reversal</a></li>
                                <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/restorebatch"><i class="fa fa-angle-double-left"></i><span> Restore Batch</span></a></li>
                                <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/restoreitem"><i class="fa fa-angle-left"></i><span> Restore Item</span></a></li>

                                <?php if ($this->session->userdata('role') <= '1'){ ?>
                                    <li style="height: 8px !important; padding-top: 10px"><div style="width: 90%; margin: auto" class="ln_solid"></div></li>
                                    <li style="height: 32px !important;"><a>Admin Users</a></li>
                                    <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/admins"><i class="fa fa-user-md"></i><span> Manage Admins</span></a></li>

                                    <li style="height: 8px !important; padding-top: 10px"><div style="width: 90%; margin: auto" class="ln_solid"></div></li>
                                    <li style="height: 32px !important;"><a>License Agreement</a></li>
                                    <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/eula"><i class="fa fa-pencil"></i><span> Version Manageent</span></a></li>
                                    <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/eulareport"><i class="fa fa-pencil"></i><span> Client Agreements</span></a></li>

                                    <li style="height: 8px !important; padding-top: 10px"><div style="width: 90%; margin: auto" class="ln_solid"></div></li>
                                    <li style="height: 32px !important;"><a>Billing</a></li>
                                    <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/billingreport"><i class="fa fa-edit"></i><span> Billing Report</span></a></li>
                                    <li style="height: 32px !important;"><a href="<?php echo base_url() ?>admin/billingplans"><i class="fa fa-gears"></i><span> Manage Plans</span></a></li>
                                <?php
                          }
                }
                if ($this->session->userdata('isadmin') && $this->session->userdata('role') <= '0'){ ?>

                                <li style="height: 8px !important; padding-top: 10px"><div style="width: 90%; margin: auto" class="ln_solid"></div></li>
                                <li style="height: 32px !important;"><a>DB Management</a></li>
                                <li><a href="<?php echo base_url().'admin/db/tables'?>">Tables</a>
                                </li>
                                <li><a href="<?php echo base_url().'admin/db/procs'?>">Procedures</a>
                                </li>
                                <li><a href="<?php echo base_url().'admin/db/funcs'?>">Functions</a>
                                </li>
                                <li><a href="<?php echo base_url().'admin/db/sql'?>">Run SQL</a>
                                </li>
                <?php } ?>

                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav" >

                <div class="nav_menu" style="background-color: darkorange!important">
                    <nav class="" role="navigation" >
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <div class="navbar nav_title" style="border: 0; background-color: darkorange!important">
                            <a href="#" class="site_title"><i class="fa fa-user-md"></i>
                                <span>
                                    Admin Area
                                </span>
                            </a>
                        </div>

                        <ul class="nav navbar-nav navbar-right" style="padding-top: 16px;">

                            <li class="">
                                <a href="<?php echo base_url()?>logout">
                                    <button type="button" class="btn btn-dark btn-sm"><i class="fa fa-sign-out"></i>     Logout     </button>
                                </a>
                            </li>

                            <li><a href="<?php echo base_url() ?>admin/changepassword"> Change Password &nbsp;&nbsp;<i class="fa fa-lock"></i></a>
                            </li>

                            <li><a href="<?php echo base_url() ?>help"> Help  &nbsp;&nbsp;<i class="fa fa-info-circle"></i></a>
                            </li>

                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->
