<?php
include "header.php";

$company = $this->admin_model->getCompany($this->session->userdata('company'))->row();
?>

<body class="nav-md">

    <div class="container body">


        <div class="main_container">

        <?php
        include "navbar.php";
        ?>

            <!-- page content -->
            <div class="right_col" role="main">

                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Leave Types</h3>
                        </div>
                        <div class="modal fade bs-user-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Leave Types</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>admin/addleavetype" method="post">

                                            <div class="item form-group">
                                                <input type="hidden" name="company" id="company" value="<?php echo $company->id ?>">
                                                <input type="hidden" name="id" id="id" value="">
                                            </div>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" name="description" class="form-control has-feedback-left" id="desc" placeholder="Description" required="required">
                                                    <span class="fa fa-font form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Value
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" name="value" class="form-control has-feedback-left" id="value" placeholder="Must Match Values in Attache" data-toggle="tooltip" data-original-title="Leave this Blank for types you DON'T want exported to Attache">
                                                    <span class="fa fa-font form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="notes">Leave Policy Notes
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <textarea type="text" name="notes" class="form-control has-feedback-left" id="notes" placeholder="optional" value=""></textarea>
                                                    <span class="fa fa-newspaper-o form-control-feedback left" aria-hidden="true"></span>
                                                </div>


                                                <div class="item form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        &nbsp;
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-6 col-md-offset-3">
                                                        <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-ban"></i> Cancel & Close</button>
                                                        <button id="send" type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                                                    </div>
                                                </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>



                    <div class="row">
                            <div class="col-md-12">

                                     <div class="panel">

                                         <a class="panel-heading " role="tab" id="heading<?php echo $company->id?>">
                                             <h4 class="panel-title"><?php echo $company->name ?></h4>
                                         </a>
                                         Token: <?php echo $company->token ?>
                                         <div >
                                             <div class="panel-body">

                                        <!-- start project list -->
                                        <table class="table table-striped projects">
                                        <thead>
                                        <tr>
                                            <th>Description</th>
                                            <th>Value</th>
                                            <th>Leave Policy Notes</th>
                                            <th style="width: 20%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php
                                        // for each team in get top teams
                                            $leaveTypes = $this->user_model->getLeaveTypes($company->id);
                                            if (isset($leaveTypes)){
                                                ?>
                                                    <?php
                                                    foreach($leaveTypes->result() as $type) {
                                                        ?>
                                                        <tr id="row<?php echo $type->id ?>">
                                                        <td><?php echo $type->description ?></td>
                                                        <td><?php echo $type->value ?></td>
                                                        <td><?php echo $type->notes ?></td>
                                                        <td>
                                                            <button onclick="setmodal('<?php echo $type->id?>', '<?php echo $type->value?>', '<?php echo $type->description?>', '<?php echo $type->notes?>');"
                                                                type="button" class="btn btn-primary btn-xs"  data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-pencil-square-o"></i> Edit Type </button>
                                                            <button onclick="remtype('<?php echo $type->id?>')"  type="button" class="btn btn-danger  btn-xs"><i class="fa fa-trash"></i> Delete Type </button>
                                                        </td>
                                                        </tr>
                                                    <?php } ?>

                                                <tr>
                                                    <td colspan="4">
                                                        <button onclick="setmodal('','','','')" type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-check"></i> Create New Type </button>
                                                    </td>
                                                </tr>
                                            <?php }?>

                                        </tbody>
                                        </table>
                                        <!-- end project list -->


                                             </div>
                                        </div>
                                    </div>

                            </div>
                    </div>


                    

                </div>


            </div>
            <!-- /page content -->
        </div>

    </div>



    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

    <script src="<?php echo base_url()?>js/custom.js"></script>

    <script src="<?php echo base_url()?>js/validator/validator.js"></script>
    <!-- PNotify -->
    <script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
    <script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
    <script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>

    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

        // bind the validation to the form submit event
        //$('#send').click('submit');//.prop('disabled', true);

        $('form').submit(function (e) {
//            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                return true;
                //this.submit();
            return false;
        });

        /* FOR DEMO ONLY */
        $('#vfields').change(function () {
            $('form').toggleClass('mode2');
        }).prop('checked', false);

        $('#alerts').change(function () {
            validator.defaults.alerts = (this.checked) ? false : true;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);

        function setmodal(id, val, desc, note){
            document.getElementById('id').value=id;
            document.getElementById('desc').value=desc;
            document.getElementById('value').value=val;
            document.getElementById('notes').value=note;
            return true;
        }

        function remtype(id){
//            document.getElementById('did').value=id;
            $.ajax({
                url : "<?php echo base_url()?>/ajax/deleteLeave",
                type: "POST",
                data : {leavetype: id},
                success: function(data){
                    document.getElementById("row"+id).style.display = 'none';
                    new PNotify({ title: 'Delete Leave Type', text: data, type: 'success' });
                },
                error: function(data){
                    new PNotify({ title: 'Delete Leave Type', text: data, type: 'error' });
                }
            });

        }


    </script>

    <!-- footer content -->
    <?php include "footer.php" ?>
    <!-- /footer content -->
</body>

</html>
