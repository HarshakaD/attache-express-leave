<?php
include "header.php";
?>
<link href="<?php echo base_url() ?>fonts/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url() ?>css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
<link href="<?php echo base_url() ?>css/editor/index.css" rel="stylesheet">
<script src="<?php echo base_url() ?>js/html.sortable.src.js"></script>

<!-- select2 -->
<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        if ($this->session->userdata("logged_in"))
            include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main" style="overflow: auto">

            <div class="page-title col-xs-12 col-lg-10" >
                <div class="title_left">
                    <h3>
                        Express Leave - EULA Management
                    </h3>
                </div>


                <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">X</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">View License Agreement</h4>
                            </div>
                            <div class="modal-body ">
                                <iframe id="modalsrc" src="" width="100%"></iframe>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>

                <div class="modal fade bs-user-modal" tabindex="-1" role="dialog" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">License Version</h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal form-label-left" novalidate=""
                                      action="<?php echo base_url() ?>admin/editeula" method="post" enctype="multipart/form-data">

                                    <div class="item form-group">
                                        <input type="hidden" name="id" id="id" value="">
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="summary">Version Number <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" name="version" class="form-control" id="version"
                                                   placeholder="" required="required">
                                            <br><b>Note:</b><br> Major increments (eg 3.x to 4.x) will prompt client
                                            Admin to re-acknowledge the agreement. <br>Minor increments (eg 3.3 to 3.4)
                                            will not.
                                            <br>
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                               for="summary">Status<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control has-feedback-left" name="status" id="status">
                                                <?php
                                                // for each team in get top teams

                                                $statustypes = $this->user_model->getLeaveStatus();
                                                if (isset($statustypes))
                                                    foreach ($statustypes->result() as $type) {

                                                        ?>
                                                        <option value="<?php echo $type->id ?>" <?php
                                                        $new = true; // just for now
                                                        if (!$new) {
                                                            if ($type->id == $company->status) echo 'selected';
                                                        } ?>><?php echo $type->description ?>
                                                        </option>
                                                    <?php } ?>
                                            </select>
                                            <span class="fa fa-suitcase form-control-feedback left"
                                                  aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="item form-group" id="file" style="display: none;">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            License File
                                        </label>
                                        <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="content" value="" readonly>
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            Attach License File (PDF)
                                        </label>
                                        <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="content" value="" readonly style="display: none;">
                                            <input type="file" class="form-control" id="attachment" name="userfile"
                                                   style="border: 1px solid #DDE2E8; line-height: 0px"/>
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            &nbsp;
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <button type="button" class="btn btn-warning" data-dismiss="modal"><i
                                                    class="fa fa-ban"></i> Cancel & Close
                                            </button>
                                            <button id="send" type="submit" class="btn btn-success xcxc"><i
                                                    class="fa fa-user"></i> Click to Save
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>


            <div class="row col-xs-12 col-lg-10">
                <div class="col-xs-12">
                    <div class="x_panel" style="">
                        <div class="x_title">
                            <h2>License Versions</h2>
                            <div class="navbar-right">
                                <button type="button" class="btn btn-primary btn-new" data-toggle="modal"
                                        data-target=".bs-user-modal"><i class="fa fa-star"></i> Add New License
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-striped projects">
                                <thead>
                                <tr>
                                    <th>
                                        Version
                                    </th>

                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Last Edited
                                    </th>

                                    <th>
                                        Link to File
                                    </th>

                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $licenses = $this->user_model->getEula();
                                $format = 'd M Y';
                                foreach ($licenses->result() as $eula) {
                                    ?>

                                    <tr>
                                        <td>
                                            <?php echo $eula->version ?>
                                        </td>

                                        <td>
                                            <?php echo $this->user_model->getLeaveStatus($eula->status)->row()->description ?>
                                        </td>

                                        <td>
                                            <?php echo date_format(date_create($eula->edited), $format) ?>
                                        </td>

                                        <td>
                                            <button type="button" class="btn-link btn-primary btn-xs"
                                                    data-toggle="modal" data-target=".bs-world-modal"
                                                    onclick="document.getElementById('modalsrc').src ='<?php echo base_url() . 'license/' . $eula->content ?>'">
                                                <i class="fa fa-file"></i> <?php echo $eula->content ?>
                                            </button>
                                        </td>

                                        <td>
                                            <button type="button" class="btn btn-primary btn-xs btn-edit"
                                                    tag="<?php echo $eula->id ?>" data-toggle="modal"
                                                    data-target=".bs-user-modal" onclick="setmodal(<?php echo "'".$eula->id."', '".$eula->version."', '".$eula->status."', '".$eula->content."'" ?>)"><i class="fa fa-edit"></i> Edit
                                            </button>

                                        </td>
                                    </tr>

                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>


            </div>


        </div>
    </div>


</div>
<!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
<!-- richtext editor -->
<script src="<?php echo base_url() ?>js/editor/bootstrap-wysiwyg.js"></script>
<script src="<?php echo base_url() ?>js/editor/external/jquery.hotkeys.js"></script>
<script src="<?php echo base_url() ?>js/editor/external/google-code-prettify/prettify.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- PNotify -->
<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>

<script>

    $(document).ready(function () {
        $(".btn-edit").on({
            click: function () {
                var id = this.getAttribute('Tag');
                var type = this.getAttribute('vis');
                var idname1 = "summary" + id;
                var idname2 = "detail" + id;
                document.getElementById('summary').value = document.getElementById(idname1).innerText;
                $('#editor').html(document.getElementById(idname2).innerHTML);
                document.getElementById('id').value = id;
                document.getElementById('usertype').value = type;
                return true;
            }
        });

        $(".btn-new").on({
            click: function () {
                document.getElementById('summary').value = " ";
                $('#editor').html(" ");
                document.getElementById('detail').value = " ";
                document.getElementById('id').value = "";
                return true;
            }
        });



    });
</script>
<!-- editor -->
<script>
    $(document).ready(function () {


    });

    $('.modal').on('show.bs.modal', function () {
        $('.modal-body').css('height', $(window).height() * 0.75);
        $('#modalsrc').css('height', $(window).height() * 0.75);

    });
    function setmodal(id, version, status, content){
        document.getElementById('id').value=id;
        document.getElementById('version').value=version;
        document.getElementById('status').value=status;
        $("#content").val(content);
        $("#file").show();
        return true;
    }

</script>
<!-- /editor -->
<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>
