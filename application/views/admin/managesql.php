<?php
include "header.php";
?>
<script src="<?php echo base_url(); ?>js/datagrid.js"></script>

<style>
    .dg_form table{
        border:1px solid silver;
    }

    .dg_form th{
        background-color:gray;
        font-family:"Courier New", Courier, mono;
        font-size:12px;
    }

    .dg_form td{
        background-color:gainsboro;
        font-size:12px;
    }

    .dg_form input[type=submit]{
        margin-top:2px;
    }
</style>

<body class="nav-md">

    <div class="container body">


        <div class="main_container">

        <?php
        include "navbar.php";
        ?>

            <!-- page content -->
            <div class="right_col" role="main" style="overflow: auto">

                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Tables</h3>
                        </div>

                    </div>
                    <div class="clearfix"></div>


                    <div class="row">
                        <div class="col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>SQL</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <div class="item form-group">
                                        <div class="col-sm-1">
                                        </div>
                                        <div class="col-sm-10 col-xs-12">
                                            <textarea id="sql" class="resizable_textarea form-control" style="width: 100%; height: 74px; overflow: hidden; overflow-y: hidden; word-wrap: break-word;" data-autosize-on="true">
                                            </textarea>
                                        </div>
                                        <div class="col-sm-1">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <div class="col-xs-4">
                                        </div>
                                        <div class="col-xs-4">
                                            <button class="btn btn-success" id="send">Click Me</button>
                                        </div>
                                        <div class="col-xs-4">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row" >
                        <div class="col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Results (if any) </h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" >
                                    <br />
                                    <div id="tabledata">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
                include "footer.php";
                ?>

            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

    <script src="<?php echo base_url()?>js/custom.js"></script>

    <script>
        $( document ).ready(function() {
            // Handler for .ready() called.

            $("#send" ).on ({
                click:function( event,ui ) {
                    var sql = $('#sql').val();
                    $.ajax({
                        url: '<?php echo base_url() ?>ajax/execute',
                        type: 'post',
                        data: {
                            sql: sql
                        },
                        success: function (data) {
                            $('#tabledata').html(data);
                        }
                    }).error(function() {
                            alert ('An error occurred');
                    });

                }
            });

        });



    </script>
    <script>


    </script>

        <script src="<?php echo base_url()?>js/textarea/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>

</body>

</html>