<?php
// maindashboard
include "header.php";
?>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <br />
            <div class="">


                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Restore Leave (Item)</h2>
                                <div class="clearfix"></div>

                            </div>
                            <div class="x_content">
                                <div class="row" style="border-bottom: 1px solid #E0E0E0; padding-bottom: 5px; margin-bottom: 5px;">

                                    <div class="col-xs-12" style="overflow:hidden;">


                                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                                            <thead>
                                            <tr class="headings">
                                                <th>Staff Member</th>
                                                <th>Leave Type</th>
                                                <th>Dates</th>
                                                <th>Status</th>
                                                <th>Created On</th>
                                                <th>Actioned By</th>
                                                <th>Processed</th>
                                                <th class="last">Actions</th>
                                            </tr>
                                            </thead>

                                            <tbody>

                                                <?php
                                                $leaveresults = $this->user_model->getLeaveByBatch($batchID);
                                                $i = 1;
                                                foreach ($leaveresults->result() as $leave) {  ?>
                                                    <tr>
                                                        <td class=" "><?php echo $this->user_model->getUser($leave->user)->row()->name ?></td>
                                                        <td class=" ">
                                                            <?php
                                                            echo $leave->days." days of ".$this->user_model->getLeaveType($leave->leavetype)->row()->description." (".$leave->hours." hours)" ;
                                                            if (!is_null($leave->attachment) && $leave->attachment != ""){?>
                                                                <br><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target=".bs-world-modal" onclick="document.getElementById('modalsrc').src ='<?php echo base_url().$leave->attachment ?>'"><i class="fa fa-file"></i> View Attachment</button>
                                                            <?php
                                                            }
                                                            ?>
                                                        </td>
                                                        <td class=" ">
                                                            From: <?php echo $leave->fromdate ?><br>
                                                            To&nbsp;&nbsp;: <?php echo $leave->todate ?>
                                                        </td>
                                                        <td class=" "><?php echo $this->user_model->getLeaveStatus($leave->status)->row()->description ?></td>
                                                        <td class="a-right a-right "><?php echo $leave->created ?></td>
                                                        <td class=" "><?php echo $this->user_model->getUser($leave->approvedby)->row()->name ?></td>
                                                        <td>
                                                            <?php
                                                            if ($leave->processed) echo "YES";
                                                            else echo "No";
                                                            ?>
                                                        </td>
                                                        <td class=" last">
                                                            <?php
                                                            if ($leave->processed) {?>
                                                                <button type="button" class="btn btn-danger btn-xs itemrestore" Tag="<?php echo $leave->id ?>"><i class="fa fa-recycle"></i> Restore</button>
                                                            <?php } ?>
                                                        </td>

                                                    </tr>
                                                    <?php
                                                } ?>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>




            </div>

            <?php
            include "footer.php";
            ?>

        </div>
        <!-- /page content -->
    </div>


</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>

<!-- chart js -->
<script src="<?php echo base_url() ?>js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>
<!-- sparkline -->
<script src="<?php echo base_url() ?>js/sparkline/jquery.sparkline.min.js"></script>

<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- PNotify -->
<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>

<!-- Datatables -->
<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>
<script>


    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('.otable').dataTable({
            "oLanguage": {
                "sSearch": "Search:"
            },
            'iDisplayLength': 25,
            "sPaginationType": "full_numbers"
            //   "dom": 'T<"clear">lfrtip',
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });

        $("#company" ).on ({
            change: function( event,ui ) {
                if($(this).val() == 'ALL'){
                    $('.tr').show();
                }
                else{
                    $('.tr').hide();
                    $('.table'+this.value).show();
                }
            }
        });

        $(".itemrestore" ).click(function () {
                var id = this.getAttribute('Tag');
                if(confirm("Caution: Please confirm leave item rollback. This cannot be undone.")){
                    $.ajax({
                        url : "<?php echo base_url()?>/ajax/restoreitem",
                        type: "POST",
                        data: {
                            leaveid: id
                        },
                        success: function(data){
                            new PNotify({ title: 'Item Rollback Complete', text: data, type: 'success' });
                        },
                        error: function(data){
                            new PNotify({ title: 'Item Rollback Failed', text: data, type: 'error' });
                        }
                    });
                    $(this).hide();
                }
        });

    });

</script>

<!-- /datepicker -->
</body>

</html>