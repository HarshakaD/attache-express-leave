<?php
include "header.php";
?>
<link href="<?php echo base_url() ?>fonts/css/font-awesome.css" rel="stylesheet">
<!--<link href="--><?php //echo base_url() ?><!--css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">-->
<!--<link href="--><?php //echo base_url() ?><!--css/editor/index.css" rel="stylesheet">-->
<!--<script src="--><?php //echo base_url() ?><!--js/html.sortable.src.js"></script>-->
<link rel="stylesheet" href="<?php echo base_url() ?>css/datatables/css/dataTables.bootstrap.min.css">

<!-- select2 -->
<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        if ($this->session->userdata("logged_in"))
            include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main" style="overflow: auto">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Express Leave - EULA Report
                    </h3>
                </div>
                <div class="navbar-right">
                    <!--                    <button type="button" class="btn btn-primary btn-new" data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-star"></i> Add New License</button>-->
                </div>

                <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">X</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">View License Agreement</h4>
                            </div>
                            <div class="modal-body ">
                                <iframe id="modalsrc" src="" width="100%"></iframe>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>

                <div class="modal fade bs-user-modal" tabindex="-1" role="dialog" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">License Management</h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal form-label-left" novalidate=""
                                      action="<?php echo base_url() ?>admin/editeula" method="post">

                                    <div class="item form-group">
                                        <input type="hidden" name="id" id="id" value="">
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="summary">Change
                                            Type <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="version" class="form-control" id="version"
                                                   placeholder="0.5" required="required">
                                            <br><b>Note:</b><br> Major version changes (eg 3.x to 4.x) require client
                                            Admin to re-acknowledge the agreement. Minor version changes (eg 3.3 to 3.4)
                                            will not.
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                               for="summary">Status<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control has-feedback-left" name="status">
                                                <?php
                                                // for each team in get top teams

                                                $statustypes = $this->user_model->getLeaveStatus();
                                                if (isset($statustypes))
                                                    foreach ($statustypes->result() as $type) {

                                                        ?>
                                                        <option value="<?php echo $type->id ?>" <?php
                                                        $new = true; // just for now
                                                        if (!$new) {
                                                            if ($type->id == $company->status) echo 'selected';
                                                        } ?>><?php echo $type->description ?>
                                                        </option>
                                                    <?php } ?>
                                            </select>
                                            <span class="fa fa-suitcase form-control-feedback left"
                                                  aria-hidden="true"></span>
                                        </div>
                                    </div>


                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            Attach License File (PDF)
                                        </label>
                                        <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="file" class="form-control" id="attachment" name="userfile"
                                                   style="border: 1px solid #DDE2E8; line-height: 0px"/>
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            &nbsp;
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <button type="button" class="btn btn-warning" data-dismiss="modal"><i
                                                    class="fa fa-ban"></i> Cancel & Close
                                            </button>
                                            <button id="send" type="submit" class="btn btn-success xcxc"><i
                                                    class="fa fa-user"></i> Click to Save
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>


            <div class="row col-xs-12 col-lg-10">
                <div class="col-xs-12">
                    <div class="x_panel" style="">
                        <div class="x_title">
                            <h2>License Agreement Approvals</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-striped  responsive-utilities" id="table">
                                <thead>
                                <tr class="headings">
                                    <th>
                                        VIP
                                    </th>
                                    <th>
                                        Company
                                    </th>
                                    <th>
                                        EULA Version
                                    </th>
                                    <th>
                                        Link to File
                                    </th>
                                    <th>
                                        Agreed By
                                    </th>
                                    <th>
                                        Agreed On
                                    </th>
                                    <th>
                                        Agreed From (IP)
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $agreements = $this->user_model->getAgreement();
                                $format = 'd M Y';
                                foreach ($agreements->result() as $agreement) {
                                    $company = $this->user_model->getCompany($agreement->company);
                                    $eula = $this->user_model->geteula($agreement->license)->row();
                                    $user = $this->user_model->getUser($agreement->agreedby)->row();
                                    ?>
                                    <tr>
                                        <td><a href="<?php echo base_url().'admin/company/'.urlencode($company->vip) ?>"><?php echo $company->vip ?></a></td>
                                        <td><a href="<?php echo base_url().'admin/company/'.urlencode($company->vip) ?>"><?php echo $company->name.'<br>'.$this->admin_model->getStatus($company->status) ?></a></td>

                                        <td>
                                            <?php echo $eula->version ?>
                                        </td>
                                        <td>
                                            <button type="button" class="btn-link btn-primary btn-xs"
                                                    data-toggle="modal" data-target=".bs-world-modal"
                                                    onclick="document.getElementById('modalsrc').src ='<?php echo base_url() . 'license/' . $eula->content ?>'">
                                                <i class="fa fa-file"></i> <?php echo $eula->content ?>
                                            </button>
                                        </td>
                                        <td>
                                            <?php echo $user->name.'<br>'.$user->email ?>
                                        </td>
                                        <td>
                                            <?php echo date_format(date_create($agreement->agreedon),$format) ?>
                                        </td>
                                        <td>
                                            <?php echo $agreement->agreedfrom ?>
                                        </td>


                                    </tr>

                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>


            </div>


        </div>
    </div>


</div>
<!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>


<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>

<script>


    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#table').dataTable(
        {
            "oLanguage": {
                "sSearch": "Search:"
            },
            "bSort": true,
            "bAutoWidth": true,
            "stateSave": false,
            "ordering": true,
            "iDisplayLength": 25,
            "sPaginationType": "full_numbers"
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });

    function setmodal(id, name, empid, email, usertype,approver, loc){
        document.getElementById('id').value=id;
        document.getElementById('name').value=name;
        document.getElementById('empid').value=empid;
        document.getElementById('email').value=email;
        document.getElementById('email2').value=email;
        document.getElementById('location').value=loc;
        document.getElementById('approver').value=approver;
        document.getElementById('usertype').value=usertype;
        return true;
    }
</script>
<!-- editor -->
<script>
    $(document).ready(function () {


    });

    $('.modal').on('show.bs.modal', function () {
        $('.modal-body').css('height', $(window).height() * 0.75);
        $('#modalsrc').css('height', $(window).height() * 0.75);

    });


</script>
<!-- /editor -->
<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>
