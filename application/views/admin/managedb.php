<?php
include "header.php";
?>
<script src="<?php echo base_url(); ?>js/datagrid.js"></script>

<style>
    .dg_form table{
        border:1px solid silver;
    }

    .dg_form th{
        background-color:gray;
        font-family:"Courier New", Courier, mono;
        font-size:12px;
    }

    .dg_form td{
        background-color:gainsboro;
        font-size:12px;
    }

    .dg_form input[type=submit]{
        margin-top:2px;
    }
</style>

<body class="nav-md">

    <div class="container body">


        <div class="main_container">

        <?php
        include "navbar.php";
        ?>

            <!-- page content -->
            <div class="right_col" role="main" style="overflow: auto">

                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Tables</h3>
                        </div>

                    </div>
                    <div class="clearfix"></div>


                    <div class="row">
                        <div class="col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Source</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <div class="item form-group">
                                        <div class="col-md-4 col-sm-3">
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <select class="form-control" name="selecttable" id="selecttable">
                                                <option value="null">Select Table</option>
                                                <?php
                                                // for each team in get top teams
                                                $tables = $this->admin_model->getTables();
                                                foreach($tables as $table) {
                                                    ?>
                                                    <option value="<?php echo $table ?>"><?php echo $table ?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-3">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row" >
                        <div class="col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>View & Edit Table Data </h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" >
                                    <br />
                                    <div id="tabledata">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
                include "footer.php";
                ?>

            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

    <script src="<?php echo base_url()?>js/custom.js"></script>

    <script>
        $( document ).ready(function() {
            // Handler for .ready() called.
            var tbl = $('#selecttable').val();

            $("#selecttable" ).on ({
                change:function( event,ui ) {
                    tbl = $(this).val();
                    $.ajax({
                        url: '<?php echo base_url() ?>ajax/getTable',
                        type: 'post',
                        data: {
                            table: tbl
                        },
                        success: function (data) {
                            $('#tabledata').html(data);
//                            $('#loading').hide();
                        }
                    }).error(function() {
                            alert ('An error occurred');
                    });

                }
            });

        });

        function getTable(table, page){
            $.ajax({
                url: '<?php echo base_url() ?>ajax/getTable/'+page,
                type: 'post',
                data: {
                    table: table,
                    page: page
                },
                success: function (data) {
                    $('#tabledata').html(data);
//                            $('#loading').hide();
                }
            }).error(function() {
                    alert ('An error occurred');
                });
        }
    </script>
    <script>

        function setmodal(id, name, world){
            document.getElementById('modalteamid').value=id;
            document.getElementById('modalteamname').value=name;
            document.getElementById('modalteamworld').value=world;

            return true;
        }

        function setmodaluser(id, firstname, surname, email, team){
            document.getElementById('modaluserid').value=id;
            document.getElementById('modaluserfname').value=firstname;
            document.getElementById('modalusersname').value=surname;
            document.getElementById('email').value=email;
            document.getElementById('email2').value=email;
            document.getElementById('modaluserteam').value=team;

            return true;
        }
    </script>
        <script>
            function showEdit(editableObj) {
                $(editableObj).css("background-color","lightyellow");
            }
            function leaveEdit(editableObj) {
                $(editableObj).css("background-color","white");
            }

            function saveToDatabase(editableObj,pk, column,id) {

//                alert('pk='+pk+'column='+column+'&editval='+editableObj.innerHTML+'&id='+id);
                $(editableObj).css("background","url('<?php echo base_url()?>images/loading.gif') no-repeat right");

                $.ajax({
                    url: "<?php echo base_url()?>ajax/updatedb",
                    type: "POST",
                    data: {
                        table: $('#selecttable').val(),
                        column: column,
                        newval: editableObj.innerHTML,
                        pk: pk,
                        pkid: id
                    },
                    success: function(data){
                        if(data == 1){
                            $(editableObj).css({"background-color":"lightgreen"})
                        }
                        else
                            $(editableObj).css({"background-color":"red"})
                    },
                    error: function(e){
                        $(editableObj).css("background-color","darkred");
                        alert(e.toString);
                    }
                });

                $(editableObj).css({"background":""});
                $(editableObj).css({"background-color":"white", "transition":"background 1000ms linear"});

            }





        </script>
</body>

</html>