<?php
include "header.php";
?>

<body class="nav-md">

    <div class="container body">


        <div class="main_container">

        <?php
        include "navbar.php";
        ?>

            <!-- page content -->
            <div class="right_col" role="main">

                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Admin Users</h3>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-building"></i> Create New Admin User </button>
                        </div>

                                <div class="modal fade bs-user-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">Admin User</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>admin/createadmin" method="post">

                                                    <input type="hidden" id="id" name="id" value="">
                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Username <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input type="text" name="username" class="form-control has-feedback-left" id="username" placeholder="username" required="required">
                                                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                        </div>
                                                    </div>

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input type="text" name="name" class="form-control has-feedback-left" id="name" placeholder="Name" required="required">
                                                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                        </div>
                                                    </div>

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input type="email" id="email" name="email" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
                                                            <span class="fa fa-at form-control-feedback left" aria-hidden="true"></span>
                                                        </div>
                                                        <div id="invalid" style="display: none"><i class="fa fa-ban"></i> Already Registered</div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="confirm email">Confirm Email <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input type="email" id="email2" name="confirm_email" data-validate-linked="email" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
                                                            <span class="fa fa-at form-control-feedback left" aria-hidden="true"></span>
                                                        </div>
                                                    </div>

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">Role Type <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <select id="role" name="role" class="form-control">
                                                                <?php $types = $this->admin_model->getAdminType();
                                                                foreach ($types->result() as $type)
                                                                    echo "<option value=".$type->id.">".$type->description."</option>";
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="item form-group">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            &nbsp;
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-3">
                                                            <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-ban"></i> Cancel & Close</button>
                                                            <button id="send" type="submit" class="btn btn-success"><i class="fa fa-user"></i> Create / Update Admin</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Username</td>
                                                <td>Name</td>
                                                <td>Email</td>
                                                <td>Role</td>
                                                <td>Action</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $admins = $this->admin_model->getAdmins();
                                            foreach($admins->result() as $admin) {
                                                echo "<tr>";
                                                    echo "<td>".$admin->username."</td>";
                                                    echo "<td>".$admin->name."</td>";
                                                    echo "<td>".$admin->email."</td>";
                                                    echo "<td>".$this->admin_model->getAdminType($admin->role)->row()->description."</td>";
                                                    echo '<td><button type="button" class="btn btn-primary btn-xs btn-edit" tag="'.$admin->id.'" data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-edit"></i> Edit</button>';
                                                    echo '   <button type="button" class="btn btn-danger btn-xs btn-delete" tag="'.$admin->id.'"><i class="fa fa-remove"></i> Delete</button>';
                                                    echo '</td>';
                                                echo "</tr>";
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                include "footer.php";
                ?>

            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <!-- PNotify -->
    <script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
    <script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
    <script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

    <script src="<?php echo base_url()?>js/custom.js"></script>

    <script src="<?php echo base_url()?>js/validator/validator.js"></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

        // bind the validation to the form submit event
        //$('#send').click('submit');//.prop('disabled', true);

        $('form').submit(function (e) {
//            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                return true;
                //this.submit();
            return false;
        });


        $('#email')
            .on('blur', function () {
                $.ajax({
                    url : "<?php echo base_url()?>/ajax/checkemail",
                    type: "POST",
                    data : {email: this.value},
                    success: function(data, textStatus, jqXHR)
                    {
                        if (data == "TRUE"){
                            $('#invalid').hide();
                            $('#send').prop('disabled', false);
                        }//data - response from server
                        else{
                            $('#invalid').show();
                            $('#send').prop('disabled', true);
                        }
                    },
                    error: function(data)
                    {
                        alert(data);
                    }
                });

            });

        $( document ).ready(function() {
            $(".btn-edit" ).on ({
                click: function () {
                    var id = this.getAttribute('Tag');
                    $.ajax({
                        url : "<?php echo base_url()?>/ajax/admininfo",
                        type: "POST",
                        data : {
                            admin: id
                        },
                        success: function(data){
                            var vals = jQuery.parseJSON(data);
                            document.getElementById('id').value         = vals['id'];
                            document.getElementById('name').value       = vals['name'];
                            document.getElementById('username').value      = vals['username'];
                            document.getElementById('email').value      = vals['email'];
                            document.getElementById('email2').value     = vals['email'];
                            document.getElementById('role').value   = vals['role'];
                            return true;
                        },
                        error: function(data){
                            alert(data);
                            return false
                        }
                    });
                    return true;
                }
            });

            $(".btn-new" ).on ({
                click: function () {
                    document.getElementById('summary').value = " ";
                    $('#editor').html(" ");
                    document.getElementById('detail').value = " ";
                    document.getElementById('id').value = "";
                    return true;
                }
            });

            $(".btn-delete" ).on ({
                click: function( event,ui ) {
                    var id = this.getAttribute('Tag');
                    $.ajax({
                        url: '<?php echo base_url() ?>ajax/deleteAdmin',
                        type: 'post',
                        data: {
                            adminid: id
                        },
                        success: function (data) {
                            new PNotify({ title: 'Admin user Deleted', type: 'success' , text: data});
                        }
                    }).error(function() {
                            alert ('An error occurred');
                        });
                }
            });

        });



        $('#vfields').change(function () {
            $('form').toggleClass('mode2');
        }).prop('checked', false);

        $('#alerts').change(function () {
            validator.defaults.alerts = (this.checked) ? false : true;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);
    </script>
</body>

</html>