<?php
include "header.php";
?>

<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Countries & Regions</h3>
                        <button type="button" class="btn btn-primary newcountry" data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-building"></i> Create New Country </button>
                    </div>

                    <div class="modal fade bs-user-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Country</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>admin/createcountry" method="post">


                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id">Abbreviated Name<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="newid" class="form-control has-feedback-left" id="id" required="required">
                                                <input type="hidden" name="id" value="" id="idh">
                                                <span class="fa fa-code form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="name" class="form-control has-feedback-left" id="name" required="required">
                                                <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-3">
                                                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-ban"></i> Cancel & Close</button>
                                                <button id="send" type="submit" class="btn btn-success"><i class="fa fa-globe"></i> Save </button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal fade bs-region-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Regions</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal form-label-left" novalidate="" action="#" method="post">

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Country <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="country" class="form-control has-feedback-left" id="country" disabled>
                                                <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                            <input type="hidden" name="countryid" value="countrid">
                                        </div>


                                        <div class="item form-group" id="regions" >
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Regions <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <ul id="regiondetails">
                                                    <li>None</li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">New Region (Short Name)<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="short" class="form-control has-feedback-left" id="regionshort" required="required" placeholder="Abbreviation">
                                                <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">New Region (Long Name)<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="region" class="form-control has-feedback-left" id="regionname" required="required" placeholder="Name">
                                                <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-3">
                                                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-sign-out"></i> Close Window</button>
                                                <button type="button" class="btn btn-success newregion"><i class="fa fa-globe"></i> Create Region </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Country ID</th>
                                        <th>Country Name</th>
                                        <th>Regions</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $countries = $this->user_model->getCountry();
                                    foreach($countries->result() as $country) {
                                        echo "<tr>";
                                        echo "<td>".$country->id."</td>";
                                        echo "<td id='".$country->id."'>".$country->name."</td>";
                                        echo "<td>";
                                        $regions = $this->user_model->getRegionsByCountry($country->id);
                                        if (is_object($regions)){
                                            foreach($regions->result() as $region){
                                                echo $region->region." / ".$region->name."<br>";
                                            }
                                        }
                                        echo "</td>";
                                        echo '<td><button type="button" class="btn btn-primary btn-xs btn-edit" tag="'.$country->id.'" data-toggle="modal" data-target=".bs-user-modal"><i class="fa fa-edit"></i> Edit Country</button>';
                                        echo '    <button type="button" class="btn btn-primary btn-xs btn-region" tag="'.$country->id.'" data-toggle="modal" data-target=".bs-region-modal"><i class="fa fa-edit"></i> Edit Regions</button>';
                                        echo '    <button type="button" class="btn btn-danger btn-xs btn-delete" tag="'.$country->id.'"><i class="fa fa-remove"></i> Delete</button>';
                                        echo '</td>';
                                        echo "</tr>";
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            include "footer.php";
            ?>

        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>

<script src="<?php echo base_url()?>js/validator/validator.js"></script>
<script>

    function remregion(rid){
        var cid = document.getElementById('idh').value;

        $.ajax({
            url: '<?php echo base_url() ?>ajax/removeregion',
            type: 'post',
            data: {
                countryid: cid,
                regionid: rid
            },
            success: function (data) {
                document.getElementById('regiondetails').innerHTML = data;
            }
        }).error(function() {
                alert ('An error occurred');
            });
    }



    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
        .on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
//            e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            return true;
        //this.submit();
        return false;
    });


    $( document ).ready(function() {
        $(".btn-edit" ).on ({
            click: function () {
                $('#id').prop("disabled", true);
                var id = this.getAttribute('Tag');
                document.getElementById('id').value = id;
                document.getElementById('idh').value = id;
                document.getElementById('name').value = document.getElementById(id).innerHTML;
                return true;
            }
        });
        $(".btn-region" ).on ({
            click: function () {
                var id = this.getAttribute('Tag');
                $.ajax({
                    url : "<?php echo base_url()?>/ajax/countryinfo",
                    type: "POST",
                    data : {
                        country: id
                    },
                    success: function(data){
                        document.getElementById('id').value = id;
                        document.getElementById('idh').value = id;
                        document.getElementById('country').value = document.getElementById(id).innerHTML;
                        document.getElementById('regiondetails').innerHTML = data;
                        return true;
                    },
                    error: function(data){
                        alert(data);
                        return false
                    }
                });
                return true;
            }
        });

        $(".newcountry" ).on ({
            click: function () {
                $('#id').prop("disabled", false);
                document.getElementById('id').value = '';
                document.getElementById('idh').value = '';
                document.getElementById('name').value = '';
            }
        });



        $(".newregion" ).on ({
            click: function () {
                var id = this.getAttribute('Tag');
                var cid = document.getElementById('idh').value;
                var short = document.getElementById('regionshort').value;
                var name = document.getElementById('regionname').value;

                $.ajax({
                    url: '<?php echo base_url() ?>ajax/createregion',
                    type: 'post',
                    data: {
                        country: cid,
                        short: short,
                        name: name
                    },
                    success: function (data) {
                        document.getElementById('regiondetails').innerHTML = data;
                        document.getElementById('regionshort').value = "";
                        document.getElementById('regionname').value = "";
                    }
                }).error(function() {
                        alert ('An error occurred');
                    });
            }
        });



        $(".btn-delete" ).on ({
            click: function( event,ui ) {
                var id = this.getAttribute('Tag');
                $.ajax({
                    url: '<?php echo base_url() ?>ajax/deletecountry',
                    type: 'post',
                    data: {
                        country: id
                    },
                    success: function (data) {

                    }
                }).error(function() {
                        alert ('An error occurred');
                    });
            }
        });

    });


    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);
</script>
</body>

</html>