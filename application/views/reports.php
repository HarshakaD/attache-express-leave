<?php
// maindashboard
include "header.php";
?>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

        <?php
            include "navbar.php";
          ?>

            <!-- page content -->
        <div class="right_col" role="main">
        <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Marker Reports<br>
                    <small>
                        Market Research Data is available if purchased in submission questions
                    </small>
                </h3>
            </div>

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                        <thead>
                            <tr class="headings">
                                <th>Report Name</th>
                                <th>Publish Date</th>
                                <th>Status </th>
                                <th>Cost </th>
                                <th class=" no-link last"><span class="nobr">Action</span></th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php
                            $months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Jan');

                            for ($i = 0; $i < 4; $i++){
                                if ($i & 1) {
                                    echo  '<tr class="odd pointer selected">';
                                } else {
                                    echo  '<tr class="even pointer selected">';
                                }
                                ?>
                                    <td class=" "><a href="#"><?php echo $months[$i]." ".date("Y") ?> Market Report</a></td>
                                    <td class=" "><a href="#"><?php echo $months[$i+1]." ".date("Y") ?> </a></td>
                                    <td class=" ">Paid</td>
                                    <td class="a-right a-right ">$10,000</td>
                                    <td class=" last"><a href="#">View Report</a></td>
                                </tr>
                                <?php
                            } ?>
                        <?php

                        for ($i = 4; $i < 12; $i++){
                            if ($i & 1) {
                                echo  '<tr class="odd pointer">';
                            } else {
                                echo  '<tr class="even pointer">';
                            }
                            ?>
                            <td class=" "><?php echo $months[$i]." ".date("Y") ?> Market Report</td>
                            <td class=" ">yet to be Published</td>
                            <td class=" ">yet to be Published</td>
                            <td class="a-right a-right ">&nbsp;</td>
                            <td class=" last">&nbsp;</td>
                            </tr>
                        <?php
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <br />
        <br />
        <br />

        </div>
        </div>
        <!-- footer content -->
        <?php include "footer.php" ?>
        <!-- /footer content -->

        </div>
            <!-- /page content -->
        </div>

    </div>
\    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

    <script src="<?php echo base_url()?>js/custom.js"></script>

    <!-- image cropping -->
    <script src="<?php echo base_url()?>js/cropping/cropper.min.js"></script>
    <script src="<?php echo base_url()?>js/cropping/main.js"></script>

    
    <!-- daterangepicker -->
    <script type="text/javascript" src="<?php echo base_url()?>js/moment.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>js/datepicker/daterangepicker.js"></script>
    <!-- moris js -->
    <script src="<?php echo base_url()?>js/moris/raphael-min.js"></script>
    <script src="<?php echo base_url()?>js/moris/morris.js"></script>

    <!-- datepicker -->

    <!-- /datepicker -->
</body>

</html>