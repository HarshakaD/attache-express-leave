<?php
include "header.php";

$company = $this->user_model->getCompany($this->session->userdata('company'));

?>
<!-- style>
    html,
    body {
        margin:0;
        padding:0;
        height:100%;
    }
    #wrapper {
        min-height:100%;
        position:relative;
    }
    #header {
        background:#ededed;
        padding:10px;
    }
    .container {
        padding-bottom:100px; /* Height of the footer element */
    }
    footer {
        position:fixed; 
        bottom:0;
        left:0;
    }
</style -->
<body class="nav-md" xmlns="http://www.w3.org/1999/html">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Audit Report</h3>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12">

                        <div >

                            <?php
                            // for each team in get top teams


                            ?>
                            <div class="panel">

                                <div >
                                    <div class="panel-body">
                                        <p>

                                            <!-- start project list -->
                                        <table class="table table-striped projects" id="example">
                                            <thead>
                                            <tr>
                                                <th>Employee</th>
                                                <th>Requested By</th>
                                                <th>Approved By</th>
                                                <th>Default Approver</th>
                                                <th>Period</th>
                                                <th></th>
                                                <th>Leave Type</th>
                                                <th>Status</th>
                                                <th>Processed</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php
                                            // for each team in get top teams
                                            $allleave = $this->user_model->getLeaveAudit($company->id);
                                            if (isset($allleave)){
                                                foreach($allleave->result() as $leave) {
                                                    $employee = $this->user_model->getUser($leave->user)->row();
                                                    if(!is_null($leave->createdby)){
                                                        $requestor = $this->user_model->getUser($leave->createdby)->row();
                                                    }
                                                    $approvedby = $this->user_model->getUser($leave->approvedby)->row();
                                                    $default = $this->user_model->getUser($employee->defaultapprover)->row();
                                                    if (!$leave->processed && $leave->status ==2){
                                                        ?>

                                                        <tr>
                                                        <td>
                                                            <a><?php echo $employee->name ?></a>
                                                            <br />
                                                            <small>EmployeeID: <?php echo $employee->employeeid ?></small>
                                                        </td>

                                                            <td>
                                                                <?php                                                     if(!is_null($leave->createdby)){
                                                            ?>
                                                                <a><?php echo $requestor->name ?></a>
                                                                <br />
                                                                <small>EmployeeID: <?php echo $requestor->employeeid ?></small>
                                                            <?php } ?>
                                                            </td>

                                                        <td>
                                                            <a><?php echo $approvedby->name ?></a>
                                                            <br />
                                                            <small>EmployeeID: <?php echo $approvedby->employeeid ?></small>
                                                        </td>

                                                        <td>
                                                            <a><?php echo $default->name ?></a>
                                                            <br />
                                                            <small>EmployeeID: <?php echo $default->employeeid ?></small>
                                                        </td>

                                                        <td class=" ">
                                                            From: <?php echo date_format(date_create($leave->fromdate), 'D d M Y') ?><br>
                                                            To&nbsp;&nbsp;: <?php echo  date_format(date_create($leave->todate), 'D d M Y') ?>
                                                        </td>
<td>
<?php echo date_format(date_create($leave->fromdate), 'U') ?>
</td>
                                                        <td class=" ">
                                                            <?php
                                                            echo $leave->days." days of ".$this->user_model->getLeaveType($leave->leavetype)->row()->description." / ".$leave->hours." hours" ;
                                                            if (!is_null($leave->attachment) && $leave->attachment != ""){?>
                                                                <br><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target=".bs-world-modal" onclick="document.getElementById('modalsrc').src ='<?php echo base_url().$leave->attachment ?>'"><i class="fa fa-file"></i> View Attachment</button>
                                                            <?php
                                                            }
                                                            ?>
                                                        </td>
                                                        <td class=" "><?php echo $this->user_model->getLeaveStatus($leave->status)->row()->description ?></td>
                                                        <td class=" last">
                                                            <?php
                                                            if ($leave->processed) echo "YES";
                                                            else echo "No";
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            if (!$leave->processed && $leave->status < 3) { ?>
                                                                <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>user/processleave" method="post">
                                                                    <input type="hidden" name="id" value="<?php echo $leave->id ?>">
                                                                    <div class="form-group">
                                                                        <div class="col-xs-12">
                                                                            <button type="submit" name="reject" class="btn btn-danger btn-xs" ><i class="fa fa-trash"></i> Reject</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>

                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>


                                            </tbody>
                                        </table>
                                        <!-- end project list -->
                                    </div>

                                </div>
                            </div>
                            <?php

                            ?>

                        </div>

                    </div>
                </div>


            </div>


        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>

<script src="<?php echo base_url()?>js/validator/validator.js"></script>

<!-- PNotify -->
<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>


<script>
    var asInitVals = new Array();
    $(document).ready(function () {


        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search:"
            },
            "bStateSave": false,
            "responsive": false,
	    "aoColumnDefs": [
                {
                        "bSortable": false,
                        "aTargets": [6]
                },
                {
                        "iDataSort": 5,
                        "aTargets": [4]
                },
                {
                        "bVisible": false,
                        "aTargets": [5]
                }
            ],
            "bStateSave": false
            ,
            "iDisplayLength": 50,
            "sPaginationType": "full_numbers" ,
            "dom": '<"clear">lfrtip'
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });


    });



    $('.modal').on('show.bs.modal', function () {
        $('.modal-body').css('height',$( window ).height()*0.75);
        $('#modalsrc').css('height',$( window ).height()*0.75);

    });

</script>


<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>
