<?php
include "header.php";
$today = date_format(date_create(), 'd/m/Y');
?>
<!-- style>
    html,
    body {
        margin:0;
        padding:0;
        height:100%;
    }
    #wrapper {
        min-height:100%;
        position:relative;
    }
    #header {
        background:#ededed;
        padding:10px;
    }
    .container {
        padding-bottom:100px; /* Height of the footer element */
    }
    footer {
       position:fixed;
        bottom:0;
        left:0;
    }
</style -->

<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <?php
            include "navbar.php";
            ?>

            <!-- page content -->
            <div class="right_col" role="main">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>
                                Create on Behalf
                            </h3>
                        </div>

                        <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg" >
                                <div class="modal-content" >

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">View Attachment</h4>
                                    </div>
                                    <div class="modal-body ">
                                        <iframe id="modalsrc" src="" width="100%"></iframe>
                                    </div>
                                    <div class="modal-footer">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>


                    <div class="row">
                    <!-- form date pickers -->
                        <div class="col-md-9 col-xs-12">
                            <div class="x_panel" style="">
                                <div class="x_content">

                                    <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>user/addleave" method="post" enctype="multipart/form-data">
                                        <div id="leaverow">
                                            <?php
                                            $message = $this->session->userdata('message'); $this->session->set_userdata(array('message' => ""));
                                            echo '<fieldset>
                                                                        <div class="control-group">
                                                                            <div class="controls"><h3 class="danger flash col-xs-12">'.$message.'</h3>
                                                                        </div>
                                                                  </fieldset>';
                                            //if(isset($message) && $message !="") echo "<div class='error_message'>".$message."</div>"
                                            ?>
                                            <input id="leaveid" name="leaveid" type="hidden" value="">

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                        Employee <span class="required">*</span>
                                                </label>
                                            
                                            
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select  class="form-control" name="user" id="user" required="required" onchange="applydates()">
                                                        <option value="" selected>Select Employee</option>
                                                        <?php
                                                        $users = $this->user_model->getUsers();
                                                        foreach($users->result() as $user) {
                                                            if ($user->status == 2) {
                                                                echo "<option value='" . $user->id . "' tag='" . $user->hours . "'>" . $user->name . "</option>";
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                    Leave Period <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <fieldset>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <div class="input-prepend input-group">
                                                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                                    <input type="text"  name="reservation" id="reservation" class="form-control active" placeholder="dd/mm/yyyy to dd/mm/yyyy" required="required" value="<?php echo $today.' to '.$today ?>">
                                                                </div>
                                                                <input type="hidden" name="fromdate" value="<?php echo $today ?>" id="fromdate">
                                                                <input type="hidden" name="todate" value="<?php echo $today ?>" id="todate">
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                
                                

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                    Leave Type <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <?php $leaveTypes = $this->user_model->getLeaveTypes();
                                                    if (isset($leaveTypes)){
                                                        ?>
                                                        <select class="form-control" name="type" id="type" required="required">
                                                            <option value="" selected>Select Leave Type</option>
                                                            <?php
                                                            foreach($leaveTypes->result() as $type) {
                                                                ?>
                                                                <option value="<?php    echo $type->id ?>"><?php echo $type->description ?></option>

                                                            <?php } ?>
                                                        </select>
                                                    <?php }?>
                                                    <br>
                                                </div>
                                            </div>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                    Number of Work Days
                                                </label>
                                                <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                                        <input id="duration" class="form-control col-xs-12 has-feedback" disabled data-validate-length-range="1"  value="" required="required" type="number">
                                                        <input id="durationd" type="hidden" name="duration" value="">
                                                </div>
                                                <br>
                                            </div>

                                            <div class="item form-group" id="fullday">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                    Number of Hours
                                                </label>
                                                <div class="item form-group  col-md-6 col-sm-6 col-xs-12">
                                                    <input id="hours" class="form-control col-xs-12" data-validate-length-range="1" value="" type="number">
                                                    <input id="hoursh" type="hidden" name="hours" value="">
                                                </div>
                                            </div>

                                            <div class="item form-group" id="halfday" style="display: none">
                                                <label class="control-label col-sm-3 col-xs-6">
                                                    Half Day
                                                </label>
                                                <div class="item form-group col-md-3 col-sm-3 col-xs-6">
                                                    <input type="checkbox" id="half" value="half" class="form-control col-xs-12" name="half">
                                                </div>
                                            </div>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                    Approver <span class="required">*</span>
                                                </label>
                                                <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                                    <?php $approvers = $this->user_model->getApprovers();
                                                    if (isset($approvers)){
                                                        ?>
                                                        <select class="form-control" name="approver" id="approver" required="required">
                                                            <?php
                                                            foreach($approvers->result() as $approver) {
                                                                ?>
                                                                <option value="<?php    echo $approver->id ?>" <?php if($approver->id == $user->defaultapprover) echo 'selected' ?>><?php echo $approver->name ?></option>

                                                            <?php }
                                                            ?>
                                                        </select>
                                                    <?php
                                                    } ?>
                                                    <br>
                                                </div>
                                            </div>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                    Attachment if required
                                                </label>
                                                <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                                    <input type="file" class="form-control" id="attachment" name="userfile" style="border: 1px solid #DDE2E8; line-height: 0px"/>
                                                </div>
                                            </div>


	                                   
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                    Comments
                                                </label>
                                                <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                                    <textarea id="message"  class="form-control" name="comments"></textarea>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                </label>

                                            <div class="col-xs-12 col-sm-6">
                                                <button id="send" type="submit" class="btn btn-primary">Submit</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="location.reload()">Cancel</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>

                            </div>
                        </div>
                    <!-- /form datepicker -->

                    </div>


            </div>
            <!-- /page content -->
        </div>



        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>

    </div>

        <script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

        <!-- bootstrap progress js -->
        <script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
        <script src="<?php echo base_url() ?>js/custom.js"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>


    <script src="<?php echo base_url()?>js/validator/validator.js"></script>
    <script>

            // initialize the validator function
            validator.message['date'] = 'not a real date';

            // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
            $('form')
                .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                .on('change', 'select.required', validator.checkField)
                .on('keypress', 'input[required][pattern]', validator.keypress);

            $('.multi.required')
                .on('keyup blur', 'input', function () {
                    validator.checkField.apply($(this).siblings().last()[0]);
                });

            // bind the validation to the form submit event
            //$('#send').click('submit');//.prop('disabled', true);

            $('form').submit(function (e) {
//            e.preventDefault();
                var submit = true;
		$('#hoursh').val($('#hours').val());
                // evaluate the form using generic validaing
                if (!validator.checkAll($(this))) {
                    submit = false;
                }

                if (submit)
                    return true;
                //this.submit();
                return false;
            });


            $('#vfields').change(function () {
                $('form').toggleClass('mode2');
            }).prop('checked', false);

            $('#alerts').change(function () {
                validator.defaults.alerts = (this.checked) ? false : true;
                if (this.checked)
                    $('form .alert').remove();
            }).prop('checked', false);

            function applydates (start, end, label) {

                var drange;
                var hours;
                var us = document.getElementById('user');
                hours = us.options[us.selectedIndex].getAttribute('tag');
                drange = document.getElementById('reservation').value.split(" to ");
                start = drange[0];
                end = drange[1];
                document.getElementById('fromdate').value = start;
                document.getElementById('todate').value = end;
                $.ajax({
                    url: "<?php echo base_url()?>ajax/getWorkingDays",
                    data: {
                        "employee": us.value,
                        "datef": start,
                        "datet": end
                    },
                    dataType:"html",
                    type: "post",
                    success: function(data){
                        var vals = jQuery.parseJSON(data);
                        var days = vals['days'];
                        document.getElementById('duration').value = days;
                        document.getElementById('durationd').value = days;
//                                if(data == 1){
//                                    document.getElementById('halfday').style.display = 'block';
//                                    document.getElementById('fullday').style.display = 'none';
//                                }
//                                else{
//                                    document.getElementById('halfday').style.display = 'none';
//                                    document.getElementById('fullday').style.display = 'block';
//                                }
                        document.getElementById('hours').value = Math.round(days*hours*100)/100;
                        document.getElementById('hoursh').value = Math.round(days*hours*100)/100;
			if (days > 1) document.getElementById('hours').disabled = true;
			else document.getElementById('hours').disabled  = false;
                    },
                    error: function(data){
                        alert(data);
                    }
                });
            }

            $( document ).ready(function() {
           
            var dateoptions = {
                separator: ' to ',
                startDate: '<?php echo date_format(date_create(), "d/m/Y")?>',
                endDate: '<?php echo date_format(date_create(), "d/m/Y")?>',
                dateLimit: {
                    days: 60
                },
                showDropdowns: false,
                showWeekNumbers: true,
                autoApply: true,
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary',
                cancelClass: 'btn-small',
                format: 'DD/MM/YYYY'
            };

            

            applydates();
            var available = 0;


                $('#reservation').daterangepicker(dateoptions, applydates);
                $('#reservation').on('apply.daterangepicker', applydates);



                $('#destroy').click(function () {
                    $('#reportrange').data('daterangepicker').remove();
                });

                $('input.tableflat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });


                $("#user" ).on ({
                    change:function( event,ui ) {
                        q = $(this).val();
                        $.ajax({
                            url: '<?php echo base_url() ?>ajax/getApprover',
                            type: 'post',
                            data: {
                                user: q
                            },
                            success: function (data) {
                                $('#approver').val(data);
                            }
                        }).error(function() {
                                alert ('An error occurred');
                        });
                        $.ajax({
                            url: '<?php echo base_url() ?>ajax/getHours',
                            type: 'post',
                            data: {
                                user: q
                            },
                            success: function (data) {
                                available = parseFloat(data);
                            }
                        }).error(function() {
                            alert ('An error occurred');
                        });
                    }
                });

                function setdata(id, fromdate, todate, duration, type, approver, hours, half){
                    document.getElementById('leaveid').value=id;
                    document.getElementById('fromdate').value=fromdate;
                    document.getElementById('todate').value=todate;
                    document.getElementById('single_cal4').value=fromdate+" to "+todate;
                    document.getElementById('duration').value=duration;
                    document.getElementById('durationd').value=duration;
                    document.getElementById('hours').value=hours;
                    document.getElementById('hoursh').value=hours;
                    document.getElementById('type').value=type;
                    document.getElementById('approver').value=approver;
                    document.getElementById('attachment').value='';
//                if(duration == 1){
//                        document.getElementById('halfday').style.display = 'block';
//                        document.getElementById('fullday').style.display = 'none';
//                        document.getElementById('half').checked = half;
//                }
//                else{
//                        document.getElementById('halfday').style.display = 'none';
//                        document.getElementById('fullday').style.display = 'block';
//                }
                    return true;
                }

                $('.modal').on('show.bs.modal', function () {
                    $('.modal-body').css('height',$( window ).height()*0.75);
                    $('#modalsrc').css('height',$( window ).height()*0.75);

                });


                $('#hours').on('input', checkhours);
                function checkhours(){
                    var input = $('#hours').val();
                    if (input < 0) $('#hours').val(0);
                    if (input > available) $('#hours').val(available);
                }

            });
        </script>

        <!-- Datatables -->
        <script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>


        <script>



        </script>

    <!-- footer content -->
    <?php include "footer.php" ?>
    <!-- /footer content -->
</body>
</html>
