<?php
    if($this->session->userdata('logged_in'))
    $this->user_model->logUser();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>images/favicon.ico"/>

    <title>
        Express Leave - By Attaché Software
    </title>

    <!-- Bootstrap core CSS -->

    <link href="<?php echo base_url() ?>css/bootstrap.min.css" rel="stylesheet">

    <!-- link href="<?php echo base_url() ?>css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet" -->
    <link href="<?php echo base_url() ?>fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>css/animate.min.css" rel="stylesheet">


    <link href="<?php echo base_url() ?>css/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>css/datatables/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>css/datatables/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>css/datatables/css/colReorder.bootstrap.min.css" rel="stylesheet">

	<!-- Carpark CSS -->
	<link href="<?php echo base_url() ?>css/datatables/css/style.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->

    <link href="<?php echo base_url() ?>css/maps/jquery-jvectormap-2.0.1.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>css/icheck/flat/green.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>css/floatexamples.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>css/custom.css" rel="stylesheet" />

    <script src="<?php echo base_url() ?>js/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.bootstrap.min.js"></script>

    <script src="https://cdn.datatables.net/colreorder/1.3.1/js/dataTables.colReorder.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.0.2/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.0.2/js/responsive.bootstrap.min.js"></script>


    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    <style>
        html,
        .body {
            margin:0;
            padding:0;
            min-height:600px;
            height:100%;
        }
        #wrapper {
            min-height:100%;
            position:relative;
        }
        #header {
            background:#ededed;
            padding:10px;
        }
        .container {
            padding-bottom:100px; /* Height of the footer element */
        }
        footer {
            position:fixed;
            width: 100% !important;
            left:0;
            display: none;
            margin:0;
        }
    </style>

    <script>
        window.onscroll = function(ev) {

            if ((window.innerHeight + window.scrollY) +40 >= document.body.scrollHeight) {
                $('footer').show();
                // you're at the bottom of the page
            }
            else{
                $('footer').hide();
            }
        };
        $(document).ready(function() {
            $('.main_container').attr("style", "min-height:"+ $('#sidebar-menu').height() + "px");
            // Check if body height is higher than window height :)\
            if ((window.innerHeight + window.scrollY) -40 >= document.body.scrollHeight) {
                $('footer').show();
                // you're at the bottom of the page
            }
            else{
                $('footer').hide();
            }



            // Check if body width is higher than window width :)
//        if ($("body").width() > $(window).width()) {
//            alert("Horizontal Scrollbar! D:<");
//        }
        });
    </script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-79596313-1', 'auto');
  ga('send', 'pageview');

</script>


</head>
