<?php
// maindashboard
include "header.php";
?>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <br />
            <div class="">


                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Processed Entries</h2>
                                <div class="clearfix"></div>

                            </div>
                            <div class="x_content">
                                <div class="row" style="border-bottom: 1px solid #E0E0E0; padding-bottom: 5px; margin-bottom: 5px;">

                                    <div class="col-xs-12" style="overflow:hidden;">
                                        <table class="table projects">
                                            <thead>
                                            <tr>
                                                <th>Company</th>
                                                <th>Status</th>
                                                <th>Records / Entries</th>
                                                <th>Date Processed</th>
                                                <th>Processed By</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $company = $this->user_model->getCompany();


                                                    ?>
                                                        <?php
                                                        $batches = $this->user_model->getBatches($company->id);
                                                        foreach($batches->result() as $batch){
                                                            ?>
                                                            <tr class="tr table<?php echo $company->id ?>" >

                                                                <td><a href="<?php echo base_url().'user/company/'.urlencode($company->vip) ?>"><?php echo $company->name ?></a></td>
                                                                <td><?php 
									if ($batch->status == 4)
										echo "Completed";
									else 
										echo $this->user_model->getStatus($batch->status)->description ;
								 ?></td>
                                                                <td class="right"><a class="btn btn-link" href="<?php echo base_url().'user/restoreitem/'.$batch->id ?>" ><?php echo $batch->count ?> Records</a></td>
                                                                <td><?php echo date_format(date_create($batch->dateprocessed), "d M Y h:i:s A") ?></td>
                                                                <td><?php echo $this->user_model->getUser($batch->processedby)->row()->name ?></td>

                                                                <td class="right">
                                                                    <?php if ($batch->status == 2) {?>
                                                                        <button type="button" class="btn btn-primary btn-xs batchcomplete" Tag="<?php echo $batch->id ?>" style="width:100%;max-width:150px" >Mark As Complete</button><br>
                                                                    <?php } ?>
                                                                    <?php if ($batch->status <= 4) {?>
                                                                        <button type="button" class="btn btn-danger btn-xs batchrestore" Tag="<?php echo $batch->id ?>" style="width:100%;max-width:150px"> Restore </button>
                                                                    <?php } ?>
                                                                </td>

                                                            </tr>
                                                        <?php
                                                        }
                                                        ?>
                                                <?php

                                                ?>

                                            </tbody>
                                        </table>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>




            </div>

            <?php
            include "footer.php";
            ?>

        </div>
        <!-- /page content -->
    </div>


</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>

<!-- chart js -->
<script src="<?php echo base_url() ?>js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>
<!-- sparkline -->
<script src="<?php echo base_url() ?>js/sparkline/jquery.sparkline.min.js"></script>

<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- PNotify -->
<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>

<!-- Datatables -->
<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>
<script>


    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('.otable').dataTable({
            "oLanguage": {
                "sSearch": "Search:"
            },
            "bStateSave": false,
            "responsive": false,
            'iDisplayLength': 25,
            "sPaginationType": "full_numbers"
            //   "dom": 'T<"clear">lfrtip',
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });

        $("#company" ).on ({
            change: function( event,ui ) {
                if($(this).val() == 'ALL'){
                    $('.tr').show();
                }
                else{
                    $('.tr').hide();
                    $('.table'+this.value).show();
                }
            }
        });


        $(".batchrestore" ).click(function () {
            var id = this.getAttribute('Tag');
            if(confirm("Caution: Please confirm batch rollback. This cannot be undone.")){
                $.ajax({
                    url : "<?php echo base_url()?>/ajax/restorebatch",
                    type: "POST",
                    data: {
                        batchid: id
                    },
                    success: function(data){
                        new PNotify({ title: 'Batch Rollback Complete', text: data, type: 'success' });
                    },
                    error: function(data){
                        new PNotify({ title: 'Batch Rollback Failed', text: data, type: 'error' });
                    }
                });
                $(this).hide();
            }
        });


        $(".batchcomplete" ).click(function () {
            var id = this.getAttribute('Tag');
            $.ajax({
                url : "<?php echo base_url()?>/ajax/completebatch",
                type: "POST",
                data: {
                    batchid: id
                },
                success: function(data){
                    new PNotify({ title: 'Batch Completed', text: data, type: 'success' });
                },
                error: function(data){
                    new PNotify({ title: 'Batch Failed', text: data, type: 'error' });
                }
            });
            $(this).hide();
        });

    });

</script>

<!-- /datepicker -->
</body>

</html>
