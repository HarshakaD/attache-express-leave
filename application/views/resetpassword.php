<?php
include "header.php";
?>

<body class="nav-md">

<div class="container body">


<div class="main_container">

<?php
if ($this->session->userdata("logged_in"))
    include "navbar.php";
?>

<!-- page content -->
<div class="right_col" role="main">

<div class="page-title">
    <div class="title_left">
        <h3>
<?php 
        if ($this->session->userdata('logged_in')) echo "Change Password";
	else echo "Welcome to Attaché Express Leave";
?>
        </h3>
    </div>
    <div class="title_right">
        <h3>

        </h3>
    </div>

</div>
<div class="clearfix"></div>


<div class="row">
    <!-- form date pickers -->
    <div class="col-xs-12 col-sm-9 ">
        <div class="x_panel" style="">
<?php
 if (!$this->session->userdata('logged_in')){
?>
            <div class="x_title">
                <h2>Confirm your details & create a password<br>
                </h2>
                <div class="clearfix"></div>
            </div>
<?php
}
?>
            <div class="x_content">
                <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>user/changepassword" method="post" >
                    <div id="leaverow">

                        <?php
                        $message = $this->session->userdata('message');
                        echo "<div class='has-error'>".$message."</div>";
                        //if(isset($message) && $message !="") echo "<div class='error_message'>".$message."</div>"
                        ?>

                        <div class="item form-group">
                            <input type="hidden" name="r1" id="r1" value="<?php echo $token ?>">
                            <input type="hidden" name="r2" id="r2" value="<?php echo $password ?>">
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Email
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="email" class="form-control has-feedback-left" id="email" value="<?php echo $this->session->userdata('email'); ?>" disabled>
                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>
                        
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company">Company Name
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control has-feedback-left" name="company" id="company">
                                    <?php // if get list of companies the email adddress belongs to 
                                        foreach($userlist->result() as $user){
                                            $company = $this->user_model->getCompany($user->company);
                                            ?>
                                            <option class="form-control has-feedback-left" value="<?php echo $user->id ?>" <?php if($user->id == $this->session->userdata('id')) echo "selected" ?> ><?php echo $company->name ?></option>
                                        <?php }
                                    ?>
                                </select>
                                <!-- input type="text" name="company" class="form-control has-feedback-left" id="company" value="<?php echo $this->session->userdata('companyname'); ?>" disabled -->
                                <span class="fa fa-building form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="name" class="form-control has-feedback-left" id="name" value="<?php echo $this->session->userdata('name'); ?>" disabled>
                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>
                        
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empid">Employee Code
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="empid" class="form-control has-feedback-left" id="empid" value="<?php echo $this->session->userdata('employeeid'); ?>" disabled>
                                <span class="fa fa-info-circle form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">New Password
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="password" name="password" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
                                <span class="fa fa-lock form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="confirm email">Confirm Password
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="password2" name="confirm_password" data-validate-linked="password" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
                                <span class="fa fa-lock form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button id="send" type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
    <!-- /form datepicker -->


</div>
</div>


</div>
<!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>


<script src="<?php echo base_url()?>js/validator/validator.js"></script>
<script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
        .on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });
        
    $( document ).ready(function() {
        $('#company').change(function(){
            $.ajax({
                    url: "<?php echo base_url()?>ajax/userinfo",
                    data: {
                        "employee": $('#company').val(),
                        "set": true
                    },
                    dataType:"html",
                    type: "post",
                    success: function(data){
                        var vals = jQuery.parseJSON(data);
                        document.getElementById('name').value       = vals['name'];
                        document.getElementById('empid').value      = vals['employeeid'];
                        document.getElementById('email').value      = vals['email'];
                        document.getElementById('r1').value         = vals['r1'];
                        document.getElementById('r2').value         = vals['r2'];
                    },
                    error: function(data){
                        alert(data);
                    }
            });
        });
    });
    
    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
//            e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            return true;
        //this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);
</script>

<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>
