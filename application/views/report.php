<?php
include "header.php";

$company = $this->user_model->getCompany($this->session->userdata('company'));

?>
<!-- style>
    html,
    body {
        margin:0;
        padding:0;
        height:100%;
    }
    #wrapper {
        min-height:100%;
        position:relative;
    }
    #header {
        background:#ededed;
        padding:10px;
    }
    .container {
        padding-bottom:100px; /* Height of the footer element */
    }
    footer {
        position:fixed; 
        bottom:0;
        left:0;
    }
</style -->
<body class="nav-md" xmlns="http://www.w3.org/1999/html">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Filtered Report</h3>
                    </div>
                    <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true"
                         style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span
                                            aria-hidden="true">X</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">View Attachment</h4>
                                </div>
                                <div class="modal-body ">
                                    <iframe id="modalsrc" src="" width="100%"></iframe>
                                </div>
                                <div class="modal-footer">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12">

                        <div >

                            <?php
                            // for each team in get top teams


                            ?>
                            <div class="panel">

                                <div >
                                    <div class="panel-body">
                                        <div class="col-xs-12">
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <div class="item form-group col-md-10 col-sm-11 col-xs-12">
                                                    <label class="control-label col-sm-6 col-xs-12" for="role">
                                                        Add Search Criteria
                                                    </label>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <select name="filters" id="filters" class="form-control">
                                                            <option value="" selected disabled>Select</option>
                                                            <option value="user">Employee</option>
                                                            <option value="approvedby">Approver</option>
                                                            <option value="leavetype">Leave Type</option>
                                                            <option value="status">Status</option>
<!--                                                            <option value="Type">Submission Type</option>-->
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-sm-1">

                                                </div>

                                                <div class="item form-group col-md-10 col-sm-11 col-xs-12">
                                                    <label class="control-label col-sm-6 col-xs-12" for="items" id="itemslabel">

                                                    </label>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <select name="items" id="items" class="form-control">

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-sm-1">
                                                    <input type="button" value="add" id="addbutton" class="btn btn-primary">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">

                                <div >
                                    <div class="panel-body">

                                        <div class="col-xs-12">
                                                    <div id="searchstring" class="col-sm-offset-2">
                                                        <br>
                                                    </div>

                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <div class="item form-group col-md-10 col-sm-11 col-xs-12">
                                            <label class="control-label col-sm-6 col-xs-12">

                                            </label>
                                            <div class="col-sm-6 col-xs-12">
<br>
                                                <input type="button" class="btn btn-xs btn-primary" value="Search" id="searchbutton" style="width: 100px">

                                                <input type="button" class="btn btn-xs btn-default" value="Clear" id="clearbutton" style="width: 100px">

                                                <input type="button" class="btn btn-xs btn-success" value="Download CSV" id="csvbutton" style="width: 100px">

                                            </div>
</div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="panel">

                                <div >
                                    <div class="panel-body" id="results">

                                            <!-- start project list -->
                                        <!-- end project list -->
                                    </div>

                                </div>
                            </div>
                            <?php

                            ?>

                        </div>

                    </div>
                </div>


            </div>


        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>

<script src="<?php echo base_url()?>js/validator/validator.js"></script>

<!-- PNotify -->
<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>


<script>
    var asInitVals = new Array();
    $(document).ready(function () {





    });



    $('.modal').on('show.bs.modal', function () {
        $('.modal-body').css('height',$( window ).height()*0.75);
        $('#modalsrc').css('height',$( window ).height()*0.75);

    });

    $('#addbutton').prop('disabled', true);
    $('#searchbutton').prop('disabled', true);
    $('#csvbutton').hide();


    $('#filters').change(function () {
        $('#addbutton').prop('disabled', true);
        var option = $(this).find('option:selected').val();


        // get the data via ajax
        // add to the items

        $.ajax({
            url: "<?php echo base_url()?>ajax/getReportFilter",
            data: {
                "filter": option
            },
            dataType: "html",
            type: "post",
            success: function (data) {
                $('#items').html(data);
                $('#addbutton').prop('disabled', false);;
            }
        });

    });
    var first = true;
    var firstchild = true;
    var searchitopics = {};
    var searchtext = {};
    var searchitems = {};

    $('#addbutton').click(function () {
        searchitems = {};

        var criteria = $('#filters').find('option:selected').text();
        var criteriavalue = $('#filters').find('option:selected').val();
        var specific = $('#items').find('option:selected').text();
        var specificvalue = $('#items').find('option:selected').val();
        var resultstring = '';

        if(searchitopics[criteriavalue]){
            searchitems = searchitopics[criteriavalue];
            searchitems[specificvalue] = specific;
            searchitopics[criteriavalue] = searchitems;
        }
        else{
            searchitems[specificvalue] = specific;
//            searchitopics.push(criteria);
            searchitopics[criteriavalue] = searchitems;
            searchtext[criteriavalue] = criteria;
        }

        for(var key in searchtext) {
            if(first) resultstring = "Where ";
            else resultstring += " <br>and ";
            first = false;
            resultstring += searchtext[key] + " is ";
            var vals = searchitopics[key];
            firstchild = true;
            for(var val in vals) {
                if(!firstchild) resultstring += " or ";
                resultstring += vals[val];
                firstchild = false;
            }
//            resultstring += " and "

        }


        $('#searchstring').html(resultstring);
        first = true;
        $('#searchbutton').prop('disabled', false);

    })

    $('#clearbutton').click(function () {
        first = true;
        firstchild = true;
        searchitopics = {};
        searchitems = {};
        searchtext = {};
        $('#searchstring').html('Cleared');
        $('#searchbutton').prop('disabled', true);
        $('#csvbutton').hide();

    })

    $('#searchbutton').click(function () {
        var jsonString = JSON.stringify(searchitopics);

        $.ajax({
            url: "<?php echo base_url()?>ajax/getReportData",
            data: {"criteria":jsonString},
            dataType: "json",
            type: "post",
            complete: function (data) {
                $('#results').html(data.responseText);
                formattable();
               // $('#csvbutton').show();
            }
        });

    })

    $('#csvbutton').click(function () {
        var jsonString = JSON.stringify(searchitopics);

        $.ajax({
            url: "<?php echo base_url()?>ajax/getReportDataJSON",
            data: {"criteria":jsonString},
            // dataType: "json",
            type: "post",
            success: function (data) {
                //create file
                //Generate a file name
                fileName = "Attache_Report_Download";
                //this will remove the blank-spaces from the title and replace it with an underscore

                var a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";

                var
                    blob = new Blob([data], {type: "data:text/csv"}),
                    url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = fileName+ ".csv";
                a.click();
                window.URL.revokeObjectURL(url);
            },
            error: function (data) {
                alert("error "+data);
            },
            complete: function (data) {
                alert("complete "+data);
            }
        });

    });

   function formattable() {
       var oTable = $('#example').dataTable({
           "oLanguage": {
               "sSearch": "Search:"
           },
           "aoColumnDefs": [
               {
                   "iDataSort": 5,
                   "aTargets": [4]
               },
               {
                   "iDataSort": 7,
                   "aTargets": [6]
               },
               {
                   "iDataSort": 10,
                   "aTargets": [9]
               },
               {
                   "bVisible": false,
                   "aTargets": [5, 7,  10]
               }
           ],
           "bStateSave": false,
           "responsive": false,
           "iDisplayLength": 50,
           "sPaginationType": "full_numbers" ,
           "dom": '<"clear">lfrtip'
       });

       $("tfoot input").keyup(function () {
           /* Filter on the column based on the index of this element's parent <th> */
           oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
       });
       $("tfoot input").each(function (i) {
           asInitVals[i] = this.value;
       });
       $("tfoot input").focus(function () {
           if (this.className == "search_init") {
               this.className = "";
               this.value = "";
           }
       });
       $("tfoot input").blur(function (i) {
           if (this.value == "") {
               this.className = "search_init";
               this.value = asInitVals[$("tfoot input").index(this)];
           }
       });

   }


</script>


<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>
