<?php
include "header.php";
?>

<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        if ($this->session->userdata("isadmin"))
            include "admin/navbar.php";
        elseif ($this->session->userdata("logged_in"))
            include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main" style="overflow: auto">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Express Leave - License Agreement
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row col-xs-12 col-lg-10">
                <?php
                $format = 'd-M-Y';
                $eula = $this->user_model->getEula(null, true)->row();
                ?>
                    <div class="col-xs-12" id="help<?php echo $eula->id ?>">
                        <div class="x_panel" style="">
                            <div class="x_title">
                                <h2>Version <?php echo $eula->version ?></h2>
                                <h2 class="nav navbar-right panel_toolbox"> Last edited <?php echo date_format(date_create($eula->edited), $format) ?></h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content" style="min-height: 500px; overflow-y: hidden;">
                                <embed src="<?php echo base_url() ?>license/<?php echo $eula->content ?>" width="100%"  height="500px" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">
                            </div>

                        </div>
                    </div>
                    <?php

                ?>
                <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>user/eula" method="post">
                    <input type="hidden" name="eula" value="<?php echo $eula->id ?>">
                    <div class="col-xs-12">
                        <div class="x_panel" style="">
                            <div class="x_title">
                                <h2>Acceptance</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                            <div class="item form-group" >
                                <label class="control-label col-sm-3 col-xs-6">
                                    I Agree on as an authorised representative of <?php echo $this->session->userdata('companyname'); ?>:
                                </label>
                                <div class="item form-group col-md-3 col-sm-3 col-xs-6">
                                    <input type="checkbox" id="agree" value="agree" class="form-control col-xs-12" name="agree" style="width:36px">
                                    &nbsp; &nbsp; <button class="btn btn-primary" type="submit" disabled id="continue">Activate Express Leave</button>
                                </div>

                            </div>

                        </div>
                    </div>
                </form>


                </div>


            </div>
        </div>


    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>

<script>
    $("#agree").click(function() {
        $("#continue").attr("disabled", !this.checked);
    });

</script>

<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>