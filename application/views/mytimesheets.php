<?php
include "header.php";
$user = $this->user_model->getUser($this->session->userdata('id'))->row();
$company = $this->user_model->getCompany($this->session->userdata('company'));
?>
<body class="nav-md" xmlns="http://www.w3.org/1999/html">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        My Timesheets
                        <small>(<?php echo $this->session->userdata('name'); ?>)</small>
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>
                <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">X</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">View Attachment</h4>
                            </div>
                            <div class="modal-body ">
                                <iframe id="modalsrc" src="" width="100%"></iframe>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>


            <?php
            $message = $this->session->userdata('message');
            $this->session->set_userdata(array('message' => ""));
            if (isset($message) && !is_null($message) && $message != "") {
                ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="x_panel">
                            <?php
                            echo "<div class='message'>" . $message . "</div>"
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <div class="row">
                <div class="col-xs-12">
                    <div class="x_panel">
                        <form class="form-horizontal form-label-left" novalidate=""
                              action="<?php echo base_url() ?>user/timesheet" method="post">
                            <div class="x_title">
                                <h2>Create Timesheet</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="item form-group">
                                    <label class="control-label col-lg-1 col-sm-1 col-xs-12">Period:</label>
                                    <div class="item form-group col-lg-5 col-sm-7 col-xs-12">
                                        <select class="form-control" style="max-width:400px" name="handle">
                                            <?php
                                            $thisyear = date_format(date_create(), 'Y');
                                            $thisweek = date_format(date_create(), 'W');
                                            //					$begin = new DateTime(date("l, M jS, Y", strtotime($year."W".$week."1")) ); // First day of week $$ CHANGE TO COMPANY START DAY

                                            $today = new DateTime();
                                            $start = new DateTime(date("l, M jS, Y", strtotime($thisyear . "W" . $thisweek . $company->timesheetstart)));; // First day of week $$ CHANGE TO COMPANY START DAY
                                            $end = new DateTime(date("l, M jS, Y", strtotime($thisyear . "W" . $thisweek . $company->timesheetstart)));; // First day of week $$ CHANGE TO COMPANY START DAY
                                            $start = $start->modify('-6 week');
                                            $end = $end->modify('+3 week');

                                            $interval = new DateInterval('P1W');
                                            $daterange = new DatePeriod($start, $interval, $end);
                                            $str = '';
                                            foreach ($daterange as $date) {
                                                // if not already inthe timesheet table, then ...
                                                if ($this->user_model->timesheetExists($user->id, $date->format("W"), $date->format("Y"))) {
                                                    // if timesheet already existsi, dont ad it to the 'create new timesheet' list

                                                } else {
                                                    $t = '<option value="' . $date->format("YW") . '"';
                                                    if ($date->format("W") == $thisweek) $t .= ' selected ';
                                                    $t .= '>';
                                                    $t .= $date->format("l d/m/Y") . ' to ' . $date->modify('+6 day')->format("l d/m/Y") . '</option>';
                                                    $str = $t . $str;
                                                }
                                            }
                                            echo $str;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="item form-group col-lg-6 col-sm-4 col-xs-12">
                                        <input type="submit" class="btn btn-primary" value="Create New Timesheet">
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>


                <div class="row">
                    <!-- form input knob -->
                    <div class="col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>My Timesheet History</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <table id="example" class="table table-striped  responsive-utilities">
                                    <thead>
                                    <tr class="headings">
                                        <th>Period</th>
                                        <th>fromdate</th>
                                        <th>Details</th>
                                        <th>Status</th>
                                        <th class="hidden-xs">Approver</th>
                                        <th class="hidden-xs">Created On</th>
                                        <th>Created</th>
                                        <th class=" no-link last"><span class="nobr">Action</span></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php
                                    $format = 'd M Y';
                                    $format2 = 'd/m/Y';


                                    // get timesheets by user
                                    $timesheets = $this->user_model->getTimesheetsByUser();

                                    if ($timesheets->num_rows() > 0) {
                                        foreach ($timesheets->result() as $timesheet) {
                                            $colWeek = '' . $timesheet->week;
                                            $colFrom_Sort = '';
                                            $colType = '';
                                            $colStatus = '';
                                            $colApprover = '';
                                            $colCreated = '';
                                            $colCreated_Sort = '';
                                            $colAction = '';

                                            $editbutton = false;
                                            $cancelbutton = false;
                                            $attachment = false;
                                            $comments = false;

                                            $leaveresults = $this->user_model->getLeaveByTimesheet($timesheet->id);
                                            $i = 1;

//                                            $fromdate = new DateTime(date("l, M jS, Y", strtotime($timesheet->year . "W" . $timesheet->week . $company->timesheetstart))); // First day of week $$ CHANGE TO COMPANY START DAY
//                                            $todate = new DateTime(date("l, M jS, Y", strtotime($timesheet->year . "W" . $timesheet->week . $company->timesheetstart)));;
//                                            $todate = $todate->modify('+6 days');    // need to add one more day

//                                            $fromdate = date_create($leave->fromdate);
//                                            $todate = date_create($leave->todate);

                                            $r = "even ";


                                            if (isset($leaveresults) && $leaveresults->num_rows() > 0) {
                                                $first_entry = $leaveresults->row();
                                                $fromdate = date_create($first_entry->fromdate);
                                                $todate = date_create($first_entry->todate);

                                                $colWeek = date_format($fromdate, 'D ' . $format) . ' to ' . date_format($todate, 'D ' . $format);
                                                $colFrom_Sort = date_format($fromdate, 'U');

                                                $future = false; // assume the date is in the past
                                                if (date_format($todate, 'U') >= time()) $future = true;


                                                foreach ($leaveresults->result() as $leave) {

                                                    $colCreated_Sort = date_format(date_create($leave->created), 'U');
                                                    $colCreated = date_format(date_create($leave->created), $format);

                                                    if (!$attachment && !is_null($leave->attachment) && $leave->attachment != "") {
                                                        $colWeek .= '<br>
                                                        <button type="button" class="btn btn-primary btn-xs"
                                                                data-toggle="modal" data-target=".bs-world-modal"
                                                                onclick="document.getElementById(\'modalsrc\').src =\'' . base_url() . $leave->attachment . '\'">
                                                            <i class="fa fa-file"></i> View Attachment
                                                        </button>';
                                                        $attachment = true;
                                                    }
                                                    if (!$comments && !is_null($leave->comments) && $leave->comments != "") {
                                                        $colWeek .= '<br>
                                                        <button type="button" class="btn btn-default btn-xs"
                                                                data-toggle="tooltip" data-placement="top" title
                                                                data-original-title="' . $leave->comments . '"
                                                                onclick="alert(\'' . $leave->comments . '\')"><i
                                                                class="fa fa-comment"></i> View Comments
                                                        </button>';
                                                        $comments = true;
                                                    }


                                                    $type = $this->user_model->getLeaveType($leave->leavetype);
                                                    if (isset($type) && $type->num_rows() > 0) {
                                                        $colType .= $leave->hours . " hours of ";
                                                        $colType .= $type->row()->description . '<BR>';
                                                    }
                                                    $status = $this->user_model->getLeaveStatus($leave->status);
                                                    if (isset($status) && $status->num_rows() > 0)
                                                        $colStatus = $status->row()->description;
                                                    $colApprover = $this->user_model->getUser($leave->approver)->row()->name;


                                                    if ($leave->status < 2 && !$leave->processed && !$editbutton) { // awaiting approval or cancelled
                                                        $editbutton = true;
                                                    }
//                                                    if ($leave->status < 2 && !$leave->processed && !$cancelbutton) {
                                                    $cancelbutton = false;
//                                                    }

                                                    $i++;
                                                }
                                            } else {
                                                // the timesheet exists in the timesheet table, but no entries are in the
                                                // if within 6 weeks, allow edit
                                                $colType = 'No Entries';
                                                $fromdate = new DateTime();
                                                $fromdate->setISODate($timesheet->year,$timesheet->week,$company->timesheetstart);

                                                $todate = new DateTime();
                                                $todate->setISODate($timesheet->year,$timesheet->week,$company->timesheetstart);
                                                $todate = $todate->modify('+6 days');

                                                $colWeek = date_format($fromdate, 'D ' . $format) . ' to ' . date_format($todate, 'D ' . $format);
                                                $dateinterval = $fromdate->diff($today);
                                                if ($dateinterval->days < 42) // less than than 6 weeks ago (42 days)
                                                    $editbutton = true;
                                            }


                                            $colAction = '
                                                        <div class="form-group">
                                                            <div class="col-xs-12">
                                                                <form class="form-horizontal form-label-left"
                                                                      novalidate=""
                                                                      action="' . base_url() . 'user/edittimesheet"
                                                                      method="post">';

                                            if ($editbutton || $cancelbutton) {
                                                $colAction .= '<input type="hidden" name="id"
                                                                           value="' . $timesheet->id . '">
                                                                    <div class="form-group">
                                                                        <div class="col-xs-12">';
                                                if ($editbutton) {
                                                    $colAction .= '<button type="submit" name="edit"
                                                                       class="btn btn-primary btn-xs"
                                                                       style="width:100%;max-width:150px; min-width: 150px">
                                                                       Edit
                                                                       </button><br>';
                                                }
                                                if ($cancelbutton) {
                                                    $colAction .= '<button type="submit" name="cancel"
                                                                        class="btn btn-danger btn-xs"
                                                                        style="width:100%;max-width:150px;min-width: 150px">
                                                                        Cancel
                                                                        </button><br>';
                                                }


                                            }
                                            if(!$editbutton){
                                                $colAction .= '<button type="submit" name="view"
                                                                        class="btn btn-default btn-xs"
                                                                        style="width:100%;max-width:150px;min-width: 150px">
                                                                        View
                                                                        </button><br>';
                                            }

                                            $colAction .= '
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>';

                                            echo '<tr>';
                                            echo '<td style="white-space: nowrap">';
                                            echo $colWeek;
                                            echo '</td>';
                                            echo '<td>';
                                            echo $colFrom_Sort;
                                            echo '</td>';
                                            echo '<td style="white-space: nowrap">';
                                            echo $colType;
                                            echo '</td>';
                                            echo '<td style="white-space: nowrap">';
                                            echo $colStatus;
                                            echo '</td>';
                                            echo '<td style="white-space: nowrap">';
                                            echo $colApprover;
                                            echo '</td>';
                                            echo '<td style="white-space: nowrap">';
                                            echo $colCreated;
                                            echo '</td>';
                                            echo '<td>';
                                            echo $colCreated_Sort;
                                            echo '</td>';
                                            echo '<td>';
                                            echo $colAction;
                                            echo '</td>';
                                            echo '</tr>';
                                        }

                                    }

                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /form input knob -->

                </div>

            </div>


        </div>
        <!-- /page content -->
    </div>


    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>
    <link rel="stylesheet" href="<?php echo base_url() ?>css/datatables/css/dataTables.bootstrap.min.css">
    <script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
    <script src="<?php echo base_url() ?>js/custom.js"></script>

    <script>
        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Search:"
                },
                "bStateSave": false,
                "responsive": false,
                "aaSorting": [1, "desc"],
                "aoColumnDefs": [
                    {
                        "aaSorting": [1, "desc"]
//                        "asSorting": ["desc", "asc"],
//                        "aTargets": [6]
                    }, //disables sorting for column one
                    {
                        "bSortable": false,
                        "aTargets": [7]
                    }, //disables sorting for column one
                    {
                        "bVisible": false,
                        "aTargets": [1, 6]
                    },
                    {
                        "iDataSort": 6,
                        "aTargets": [5]
                    },
                    {
                        "iDataSort": 1,
                        "aTargets": [0]
                    }

                ],
                "iDisplayLength": 10,
                "sPaginationType": "full_numbers",
                "dom": '<"clear">lfrtip'
            });

            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });


        $('.modal').on('show.bs.modal', function () {
            $('.modal-body').css('height', $(window).height() * 0.75);
            $('#modalsrc').css('height', $(window).height() * 0.75);

        });

    </script>

    <!-- footer content -->
    <?php include "footer.php" ?>
    <!-- /footer content -->
</body>
</html>
