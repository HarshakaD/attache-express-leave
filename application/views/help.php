<?php
include "header.php";
?>

<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        if ($this->session->userdata("isadmin"))
            include "admin/navbar.php";
        elseif ($this->session->userdata("logged_in"))
            include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main" style="overflow: auto">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Express Leave - Help
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row col-xs-12 col-lg-10">
                <?php
                $helpitems = $this->user_model->getHelp();
                                
                foreach($helpitems->result() as $help){
                    if(($help->order == 0) && ($help->visibleto == 9 || ($this->session->userdata('logged_in') && $this->session->userdata('usertype') <= $help->visibleto))){
                    ?>
                    <div class="col-xs-12" id="help<?php echo $help->id ?>">
                        <div class="x_panel" style="">
                            <div class="x_title">
                                <h2><?php echo $help->summary?></h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <?php echo $help->detail ?>
                            </div>

                        </div>
                    </div>
                    <?php
                    }
                }
                ?>
                <div class="col-xs-12">
                    <div class="x_panel" style="">
                        <div class="x_title">
                            <h2>Contents</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <ul>
                                <?php
                                    foreach($helpitems->result() as $help){
                                        if(($help->order != 0) && ($help->visibleto == 9 || ($this->session->userdata('logged_in') && $this->session->userdata('usertype') <= $help->visibleto)))
                                        echo '<li><a href="#help'.$help->id.'">'.$help->summary.'</a></li>';
                                    }
                                ?>
                            </ul>
                        </div>

                    </div>
                </div>

                <?php
                foreach($helpitems->result() as $help){
                    if(($help->order != 0) &&  ($help->visibleto == 9 || ($this->session->userdata('logged_in') && $this->session->userdata('usertype') <= $help->visibleto))){
                    ?>
                    <div class="col-xs-12" id="help<?php echo $help->id ?>">
                        <div class="x_panel" style="">
                            <div class="x_title">
                                <h2><?php echo $help->summary?></h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <?php echo $help->detail ?>
                            </div>

                        </div>
                    </div>
                    <?php
                    }
                }
                ?>

                </div>


            </div>
        </div>


    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>

<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>