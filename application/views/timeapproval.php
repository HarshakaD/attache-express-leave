<?php
include "header.php";
?>

<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="">
                    <h3>
                        Timesheets Pending Approval
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>
                <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">X</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">View Attachment</h4>
                            </div>
                            <div class="modal-body ">
                                <iframe id="modalsrc" src="" width="100%"></iframe>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row">
                <!-- form date pickers -->
                <!-- /form datepicker -->
                <?php
                $leaveresults = $this->user_model->getTimeByApprover();
                $i = 1;
                ?>
                <!-- form input knob -->


                <div class="x_panel hidden-xs">
                    <div class="x_content">
                        <table id="example" class="table table-striped responsive-utilities">
                            <thead>
                            <tr class="headings">
                                <th>Employee</th>
                                <th>Period</th>
                                <th></th>
                                <th>Details</th>
                                <th>Status</th>
                                <th>Created On</th>
                                <th></th>
                                <th>Action</th>
                                <th class=" no-link last"><span class="nobr">Reassign</span></th>
                            </tr>
                            </thead>

                            <tbody>

                            <?php
                            $counter = 0;
                            $rows = 0;

// get distincy timesheet values from leave where pending approval (status = 1).
// each of these is a row
// for each timesheet (with unapproved items)
// display the timesheet details, and each item, along with approve / reject / reassign
$timesheets = $this->user_model->getUnapprovedTimesheetsByApprover();

                            $format = 'd M Y';
                            $format2 = 'd/m/Y';

                            foreach ($timesheets->result() as $leave){
                                $timesheetID = $leave->timesheet;
                                ?>
                                <tr>
                                    <td class=" ">
                                        <?php
                                        $requestor = $this->user_model->getUser($leave->user)->row();
                                        echo $requestor->name;
                                        ?>
                                    </td>
                                    <td class=" " >
                                        <?php
                                        $fromdate = date_create($leave->fromdate);
                                        $todate = date_create($leave->todate);
                                        echo date_format($fromdate, 'D ' . $format).' to ' . date_format($todate, 'D ' . $format);
                                        if (!is_null($leave->attachment) && $leave->attachment != "") {
                                            ?>
                                            <br>
                                            <button type="button" class="btn btn-primary btn-xs"
                                                    data-toggle="modal" data-target=".bs-world-modal"
                                                    onclick="document.getElementById('modalsrc').src ='<?php echo base_url() . $leave->attachment ?>'">
                                                <i class="fa fa-file"></i> View Attachment
                                            </button>
                                            <?php
                                        }
                                        if (!is_null($leave->comments) && $leave->comments != "") {
                                            ?>
                                            <br>
                                            <button type="button" class="btn btn-default btn-xs"
                                                    data-toggle="tooltip" data-placement="top" title
                                                    data-original-title="<?php echo $leave->comments ?>"
                                                    onclick="alert('<?php echo $leave->comments ?>')"><i
                                                    class="fa fa-comment"></i> View Comments
                                            </button>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php echo date_format(date_create($leave->fromdate), 'U'); ?>
                                    </td>
                                    <td>

                                <?php
                                // get leave by timesheet
                                $leaveresults = $this->user_model->getLeaveByTimesheet($timesheetID);

                                foreach ($leaveresults->result() as $entry) {
                                    echo number_format($entry->hours, '1') . " hour(s)";

                                    $requesttype = $this->user_model->getLeaveType($entry->leavetype)->row();
                                    echo " of " . $requesttype->description  ?>
                                    <br>
                                    <?php
                                }
                                ?>
                                </td>
                                    <td>
                                    <?php echo $this->user_model->getLeaveStatus($entry->status)->row()->description ?>
                                    </td>
                                    <td class="a-right a-right "><?php echo date_format(date_create($leave->created), $format) ?></td>
                                    <td>
                                        <?php echo date_format(date_create($leave->created), 'U'); ?>
                                    </td>

                                    <td class=" ">
                                        <form class="form-horizontal form-label-left" novalidate=""
                                              action="<?php echo base_url() ?>user/processtime" method="post">
                                            <input type="hidden" name="id" value="<?php echo $timesheetID ?>">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <?php if ($leave->status == 1) { // awaiting approval ?>

                                                        <button type="submit" name="approve"
                                                                class="btn btn-success btn-xs"
                                                                style="width: 100%; max-width:150px">Approve
                                                        </button>

                                                        <button type="submit" name="edit"
                                                                class="btn btn-primary btn-xs"
                                                                style="width:100%;max-width:150px; min-width: 150px">
                                                            Review
                                                        </button>

                                                    <?php }
                                                    if ($leave->status < 3) { ?>

<!--                                                        <button type="submit" name="reject"-->
<!--                                                                class="btn btn-danger btn-xs"-->
<!--                                                                style="width: 100%; max-width:150px">Reject-->
<!--                                                        </button>-->

                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                    <td class=" last">
                                        <?php
                                        if ($leave->status == 1) { // awaiting approval
                                            $approvers = $this->user_model->getApprovers();
                                            if (isset($approvers)) {
                                                ?>
                                                <select class="form-control input-sm reassign" name="approver"
                                                        tag="<?php echo $leave->id ?>">
                                                    <option value="">Reassign to ...</option>
                                                    <?php
                                                    foreach ($approvers->result() as $approver) {
                                                        ?>
                                                        <option
                                                            value="<?php echo $approver->id ?>" <?php //if($approver->id == $user->defaultapprover) echo 'selected' ?> ><?php echo $approver->name ?></option>

                                                    <?php }
                                                    ?>
                                                </select>
                                                <?php
                                            }
                                        } else {
                                            echo 'NA';
                                        }
                                        ?>
                                    </td>
                                </tr>










                                        <?php
                                }
                             ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /form input knob -->

        </div>
    </div>


</div>
<!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- daterangepicker -->


<!-- /input mask -->

<!-- Datatables -->
<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>
<!-- PNotify -->
<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>

<script>
    $(document).ready(function () {
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search:"
            },
            "bStateSave": false,
            "responsive": false,
            "aoColumnDefs": [
                {
                    "bVisible": false,
                    "aTargets": [2, 6]
                },
                {
                    "iDataSort": 2,
                    "aTargets": [1]
                },
                {
                    "iDataSort": 4,
                    "aTargets": [5]
                }

            ],

            'iDisplayLength': 10,
            "sPaginationType": "full_numbers"

            //   "dom": 'T<"clear">lfrtip',
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });

    $('.modal').on('show.bs.modal', function () {
        $('.modal-body').css('height', $(window).height() * 0.75);
        $('#modalsrc').css('height', $(window).height() * 0.75);

    });

    $('.reassign').change(function () {
        $.ajax({
            url: "<?php echo base_url()?>ajax/reassign",
            data: {
                "leave": this.getAttribute('Tag'),
                "approver": this.value
            },
            dataType: "html",
            type: "post",
            success: function (data) {
                new PNotify({title: 'Reassignment', text: data, type: 'info'});
            },
            error: function (data) {
                new PNotify({title: 'Reassignment', text: data, type: 'error'});
            }
        });
    });

</script>

<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>
