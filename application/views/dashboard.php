<?php
include "header.php";
$user = $this->user_model->getUser($this->session->userdata('id'))->row();

?>
<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        My Leave
                        <small>(<?php echo $this->session->userdata('name'); ?>)</small>
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>
                <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">X</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">View Attachment</h4>
                            </div>
                            <div class="modal-body ">
                                <iframe id="modalsrc" src="" width="100%"></iframe>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>


            <?php
            // ajax call to do the aosync?
            $message = $this->session->userdata('message');
            $this->session->set_userdata(array('message' => ""));
            if (isset($message) && !is_null($message) && $message != "") {
                ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="x_panel">
                            <?php
                            echo "<div class='message'>" . $message . "</div>"
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <div class="row">
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>My Leave Balance(s)
                                <small>as at
                                </small> <?php echo date_format(date_create($user->leaveupdated), "d M Y") ?> </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table responsive-utilities">
                                <thead>
                                <tr class="headings">
                                    <th style="width:30%">Leave Type</th>
                                    <th>Balance</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php


                                $mapping = $this->user_model->getLeaveMapping();
                                $types = $this->user_model->getLeaveTypes();

                                foreach ($mapping->result() as $map) { // leave type
                                    $balanceh = "Not Visible";
                                    $balanced = "Not Visible";
                                    $leavedesc = "";
                                    $show = false;
                                    // get the leave types for the company
                                    // the leave type has visiability <= current user type
                                    foreach ($types->result() as $type) { // leave type
                                        if ($map->id == $type->balancemap && $type->visibleto >= 4) {
                                            $show = true;
                                            $balanceh = $user->{$map->userfield};
                                            $balanced = number_format($balanceh / $user->hours, 1);
                                            $leavedesc .= ' / ' . $type->description;
                                        }
                                    }
                                    if ($show) { ?>
                                        <tr>
                                            <th><?php echo substr($leavedesc, 3) ?></th>
                                            <td><?php
                                                echo $balanced . " day";
                                                if ($balanced > 1) echo "s";
                                                echo " / " . $balanceh . " hour";
                                                if ($balanceh > 1) echo "s";
                                                ?></td>
                                        </tr>
                                        <?php

                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <!-- form input knob -->
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>My Leave Request History</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table id="example" class="table table-striped  responsive-utilities">
                                <thead>
                                <tr class="headings">
                                    <th>Leave Period</th>
                                    <th>fromdate</th>
                                    <th>Leave Type</th>
                                    <th>Status</th>
                                    <th class="hidden-xs">Approver</th>
                                    <th>Created By</th>
                                    <th class="hidden-xs">Created On</th>
                                    <th>Created</th>
                                    <th class=" no-link last"><span class="nobr">Action</span></th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $format = 'd M Y';
                                $format2 = 'd/m/Y';

                                $leaveresults = $this->user_model->getLeaveByUser();
                                $i = 1;
                                if (isset($leaveresults)) {
                                    foreach ($leaveresults->result() as $leave) {

                                        $fromdate = date_create($leave->fromdate);
                                        $todate = date_create($leave->todate);
                                        $future = false; // assume the date is in the past
                                        if (date_format($todate, 'U') >= time()) $future = true;


                                        $r = "even ";
                                        if ($i & 1) {
//                                        $r = "odd ";
                                        }
                                        $s = "";
                                        if ($leave->status == 2)
                                            $s = "selected ";
                                        else if ($leave->status == 3)
                                            $s = "danger ";
                                        else if ($leave->status == 4)
                                            $s = "info  ";
                                        echo '<tr class="">'; //'.$r.' pointer '.$s.'">';
                                        ?>
                                        <td class="  ">

                                            <?php echo date_format($fromdate, 'D ' . $format) ?>
                                            to <?php echo date_format($todate, 'D ' . $format) ?>
                                        </td>
                                        <td class="  ">
                                            <?php echo date_format($fromdate, 'U') ?>
                                        </td>
                                        <td class=" ">
                                            <?php
                                            if ($leave->hours < $user->hours)
                                                echo $leave->hours . " hours of " . $this->user_model->getLeaveType($leave->leavetype)->row()->description;
                                            else {
                                                echo $leave->days;
                                                if ($leave->days > 1)
                                                    echo " days of ";
                                                else
                                                    echo " day of ";
                                                echo $this->user_model->getLeaveType($leave->leavetype)->row()->description . " (" . $leave->hours . " hours)";
                                            }
                                            if (!is_null($leave->attachment) && $leave->attachment != "") {
                                                ?>
                                                <br>
                                                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal"
                                                        data-target=".bs-world-modal"
                                                        onclick="document.getElementById('modalsrc').src ='<?php echo base_url() . $leave->attachment ?>'">
                                                    <i class="fa fa-file"></i> View Attachment
                                                </button>
                                                <?php
                                            }
                                            if (!is_null($leave->comments) && $leave->comments != "") {
                                                ?>
                                                <br>
                                                <button type="button" class="btn btn-default btn-xs"
                                                        data-toggle="tooltip" data-placement="top" title
                                                        data-original-title="<?php echo $leave->comments ?>"
                                                        onclick="alert('<?php echo $leave->comments ?>')"><i
                                                        class="fa fa-comment"></i> View Comments
                                                </button>
                                                <?php
                                            }
                                            ?>
                                        </td>
                                        <td class=" ">
                                            <?php echo $this->user_model->getLeaveStatus($leave->status)->row()->description;
                                            ?>
                                        </td>
                                        <td class="hidden-xs"><?php echo $this->user_model->getUser($leave->approver)->row()->name ?></td>
                                        <td class="  ">
                                            <?php
                                            if(!is_null($leave->createdby)) {
                                                $requestor = $this->user_model->getUser($leave->createdby)->row();
                                                echo $requestor->name;
                                            }
                                            else
                                                echo $user->name;
                                            ?>
                                        </td>
                                        <td class="a-right a-right hidden-xs">
                                            <?php
                                            $datecreated = date_create($leave->created);
                                            echo date_format($datecreated, $format);
                                            ?>
                                        </td>
                                        <td class="a-right a-right">
                                            <?php
                                            $datecreated = date_create($leave->created);
                                            echo date_format($datecreated, 'U');
                                            ?>
                                        </td>

                                        <td class=" last ">
                                                <div class="col-xs-12">
                                                    <form class="form-horizontal form-label-left" novalidate=""
                                                          action="<?php echo base_url() ?>user/edit" method="post">
                                                        <input type="hidden" name="id" value="<?php echo $leave->id ?>">
                                                                <?php if ($leave->status != 2 && $leave->status != 4 && $future) { // awaiting approval or cancelled
                                                                    $halfcb = "false";
                                                                    if ($leave->days == 1) {
                                                                        if ($user->hours == $leave->hours * 2) {
                                                                            $halfcb = "true";
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <button type="submit" name="edit"
                                                                            class="btn btn-primary btn-xs"
                                                                            style="width:100%;max-width:150px">Edit
                                                                    </button>
                                                                <?php }
                                                                if ($leave->status < 3 && $future  && !$leave->processed) { ?>
                                                                    <button type="submit" name="cancel"
                                                                            class="btn btn-danger btn-xs"
                                                                            style="width:100%;max-width:150px">Cancel
                                                                    </button>
                                                                <?php } ?>
                                                    </form>
                                                </div>
                                        </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                } else { ?>
                                    <tr class="odd pointer selected">
                                        <td colspan="7">You have no leave requests visible</td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /form input knob -->

            </div>

        </div>


    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>

<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>

<script>
    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search:"
            },
            "bStateSave": false,
            "responsive": false,
            "aaSorting": [7, "desc"],
            "aoColumnDefs": [
                {
                    "aaSorting": [6, "desc"]
//                        "asSorting": ["desc", "asc"],
//                        "aTargets": [6]
                }, //disables sorting for column one
                {
                    "bSortable": false,
                    "aTargets": [8]
                }, //disables sorting for column one
                {
                    "bVisible": false,
                    "aTargets": [1, 7]
                },
                {
                    "iDataSort": 7,
                    "aTargets": [6]
                },
                {
                    "iDataSort": 1,
                    "aTargets": [0]
                }

            ],
            "iDisplayLength": 10,
            "sPaginationType": "full_numbers",
            "dom": '<"clear">lfrtip'
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });


    $('.modal').on('show.bs.modal', function () {
        $('.modal-body').css('height', $(window).height() * 0.75);
        $('#modalsrc').css('height', $(window).height() * 0.75);

    });

    <?php if($this->session->userdata('aosync')) { ?>
    new PNotify({title: 'Data Sync', text: 'Updating data ...', type: 'info'});
    $.ajax({
        url: "<?php echo base_url()?>user/aosync",
        dataType: "html",
        type: "post",
        success: function (data) {
            if (data != '')
                new PNotify({title: 'Data Sync', text: data, type: 'info'});
        }
    });
    <?php } ?>

</script>

<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>
</html>
