<?php
include "header.php";
$company = $this->user_model->getCompany($this->session->userdata('company'));
$selectedcountry = $this->input->post('country');
$selectedlocation = $this->input->post('locations');
$selectedtype = $this->input->post('types');
?>
<!-- style>
    html,
    body {
        margin:0;
        padding:0;
        height:100%;
    }
    #wrapper {
        min-height:100%;
        position:relative;
    }
    #header {
        background:#ededed;
        padding:10px;
    }
    .container {
        padding-bottom:100px; /* Height of the footer element */
    }
    footer {
        position:fixed;
        bottom:0;
        left:0;
    }
</style -->

<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Process for Attaché Payroll
                    </h3>
                </div>
                <div class="title_right">
                    <h3>

                    </h3>
                </div>
                <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">X</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">View Attachment</h4>
                            </div>
                            <div class="modal-body ">
                                <iframe id="modalsrc" src="" width="100%"></iframe>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row">
                <!-- form date pickers -->
                <!-- /form datepicker -->

                <!-- form input knob -->
                <form class="form-horizontal form-label-left" id="downloadform" novalidate=""
                      action="<?php echo base_url() ?>user/download" method="post">

                    <div class="col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h5><span style="color:#1D92EF !important">&#9312; Select your ...</span></h5>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="col-xs-12">
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <br>
                                        <div class="item form-group col-md-10 col-sm-11 col-xs-12">
                                            <label class="control-label col-sm-6 col-xs-12" for="todate">Last Day of Pay
                                                Cycle
                                            </label>
                                            <div class="col-xs-6 col-xs-12">
                                                <fieldset>
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <div class="xdisplay_inputx form-group has-feedback">
                                                                <input type="text"
                                                                       class="form-control has-feedback-left"
                                                                       id="single_cal4t" name="todate"
                                                                       placeholder=" dd/mm/yyyy"
                                                                       aria-describedby="inputSuccess2Status4t"
                                                                       value="<?php echo $this->input->post('todate') ?>">
                                                                <span
                                                                    class="fa fa-calendar-o form-control-feedback left"
                                                                    aria-hidden="true"></span>
                                                                <span id="inputSuccess2Status4t" class="sr-only">(success)</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    if ($company->timesheets == 1) {
                                        ?>
                                        <div class="col-xs-12">
                                            <div class="col-md-8 col-sm-6 col-xs-12">
                                                <div class="item form-group col-md-10 col-sm-11 col-xs-12">
                                                    <label class="control-label col-sm-6 col-xs-12" for="role">Submission
                                                        Types
                                                    </label>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <select name="types" id="types" class="form-control">
                                                            <option value="ALL" selected>All Types</option>
                                                            <option value="LEAVE"
                                                                <?php
                                                                if (!is_null($selectedtype) && $selectedtype == 'LEAVE')
                                                                    echo "selected";
                                                                ?>
                                                            >Leave Entries
                                                            </option>
                                                            <option value="TIME"
                                                                <?php
                                                                if (!is_null($selectedtype) && $selectedtype == 'TIME')
                                                                    echo ' selected';
                                                                ?>
                                                            >Timesheets
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-sm-1">
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="col-xs-12">
                                        <div class="col-md-8 col-sm-6 col-xs-12">
                                            <div class="item form-group col-md-10 col-sm-11 col-xs-12">
                                                <label class="control-label col-sm-6 col-xs-12" for="role">Locations
                                                </label>
                                                <div class="col-sm-6 col-xs-12">
                                                    <select name="locations" id="locations" class="form-control">
                                                        <option value="ALL" selected>All Locations</option>
                                                        <option value="BLANK"
                                                            <?php
                                                            if ($selectedlocation == 'BLANK')
                                                                echo ' selected ';
                                                            ?>
                                                        >Blank Locations
                                                        </option>
                                                        <?php
                                                        $locations = $this->user_model->getLocations();
                                                        foreach ($locations->result() as $location) {
                                                            echo '<option value="' . $location->location . '" ';
                                                            if ($selectedlocation == $location->location)
                                                                echo ' selected ';
                                                            echo ' >' . $location->location . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-1">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        <div class="col-md-8 col-sm-6 col-xs-12">
                                            <div class="item form-group col-md-10 col-sm-11 col-xs-12">
                                                <label class="control-label col-sm-6 col-xs-12" for="role">Country
                                                </label>
                                                <div class="col-sm-6 col-xs-12">
                                                    <select name="country" id="country" class="form-control">
                                                        <option value="ALL" selected>All Countries</option>
                                                        <?php $countries = $this->user_model->getCountry();
                                                        foreach ($countries->result() as $country) {
                                                            echo "<option value='" . $country->id . "' ";
                                                            if (!is_null($selectedcountry) && $selectedcountry == $country->id)
                                                                echo "selected";
                                                            echo ">" . $country->name . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-1">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <br>
                                        <div class="item form-group col-md-10 col-sm-11 col-xs-12">
                                            <label class="control-label col-sm-6 col-xs-12" for="b">
                                            </label>
                                            <div class="col-xs-6 col-xs-12">
                                                <fieldset>
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <div class="xdisplay_inputx form-group has-feedback">
                                                                <button id="send" type="submit" class="btn btn-primary"
                                                                        style="visibility:hidden">Get Leave Requests
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>

                                    <!--
                                                    <div class="col-xs-12">
                                                    <br>
                                                       <div class="form-group">
                                                           <div class="col-xs-6 col-xs-offset-3">
                                                               <button id="send" type="submit" class="btn btn-success"><i class="fa fa-gears"></i> Apply Date Range Filter</button>
                                                           </div>
                                                       </div>
                                                    </div>
                                    -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h5><span style="color:#1D92EF !important">&#9313; Create output file for Attaché Payroll</span>
                                </h5>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <table id="example" class="table table-striped responsive-utilities">
                                    <thead>
                                    <tr class="headings">
                                        <th>Employee</th>
                                        <th>Location</th>
                                        <th>Type</th>
                                        <th>Leave Balance</th>
                                        <th>Request</th>
                                        <th></th>
                                        <th>This Pay Period</th>
                                        <th></th>
                                        <th>Status</th>
                                        <th>Last Action by</th>
                                        <th class=" no-link last"><span class="nobr">Processed</span></th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php
                                    $download = false;
                                    $format = 'D jS \of M';
                                    $datefilter = false;
                                    if (!is_null($this->input->post('todate'))) {

                                        if (strpos($this->input->post('todate'), '/') > 1) {
                                            $datefilter = true;

                                            $td = explode('/', $this->input->post('todate'));
                                            $tofilter = strtotime($td[2] . "-" . $td[1] . "-" . $td[0]);
                                        }
                                    }
                                    $leaveresults = $this->user_model->getApprovedLeaveByCompany();
                                    $i = 1;
                                    $country = $this->input->post('country');
                                    $countryFilter = false;
                                    if ($country != "ALL")
                                        $countryFilter = true;

                                    $types = $this->input->post('types');
                                    $typesFilter = false;
                                    if ($types != "ALL")
                                        $typesFilter = true;

                                    $locations = $this->input->post('locations');
                                    $locationsFilter = false;
                                    if ($locations != "ALL")
                                        $locationsFilter = true;


                                    if (isset($leaveresults)) {
                                        foreach ($leaveresults->result() as $leave) {
                                            $show = false;
                                            $todate = strtotime($leave->todate);
                                            $fromdate = strtotime($leave->fromdate);
                                            $days = $leave->days;

                                            if ($datefilter) {
                                                $show = false;
                                                if (!$leave->processed && $fromdate <= $tofilter) {
//                                    if($todate >= $fromfilter && $fromdate <= $tofilter){
                                                    $show = true;
//                                        if($fromdate < $fromfilter)
//                                            $fromdate = $fromfilter;
                                                    if ($todate > $tofilter)
                                                        $todate = $tofilter;

                                                }

                                            }
                                            if ($show) {
                                                $requestor = $this->user_model->getUser($leave->user)->row();
                                                if ($countryFilter) {
                                                    if ($requestor->country != $country) {
                                                        $show = false;
                                                    }
                                                }
                                                if ($typesFilter) {
                                                    if ($types == 'LEAVE' && !is_null($leave->timesheet)) {
                                                        $show = false;
                                                    } elseif ($types == 'TIME' && is_null($leave->timesheet)) {
                                                        $show = false;
                                                    }
                                                }
                                                if ($locationsFilter) {
                                                    if ($locations == 'BLANK') {
                                                        $l = '' . trim($requestor->location);
                                                        if ($l != '') {
                                                            $show = false;
                                                        }
                                                    } elseif ($requestor->location != $locations) {
                                                        $show = false;
                                                    }
                                                }
                                            }
                                            if ($show) {
                                                $download = true;
                                                if (is_null($leave->timesheet))
                                                    $days = $this->user_model->getWorkingDays($fromdate, $todate, $leave->user)['days'];
                                                else
                                                    $days = $leave->days;
//                                    $requestor = $this->user_model->getUser($leave->user)->row();

                                                $future = false; // assume the date is in the past
                                                if (strtotime($todate) >= time()) $future = true;

                                                $r = "even ";

                                                $s = "";
                                                echo '<tr class="' . $r . ' pointer ' . $s . '">';
                                                ?>
                                                <td class=" "><?php echo $requestor->name ?></td>
                                                <td class=" "><?php echo $requestor->location ?></td>
                                                <td class=" ">
                                                    <?php echo $this->user_model->getLeaveType($leave->leavetype)->row()->description . " <br>(" . $leave->hours . " hours)";
                                                    if (!is_null($leave->attachment) && $leave->attachment != "") {
                                                        ?>
                                                        <br>
                                                        <button type="button" class="btn btn-primary btn-xs"
                                                                data-toggle="modal" data-target=".bs-world-modal"
                                                                onclick="document.getElementById('modalsrc').src ='<?php echo base_url() . $leave->attachment ?>'">
                                                            <i class="fa fa-file"></i> View Attachment
                                                        </button>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                <td class=" ">
                                                    <?php
                                                    if (!is_null($requestor->leaveupdated) && $requestor->leaveupdated != "0000-00-00") {
                                                        //--get leave type (record)
                                                        $type = $this->user_model->getLeaveType($leave->leavetype)->row();
                                                        //--get balancemap
                                                        if ($type->balancemap != 0) {
                                                            $mapid = $type->balancemap;
                                                            $map = $this->user_model->getLeaveMapping($mapid)->row();
                                                            $balanceh = $requestor->{$map->userfield};
                                                            $balanced = number_format($balanceh / $requestor->hours, 1);
                                                            echo $balanceh . " hours (" . $balanced . " days) as at " . date_format(date_create($requestor->leaveupdated), "d/m/Y");
                                                        } else {
                                                            echo "Not Applicable";
                                                        }
                                                    } else
                                                        echo "Not Available";
                                                    ?>
                                                </td>
                                                <td class=" " style="white-space: nowrap"><?php
                                                    if (!is_null($leave->timesheet) && $leave->timesheet > 0)
                                                        echo date($format, strtotime($leave->fromdate)) . " to " . date($format, strtotime($leave->todate));
                                                    else
                                                        echo $leave->days . " days <br> " . date($format, strtotime($leave->fromdate)) . " to " . date($format, strtotime($leave->todate));
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php echo date_format(date_create($leave->fromdate), "U"); ?>
                                                </td>
                                                <td class=" " style="white-space: nowrap">
                                                    <?php
                                                    if (!is_null($leave->timesheet) && $leave->timesheet > 0)
                                                        echo date($format, strtotime($leave->fromdate)) . " to " . date($format, strtotime($leave->todate));
                                                    else
                                                        echo $days . " days <br> " . date($format, $fromdate) . " to " . date($format, $todate);
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php echo date_format(date_create($leave->fromdate), "U"); ?>
                                                </td>
                                                <td class=" "><?php echo $this->user_model->getLeaveStatus($leave->status)->row()->description ?></td>
                                                <td>
                                                    <?php if ($leave->status != 1) echo $this->user_model->getUser($leave->approvedby)->row()->name . "<br> on " . date($format, strtotime($leave->approvedon)) ?>
                                                </td>
                                                <td class=" last">
                                                    <?php
                                                    if ($leave->processed) echo "YES";
                                                    else echo "No";
                                                    ?>
                                                </td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                        }
                                    } else {
                                        ?>
                                        <tr class="odd pointer selected">
                                            <td colspan="7">You have no leave items visible</td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                <?php
                                if ($download) {
                                    ?>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div class="col-xs-9 col-xs-offset-3">
                                                    <br>
                                                    <button type="button" id="download" class="btn btn-primary"><i
                                                            class="fa fa-download"></i> Process <?php if ($company->timesheets) echo 'Entries'; else echo 'Leave'?> & Download File
                                                    </button>
                                                    <button type="submit" name="download" id="submit"
                                                            style="visibility:hidden"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- /form input knob -->
                </form>

            </div>
        </div>


    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>


<!-- datepicker -->
<script type="text/javascript">
    $('#send').hide();

    $(document).ready(function () {

        $('#more').click(function () {
            var newNode = document.createElement('div');
            newNode.innerHTML = newrow(document.getElementById('rowcount').value);
            document.getElementById('leaverow').appendChild(newNode);

            document.getElementById('rowcount').value += 1;

        });

        var cb = function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange_right span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
        }

        var optionSet1 = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2015',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };

        $('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

        $('#reportrange_right').daterangepicker(optionSet1, cb);

        $('#reportrange_right').on('show.daterangepicker', function () {
            console.log("show event fired");
        });
        $('#reportrange_right').on('hide.daterangepicker', function () {
            console.log("hide event fired");
        });
        $('#reportrange_right').on('apply.daterangepicker', function (ev, picker) {
            console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange_right').on('cancel.daterangepicker', function (ev, picker) {
            console.log("cancel event fired");
        });

        $('#options1').click(function () {
            $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
        });

        $('#options2').click(function () {
            $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
        });

        $('#destroy').click(function () {
            $('#reportrange_right').data('daterangepicker').remove();
        });


    });
</script>
<!-- datepicker -->

<!-- /datepicker -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#single_cal4f').daterangepicker({
            singleDatePicker: true,
            format: 'DD/MM/YYYY',
            calender_style: "picker_4"
        }, function (start, end, label) {

///
        });
    });

    $(document).ready(function () {
        $('#single_cal4t').daterangepicker({
            singleDatePicker: true,
            format: 'DD/MM/YYYY',
            calender_style: "picker_4"
        }, function (start, end, label) {
            $('#send').hide();
//	    $('#send').removeClass('btn-success');
//            $('#send').addClass('btn-danger');	    
            $('#download').prop('disabled', true);
            ;
            $('#send').trigger('click');
//var oTable = $('#example').dataTable();
        });
    });
</script>

<!-- /datepicker -->
<!-- input_mask -->
<script>
    function calcBusinessDays() {         // input given as Date objects
        var d1 = document.getElementById('single_cal4f').value;
        var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
        var dDate1 = new Date(d1.replace(pattern, '$3-$2-$1'));

        var d2 = document.getElementById('single_cal4t').value;
        var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
        var dDate2 = new Date(d2.replace(pattern, '$3-$2-$1'));

//        var dDate1 =  new Date(document.getElementById('single_cal4f').value);
//        var dDate2 =  new Date(document.getElementById('single_cal4t').value);

        var iWeeks, iDateDiff, iAdjust = 0;

        if (dDate1 == '' || dDate2 == '') return -1;
        if (dDate2 < dDate1) return -1;                 // error code if dates transposed

        var iWeekday1 = dDate1.getDay();                // day of week
        var iWeekday2 = dDate2.getDay();

        iWeekday1 = (iWeekday1 == 0) ? 7 : iWeekday1;   // change Sunday from 0 to 7
        iWeekday2 = (iWeekday2 == 0) ? 7 : iWeekday2;

        if ((iWeekday1 > 5) && (iWeekday2 > 5)) iAdjust = 1;  // adjustment if both days on weekend

        iWeekday1 = (iWeekday1 > 5) ? 5 : iWeekday1;    // only count weekdays
        iWeekday2 = (iWeekday2 > 5) ? 5 : iWeekday2;

        // calculate differnece in weeks (1000mS * 60sec * 60min * 24hrs * 7 days = 604800000)
        iWeeks = Math.floor((dDate2.getTime() - dDate1.getTime()) / 604800000)

        if (iWeekday1 <= iWeekday2) {
            iDateDiff = (iWeeks * 5) + (iWeekday2 - iWeekday1)
        } else {
            iDateDiff = ((iWeeks + 1) * 5) - (iWeekday1 - iWeekday2)
        }

        iDateDiff -= iAdjust                            // take into account both days on weekend

        // return (iDateDiff + 1);                         // add 1 because dates are inclusive
        document.getElementById('duration').value = (iDateDiff + 1);
    }
</script>
<!-- /input mask -->

<!-- Datatables -->
<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>
<!-- PNotify -->
<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>
<script>
    $(document).ready(function () {
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search:"
            },
            "bStateSave": false,
            "responsive": false,
            "aoColumnDefs": [
                {
                    "iDataSort": 5,
                    "aTargets": [4]
                },
                {
                    "iDataSort": 7,
                    "aTargets": [6]
                },
                {
                    "bVisible": false,
                    "aTargets": [5, 7]
                }
            ],

            "iDisplayLength": 25,
            "sPaginationType": "full_numbers",
            "dom": '<"clear">lfrtip'
        });


        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });

        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });

    $('.modal').on('show.bs.modal', function () {
        $('.modal-body').css('height', $(window).height() * 0.75);
        $('#modalsrc').css('height', $(window).height() * 0.75);

    });


    $('#country').change(function () {
        $('#send').hide();
//        $('#send').removeClass('btn-success');
//        $('#send').addClass('btn-danger');
        $('#download').prop('disabled', true);
        $('#send').trigger('click');
    });


    $('#types').change(function () {
        $('#send').hide();
//        $('#send').removeClass('btn-success');
//        $('#send').addClass('btn-danger');
        $('#download').prop('disabled', true);
        $('#send').trigger('click');
    });

    $('#locations').change(function () {
        $('#send').hide();
//        $('#send').removeClass('btn-success');
//        $('#send').addClass('btn-danger');
        $('#download').prop('disabled', true);
        $('#send').trigger('click');
    });

    $('.reassign').change(function () {
        $.ajax({
            url: "<?php echo base_url()?>ajax/reassign",
            data: {
                "leave": this.getAttribute('Tag'),
                "approver": this.value
            },
            dataType: "html",
            type: "post",
            success: function (data) {
                new PNotify({title: 'Reassignment', text: data, type: 'info'});
            },
            error: function (data) {
                new PNotify({title: 'Reassignment', text: data, type: 'error'});
            }
        });
    });

    $("#download").click(function () {
        if (confirm("Please confirm you want to process these and download the file?")) {
            $('#submit').trigger('click');
            $('#example_wrapper').hide();
            $('#download').hide();
        }
    });

</script>

<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>