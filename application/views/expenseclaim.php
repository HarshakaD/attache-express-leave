<?php
include "header.php";
//$company = $this->user_model->getCompany($this->session->userdata('company'))->row();
$user = $this->user_model->getUser($this->session->userdata('id'))->row();

$claim = $this->user_model->getExpenseClaim($claimID);
if($claim != false)
    $items = $this->user_model->getExpenseItemsByClaim($claimID);
else
    $claimID = 0;

?>

<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Expense Claim <small>(<?php echo $this->session->userdata('name'); ?>)</small>
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>
                <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">X</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">View Attachment</h4>
                            </div>
                            <div class="modal-body modal-viewer">
                                <iframe id="modalsrc" src="" width="100%"></iframe>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal fade bs-attach-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg" >
                        <div class="modal-content" >

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Attachment</h4>
                            </div>
                            <div class="modal-body ">
                                <div class="col-xs-12">
                                    <div class="item form-group">
                                <form action="<?php echo base_url()?>ajax/addexpattachment" enctype="multipart/form-data" class="dropzone" style="border: 1px solid #e5e5e5; height: 250px; " id="my-awesome-dropzone">
                                    <div id="leaverow">
                                        <input id="attclaimid" name="attclaimid" type="hidden" value="mytest">
                                        <input id="attitemid" name="attitemid" type="hidden" value="">
                                    </div>
                                </form>

                                        </div>


                                    </div>


                                </form>
<!--if receipt has already been uploaded
display it here  -->
<!--                                <iframe id="modalsrc" src="" width="100%"></iframe>-->
                            </div>

                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row">
                <!-- form date pickers -->
                <div class="col-xs-12">
                    <div class="x_panel" style="">
<!--                        <div class="x_title">-->
<!--                            <h2>-->
<!--                                <small></small>-->
<!--                            </h2>-->
<!--                            <div class="clearfix"></div>-->
<!--                        </div>-->
                        <div class="x_content">
                            <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>user/addexpense" method="post" enctype="multipart/form-data">
                                <div id="leaverow">
                                    <input id="claimid" name="claimid" type="hidden" value="<?php echo $claimID ?>">

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            Name this Expense Claim
                                        </label>
                                        <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                            <input id="name" name="claimname" class="form-control col-xs-12"
                                                   data-validate-length-range="2" maxlength="32" value="<?php if($claim != false) echo $claim->name?>" required="required"
                                                   type="text" placeholder="e.g. November Expenses">
                                        </div>
                                    </div>




<div class="col-md-8 col-md-offset-2">
                                    <table id="mytable" class="table" >
                                        <thead>
                                        <tr id="row0" class="headings">
                                            <th style="width: 10%">Date</th>
                                            <th style="width: 40%">Description</th>
                                            <th style="width: 15%; text-align: right" class="amount">Amount</th>
                                            <th style="width: 20%;">Attachments</th>
                                            <th style="width: 10%" class=" no-link last"><span class="nobr">Action</span></th>
                                        </tr>
                                        </thead>
<tbody>
                                        <?php
                                        $format = 'D M Y';
                                        $format2 = 'd/m/Y';
                                        $total = 0;
                                        $i = 0;
                                        if (isset($items) && $items->num_rows() > 0){

                                            echo '<input type="hidden" name="items" value="'.$items->num_rows().'">';
                                            foreach ($items->result() as $item) {
                                                $i++;
                                                $expensedate = date_create($item->transactiondate);
                                                $total += $item->amount;
                                                ?>
                                                <tr id="row<?php echo $i ?>">
                                                    <input type="hidden" id="item<?php echo $i ?>" name="item<?php echo $i ?>" value="<?php echo $item->id ?>">
                                                    <td><input class="form-control" name="expdate<?php echo $i ?>" id="expdate<?php echo $i ?>" <?php if($readonly) echo "readonly" ?> required="required" type="date" placeholder=" dd/mm/yyyy" style="width: 100%;" value="<?php echo $item->transactiondate ?>"></td>
                                                    <td><input class="form-control" name="expdesc<?php echo $i ?>" id="expdesc<?php echo $i ?>" <?php if($readonly) echo "readonly" ?> data-validate-length-range="2" required="required" type="text" maxlength="32" style="width: 100%" value="<?php echo $item->description ?>"></td>
                                                    <td><input class="form-control" name="expamount<?php echo $i ?>" id="expamount<?php echo $i ?>" <?php if($readonly) echo "readonly" ?> required="required" type="number" step=".01" style="width: 100%; text-align: right" value="<?php echo $item->amount ?>"></td>
                                                    <td name="attachment<?php echo $i ?>" id="attachment<?php echo $i ?>">
                                                        <button type="button" onclick="setmodal('<?php echo $i ?>')"
                                                                class="btn btn-xs btn-primary" data-toggle="modal"
                                                                data-target=".bs-attach-modal"><i class="fa fa-paperclip"></i></button>
                                                        <?php
                                                        $attachments = $this->user_model->getAttachments($item->id);
                                                        if(isset($attachments) && $attachments->num_rows() > 0){
                                                            $a = 0;
                                                            foreach ($attachments->result() as $att){
                                                                $a++;
                                                                    ?><button type="button" class="btn btn-primary btn-xs"
                                                                    data-toggle="modal" data-target=".bs-world-modal"
                                                                    onclick="document.getElementById('modalsrc').src ='<?php echo  base_url() . $att->location ?>'">
                                                                <i class="fa fa-file"></i> <?php echo $a ?>
                                                                    </button>
                                                                    <?php
                                                            }
                                                        }?>
                                                    </td>
                                                    <td class=" last ">
                                                    <button type="button" class="btn btn-xs btn-danger" id="remove<?php echo $i ?>" onclick="removerow('<?php echo $i?>')"><i class="fa fa-remove"></i></button>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        else {
                                        }


                                        if($i == 0){
                                            $i++;

                                            ?>
                                        <tr id="row<?php echo $i ?>">
                                            <input type="hidden" id="item<?php echo $i ?>" name="item<?php echo $i ?>" value="<?php echo rand (-999999, -9999 ) ?>">
                                            <td><input class="form-control" name="expdate<?php echo $i ?>" id="expdate<?php echo $i ?>" required="required" type="date" placeholder=" dd/mm/yyyy" style="width: 100%;height: 23.5px"></td>
                                            <td><input class="form-control" name="expdesc<?php echo $i ?>" id="expdesc<?php echo $i ?>" data-validate-length-range="2" required="required" type="text" maxlength="32" style="width: 100%"></td>
                                            <td><input class="form-control" name="expamount<?php echo $i ?>" id="expamount<?php echo $i ?>" required="required" type="number" step=".01" style="width: 100%; text-align: right" onchange="recalc()"></td>
                                            <td name="attachment<?php echo $i ?>" id="attachment<?php echo $i ?>">
                                                    <button type="button" onclick="setmodal('<?php echo $i ?>')"
                                                        class="btn btn-xs btn-primary" data-toggle="modal"
                                                        data-target=".bs-attach-modal"><i class="fa fa-paperclip"></i></button>
                                            </td>
                                            <td class=" last ">
                                                <button type="button" class="btn btn-xs btn-danger" id="remove<?php echo $i ?>" onclick="removerow('<?php echo $i?>')"><i class="fa fa-remove"></i></button>
                                            </td>
                                        </tr>
                                        <?php }
                                        ?>
                                        <tr>
                                            <td>
                                                <button type="button" class="btn btn-xs btn-primary" onclick="addrow()">New Row</button>
                                            </td>
                                            <td id="itemcount"><?php echo $i ?> items</td>
                                            <td style="text-align: right" id="total">$<?php echo number_format($total, 2)  ?></td>
                                            <td></td> <td></td>

                                        </tr>
</tbody>
                                    </table>
<input type="hidden" name="items" id="items" value="<?php echo $i ?>">

</div>

                                    <div class="col-md-12">
                                    </div>

                                    <div class="item form-group">

                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            Approver
                                        </label>
                                        <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                            <?php $approvers = $this->user_model->getApprovers();
                                            if (isset($approvers)){
                                                ?>
                                                <select class="form-control" name="approver" id="approver">
                                                    <?php
                                                    foreach($approvers->result() as $approver) {
                                                        ?>
                                                        <option value="<?php    echo $approver->id ?>" <?php if($approver->id == $user->defaultapprover) echo 'selected' ?>><?php echo $approver->name ?></option>

                                                    <?php }
                                                    ?>
                                                </select>
                                                <?php
                                            } ?>
                                            <br>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            Comments
                                        </label>
                                        <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                            <?php
                                            // get any prior comments and show them
                                            if($claimID != 0){
                                            $comments = $this->user_model->getExpenseComments($claim->id);
                                            foreach ($comments->result() as $comment ){
                                                echo '<p>';
                                                echo $this->user_model->getUser($comment->madeby)->row()->name.': '.$comment->created;
                                                echo '<br>'.$comment->comment;
                                                echo '</p>';
                                            }
                                            }
                                            ?>
                                            <textarea name="comments" class="form-control"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">

                                                <button  type="button" onclick="window.history.back();" class="btn btn-default" style="width: 100%; max-width:150px; color:#1D92EF; font-weight: 600" >
                                                    Close
                                                </button>
<?php
                                            if ($claimID !=0 && $claim->approver == $this->session->userdata('id') ){
                                                // if its the approver viewing it
                                                if($claim->status == 1 || $claim->status == 3){
    ?>
                                                <button id="approve" name="approve" type="submit"
                                                        class="btn btn-primary"
                                                        style="width: 100%; max-width:150px">Approve
                                                </button>
                                                    <?php }

                                                if($claim->status == 1 || $claim->status == 2) {
                                                    ?>

                                                    <button id="reject" name="reject" type="submit"
                                                            class="btn btn-danger"
                                                            style="width: 100%; max-width:150px">Reject
                                                    </button>

                                                    <?php
                                                }
                                            }
                                            else{
                                                if($claimID != 0 && $claim->status < 4){
                                                ?>
                                                <button id="btnSubmit" name="btnSubmit" type="submit"
                                                        class="btn btn-primary"
                                                        style="width: 100%; max-width:150px"><?php if($claimID != 0 && $claim->status > 2) echo "Resubmit"; else echo "Submit"?>
                                                </button>
                                                <button id="btnSave" name="btnSave" type="submit"
                                                        class="btn btn-default"
                                                        style="width: 100%; max-width:150px; color:#1D92EF; font-weight: 600">
                                                        Save As Draft
                                                </button>
                                                <?php
                                                if($claimID != 0 ){
                                                    ?>
                                                    <button id="cancel" name="cancel" type="submit"
                                                            class="btn btn-danger"
                                                            style="width: 100%; max-width:150px;">
                                                        Cancel this Claim
                                                    </button>
                                            <?php
                                                }
                                                }
                                            }


                                            ?>
                                        </div>
                                    </div>

                                </div>






                            </form>
                        </div>

                    </div>
                </div>
            </div>







        </div>

        <!-- footer content -->
        <?php include "footer.php" ?>
        <!-- /footer content -->

    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
<script src="<?php echo base_url()?>js/validator/validator.js"></script>


<!-- /input mask -->
<!-- Datatables -->
<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>


<script src="<?php echo base_url()?>js/dropzone/dropzone.js"></script>

<script src="<?php echo base_url()?>js/validator/validator.js"></script>
<script>

    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#mytable').dataTable({
            "ordering": false,
            "searching": false,
            "bStateSave": false,
            "responsive": false,
            "paging": false,
            "autoWidth": false,
            "info": false
        });




    });

    function addrow(){
        newrow();
        recalc();
    }

    function removerow(row){
        row = row * 1;  // make sure its a number not text
        var node = $('#row'+row).remove();
        var items = parseInt($("#items").val(),10);
        $('#your_element').attr('id','the_new_id');

        for(var i = row+1; i <= items; i++){
            $('#row'+i).attr('id','row'+(i-1));

            $('#item'+i).attr('name','item'+(i-1));
            $('#item'+i).attr('id','item'+(i-1));

            $('#expdate'+i).attr('name','expdate'+(i-1));
            $('#expdate'+i).attr('id','expdate'+(i-1));

            $('#expdesc'+i).attr('name','expdesc'+(i-1));
            $('#expdesc'+i).attr('id','expdesc'+(i-1));

            $('#expamount'+i).attr('name','expamount'+(i-1));
            $('#expamount'+i).attr('id','expamount'+(i-1));

            $('#attachment'+i).attr('name','attachment'+(i-1));
            $('#attachment'+i).attr('id','attachment'+(i-1));

            $('#remove'+i).attr('onclick','removerow('+(i-1)+')');
            $('#remove'+i).attr('id','remove'+(i-1));
        }
        $("#items").val(items-1);
        recalc();
    }


    function recalc() {
        var total = 0*1;
        var items = parseInt($("#items").val(),10);
        for(var i = 1; i <= items; i++){
            var x = parseFloat($("#expamount"+i).val()).toFixed(2);
            if(!isNaN(x))
                total = total*1 + x*1;
        }
        $("#itemcount").html(items+" items");
        $("#total").html("$"+parseFloat(total).toFixed(2));

    }

    function newrow() {
        var rows = $('#items').val()*1+1;
        $('#items').val(rows);
        var tableRef = document.getElementById('mytable').getElementsByTagName('tbody')[0];

// Insert a row in the table at the last row
        var newRow = tableRef.insertRow(tableRef.rows.length - 1);
        newRow.id = 'row' + rows;

        var newCell0 = newRow.insertCell(-1);
//new cells
        var r = '<input type="hidden" id="item'+rows+'" name="item'+rows+'" value="'+Math.floor(Math.random() * 1000)*-1+'">';

        var c0 = r+'<td><input class="form-control" name="expdate'+rows+'" id="expdate'+rows+'" required="required" type="date" placeholder=" dd/mm/yyyy" style="width: 100%;"></td>';
        var c1 = '<td><input class="form-control" name="expdesc'+rows+'" id="expdesc'+rows+'" data-validate-length-range="2" required="required" type="text" maxlength="32" style="width: 100%"></td>';
        var c2 = '<td><input class="form-control" name="expamount'+rows+'" id="expamount'+rows+'" required="required" type="number" step=".01" style="width: 100%; text-align: right" onchange="recalc()"></td>';
        var c3 = '<td name="attachment'+rows+'" id="attachment'+rows+'" ><button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target=".bs-attach-modal"><i class="fa fa-paperclip"></i></button></td>';
        var c4 = '<td class=" last "><button type="button" class="btn btn-xs btn-danger" onclick="removerow('+rows+')" id="remove'+rows+'"><i class="fa fa-remove"></i></button></td>';

//        var newText  = document.createTextNode(c0);
        newCell0.innerHTML = c0;
        var newCell1 = newRow.insertCell(-1);
        newCell1.innerHTML = c1;
        var newCell2 = newRow.insertCell(-1);
        newCell2.innerHTML = c2;
        var newCell3 = newRow.insertCell(-1);
        newCell3.innerHTML = c3;
        var newCell4 = newRow.insertCell(-1);
        newCell4.innerHTML = c4;

    }

    var currentid = 0;
    function setmodal(id){
        currentid = id;
        // set modal attachment id to id
        // upload attachment to server via ajax
        // link attachment id to expense item
        // display attachment ?
        $('#attitemid').val($('#item'+id).val());

        return true;
    }

    $('.modal').on('show.bs.modal', function () {
        $('.modal-viewer').css('height',$( window ).height()*0.75);
        $('#modalsrc').css('height',$( window ).height()*0.75);

    });



    // dropzone stuff
    Dropzone.options.myAwesomeDropzone = {
        init: function () {
            var cd;
            this.on("success", function (file, response) {
                $('.dz-progress').hide();
                $('.dz-size').hide();
                $('.dz-error-mark').hide();
                console.log(response);
                console.log(file);
                cd = response.trim();
                $('#attachment'+currentid).append('<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target=".bs-world-modal" onclick="document.getElementById(\'modalsrc\').src =\''+cd+'\'"><i class="fa fa-file"></i></button>');
            });
        }
    }

    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
        .on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
//            e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            return true;
        //this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);
</script>

</body>
</html>