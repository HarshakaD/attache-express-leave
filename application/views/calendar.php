<?php
include "header.php";
$company = $this->user_model->getCompany();
?>
<!-- style>
    html,
    body {
        margin:0;
        padding:0;
        height:100%;
    }
    #wrapper {
        min-height:100%;
        position:relative;
    }
    #header {
        background:#ededed;
        padding:10px;
    }
    .container {
        padding-bottom:100px; /* Height of the footer element */
    }
    footer {
        position:fixed;
        bottom:0;
        left:0;
    }
</style -->
<body class="nav-md">

<link href="<?php echo base_url() ?>css/calendar/fullcalendar.css" rel="stylesheet">
<link href="<?php echo base_url() ?>css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="page-title">
                    <div class="title_left">
                        <h3>
                            Leave Calendar (All Staff)
                        </h3>
                    </div>

                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <div class="item form-group">
                                    <label class="col-sm-3">
                                    </label>
                                    <label class="control-label col-sm-3 col-xs-12" for="location">Show Public Holidays
                                    </label>
                                    <div class="col-sm-6 col-xs-12">
                                        <?php
                                        $countries = $this->user_model->getCountry();
                                        foreach ($countries->result() as $country) {
                                            ?>
                                            <input type="checkbox" name="country" value="<?php echo $country->id ?>"
                                                   onchange="recal()"> <?php echo $country->name ?><br>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <hr>
                                    <br>
                                </div>
                                <div id='calendar'></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>


        <!-- End Calender modal -->
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<script src="<?php echo base_url() ?>js/nprogress.js"></script>
<!-- chart js -->
<script src="<?php echo base_url() ?>js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>

<script src="<?php echo base_url() ?>js/custom.js"></script>

<script src="<?php echo base_url() ?>js/moment.min.js"></script>
<script src="<?php echo base_url() ?>js/calendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url() ?>js/randomColor.js"></script>

<script>
    $(window).load(function () {
        cal();
    });

    function recal() {
        //for each checkbox, 
        // if checked, add the id to an array. 
        // implement servicer side. 
        var checkboxes = document.getElementsByName('country');
        var countries = '';
        // loop over them all
        for (var i = 0; i < checkboxes.length; i++) {
            // And stick the checked ones onto an array...
            if (checkboxes[i].checked) {
                if (countries != '') countries += ':'
                countries += checkboxes[i].value;
            }
        }

        var source = "<?php echo base_url() ?>Ajax/getHolidays/";
        // if checked
        $('#calendar').fullCalendar('destroy');
        $.ajax({
            type: "POST",
            url: source,
            data: {countries: countries},
            success: function (result) {
                cal(result);
            }
        });


        // if unchecked
//        $('#calendar').fullCalendar( 'removeEventSource', source )

    }

    <?php
    $eventStr = "";
    $first = true;
    $allLeave = $this->user_model->getLeaveByCompany();
    foreach ($allLeave->result() as $leave) {
        if ($leave->status <= 2) {
            if ($first) {
                $first = false;
                $eventStr .= '{';
            } else
                $eventStr .= ',{';

            $letters = 'ABCDEF';
            $requestor = $this->user_model->getUser($leave->user)->row();
            $dcode = crc32($requestor->name);
            $c1 = substr($dcode, 0, 6);
            $td = date_create($leave->todate);
            $td->modify('+1 day'); // to date needs to be incremented to allow the JS calendar to work correctly
            $td = explode("-", date_format($td, "Y-m-d"));

            $fd = explode("-", $leave->fromdate);
//                                $td = explode("-",$leave->todate);
            $eventStr .= '  "title":"' . $requestor->name . ': ';
            if ($leave->days > 1)
                $eventStr .= $leave->days . ' days ';
            else
                $eventStr .= $leave->hours . ' hours ';

            // only show leave type to HR and Admin
            if ($this->session->userdata("usertype") < 3)
                $eventStr .= $this->user_model->getLeaveType($leave->leavetype)->row()->description;
            $color = '#' . substr(md5($requestor->name), 0, 6);
            $contrast = (hexdec($color) > 0xffffff / 1.7) ? 'black' : 'white';
            $eventStr .= '",';
            $eventStr .= ' "color": "' . $color . '",';//randomColor({"seed":"'.$c1.'"}),';
            $eventStr .= ' "textColor": "' . $contrast . '",';

            $eventStr .= '  "start": "' . $fd[0] . '-' . $fd[1] . '-' . $fd[2] . '",';
            $eventStr .= '  "end": "' . $td[0] . '-' . $td[1] . '-' . $td[2] . '",';
//                                $eventStr .= '  "start": "new Date('.$fd[0].'-'.($fd[1]-0).'-'.$fd[2].' 00:00:00)",';
//                                $eventStr .= '  "end": "new Date('.$td[0].'-'.($td[1]-0).'-'.($td[2]+1).' 00:00:00)",';
            $eventStr .= '  "allDay": true';
            $eventStr .= '}';
        }// if stats <= 2
    } // foreach

    //echo '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);

    ?>

    function cal(events) {
        events = typeof events !== 'undefined' ? events : '';
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var started;
        var categoryClass;
        var calConfig = '{ ';
        calConfig += '"header": { "left": "month, basicWeek, today", "center": "prev title next", "right":"" },';
        calConfig += '"footer": { "left": "month, basicWeek, today", "center": "prev title next", "right":"" },';
        calConfig += '"titleRangeSeparator": " to ", "selectable": false, "editable": false, "weekends": <?php if ($company->sevendayleave) echo "true"; else echo "false";?>,';
//        calConfig += '"columnFormat": {  "month": "ddd", "week": "dddd", "day": "dddd"  },';
        <?php
        $startday = 1; // monday
//        if ($company->timesheets) {
//            $startday = $company->timesheetstart;
//        }
        ?>

        calConfig += '"firstDay": <?php echo $startday ?>,';
        calConfig += '"events": [';
        calConfig += '<?php echo $eventStr?>';
        if (events != '')
            calConfig += ', ' + events;
        calConfig += ']';
        calConfig += '}';

        var calendar = $('#calendar').fullCalendar(JSON.parse(calConfig));
        // var valendar
    }


</script>
<?php
include "footer.php";
?>
</body>

</html>


