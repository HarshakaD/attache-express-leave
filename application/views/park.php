<?php
include "header.php";
$company = $this->user_model->getCompany($this->session->userdata('company'));

?>

<body class="nav-md">
<style type="text/css" class="init">

    tr.group,
    tr.group:hover {
        background-color: #ddd !important;
    }

</style>
<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <?php
                $message = $this->session->userdata('message');
                $this->session->set_userdata(array('message' => ""));
                echo "<div class='has-error'>" . $message . "</div>";
                ?>

                <div class="title_left">
                    <h3>
                        Park
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>

            </div>
            <div class="clearfix"></div>


            <div class="row">
                <!-- form date pickers -->
                <!-- /form datepicker -->

                <!-- form input knob -->
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">

                            <form class="form-horizontal form-label-left" novalidate=""
                                  action="<?php echo base_url() ?>user/park" method="post">


                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-6" for="name">Date <span class="required">*</span>
                                    </label>
                                    <div class="col-md-2 col-sm-3 col-xs-6">
                                        <input type="date" class="form-control col-md-7 col-xs-12"  name="parkdate" id="parkdate" value="">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"><span class="required"></span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <button class="btn btn-primary" id="getdataset">Park</button>
                                    </div>
                                </div>

                            </form>

                            <br>




<!--                            <button class="btn btn-default" id="getdataset2">Step 2</button>-->
                            <br>
                            <div id="stat">

                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>

        <!-- footer content -->

        <!-- /footer content -->

    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<script src="<?php echo base_url() ?>js/input_mask/jquery.inputmask.bundle.js"></script>

        <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>
        <script src="<?php echo base_url()?>js/custom.js"></script>



<?php include "footer.php" ?>

</body>

</html>
