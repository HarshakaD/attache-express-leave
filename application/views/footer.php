<?php
// global footer
?>

<!-- footer content -->

<div id="footer">
   <footer class="footer" <?php if (ENVIRONMENT != 'production') echo 'style="background-color: '.ENVIRONMENT.'"'; ?>>

    <div class="wrap">
        <a href="http://www.attachesoftware.com/contact-us/" target="_blank">Contact Us</a>
        |
        <a href="<?php echo base_url() ?>help"> FAQs </a>
        |
        <a href="http://www.attachesoftware.com/legal-information/" target="_blank">Legal Notices</a>
        <span>&copy; Attaché Software</span>
        <p><a href="http://www.attachesoftware.com" target="_blank"><img src="<?php echo base_url()?>images/logo.png" alt="Attaché Software" title="Attaché Software"></a></p>
    </div>
</footer>
        </div>


