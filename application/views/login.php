<?php

include "header.php";
?>
<style>
    body {
        /* background: transparent url("<?php echo base_url()?>images/bg-full.jpg") no-repeat center center fixed; */
        background-color: #F4F5F6;
        background-size: cover;
    }
</style>

<body xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html">

<div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>
    <div id="wrapper">
        <img src="<?php echo base_url()?>images/expressleave.png" alt="Attache Express Leave" style="display: block;margin-left: auto;    margin-right: auto; margin-bottom: 30px;">

        <div class="col-xs-12">
            <div class="x_panel">
<div style="text-align: center;">
			<h2>Log In</h2>
</div>
                <div class="x_content" style="min-height: 350px">
                    <div id="login" class="animate form">
                        <section class="login_content">
                            <form novalidate="" class="form-horizontal form-label-left"
                                  action="<?php echo base_url().'user/login/'.$this->uri->segment(1).'/'.$this->uri->segment(2) ?>" method="post">
                                <?php
                                $message = $this->session->userdata('message');
                                $this->session->set_userdata(array('message' => ""));
                                echo "<div class='has-error'>" . $message . "</div>";
                                ?>
                                <div class="clearfix"></div>
                                <div class="item form-group">
                                    <div>
                                        <input type="email" name="email" class="form-control"
                                               placeholder="Enter your email address" required="required"/>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <div>
                                        <input type="password" name="password" class="form-control"
                                               placeholder="Enter your password" required="required"/>
                                    </div>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-primary btn-md submit col-xs-12">
                                        Log In
                                    </button>
                                </div>
                                <div class="clearfix"></div>
<br>
                                <div>
                                    <a href="#toregister" class="to_register">
                                        <button type="button" class="btn btn-link btn-sm"> Forgot your Password?
                                        </button>
                                    </a>
                                </div>

                                <div>
                                    <a href="<?php echo base_url() ?>help" target="_blank" class="btn btn-sm btn-link">
                                        Help
                                    </a>
                                </div>
                            </form>
                            <!-- form -->
                        </section>

                    </div>
                    <div id="register" class="animate form">
                        <section class="login_content">
                            <form novalidate="" class="form-horizontal form-label-left"
                                  action="<?php echo base_url() ?>user/changepassword" method="post">

                                <div class="item form-group">
                                    <div>
                                        <input type="email" class="form-control" name="reset" placeholder="Enter your email address"
                                               required="required"/>
                                    </div>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-warning btn-md submit col-xs-12"><i class="fa fa-key"></i> Reset
                                        Password
                                    </button>
                                </div>
                                <div class="clearfix"></div>

                                <div>
                                    <a>
                                      &nbsp;
                                    </a>
                                </div>
                                <div class="separator">
                                </div>

                                <div>
                                    <a>
                                        &nbsp;
                                    </a>
                                <div>
                                    <a href="#tologin" class="to_register">
                                        <button type="button" class="btn btn-sm"> Remembered it?
                                        </button>
                                    </a>
                                </div>
                                <div class="clearfix"></div>


                            </form>
                            <!-- form -->
                        </section>
                        <!-- content -->
                    </div>
                </div>

            </div>

        </div>

    </div>
    <?php

    include "footer.php";
    ?>
</body>
<script src="<?php echo base_url() ?>js/validator/validator.js"></script>
<script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
        .on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
//            e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            return true;
        //this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);
</script>
</html>
