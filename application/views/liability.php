<?php
include "header.php";

$company = $this->user_model->getCompany($this->session->userdata('company'));

$leaveCols = array();

$leaveTypes = $this->user_model->getLeaveTypes($company->id);
//if (isset($leaveTypes)){
  foreach($leaveTypes->result() as $type) {
      $mapping = $this->user_model->getLeaveMapping($type->balancemap)->row();
//      if (isset($mapping) && !is_null($mapping))
	if($type->balancemap != 0)
            $leaveCols[$mapping->userfield] = $mapping->description;
//      }
  }
//}

?>
<body class="nav-md" xmlns="http://www.w3.org/1999/html">

<div class="container body">


<div class="main_container">

<?php
include "navbar.php";
?>

<!-- page content -->
<div class="right_col" role="main">

<div class="">
<div class="page-title">
    <div class="title_left">
        <h3>Leave Balance Report</h3>
    </div>

</div>
<div class="clearfix"></div>


<div class="row">
    <div class="col-md-12">

        <div >
            <div class="panel">

                <div >
                    <div class="panel-body">
                        <p>

                            <!-- start project list -->
                        <table class="table table-striped projects" id="example">
                            <thead>
                            <tr>
                                <th>Employee</th>
                                <th>State/Region</th>
                                <th>Balance as at</th>
<?php 
foreach ($leaveCols as $l=>$d){
	echo '<th>'.$d.'</th><th></th>';
}
?>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            // for each team in get top teams
                            $staff = $this->user_model->getLeaveLiability($company->id);
                            if (isset($staff))
                                foreach($staff->result() as $employee) {
                                    ?>

                                    <tr id="row<?php echo $employee->id ?>">
                                        <td>
                                            <a><?php echo $employee->name ?></a>
                                            <br />
                                            <small>EmployeeID: <?php echo $employee->employeeid ?></small>
                                        </td>
                                        <td>
                                            <?php echo $this->user_model->getRegion($employee->state)->row()->name ?>,<br>
                                            <?php echo $this->user_model->getCountry($employee->country)->row()->name ?>
                                        </td>
                                        <td>
Balances as at:<br>
                                            <?php
 echo date_format(date_create($employee->leaveupdated),'d/m/Y');
?>                                        </td>

<?php
foreach ($leaveCols as $l=>$d){
//        echo '<td>'.$l.'</td>';
        echo '<td>'.$employee->$l.' hours or <br>';
	echo number_format($employee->$l/$employee->hours,"1").' days';
	echo '</td>';
        echo '<td>'.$employee->$l.'</td>';
}
?>

                                    </tr>

                                <?php
                                }
                            ?>


                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <?php

            ?>

        </div>

    </div>
</div>


</div>


</div>
<!-- /page content -->
</div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>

<script src="<?php echo base_url()?>js/validator/validator.js"></script>

<!-- PNotify -->
<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>

<!-- Datatables -->
<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>


<script>
    var asInitVals = new Array();
    $(document).ready(function () {

        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search:"
            },
            "bStateSave": false,
            "responsive": false,
	    "aoColumnDefs": [
<?php 
for ($i = 0; $i < count($leaveCols) ; $i++){
	$index = ($i * 2 ) + 4;
	echo '{"iDataSort": '.$index.', "aTargets":['.($index-1).']},';
	echo '{"bVisible": false, "aTargets":['.$index.']},';
} 
?>
	    ],
            "iDisplayLength": 50,
            "sPaginationType": "full_numbers" ,
            "dom": '<"clear">lfrtip'
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });

    });


</script>


<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>
