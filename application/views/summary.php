<?php
include "header.php";
$company = $this->user_model->getCompany($this->session->userdata('company'));
$selectedcountry = $this->input->post('country');
$selectedlocation = $this->input->post('locations');
$selectedtype = $this->input->post('types');
?>
<!-- style>
    html,
    body {
        margin:0;
        padding:0;
        height:100%;
    }
    #wrapper {
        min-height:100%;
        position:relative;
    }
    #header {
        background:#ededed;
        padding:10px;
    }
    .container {
        padding-bottom:100px; /* Height of the footer element */
    }
    footer {
        position:fixed; 
        bottom:0;
        left:0;
    }
</style -->
<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Check Pay Period
                    </h3>
                </div>
                <div class="title_right">
                    <h3>

                    </h3>
                </div>
                <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">X</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">View Attachment</h4>
                            </div>
                            <div class="modal-body ">
                                <iframe id="modalsrc" src="" width="100%"></iframe>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row">
                <!-- form date pickers -->
                <!-- /form datepicker -->

                <!-- form input knob -->
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Use this page to identify any outstanding items that need to be actioned for the pay
                                period</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-xs-12">
                                <h4></h4>
                            </DIV>
                            <?php

                            $datefilter = false;
                            //                        if (!is_null($this->input->post('fromdate')) && !is_null($this->input->post('fromdate'))){
                            $tofilter = $this->input->post('todate');
                            if (!is_null($tofilter) && $tofilter != "") {
                                $datefilter = true;
                            } else {
                                $tofilter = date_format(date_create(), "d/m/Y");
                            }


                            $td = explode('/', $tofilter);
                            $tofilter = date_create($td[2] . "-" . $td[1] . "-" . $td[0]);

                            ?>
                            <form class="form-horizontal form-label-left" novalidate=""
                                  action="<?php echo base_url() ?>user/summary" method="post">
                                <!-- div class="col-md-4 col-sm-6 col-xs-12">
                                Filter from First Day of pay cycle
                                <fieldset>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div class="xdisplay_inputx form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left" id="single_cal4f" name="fromdate" placeholder=" dd/mm/yyyy"
                                                       aria-describedby="inputSuccess2Status4f" <?php if ($datefilter) echo 'value="' . $this->input->post('fromdate') . '"' ?>>
                                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                <span id="inputSuccess2Status4f" class="sr-only">(success)</span>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div -->

                                <div class=" col-xs-12">
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <div class="item form-group col-md-10 col-sm-11 col-xs-12">
                                            <label class="control-label col-sm-6 col-xs-12" for="role">Last Day of Pay
                                                Cycle
                                            </label>
                                            <div class="col-sm-6 col-xs-12">
                                                <fieldset>
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <div class="xdisplay_inputx form-group has-feedback">
                                                                <input type="text"
                                                                       class="form-control has-feedback-left"
                                                                       id="single_cal4t" name="todate"
                                                                       placeholder=" dd/mm/yyyy"
                                                                       aria-describedby="inputSuccess2Status4t"
                                                                    <?php
                                                                    if ($datefilter)
                                                                        echo 'value="' . $this->input->post('todate') . '"';
                                                                    else
                                                                        echo 'value="' . date_format(date_create(), "d/m/Y") . '"';
                                                                    ?>>
                                                                <span
                                                                    class="fa fa-calendar-o form-control-feedback left"
                                                                    aria-hidden="true"></span>
                                                                <span id="inputSuccess2Status4t" class="sr-only">(success)</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if ($company->timesheets == 1) {
                                    ?>
                                    <div class="col-xs-12">
                                        <div class="col-md-8 col-sm-6 col-xs-12">
                                            <div class="item form-group col-md-10 col-sm-11 col-xs-12">
                                                <label class="control-label col-sm-6 col-xs-12" for="role">Submission
                                                    Types
                                                </label>
                                                <div class="col-sm-6 col-xs-12">
                                                    <select name="types" id="types" class="form-control">
                                                        <option value="ALL" selected>All Types</option>
                                                        <option value="LEAVE"
                                                            <?php
                                                            if (!is_null($selectedtype) && $selectedtype == 'LEAVE')
                                                                echo "selected";
                                                            ?>
                                                        >Leave Entries
                                                        </option>
                                                        <option value="TIME"
                                                            <?php
                                                            if (!is_null($selectedtype) && $selectedtype == 'TIME')
                                                                echo ' selected';
                                                            ?>
                                                        >Timesheets
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-1">
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <div class="col-xs-12">
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <div class="item form-group col-md-10 col-sm-11 col-xs-12">
                                            <label class="control-label col-sm-6 col-xs-12" for="role">
                                            </label>
                                            <div class="col-sm-6 col-xs-12">
                                                <div><br>
                                                    <button id="send" type="submit" class="btn btn-primary"
                                                            style="width: 100%; max-width: 300px"> Apply Filter
                                                    </button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                            </div>
                        </div>

                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Results</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table id="example" class="table responsive-utilities">
                                <thead>
                                <tr class="headings">
                                    <th>Employee</th>
                                    <th>Location</th>
                                    <th>Type</th>
                                    <th style="white-space:nowrap">Duration</th>
                                    <th style="white-space:nowrap">Period</th>
                                    <th></th>
                                    <th style="white-space:nowrap">Leave Balance</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                    <th>Assigned</th>
                                    <th>Created By</th>
                                    <th>Last Action by</span></th>
                                    <th class=" no-link last">Processed</th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $format = 'D j M Y';
                                $datefilter = true;
                                $types = 'ALL';
                                $show = true;
                                if ($company->timesheets)
                                    $types = $this->input->post('types');

                                $typesFilter = false;
                                $i = 1;

                                if ($types == 'ALL' || $types == "LEAVE") {
                                    $leaveresults = $this->user_model->getLeaveByCompany();
                                    if (isset($leaveresults)) {
                                        foreach ($leaveresults->result() as $leave) {
                                            $todate = date_create($leave->todate);
                                            $fromdate = date_create($leave->fromdate);
                                            if ($datefilter) {
                                                $show = false;
                                                if (!$leave->processed && $fromdate <= $tofilter) {
                                                    $show = true;
                                                }
                                            }
                                            if ($show) {
                                                $future = false; // assume the date is in the past
                                                if (date_format($todate, 'U') >= time()) $future = true;
                                                $employee = $this->user_model->getUser($leave->user)->row();

                                                echo '<tr>';
                                                ?>
                                                <td class=" "><?php echo $employee->name ?></td>
                                                <td class=" "><?php echo $employee->location ?></td>
                                                <td>Leave</td>
                                                <td class=" " style="white-space:nowrap">
                                                    <?php
                                                    if ($leave->hours > $employee->hours)
                                                        echo $leave->days . " days / " . number_format($leave->hours, 2) . " hours ";
                                                    elseif ($leave->hours == $employee->hours)
                                                        echo "1 day / " . number_format($leave->hours, 2) . " hours ";
                                                    elseif ($leave->hours < $employee->hours)
                                                        echo number_format($leave->hours, 2) . " hours ";

                                                    echo "of " . $this->user_model->getLeaveType($leave->leavetype)->row()->description;;

                                                    if (!is_null($leave->attachment) && $leave->attachment != "") {
                                                        ?>
                                                        <br>
                                                        <button type="button" class="btn btn-primary btn-xs"
                                                                data-toggle="modal" data-target=".bs-world-modal"
                                                                onclick="document.getElementById('modalsrc').src ='<?php echo base_url() . $leave->attachment ?>'">
                                                            <i class="fa fa-file"></i> View Attachment
                                                        </button>
                                                        <?php
                                                    }

                                                    if (!is_null($leave->comments) && $leave->comments != "") {
                                                        ?>
                                                        <br>
                                                        <button type="button" class="btn btn-default btn-xs"
                                                                data-toggle="tooltip" data-placement="top" title
                                                                data-original-title="<?php echo $leave->comments ?>"
                                                                onclick="alert('<?php echo $leave->comments ?>')"><i
                                                                class="fa fa-comment"></i> View Comments
                                                        </button>
                                                        <?php
                                                    } ?>

                                                </td>
                                                <td class=" " style="white-space:nowrap">
                                                    <?php echo date_format($fromdate, $format) ?>
                                                    to <br><?php echo date_format($todate, $format) ?>
                                                </td>

                                                <td class=" ">
                                                    <?php echo date_format($fromdate, "U") ?>
                                                </td>

                                                <td class=" ">
                                                    <?php
                                                    if (!is_null($employee->leaveupdated) && $employee->leaveupdated != "0000-00-00") {
                                                        //--get leave type (record)
                                                        $type = $this->user_model->getLeaveType($leave->leavetype)->row();
                                                        //--get balancemap
                                                        if ($type->balancemap != 0) {
                                                            $mapid = $type->balancemap;
                                                            $map = $this->user_model->getLeaveMapping($mapid)->row();
                                                            $balanceh = $employee->{$map->userfield};
                                                            $balanced = number_format($balanceh / $employee->hours, 1);
                                                            echo $balanced . " days / " . $balanceh . " hours as at " . date_format(date_create($employee->leaveupdated), "d M Y");
                                                        } else {
                                                            echo "Not Applicable";
                                                        }
                                                    } else
                                                        echo "Not Available";

                                                    ?>
                                                </td>
                                                <td class=" "><?php echo $this->user_model->getLeaveStatus($leave->status)->row()->description ?></td>
                                                <td class=" " id="action<?php echo $leave->id ?>">
                                                    <form class="form-horizontal form-label-left" novalidate="" method="post"
                                                    <?php
                                                    echo 'action="'.base_url().'user/processleave" >';
                                                    echo '<input type="hidden" name="id" value="'.$leave->id.'">';
                                                    ?>
                                                    <input type="hidden" name="id" value="<?php echo $leave->id ?>">
                                                    <div class="form-group" style="width:100%">
                                                        <div class="col-xs-12">
                                                            <?php if ($leave->status == 1) { // awaiting approval ?>

                                                                <button type="button" style="width:100%" name="approve"
                                                                        class="btn btn-success btn-xs approve"
                                                                        tag="<?php echo $leave->id ?>"><i
                                                                        class="fa fa-check"></i> Approve
                                                                </button>
                                                                <button type="button" style="width:100%" name="reject"
                                                                        class="btn btn-danger  btn-xs reject"
                                                                        tag="<?php echo $leave->id ?>"><i
                                                                        class="fa fa-ban"></i> Reject
                                                                </button>
                                                            <?php } elseif (!$leave->processed && $leave->status == 2) {// && $future){
                                                                ?>
                                                                <button type="button" style="width:100%"
                                                                        name="cancel"
                                                                        class="btn btn-warning btn-xs cancel"
                                                                        tag="<?php echo $leave->id ?>"><i
                                                                        class="fa fa-trash"></i> Cancel
                                                                </button>
                                                                <?php

                                                            } ?>
                                                        </div>
                                                    </div>
                                                    </form>
                                                </td>
                                                <td class="a-right a-right " id="assign<?php echo $leave->id ?>">
                                                    <?php
                                                    if ($leave->status == 1) { // awaiting approval
                                                        $approvers = $this->user_model->getApprovers();
                                                        if (isset($approvers)) {

                                                            ?>
                                                            <select class="form-control input-sm reassign" name="approver"
                                                                    tag="<?php echo $leave->id ?>">
                                                                <?php
                                                                foreach ($approvers->result() as $approver) {
                                                                    ?>
                                                                    <option
                                                                        value="<?php echo $approver->id ?>" <?php if ($approver->id == $leave->approver) echo "selected" ?>><?php echo $approver->name ?></option>

                                                                <?php }
                                                                ?>
                                                            </select>
                                                            <?php
                                                        }
                                                    }

                                                    ?>
                                                </td>
                                                <td class=" "><?php
                                                    if(!is_null($leave->createdby)) {
                                                        $requestor = $this->user_model->getUser($leave->createdby)->row();
                                                        echo $requestor->name;
                                                    }

                                                    ?></td>
                                                <td>
                                                    <?php
                                                    if ($leave->status != 1) {
                                                        $approvalDate = date_create($leave->approvedon);
                                                        echo $this->user_model->getUser($leave->approvedby)->row()->name . " on " . date_format($approvalDate, $format);
                                                    }
                                                    ?>
                                                </td>
                                                <td class=" last">
                                                    <?php
                                                    if ($leave->processed) echo "YES";
                                                    else echo "No";
                                                    ?>
                                                </td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                        }
                                    }
                                }

                                if (($types == 'ALL' ||$types == "TIME") && $company->timesheets) {
                                    $timresults = $this->user_model->getTimesheetsByCompany();
                                    if (isset($timresults)) {
                                        foreach ($timresults->result() as $timesheet) {
//                                            $fromdate = new DateTime(date("l, M jS, Y", strtotime($timesheet->year . "W" . $timesheet->week . $company->timesheetstart))); // First day of week $$ CHANGE TO COMPANY START DAY
//                                            $todate = new DateTime(date("l, M jS, Y", strtotime($timesheet->year . "W" . $timesheet->week . $company->timesheetstart)));;
//                                            $todate = $todate->modify('+6 days');    // need to add one more day

                                            $leaveEntries = $this->user_model->getLeaveByTimesheet($timesheet->id);
                                            $leave = $leaveEntries->row(); // first row will do.
                                            if(isset($leave)){
                                                $fromdate = date_create($leave->fromdate);
                                                $todate = date_create($leave->todate);
                                                if ($datefilter) {
                                                    $show = false;
                                                    if (!$leave->processed && $fromdate <= $tofilter) {
                                                        $show = true;
                                                    }
                                                }
                                                if ($show) {
                                                    $future = false; // assume the date is in the past
                                                    if (date_format($todate, 'U') >= time()) $future = true;
                                                    $employee = $this->user_model->getUser($leave->user)->row();

                                                    echo '<tr>';
                                                    ?>
                                                    <td class=" "><?php echo $employee->name ?></td>
                                                    <td class=" "><?php echo $employee->location ?></td>
                                                    <td>Timesheet<br>
                                                    <?php
                                                        if (!is_null($leave->attachment) && $leave->attachment != "") {
                                                            ?>
                                                            <br>
                                                            <button type="button" class="btn btn-primary btn-xs"
                                                                    data-toggle="modal" data-target=".bs-world-modal"
                                                                    onclick="document.getElementById('modalsrc').src ='<?php echo base_url() . $leave->attachment ?>'">
                                                                <i class="fa fa-file"></i> View Attachment
                                                            </button>
                                                            <?php
                                                        }

                                                        if (!is_null($leave->comments) && $leave->comments != "") {
                                                            ?>
                                                            <br>
                                                            <button type="button" class="btn btn-default btn-xs"
                                                                    data-toggle="tooltip" data-placement="top" title
                                                                    data-original-title="<?php echo $leave->comments ?>"
                                                                    onclick="alert('<?php echo $leave->comments ?>')"><i
                                                                    class="fa fa-comment"></i> View Comments
                                                            </button>
                                                            <?php
                                                        } ?>
                                                    </td>
                                                    <td class=" " style="white-space:nowrap">
                                                        <?php
                                                        foreach ($leaveEntries->result() as $item){
                                                            if ($item->hours != 0) {
                                                                echo number_format($item->hours, 3) . " hours ";
                                                                echo "of " . $this->user_model->getLeaveType($item->leavetype)->row()->description.'<br>';
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <td class=" " style="white-space:nowrap">
                                                        <?php echo date_format($fromdate, $format) ?>
                                                        to <br><?php echo date_format($todate, $format) ?>
                                                    </td>

                                                    <td class=" ">
                                                        <?php echo date_format($fromdate, "U") ?>
                                                    </td>

                                                    <td class=" ">

                                                    </td>
                                                    <td class=" "><?php echo $this->user_model->getLeaveStatus($leave->status)->row()->description ?></td>
                                                    <td class=" "  id="actiontime<?php echo $timesheet->id ?>">
                                                        <div class="form-group" style="width:100%">
                                                            <div class="col-xs-12">
                                                                <?php if ($leave->status == 1) { // awaiting approval ?>

                                                                    <button type="button" style="width:100%" name="approve"
                                                                            class="btn btn-success btn-xs approveTime"
                                                                            tag="<?php echo $timesheet->id ?>"><i
                                                                            class="fa fa-check"></i> Approve
                                                                    </button>
                                                                <?php } elseif (!$leave->processed && $leave->status == 2) {// && $future){
                                                                    ?>
                                                                    <button type="button" style="width:100%"
                                                                            name="cancel"
                                                                            class="btn btn-warning btn-xs cancelTime"
                                                                            tag="<?php echo $timesheet->id ?>"><i
                                                                            class="fa fa-trash"></i> Cancel
                                                                    </button>
                                                                    <?php

                                                                } ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="a-right a-right " id="assigntime<?php echo $timesheet->id ?>">
                                                        <?php
                                                        if ($leave->status == 1) { // awaiting approval
                                                            $approvers = $this->user_model->getApprovers();
                                                            if (isset($approvers)) {

                                                                ?>
                                                                <select class="form-control input-sm reassignTime" name="approver"
                                                                        tag="<?php echo $timesheet->id ?>">
                                                                    <?php
                                                                    foreach ($approvers->result() as $approver) {
                                                                        ?>
                                                                        <option
                                                                            value="<?php echo $approver->id ?>" <?php if ($approver->id == $leave->approver) echo "selected" ?>><?php echo $approver->name ?></option>

                                                                    <?php }
                                                                    ?>
                                                                </select>
                                                                <?php
                                                            }
                                                        }

                                                        ?>
                                                    </td>
                                                    <td class=" "></td>
                                                    <td>
                                                        <?php
                                                        if ($leave->status != 1) {
                                                            $approvalDate = date_create($leave->approvedon);
                                                            echo $this->user_model->getUser($leave->approvedby)->row()->name . " on " . date_format($approvalDate, $format);
                                                        }
                                                        ?>
                                                    </td>
                                                    <td class=" last">
                                                        <?php
                                                        if ($leave->processed) echo "YES";
                                                        else echo "No";
                                                        ?>
                                                    </td>
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                        }
                                    }

                                }

                                ?>
                                </tbody>
                            </table>
                            <div class="form-group">
                                <div class="col-xs-12">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /form input knob -->

            </div>
        </div>


    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>


<!-- /datepicker -->
<script type="text/javascript">

    $(document).ready(function () {
        $('#single_cal4t').daterangepicker({
            singleDatePicker: true,
            format: 'DD/MM/YYYY',
            calender_style: "picker_4"
        }, function (start, end, label) {
//                var oTable = $('#example').dataTable();
        });
    });
</script>

<!-- /datepicker -->

<!-- /input mask -->

<!-- Datatables -->
<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>

<!-- PNotify -->
<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>
<script>
    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search:"
            },
            "bStateSave": false,
            "responsive": false,
            "aoColumnDefs": [
//                {
//                    "bSortable": false,
//                    "aTargets": [1]
//                },
                {
                    "iDataSort": 5,
                    "aTargets": [4]
                },
                {
                    "bVisible": false,
                    "aTargets": [5]
                }
            ],
            "iDisplayLength": 25,
            "sPaginationType": "full_numbers",
            "dom": '<"clear">lfrtip'
        });


        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });

        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });

    $('.modal').on('show.bs.modal', function () {
        $('.modal-body').css('height', $(window).height() * 0.75);
        $('#modalsrc').css('height', $(window).height() * 0.75);

    });

    $('.approve').click(function () {
        var id = this.getAttribute('Tag');
        $.ajax({
            url: "<?php echo base_url()?>ajax/approve",
            data: {
                "leave": id
            },
            dataType: "html",
            type: "post",
            success: function (data) {
                document.getElementById("action" + id).innerHTML = "Approved";
                document.getElementById("assign" + id).innerHTML = "";
                new PNotify({title: 'Approved', text: data, type: 'success'});
            },
            error: function (data) {
                new PNotify({title: 'Approval Failed', text: data, type: 'error'});
            }
        });
    });

    $('.approveTime').click(function () {
        var id = this.getAttribute('Tag');
        $.ajax({
            url: "<?php echo base_url()?>ajax/approveTime",
            data: {
                "time": id
            },
            dataType: "html",
            type: "post",
            success: function (data) {
                document.getElementById("actiontime" + id).innerHTML = "Approved";
                document.getElementById("assigntime" + id).innerHTML = "";
                new PNotify({title: 'Approved', text: data, type: 'success'});
            },
            error: function (data) {
                new PNotify({title: 'Approval Failed', text: data, type: 'error'});
            }
        });
    });

    $('.cancel').click(function () {
        var id = this.getAttribute('Tag');
        $.ajax({
            url: "<?php echo base_url()?>ajax/cancel",
            data: {
                "leave": this.getAttribute('Tag')
            },
            dataType: "html",
            type: "post",
            success: function (data) {
                document.getElementById("actionTime" + id).innerHTML = "Cancelled";
                document.getElementById("assignTime" + id).innerHTML = "";
                new PNotify({title: 'Cancelled', text: data, type: 'warning'});
            },
            error: function (data) {
                new PNotify({title: 'Cancel Failed', text: data, type: 'error'});
            }
        });
    });

    $('.cancelTime').click(function () {
        var id = this.getAttribute('Tag');
        $.ajax({
            url: "<?php echo base_url()?>ajax/cancelTime",
            data: {
                "time": this.getAttribute('Tag')
            },
            dataType: "html",
            type: "post",
            success: function (data) {
                document.getElementById("actionTime" + id).innerHTML = "Cancelled";
                document.getElementById("assignTime" + id).innerHTML = "";
                new PNotify({title: 'Cancelled', text: data, type: 'warning'});
            },
            error: function (data) {
                new PNotify({title: 'Cancel Failed', text: data, type: 'error'});
            }
        });
    });

    $('.reassign').change(function () {
        var id = this.getAttribute('Tag');
        $.ajax({
            url: "<?php echo base_url()?>ajax/reassign",
            data: {
                "leave": id,
                "approver": this.value
            },
            dataType: "html",
            type: "post",
            success: function (data) {
                document.getElementById("action" + id).innerHTML = "Reassigned";
                new PNotify({title: 'Reassignment', text: data, type: 'info'});
            },
            error: function (data) {
                new PNotify({title: 'Reassignment', text: data, type: 'error'});
            }
        });
    });


    $('.reassignTime').change(function () {
        var id = this.getAttribute('Tag');
        $.ajax({
            url: "<?php echo base_url()?>ajax/reassigntime",
            data: {
                "time": id,
                "approver": this.value
            },
            dataType: "html",
            type: "post",
            success: function (data) {
                document.getElementById("actiontime" + id).innerHTML = "Reassigned";
                new PNotify({title: 'Reassignment', text: data, type: 'info'});
            },
            error: function (data) {
                new PNotify({title: 'Reassignment', text: data, type: 'error'});
            }
        });
    });



    $('.reject').click(function () {
        var id = this.getAttribute('Tag');
        $.ajax({
            url: "<?php echo base_url()?>ajax/reject",
            data: {
                "leave": this.getAttribute('Tag')
            },
            dataType: "html",
            type: "post",
            success: function (data) {
                document.getElementById("action" + id).innerHTML = "Rejected";
                document.getElementById("assign" + id).innerHTML = "";
                new PNotify({title: 'Rejected', text: data, type: 'warning'});
            },
            error: function (data) {
                new PNotify({title: 'Reject Failed', text: data, type: 'error'});
            }
        });
    });


    $('.edit').click(function () {
        var id = this.getAttribute('Tag');
        //redirect to proxy & populate with  leave details
    });

</script>

<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>
