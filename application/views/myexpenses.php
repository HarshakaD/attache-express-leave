<?php
include "header.php";
$user = $this->user_model->getUser($this->session->userdata('id'))->row();
$company = $this->user_model->getCompany($this->session->userdata('company'));
?>
<body class="nav-md" xmlns="http://www.w3.org/1999/html">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        My Expenses
                        <small>(<?php echo $this->session->userdata('name'); ?>)</small>
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>
                <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">X</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">View Attachment</h4>
                            </div>
                            <div class="modal-body ">
                                <iframe id="modalsrc" src="" width="100%"></iframe>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>


            <?php
            $message = $this->session->userdata('message');
            $this->session->set_userdata(array('message' => ""));
            if (isset($message) && !is_null($message) && $message != "") {
                ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="x_panel">
                            <?php
                            echo "<div class='message'>" . $message . "</div>"
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <div class="row">

                <div class="row">
                    <!-- form input knob -->
                    <div class="col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Expense Claim History</h2>
                                <a href="<?php echo base_url()?>user/newexpenseclaim" class="btn btn-primary" style="float: right">New Expense Claim</a>
                                <div class="clearfix"></div>

                            </div>
                            <div class="x_content">
                                <table id="example" class="table table-striped  responsive-utilities">
                                    <thead>
                                    <tr class="headings">
                                        <th>Name</th>
                                        <th>Items</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th class="hidden-xs">Approver</th>
                                        <th class="hidden-xs">Created On</th>
                                        <th>Created</th>
                                        <th class=" no-link last"><span class="nobr">Action</span></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php
                                    $format = 'd M Y';
                                    $format2 = 'U';


                                    // get timesheets by user
                                    $claims = $this->user_model->getExpenseClaimsByUser();

                                    if ($claims->num_rows() > 0) {
                                        foreach ($claims->result() as $claim) {
                                            $colName = '' . $claim->name;
                                            $colItems = '';
                                            $colTotal = ''.$claim->total;
                                            $colApprover = ''.$this->user_model->getUser($claim->approver)->row()->name;
                                            $colStatus = ''.$this->user_model->getLeaveStatus($claim->status)->row()->description;
                                            $datecreated = date_create($claim->created);
                                            $colCreated = ''.date_format($datecreated, $format);
                                            $colCreated_Sort = ''.date_format($datecreated, $format2);
                                            $colAction = '';

                                            $editbutton = false;
                                            $cancelbutton = false;
                                            $attachment = false;
                                            $comments = false;

                                            if($claim->status < 2) {
                                                $editbutton = true;
                                                $cancelbutton = true;
                                            }
                                            $expenseitems = $this->user_model->getExpenseItemsByClaim($claim->id);

                                            if (isset($expenseitems) && $expenseitems->num_rows() > 0) {
                                                $colItems = ''.$expenseitems->num_rows();
//                                                $item = $expenseitems->row();
//                                                $trandate = date_create($item->transactiondate);
                                            } else {
                                                // the claim exists in the claim table, but no items are in the claim
                                                $colName = $claim->name;
                                                $colItems = '0';
                                                $editbutton = true;
                                                $cancelbutton = true;
                                            }


                                            $colAction = '
                                                        <div class="form-group">
                                                            <div class="col-xs-12">
                                                                <form class="form-horizontal form-label-left"
                                                                      novalidate=""
                                                                      action="' . base_url() . 'user/expense"
                                                                      method="post">
                                                                      <input type="hidden" name="claimid" value="'.$claim->id.'">';

                                            if ($editbutton || $cancelbutton) {
                                                $colAction .= '<input type="hidden" name="id"
                                                                           value="' . $claim->id . '">
                                                                    ';
                                                if ($editbutton) {
                                                    $colAction .= '<button type="submit" name="edit"
                                                                       class="btn btn-primary btn-xs"
                                                                       style="width:100%;max-width:150px; min-width: 150px">
                                                                       Edit
                                                                       </button><br>';
                                                }
                                                if ($cancelbutton) {
                                                    $colAction .= '<button type="submit" name="cancel"
                                                                        class="btn btn-danger btn-xs"
                                                                        style="width:100%;max-width:150px;min-width: 150px">
                                                                        Cancel
                                                                        </button><br>';
                                                }


                                            }
                                            if(!$editbutton){
                                                $colAction .= '<button type="submit" name="view"
                                                                        class="btn btn-default btn-xs"
                                                                        style="width:100%;max-width:150px;min-width: 150px">
                                                                        View
                                                                        </button><br>';
                                            }

                                            $colAction .= '
                                                                        
                                                                </form>
                                                            </div>
                                                        </div>';

                                            echo '<tr>';
                                            echo '<td style="white-space: nowrap">';
                                            echo $colName;
                                            echo '</td>';
                                            echo '<td>';
                                            echo $colItems;
                                            echo '</td>';
                                            echo '<td style="white-space: nowrap">';
                                            echo $colTotal;
                                            echo '</td>';
                                            echo '<td style="white-space: nowrap">';
                                            echo $colStatus;
                                            echo '</td>';
                                            echo '<td style="white-space: nowrap">';
                                            echo $colApprover;
                                            echo '</td>';
                                            echo '<td style="white-space: nowrap">';
                                            echo $colCreated;
                                            echo '</td>';
                                            echo '<td>';
                                            echo $colCreated_Sort;
                                            echo '</td>';
                                            echo '<td>';
                                            echo $colAction;
                                            echo '</td>';
                                            echo '</tr>';
                                        }

                                    }

                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /form input knob -->

                </div>

            </div>


        </div>
        <!-- /page content -->
    </div>


    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>
    <link rel="stylesheet" href="<?php echo base_url() ?>css/datatables/css/dataTables.bootstrap.min.css">
    <script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
    <script src="<?php echo base_url() ?>js/custom.js"></script>

    <script>
        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Search:"
                },
                "bStateSave": false,
                "responsive": false,
                "aaSorting": [1, "desc"],
                "aoColumnDefs": [
                    {
                        "aaSorting": [1, "desc"]
//                        "asSorting": ["desc", "asc"],
//                        "aTargets": [6]
                    }, //disables sorting for column one
                    {
                        "bSortable": false,
                        "aTargets": [7]
                    }, //disables sorting for column one
                    {
                        "bVisible": false,
                        "aTargets": [ 6]
                    },
                    {
                        "iDataSort": 6,
                        "aTargets": [5]
                    },
                    {
                        "iDataSort": 1,
                        "aTargets": [0]
                    }

                ],
                "iDisplayLength": 10,
                "sPaginationType": "full_numbers",
                "dom": '<"clear">lfrtip'
            });

            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });


        $('.modal').on('show.bs.modal', function () {
            $('.modal-body').css('height', $(window).height() * 0.75);
            $('#modalsrc').css('height', $(window).height() * 0.75);

        });

    </script>

    <!-- footer content -->
    <?php include "footer.php" ?>
    <!-- /footer content -->
</body>
</html>
