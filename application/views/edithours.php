<?php
include "header.php";


$days = array( 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
//$userid = $this->session->userdata('id');

$thisuser = $this->user_model->getUser($userid)->row();
$workdays = $this->user_model->getUserWorkDays($userid);

$total = 0;

?>

<body class="nav-md">
<style type="text/css" class="init">

    tr.group,
    tr.group:hover {
        background-color: #ddd !important;
    }

</style>
<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
Standard Work Hours for <?php echo $thisuser->name ?>
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>

            </div>
            <div class="clearfix"></div>


            <div class="row">
                <!-- form date pickers -->
                <!-- /form datepicker -->

                <!-- form input knob -->
                <div class="col-xs-12">

                                <?php
                                $templates = $this->user_model->getTemplate();
                                if($templates->num_rows() > 0){
                                    ?>
                                    <div class="x_panel">
                                        <div class="x_content">
                                            <div class="item col-md-10 col-xs-12">
                                    <div class="col-xs-12 form-label-left" id="templates">
                                        <label class="control-label col-xs-3" style="text-align: right">
                                            Load Hours from Template
                                        </label>
                                        <div class="item  col-xs-3">
                                            <select class="form-control" name="template" id="templatelist">
                                                <option value="none">Select Template</option>
                                                <?php
                                                foreach ($templates->result() as $template){
                                                    echo '<option value="'.$template->id.'" >'.$template->name.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                                <?php }
                                ?>

                    <div class="x_panel">
                        <div class="x_content">
                            <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url() ?>user/edithours" method="post">
                                <div class="item form-group col-md-10 col-xs-12">

                                <input type="hidden" name="id" value="<?php echo $userid ?>">
                                <?php
                                foreach ($days as $key=>$day){
                                   ?>
                                    <div class="col-xs-12">
                                        <label class="control-label col-xs-3">
                                            <?php echo $day.'s' ?>
                                        </label>
                                        <div class="item form-group col-xs-3">
                                            <?php
                                            $d = 'd'.$key;
                                            if($workdays)
                                                $today = $workdays->$d;
                                            else
                                                $today = 0;
                                            $total += $today;
                                            ?>
                                            <input id="<?php echo $d ?>" name="<?php echo $d ?>"  class="form-control col-xs-12 timesheet" type="text" style="max-width: 100px" value="<?php echo $today ?>">
                                        </div>
                                    </div>
                                <?php
                                }

                                ?>

                                <div class="col-xs-12" >
                                    <label class="control-label col-xs-3">
                                        Total Hours per week
                                    </label>
                                    <div class="item form-group col-xs-3">
                                        <input id="t0" class="form-control col-xs-12 " type="text" style="max-width: 100px; text-align: right" disabled value="<?php echo $total ?>" >
                                    </div>
                                </div>


                                <div class="col-xs-12" >
                                    <label class="control-label col-xs-3">
                                        Public Holidays
                                    </label>
                                    <div class="item form-group col-md-4 col-xs-6">
                                        <select class="form-control" name="applypublicholidays" id="applypublicholidays">
                                            <option value="1" <?php if(!isset($workdays)) echo 'selected'?>>Does Not work Public Holidays</option>
                                            <option value="0" <?php if(isset($workdays) && !$workdays->applypublicholidays) echo 'selected'?>>Works on Public Holidays</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-xs-12 col-md-offset-3">
                                        <P></P>
                                        <button id="send" type="submit" class="btn btn-primary" name="save" style="min-width: 150px">Save</button>
                                        <a href="<?php echo base_url()?>user/users" class="btn btn-default" style="min-width: 150px">Cancel
                                        </a>
                                    </div>
                                </div>

                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /form input knob -->

            </div>
        </div>

        <!-- footer content -->

        <!-- /footer content -->

    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<script src="<?php echo base_url() ?>js/input_mask/jquery.inputmask.bundle.js"></script>

        <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>
        <script src="<?php echo base_url()?>js/custom.js"></script>

<script>
    function recalc() {
        var total = 0;
        total += parseFloat($('#d0').val());
        total += parseFloat($('#d1').val());
        total += parseFloat($('#d2').val());
        total += parseFloat($('#d3').val());
        total += parseFloat($('#d4').val());
        total += parseFloat($('#d5').val());
        total += parseFloat($('#d6').val());

        $('#t0').val(total);
    }

    function reset() {
        $('#id').val('');
        $('#name').val('');
        $('#d0').val('0');
        $('#d1').val('0');
        $('#d2').val('0');
        $('#d3').val('0');
        $('#d4').val('0');
        $('#d5').val('0');
        $('#d6').val('0');
//        $('#t0').val('0');
        $('#applypublicholidays').val('1');
        $('#templatepanel').show();
        recalc();
    }


    $('#templatelist').change(function () {
        $('#templatepanel').hide();

        var id = $(this).val();
        if(id != 'none') {
            $.ajax({
                url: "<?php echo base_url()?>ajax/gettemplate",
                data: {
                    "id": id
                },
                dataType: "html",
                type: "post",
                success: function (data) {
                    var vals = jQuery.parseJSON(data);
                    $('#id').val(id);
                    $('#name').val(vals['name']);
                    $('#d0').val(vals['d0']);
                    $('#d1').val(vals['d1']);
                    $('#d2').val(vals['d2']);
                    $('#d3').val(vals['d3']);
                    $('#d4').val(vals['d4']);
                    $('#d5').val(vals['d5']);
                    $('#d6').val(vals['d6']);
                    //                $('#t0').val('0');
                    $('#applypublicholidays').val(vals['applypublicholidays']);
                    //                document.getElementById('duration').value = vals['days'];
                    //                document.getElementById('durationd').value = vals['days'];
                    recalc();

                }
            });
        }
        else{
            reset();
        }


    })

    $(document).ready(function () {
        recalc();
    });

    $('.timesheet').blur(function(){
        if($(this).val() == '')
            $(this).val('0');
        recalc();
    });


    $('.timesheet').inputmask({
        mask: "9[.99]",
        onincomplete: function () {
            // need to add a 0 or two to the end of the field
            var cellval = $(this).val();
            $(this).val(cellval.replace("_", "0"));
        },
        oncomplete: function () {
            recalc();
        },

        onCleared: function () {
            recalc();
        },
        greedy: false
        ,
        rightAlign: true

    });
</script>
<?php include "footer.php" ?>

</body>

</html>
