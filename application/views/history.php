<?php
include "header.php";
?>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Approval History
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>
                <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">X</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">View Attachment</h4>
                            </div>
                            <div class="modal-body ">
                                <iframe id="modalsrc" src="" width="100%"></iframe>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row">
                <!-- form date pickers -->
                <!-- /form datepicker -->

                <!-- form input knob -->
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <?php
                            $format = "D d M Y";
                            $selectedtype = 'ALL';
                            if ($company->timesheets == 1) {
                                $selectedtype = $this->input->post('types');
                                ?>
                                                <div class="col-md-8 col-sm-6 col-xs-12">
                                                    <form class="form-horizontal form-label-left" id="history" novalidate=""
                                                          action="<?php echo base_url() ?>user/history" method="post">
                                                        <div class="item form-group col-md-10 col-sm-11 col-xs-12">
                                                            <label class="control-label col-sm-6 col-xs-12" for="role">Filter By
                                                                Types
                                                            </label>
                                                            <div class="col-sm-6 col-xs-12">
                                                                <select name="types" id="types" class="form-control">
                                                                    <option value="ALL" selected>Leave and Timesheets</option>
                                                                    <option value="LEAVE"
                                                                        <?php
                                                                        if (!is_null($selectedtype) && $selectedtype == 'LEAVE')
                                                                            echo "selected";
                                                                        ?>
                                                                    >Leave Requests Only
                                                                    </option>
                                                                    <option value="TIME"
                                                                        <?php
                                                                        if (!is_null($selectedtype) && $selectedtype == 'TIME')
                                                                            echo ' selected';
                                                                        ?>
                                                                    >Timesheets Only
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-1">
                                                            <button id="send" type="submit" class="btn btn-primary"
                                                                    style="visibility:hidden">Get Leave Requests
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                <?php
                            }
                            ?>

                            <table id="example" class="table table-striped responsive-utilities">
                                <thead>
                                <tr class="headings">
                                    <th>Employee</th>
                                    <th>Location</th>
                                    <th>Type</th>
                                    <th>Details</th>
                                    <th>Period</th>
                                    <th>Period_Sort</th>
                                    <th>Status</th>
                                    <th>Actioned On</th>
                                    <th>Action_Sort</th>
                                    <th class=" no-link last"><span class="nobr">Actioned By</span></th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                if ($selectedtype != 'TIME') {
                                    $leaveresults = $this->user_model->getLeaveByApprover();
                                    if (isset($leaveresults)) {
                                        foreach ($leaveresults->result() as $leave) {
                                            if ($leave->status > 1) {
                                                $employee = $this->user_model->getUser($leave->user)->row();
                                                ?>
                                                <tr>
                                                <td class=" "><?php echo $employee->name ?></td>
                                                <td class=" "><?php echo $employee->location?></td>
                                                    <td>Leave<br>
                                                        <?php
                                                        if (!is_null($leave->attachment) && $leave->attachment != "") {
                                                            ?>
                                                            <br>
                                                            <button type="button" class="btn btn-primary btn-xs"
                                                                    data-toggle="modal" data-target=".bs-world-modal"
                                                                    onclick="document.getElementById('modalsrc').src ='<?php echo base_url() . $leave->attachment ?>'">
                                                                <i class="fa fa-file"></i> View Attachment
                                                            </button>
                                                            <?php
                                                        }

                                                        if (!is_null($leave->comments) && $leave->comments != "") {
                                                            ?>
                                                            <br>
                                                            <button type="button" class="btn btn-default btn-xs"
                                                                    data-toggle="tooltip" data-placement="top" title
                                                                    data-original-title="<?php echo $leave->comments ?>"
                                                                    onclick="alert('<?php echo $leave->comments ?>')"><i
                                                                    class="fa fa-comment"></i> View Comments
                                                            </button>
                                                            <?php
                                                        } ?>
                                                    </td>
                                                <td class=" ">
                                                    <?php
                                                    echo $leave->days . " days of " . $this->user_model->getLeaveType($leave->leavetype)->row()->description . " (" . $leave->hours . " hours)";
                                                    ?>
                                                </td>
                                                <td class=" ">
                                                    From: <?php echo date_format(date_create($leave->fromdate), $format) ?>
                                                    <br>
                                                    To&nbsp;&nbsp;: <?php echo date_format(date_create($leave->todate), $format) ?>
                                                </td>
                                                <td>
                                                    <?php echo date_format(date_create($leave->fromdate), 'U'); ?>
                                                </td>

                                                <td class=" ">
                                                    <?php echo $this->user_model->getLeaveStatus($leave->status)->row()->description ?>
                                                </td>


<!--                                                <td class="a-right a-right ">-->
<!--                                                    --><?php //echo date_format(date_create($leave->created), "d M Y h:i A") ?>
<!--                                                </td>-->
<!--                                                <td>-->
<!--                                                    --><?php //echo date_format(date_create($leave->created), 'U'); ?>
<!--                                                </td>-->

                                                    <td>
                                                        <?php
                                                        if ($leave->status != 1) {
                                                            $approvalDate = date_create($leave->approvedon);
                                                            echo date_format($approvalDate, "d M Y h:i A");
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($leave->status != 1) {
                                                            $approvalDate = date_create($leave->approvedon);
                                                            echo date_format($approvalDate, 'U');
                                                        }
                                                        ?>
                                                    </td>




                                                <td class=" last">
                                                    <?php if (!is_null($leave->approvedby)) echo $this->user_model->getUser($leave->approvedby)->row()->name ?>
                                                </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                $i = 0;
                                if ($company->timesheets && $selectedtype != 'LEAVE') {
                                    $timeresults = $this->user_model->getTimesheetsByApprover();
                                    if (isset($timeresults)) {
                                        foreach ($timeresults->result() as $timesheet) {
                                            $i++;
//                                            $fromdate = $timesheet->fromdate;//new DateTime(date("l, M jS, Y", strtotime($timesheet->year . "W" . $timesheet->week . $company->timesheetstart))); // First day of week $$ CHANGE TO COMPANY START DAY
//                                            $todate =  $timesheet->todate;//new DateTime(date("l, M jS, Y", strtotime($timesheet->year . "W" . $timesheet->week . $company->timesheetstart)));;
//                                            $todate = $todate->modify('+6 days');    // need to add one more day

                                            $leaveEntries = $this->user_model->getLeaveByTimesheet($timesheet->id);
                                            $leave = $leaveEntries->row(); // first row will do.
                                            if(isset($leave)){
                                                if (1 == 1) {
                                                    $fromdate = date_create($leave->fromdate);
                                                    $todate =  date_create($leave->todate);
                                                    $future = false; // assume the date is in the past
                                                    if (date_format($todate, 'U') >= time()) $future = true;
                                                    $requestor = $this->user_model->getUser($leave->user)->row();

                                                    echo '<tr>';
                                                    ?>
                                                    <td class=" "><?php echo $requestor->name ?></td>
                                                    <td class=" "><?php echo $requestor->location ?></td>
                                                    <td>Timesheet<br>
                                                        <?php
                                                        if (!is_null($leave->attachment) && $leave->attachment != "") {
                                                            ?>
                                                            <br>
                                                            <button type="button" class="btn btn-primary btn-xs"
                                                                    data-toggle="modal" data-target=".bs-world-modal"
                                                                    onclick="document.getElementById('modalsrc').src ='<?php echo base_url() . $leave->attachment ?>'">
                                                                <i class="fa fa-file"></i> View Attachment
                                                            </button>
                                                            <?php
                                                        }

                                                        if (!is_null($leave->comments) && $leave->comments != "") {
                                                            ?>
                                                            <br>
                                                            <button type="button" class="btn btn-default btn-xs"
                                                                    data-toggle="tooltip" data-placement="top" title
                                                                    data-original-title="<?php echo $leave->comments ?>"
                                                                    onclick="alert('<?php echo $leave->comments ?>')"><i
                                                                    class="fa fa-comment"></i> View Comments
                                                            </button>
                                                            <?php
                                                        } ?>
                                                    </td>
                                                    <td class=" " style="white-space:nowrap">
                                                        <?php
                                                        foreach ($leaveEntries->result() as $item){
                                                            if ($item->hours != 0) {
                                                                echo number_format($item->hours, 3) . " hours ";
                                                                echo "of " . $this->user_model->getLeaveType($item->leavetype)->row()->description.'<br>';
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <td class=" " style="white-space:nowrap">
                                                        <?php echo date_format($fromdate, $format) ?>
                                                        to <br><?php echo date_format($todate, $format) ?>
                                                    </td>

                                                    <td class=" ">
                                                        <?php echo date_format($fromdate, "U") ?>
                                                    </td>

                                                    <td class=" "><?php echo $this->user_model->getLeaveStatus($leave->status)->row()->description ?></td>
                                                    <td>
                                                    <?php
                                                    if ($leave->status != 1) {
                                                        $approvalDate = date_create($leave->approvedon);
                                                        echo date_format($approvalDate, "d M Y h:i A");
                                                    }
                                                    ?>
                                                    </td>
                                                    <td>
                                                    <?php
                                                    if ($leave->status != 1) {
                                                        $approvalDate = date_create($leave->approvedon);
                                                        echo date_format($approvalDate, 'U');
                                                    }
                                                    ?>
                                                    </td>
                                                    <td class=" last">
                                                        <?php
                                                        if ($leave->status != 1) {
                                                            echo $this->user_model->getUser($leave->approvedby)->row()->name;
                                                        }
                                                        ?>
                                                    </td>
                                                    <?php
                                                }
                                            }
                                        }                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /form input knob -->

            </div>
        </div>


    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>


<!-- datepicker -->
<script type="text/javascript">
    $(document).ready(function () {

        $('#more').click(function () {
            var newNode = document.createElement('div');
            newNode.innerHTML = newrow(document.getElementById('rowcount').value);
            document.getElementById('leaverow').appendChild(newNode);

            document.getElementById('rowcount').value += 1;

        });

        var cb = function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange_right span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
        }

        var optionSet1 = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2015',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };

        $('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

        $('#reportrange_right').daterangepicker(optionSet1, cb);

        $('#reportrange_right').on('show.daterangepicker', function () {
            console.log("show event fired");
        });
        $('#reportrange_right').on('hide.daterangepicker', function () {
            console.log("hide event fired");
        });
        $('#reportrange_right').on('apply.daterangepicker', function (ev, picker) {
            console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange_right').on('cancel.daterangepicker', function (ev, picker) {
            console.log("cancel event fired");
        });

        $('#options1').click(function () {
            $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
        });

        $('#options2').click(function () {
            $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
        });

        $('#destroy').click(function () {
            $('#reportrange_right').data('daterangepicker').remove();
        });


    });
</script>
<!-- datepicker -->

<!-- /datepicker -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#single_cal4f').daterangepicker({
            singleDatePicker: true,
            format: 'DD/MM/YYYY',
            calender_style: "picker_4"
        }, function (start, end, label) {
            calcBusinessDays();//console.log(start.toISOString(), end.toISOString(), label);
        });
    });

    $(document).ready(function () {
        $('#single_cal4t').daterangepicker({
            singleDatePicker: true,
            format: 'DD/MM/YYYY',
            calender_style: "picker_4"
        }, function (start, end, label) {
            calcBusinessDays();////console.log(start.toISOString(), end.toISOString(), label);
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#reservation').daterangepicker(null, function (start, end, label) {
            calcBusinessDays();//console.log(start.toISOString(), end.toISOString(), label);
        });
    });
</script>
<!-- /datepicker -->
<!-- input_mask -->
<script>
    function calcBusinessDays() {         // input given as Date objects
        var d1 = document.getElementById('single_cal4f').value;
        var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
        var dDate1 = new Date(d1.replace(pattern, '$3-$2-$1'));

        var d2 = document.getElementById('single_cal4t').value;
        var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
        var dDate2 = new Date(d2.replace(pattern, '$3-$2-$1'));

//        var dDate1 =  new Date(document.getElementById('single_cal4f').value);
//        var dDate2 =  new Date(document.getElementById('single_cal4t').value);

        var iWeeks, iDateDiff, iAdjust = 0;

        if (dDate1 == '' || dDate2 == '') return -1;
        if (dDate2 < dDate1) return -1;                 // error code if dates transposed

        var iWeekday1 = dDate1.getDay();                // day of week
        var iWeekday2 = dDate2.getDay();

        iWeekday1 = (iWeekday1 == 0) ? 7 : iWeekday1;   // change Sunday from 0 to 7
        iWeekday2 = (iWeekday2 == 0) ? 7 : iWeekday2;

        if ((iWeekday1 > 5) && (iWeekday2 > 5)) iAdjust = 1;  // adjustment if both days on weekend

        iWeekday1 = (iWeekday1 > 5) ? 5 : iWeekday1;    // only count weekdays
        iWeekday2 = (iWeekday2 > 5) ? 5 : iWeekday2;

        // calculate differnece in weeks (1000mS * 60sec * 60min * 24hrs * 7 days = 604800000)
        iWeeks = Math.floor((dDate2.getTime() - dDate1.getTime()) / 604800000)

        if (iWeekday1 <= iWeekday2) {
            iDateDiff = (iWeeks * 5) + (iWeekday2 - iWeekday1)
        } else {
            iDateDiff = ((iWeeks + 1) * 5) - (iWeekday1 - iWeekday2)
        }

        iDateDiff -= iAdjust                            // take into account both days on weekend

        // return (iDateDiff + 1);                         // add 1 because dates are inclusive
        document.getElementById('duration').value = (iDateDiff + 1);
    }
</script>
<!-- /input mask -->

<!-- Datatables -->
<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>
<script>
    $(document).ready(function () {
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search:"
            },
            "bStateSave": false,
            "responsive": false,
            "aoColumnDefs": [
                {
                    "asSorting": ["desc", "asc"],
                    "aTargets": [2]
                },
                {
                    "bSortable": false,
                    "aTargets": [1]
                },
                {
                    "iDataSort": 8,
                    "aTargets": [7],
                },
                {
                    "iDataSort": 5,
                    "aTargets": [4]
                }
                ,
                {
                    "bVisible": false,
                    "aTargets": [5, 8]
                }
            ],
            "iDisplayLength": 25,
            "sPaginationType": "full_numbers",
            "dom": '<"clear">lfrtip'
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });

    $('.modal').on('show.bs.modal', function () {
        $('.modal-body').css('height', $(window).height() * 0.75);
        $('#modalsrc').css('height', $(window).height() * 0.75);

    });


    <?php
    if ($company->timesheets == 1) {
    ?>
    $('#types').change(function () {
//        $('#send').removeClass('btn-success');
//        $('#send').addClass('btn-danger');
        $('#send').trigger('click');
    });
    <?php
    }
    ?>

</script>

<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>
