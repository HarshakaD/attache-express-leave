<?php
include "header.php";
?>

<style>
    .dg_form table{
        border:1px solid silver;
    }

    .dg_form th{
        background-color:gray;
        font-family:"Courier New", Courier, mono;
        font-size:12px;
    }

    .dg_form td{
        background-color:gainsboro;
        font-size:12px;
    }

    .dg_form input[type=submit]{
        margin-top:2px;
    }
</style>

<body class="nav-md">

    <div class="container body">


        <div class="main_container">

        <?php
        include "navbar.php";
        ?>

            <!-- page content -->
            <div class="right_col" role="main" style="overflow: auto">

                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Import Attaché Data</h3>
                        </div>

                    </div>
                    <div class="clearfix"></div>


                    <div class="row">
                        <div class="col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Drop / Load File</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <form action="<?php echo base_url()?>ajax/importbi" enctype="multipart/form-data" class="dropzone" style="border: 1px solid #e5e5e5; height: 250px; " id="my-awesome-dropzone"></form>

                                    <br />
                                    <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row" >
                        <div class="col-xs-12" id="tabledata">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Fields Supported</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br>
                                    <table class="tbl-qa table table-bordered jambo_table">
                                        <thead>
                                        <tr>   <th>Attaché BI Field Name </th>     <th>Attaché Accounts Field Name</th>   <th>Updating Existing Users</th>     <th>Creating New Users</th>          <th>Other Information</th></tr>
                                        </thead>

                                        <tbody>
                                        <tr class="info"><th colspan="5">Requried Fields</th></tr>
                                        <tr class="warning"><th>EmployeeCode</th>       <th>Employee Code</th>          <td>Required</td>           <td>Required</td>           <td>Needs to be Unique per Employee</td></tr>
                                        <tr class="warning"><th>AOEmail or WorkEmail</th>          <th></th>                       <td>Optional if EmployeeCode provided</td><td>Required</td><td>Needs to be Unique per Employee</td></tr>
                                        <tr class="info"><th colspan="5">Employee Details</th></tr>
                                        <tr>                <th>FullName</th>           <th>Full Name</th>              <td>Optional</td>           <td>Required</td>           <td></td></tr>
                                        <tr>                <th>Location</th>           <th>Employee Location</th>      <td>Optional</td>           <td>Optional</td>           <td>This relates to <b>"Location"</b> in Attaché Payroll</td></tr>
                                        <tr>                <th>Region / State</th>     <th>New Address - State</th>    <td>Optional</td>           <td>Optional</td>           <td>Will default to company State if value not found</td></tr>
                                        <tr>                <th>HoursPerDay</th>        <th>Hours per Day</th>          <td>Optional</td>           <td>Optional</td>           <td>New Users</td></tr>
                                        <tr><td><i>Not an Attaché field</i></td>   <td><i>Not an Attaché field</i></td><td colspan="2"><i>The default approver can be specified by adding a "DefaultApprover" column, with the approvers EmployeeCode</i></td>      <td>Set to You (<?php echo $this->session->userdata('name');?>) by default</td></tr>
                                        <tr class="info"><th colspan="5">Leave Balances</th></tr>
                                        <tr class="warning"><th>LeaveAccrueTo</th>      <th>Leave Accrued to/Last Pay Date</th><td>Optional</td>    <td>Optional</td>           <td>Used to indicate the date to which leave balances have been accrued</td></tr>
                                        <tr>                <th>SLEntitleHourAcc</th>   <th>Acc-SL-Entitlement Hours</th>   <td>Optional</td>           <td>Optional</td>           <td>Amount is combined with SL ProRata</td></tr>
                                        <tr>                <th>SLProRataHourAcc</th>   <th>Acc-SL-Pro-rata Hours</th>      <td>Optional</td>           <td>Optional</td>           <td>Amount is combined with SL Entitled</td></tr>
                                        <tr>                <th>ALEntitleHourAcc</th>   <th>Acc-AL-Entitlement Hours</th>      <td>Optional</td>           <td>Optional</td>           <td>Amount is combined with AL ProRata</td></tr>
                                        <tr>                <th>ALProRataHourAcc</th>   <th>Acc-AL-Pro-rata Hours</th>      <td>Optional</td>           <td>Optional</td>           <td>Amount is combined with AL Entitled</td></tr>
                                        <tr>                <th>LSLEntitleHourAcc</th>  <th>Acc-LSL-Entitlement Hours</th>  <td>Optional</td>           <td>Optional</td>           <td>Amount is combined with LSL ProRata</td></tr>
                                        <tr>                <th>LSLProRataHourAcc</th>  <th>Acc-LSL-Pro-rata Hours</th>     <td>Optional</td>           <td>Optional</td>           <td>Amount is combined with LSL Entitled</td></tr>
                                        <tr>                <th>RDOEntitleHourAcc</th>  <th>Acc-RDO-Entitlement Hours</th>  <td>Optional</td>           <td>Optional</td>           <td>Amount is combined with RDO ProRata</td></tr>
                                        <tr>                <th>RDOProRataHourAcc</th>  <th>Acc-RDO-Pro-rata Hours</th>     <td>Optional</td>           <td>Optional</td>           <td>Amount is combined with RDO Entitled</td></tr>

                                        <tr>                <th>STLEntitleHourAcc</th>  <th>Acc-STL-Entitlement Hours</th>  <td>Optional</td>           <td>Optional</td>           <td>Amount is combined with STL ProRata</td></tr>
                                        <tr>                <th>STLProRataHourAcc</th>  <th>Acc-STL-Pro-rata Hours</th>     <td>Optional</td>           <td>Optional</td>           <td>Amount is combined with STL Entitled</td></tr>
                                        <tr>                <th>UDL1EntitleHourAcc</th> <th>Acc-U1-Entitlement Hours</th>   <td>Optional</td>           <td>Optional</td>           <td>Amount is combined with U1 ProRata</td></tr>
                                        <tr>                <th>UDL1ProRataHourAcc</th> <th>Acc-U1-Pro-rata Hours</th>      <td>Optional</td>           <td>Optional</td>           <td>Amount is combined with U1 Entitled</td></tr>
                                        <tr>                <th>UDL2EntitleHourAcc</th> <th>Acc-U2-Entitlement Hours</th>   <td>Optional</td>           <td>Optional</td>           <td>Amount is combined with U2 ProRata</td></tr>
                                        <tr>                <th>UDL2ProRataHourAcc</th> <th>Acc-U2-Pro-rata Hours</th>      <td>Optional</td>           <td>Optional</td>           <td>Amount is combined with U2 Entitled</td></tr>

                                        <tr>                <th>FlexHourAccrue</th>     <th></th>                           <td>Optional</td>           <td>Optional</td>           <td></td></tr>

                                        <tr class="info"><th colspan="5">Default Settings</th></tr>
                                        <tr class="danger"><td><i>User Type</i></td>          <td><i>Not an Attaché field</i></td><td><i>Not an Attaché field</i></td>      <td>Initially set to "Employee (Non-Approver)"</td><td>Not an Attaché Field</td></tr>

                                        </tbody>
                                    </table>
                                    <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>


            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

    <script src="<?php echo base_url()?>js/custom.js"></script>
        <!-- dropzone -->
        <script src="<?php echo base_url()?>js/dropzone/dropzone.js"></script>

    <script>
        $( document ).ready(function() {
            // Handler for .ready() called.

            $("#send" ).on ({
                click:function( event,ui ) {
                    var sql = $('#sql').val();
                    $.ajax({
                        url: '<?php echo base_url() ?>ajax/execute',
                        type: 'post',
                        data: {
                            sql: sql
                        },
                        success: function (data) {
                            $('#tabledata').html(data);
                        }
                    }).error(function() {
                            alert ('An error occurred');
                    });

                }
            });

        });



    </script>
    <script>


    </script>

        <script src="<?php echo base_url()?>js/textarea/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));

            window.Dropzone.options.myAwesomeDropzone = {
                init: function(){
                    this.on("success", function(file, data){
                        $('#tabledata').html(data);
                    });
                }
            }

        </script>

        <!-- footer content -->
        <?php include "footer.php" ?>
        <!-- /footer content -->
</body>

</html>
