<?php
include "header.php";
$company = $this->user_model->getCompany($this->session->userdata('company'));

?>

<body class="nav-md">

<link href="<?php echo base_url() ?>css/calendar/fullcalendar.css" rel="stylesheet">
<link href="<?php echo base_url() ?>css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="page-title">
                    <div class="title_left">
                        <h3>
                            Timesheets Calendar View
                        </h3>
                    </div>

                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <div class="item form-group">
                                    <label class="col-sm-3">
                                    </label>
                                    <label class="control-label col-sm-3 col-xs-12" for="location">Show Public Holidays
                                    </label>
                                    <div class="col-sm-6 col-xs-12">
                                        <?php
                                        $countries = $this->user_model->getCountry();
                                        foreach ($countries->result() as $country) {
                                            ?>
                                            <input type="checkbox" name="country" value="<?php echo $country->id ?>"
                                                   onchange="recal()"> <?php echo $country->name ?><br>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <hr>
                                    <br>
                                </div>
                                <div id='calendar'></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>


        <!-- End Calender modal -->
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<script src="<?php echo base_url() ?>js/nprogress.js"></script>
<!-- chart js -->
<script src="<?php echo base_url() ?>js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>

<script src="<?php echo base_url() ?>js/custom.js"></script>

<script src="<?php echo base_url() ?>js/moment.min.js"></script>
<script src="<?php echo base_url() ?>js/calendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url() ?>js/randomColor.js"></script>

<script>
    $(window).load(function () {
        cal();
    });

    function recal() {
        //for each checkbox, 
        // if checked, add the id to an array. 
        // implement servicer side. 
        var checkboxes = document.getElementsByName('country');
        var countries = '';
        // loop over them all
        for (var i = 0; i < checkboxes.length; i++) {
            // And stick the checked ones onto an array...
            if (checkboxes[i].checked) {
                if (countries != '') countries += ':'
                countries += checkboxes[i].value;
            }
        }

        var source = "<?php echo base_url() ?>Ajax/getHolidays/";
        // if checked
        $('#calendar').fullCalendar('destroy');
        $.ajax({
            type: "POST",
            url: source,
            data: {countries: countries},
            success: function (result) {
                cal(result);
            }
        });


        // if unchecked
//        $('#calendar').fullCalendar( 'removeEventSource', source )

    }

    <?php
    $eventStr = "";
    $first = true;

    $timesheets = $this->user_model->getTimesheetsByUser();

    // change this to get the days, not the week.
    foreach ($timesheets->result() as $timesheet) {
        $timeentries = $this->user_model->getTimesheetDetails($timesheet->id);
        foreach ($timeentries->result() as $timeEntry){
            if($timeEntry->status == 4)
                continue;   // cancelled
            $letters = 'ABCDEF';
            $typeresult = $this->user_model->getLeaveType($timeEntry->type);
            if($typeresult->num_rows() > 0) {
                $type = $typeresult->row()->description;

                $dcode = crc32($type);
                $c1 = substr($dcode, 0, 6);

                if ($first) {
                    $first = false;
                    $eventStr .= '{';
                } else
                    $eventStr .= ',{';
$test = '';
                if($company->timesheetstart <= $timeEntry->day){
                    $fromdate = date("Y-m-d", strtotime($timesheet->year . "-W" . sprintf("%02d",$timesheet->week). "-".$timeEntry->day));
                }
                elseif($timesheet->week < 52) {
                    $fromdate = date("Y-m-d", strtotime($timesheet->year . "-W" . sprintf("%02d",$timesheet->week+1). "-".$timeEntry->day));
                }
                //                    $fromdate = date_create(); // First day of week
                //
                $eventStr .= '  "title":"' . $type . ': ';
                $eventStr .= number_format($timeEntry->hours + $timeEntry->minutes / 60, 2) . ' hours '.$test;

                //
//                $desc = '1';//$this->user_model->getStatus($timeEntry->status)->description;

                switch ($timeEntry->status){
                    case 0: // saved
                        $color = '#1D92EF';
                        break;
                    case 1: // pending approval
                        $color = '#f0ad4e';
                        break;
                    case 2: // approved
                        $color = '#26B99A';
                        break;
                    case 3: // rejected
                        $color = '#ffa500';
                        break;
                    case 4: // canclled
                        $color = '#999999';
                        break;
                }

                //            $eventStr .=  ' of '.$desc;
//                $color = '#' . substr(md5($desc), 0, 6);
                $contrast = (hexdec($color) > 0xffffff / 1.7) ? 'black' : 'white';
                $eventStr .= '",';
                $eventStr .= ' "color": "' . $color . '",';
                $eventStr .= ' "textColor": "' . $contrast . '",';
                $eventStr .= '  "start": "' . $fromdate . '",';
                $eventStr .= '  "end": "' . $fromdate . '",';
                $eventStr .= '  "allDay": true';
                $eventStr .= '}';

            }
        }
    }


    //echo '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);

    ?>

    function cal(events) {
        events = typeof events !== 'undefined' ? events : '';
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var started;
        var categoryClass;
        var calConfig = '{ ';
        calConfig += '"header": { "left": "month, basicWeek, today", "center": "prev title next", "right":"" },';
        calConfig += '"titleRangeSeparator": " to ", "selectable": false, "editable": false, "weekends": <?php if ($company->sevendayleave) echo "true"; else echo "false";?>,';


        <?php
        $startday = 1; // monday
        //        if ($company->timesheets) {
        //            $startday = $company->timesheetstart;
        //        }
        ?>

        calConfig += '"firstDay": <?php echo $startday ?>,';

//        calConfig += '"columnFormat": {  "month": "ddd", "week": "dddd", "day": "dddd"  },';
        calConfig += '"events": [';
        calConfig += '<?php echo $eventStr?>';
        if (events != '')
            calConfig += ', ' + events;
        calConfig += ']';
        calConfig += '}';

        var calendar = $('#calendar').fullCalendar(JSON.parse(calConfig));
        // var valendar
    }


</script>
<?php
include "footer.php";
?>
</body>

</html>


