<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Attaché Car Parking</title>
</head>

<body>
    <?php
        function displayUsers(){
            $host = 'localhost';
            $user = 'id7483243_carparkingdb';
            $pass = 'Unicorns4eva';
            $db = 'id7483243_carparking';
            
            $conn = mysqli_connect($host,$user,$pass,$db);
            if(!$conn){
                echo "Could not connect to database";
            }
            else{
                $day = $_GET['selectedDay'];
                $carpark = $_GET['carpark'];
                $sql = "SELECT userid,fullname FROM user WHERE userid NOT IN (SELECT user FROM carparkallocation WHERE carparkdate='" . $day . "') ORDER BY fullname";

                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result) > 0){
                    while($row = $result->fetch_assoc()){
                        $url="allocatingcarpark.php";
                        $url.= "?selectedDay=" . $day . "&amp;user=" . $row['userid'] . "&amp;carpark=" . $carpark;
                        echo "<div class=\"box\">";
                        echo "<a href=\"" . $url . "\">";
                        echo "<p>" . $row['fullname'] . "</p></div></a>";
                    }
                }
                else{
                    echo "<h3>No staff to display</h3>";
                }
            }
        }
    ?>
    <!--Title section-->
    <section class="top">
            <header class="top">
                <?php
                    $day = $_GET['selectedDay'];
                    $carpark = $_GET['carpark'];
                    $process = $_GET['process'];
                    echo "<h1>Assign Car Park " . $carpark . " for " . date('D d M',strtotime($day)) . " to...</h1>";
                    if($process=='error'){
                        echo "<h3>Could not assign car park!</h3>";
                    }
                    elseif($process=='assigned'){
                        echo "<h3>Successfully assigned car park</h3>";
                    }
                ?>
                <a href="index.php">Home</a>
            </header>
    </section>

    <!--Main section-->
    <section class="boxes">
        <?php
            displayUsers();
        ?>
    </section>
</body>

</html>