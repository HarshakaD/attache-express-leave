<?php
$this->load->view("header.php");
?>
<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php

        $this->load->view("navbar.php");
        ?>

        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Attaché Parking - Permanent Allocations
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>

            </div>
            <div class="clearfix"></div>


            <div class="row">

                <div class="wrapper">
                    <!--Title-->
<table class="table table-striped projects" id="example" style="max-width: 800px">
    <thead>
    <td>Space</td>
    <td>Allocation</td>
    <td>Allocate</td>
    <td>Notes / Properties</td>
    </thead>
<tbody>
<?php
$spaceList = $this->user_model->getAllSpots();
$staff = $this->user_model->getUsers();
if(isset($spaceList)) {
    foreach ($spaceList->result() as $spot) {
        echo "<tr>";
        echo "<td>Space# " . $spot->number."</td>";
        echo "<td>";
        if ($spot->PermanentOwner != 0) {
            $owner = $this->user_model->getUser($spot->PermanentOwner)->row();
            echo $owner->name;
        }
        echo "</td>";
        echo "<td>";
        if ($spot->PermanentOwner != 0) {
            echo "<button class='btn btn-sm btn-warning remove' tag='".$spot->number."'>un-assign</button>";
        }else {
            ?>
            <select class="form-control input-sm reassign" Tag="<?php echo $spot->number ?>">
                <option value="0">____Unallocated____</option>
                <?php
                if (isset($staff)) {
                    foreach ($staff->result() as $person) { ?>
                        <option
                                value="<?php echo $person->id ?>" <?php
                        if ($spot->PermanentOwner != 0) {
                            if ($spot->PermanentOwner == $person->id) echo 'selected';
                        }
                        ?> ><?php echo $person->name ?>
                        </option>
                    <?php }
                }
                ?>
            </select>
            <?php
        }
        echo "</td>";
        echo "<td>".$spot->notes."</td>";
        echo "</tr>";
    }
}
?>

</tbody>
</table>



                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>
<script>
    $(document).ready(function () {
        $('.reassign').change(function () {

            $.ajax({
                url: "<?php echo base_url()?>ajax/assignSpot",
                data: {
                    "user": this.value,
                    "space": this.getAttribute('Tag'),
                },
                dataType: "html",
                type: "post",
                success: function (data) {
                    if (data == "0") {
                        alert("Spot has been successfully allocated");
                    }
                    else {
                        alert("Couldnt allocate");
                    }
                }
            });
        });
        $('.remove').click(function () {

            $.ajax({
                url: "<?php echo base_url()?>ajax/assignSpot",
                data: {
                    "user": 0,
                    "space": this.getAttribute('Tag'),
                },
                dataType: "html",
                type: "post",
                success: function (data) {
                    if (data == "0") {
                        alert("Spot has been successfully unallocated");
                        window.location.reload();
                    }
                    else {
                        alert("Couldnt unallocate - something went wrong");
                    }
                }
            });
        });

    });
</script>

<!-- footer content -->
<?php $this->load->view("footer.php"); ?>
<!-- /footer content -->
</body>

</html>