<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Attaché Car Parking</title>
</head>

<body>
    <!--Main section-->
    <section class="standard">
        <h3>Assigning car park...</h3><br>

    <?php
        $host = 'localhost';
        $user = 'id7483243_carparkingdb';
        $pass = 'Unicorns4eva';
        $db = 'id7483243_carparking';

        $conn = mysqli_connect($host,$user,$pass,$db);
        if(!$conn){
            echo "Could not connect to database";
        }
        else{
            $day=$_GET['selectedDay'];
            $user=$_GET['user'];
            $carpark=$_GET['carpark'];

            $sql = "SELECT * FROM carparkallocation WHERE user=0 and carparkdate='" . $day ."' AND carpark=" . $carpark;

            $result = mysqli_query($conn,$sql);

            if(mysqli_num_rows($result) == 1){
                $sql = "UPDATE carparkallocation SET user=" . $user ." WHERE user=0 AND carparkdate='" . $day ."' AND carpark=" . $carpark;
    
                if(mysqli_query($conn,$sql)){
                    echo "<h3>Successfully assigned car park</h3>";
                    $url = "day.php";
                    $url.= "?selectedDay=" . $day . "&carpark=" . $carpark . "&process=assigned";
                    echo "<script>location.href = '" . $url ."';</script>";
                } else{
                    echo "<h3>Could not update database</h3>";
                    $url = "allocatecarpark.php";
                    $url.= "?selectedDay=" . $day . "&carpark=" . $carpark . "&process=error";
                    echo "<script>location.href = '" . $url ."';</script>";
                }
            }
            else{
                echo "<h3>Car park has already been taken</h3>";
                $url = "allocatecarpark.php";
                $url.= "?selectedDay=" . $day . "&carpark=" . $carpark . "&process=error";
                echo "<script>location.href = '" . $url ."';</script>";
            }
            $conn -> close();
        }

    ?>
        </section>
</body>

</html>