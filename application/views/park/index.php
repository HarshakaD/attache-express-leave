<?php
$this->load->view("header.php");
?>
<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php

        $this->load->view("navbar.php");
        ?>

        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Attaché Car Parking - B3
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row">

    <div class="wrapper">
        <!--Title-->

        <!--Calendar view-->
        <section class="boxes">
            <!--Available spots Function-->
            <?php
            $perm = $this->user_model->doesUserHavePerm();
            if($perm === false){
                $onrotation = $this->user_model->isUserOnRotation();
                if($onrotation === false){
                    // user not on rotation list
                    ?>
                    <div class="boxinfoaction rotate"><h3>Please add me to the car park rotation</h3></div>
                    <?php
                }
                else{
                    ?>
                    <div class="boxinfoaction norotation"><h3>You are on the car park rotation</h3><p>Take me off the car park rotation</p></div>
                    <?php
                }
            }
            else{
                ?>
                <div class="boxinfo"><h3>My usual spot is<br><?php echo $perm->number ?></h3></div>
            <?php
            }
            ?>
            <?php
            	$smsenabled = $this->user_model->doesUserHaveCarparkSMS();
				$url = $url = base_url().'user/park/addmobile';

				if($smsenabled === true){
                	echo "<div class=\"boxinfoaction\"><a href=\"" . $url . "\"><h3>Modify your SMS notifications here</h3></a></div>";
                }
				else{
					echo "<div class=\"boxinfoaction\"><a href=\"" . $url . "\"><h3>SMS notifications disabled</h3><p>Set it up here</p></a></div>";
				}
            ?>
            
            
            <!--Display the box-->
            <?php
                    $date = date('Y-m-d');
                    for($days=0;$days<30;$days++){
                        if(date('w',strtotime($date))>0 && date('w',strtotime($date))<6){
                            $datetext = date('D d M',strtotime($date));
                            $url = base_url().'user/park/day.php';
                            $url .= '?selectedDay=' . $date;


                            $spot =  $this->user_model->doesUserHaveSpot($date);
                            if($spot === false){
                                $availablespots = $this->user_model->availableSpots($date);

                                if (is_numeric($availablespots) && $availablespots > 0) {
                                    echo "<div class=\"boxfree\" ><a href=\"" . $url . "\"><h3>" . $datetext . "</h3>";
                                    echo "<h4>" . $availablespots . " spots are available</h4>";
                                    echo "<p>View available spots</p>";
                                	echo "</a>";
                                } else if ($availablespots == 0) {
                                	$isOnWaitingList = $this->user_model->isUserOnWaitingList($date);
                                	if($isOnWaitingList) {
                                    echo "<div class='boxwaiting nowaiting' Tag='".$date."'><h3>" . $datetext . "</h3>";
                                    echo "<h4>On Waiting List</h4>";                                  
                                    echo "<p>Get me off it</p>";   
                                    }
                                	else{
                                    echo "<div class='boxempty waitinglist' Tag='".$date."'><h3>" . $datetext . "</h3>";
                                    echo "<h4>No spots available</h4>";
                                	echo "<p>Add me to the waiting list</p>";
                                	}
                                } else {
                                    echo "<div class=\"boxempty \" ><h3>" . $datetext . "</h3>";
                                    echo "<p>Something went wrong</p>";
                                }
                                
                            }else {
                                    echo "<div class='boxitem book' Tag='".$date."'><h3>" . $datetext . "</h3>";
                                    echo "<h4>You have space # " . $spot->carpark . "</h4>";
                                    echo "<p>Release spot?</p>";
                            }

                            echo "</div>";
                        }
                        $date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
                    }

            ?>

        </section>


    </div>
    </div>
        </div>
    </div></div>

<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>
<script>
    $(document).ready(function () {
        $('.book').click(function () {
            if (confirm("Are you sure you want to release this spot?")) {
                $.ajax({
                    url: "<?php echo base_url()?>ajax/releaseSpot",
                    data: {
                        "date": this.getAttribute('Tag')
                    },
                    dataType: "html",
                    type: "post",
                    success: function (data) {
                        if (data == "0") {
                            alert("Spot has been successfully released");
                            window.location.replace("<?php echo base_url() . "user/park"?>");
                        }
                        else {
                            alert("Cannot release spot at this time");
                        }
                    }
                });
            }
        });
        $('.waitinglist').click(function () {
                $.ajax({
                    url: "<?php echo base_url()?>ajax/goOnWaitingList",
                    data: {
                        "date": this.getAttribute('Tag')
                    },
                    dataType: "html",
                    type: "post",
                    success: function (data) {
                        if (data == "0") {
                            alert("You have been added to the waiting list. Good luck!");
                        	window.location.replace("<?php echo base_url() . "user/park"?>");
                        }
                    	else if(data == "2"){
                        	alert("You are already on the waiting list");
                        }
                        else {
                            alert("Could not be added to the waiting list");
                        }
                    }
                });
        });
        $('.nowaiting').click(function () {
                $.ajax({
                    url: "<?php echo base_url()?>ajax/removeFromWaitingList",
                    data: {
                        "date": this.getAttribute('Tag')
                    },
                    dataType: "html",
                    type: "post",
                    success: function (data) {
                        if (data == "0") {
                            alert("You are now off the waiting list");
                        	window.location.replace("<?php echo base_url() . "user/park"?>");
                        }
                        else {
                            alert("Could not be removed from the waiting list");
                        }
                    }
                });
        });
        $('.rotate').click(function () {
                $.ajax({
                    url: "<?php echo base_url()?>ajax/addUserToRotation",
                    data: {
                        
                    },
                    dataType: "html",
                    type: "post",
                    success: function (data) {
                        if (data == "0") {
                            alert("You are now on the car park rotation");
                        	window.location.replace("<?php echo base_url() . "user/park"?>");
                        }
                        else {
                            alert("Could not be added to the car park rotation");
                        }
                    }
                });
        });
        $('.norotation').click(function () {
                $.ajax({
                    url: "<?php echo base_url()?>ajax/removeFromRotation",
                    data: {
                        
                    },
                    dataType: "html",
                    type: "post",
                    success: function (data) {
                        if (data == "0") {
                            alert("You are now off the car park rotation");
                        	window.location.replace("<?php echo base_url() . "user/park"?>");
                        }
                        else {
                            alert("Could not be removed from the car park rotation");
                        }
                    }
                });
        });
    });
</script>

        <!-- footer content -->
        <?php $this->load->view("footer.php"); ?>
        <!-- /footer content -->
</body>

</html>