<?php
$this->load->view("header.php");
?>
<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php

        $this->load->view("navbar.php");
        ?>

        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Attaché Car Parking
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row">

                <div class="wrapper">
        <section>
        <center>
                    <form method="post" action="<?php echo base_url()?>user/updatecarparkmobile">
                    <?php
                        $mobile="";
						$enabled="";
                    	$mobilerow = $this->user_model->getcarparkcontact($this->session->userdata('id'));
						if($mobilerow) {
                        	$mobile=substr($mobilerow->mobile,-8);
                        	if($mobilerow->enabled == "true") {
                            	$enabled="checked";
                            }
                        }
                    ?>
        
        			<h3>Enable SMS notifications</h3>
					<label class="cpswitch">
  						<input type="checkbox" id="enablesms" name="enablesms" value="true" <?php echo $enabled;?> >
  						<span class="cpslider round"></span> 
        			</label>
                    <div id="sms" <?php if($enabled!="checked") echo "style=\"display: none;\""; ?>>
					<h4>Enter your mobile number for SMS notifications <br><br>     
                    	<input value='04' disabled size='2'><input type='text' pattern='\d{8}' required title='Enter your mobile number' autofocus maxlength='8' minlength='8' size='8' name='mobilenumber' oninvalid="alert('Invalid Mobile Number!');" value="<?php echo $mobile;?>"></div><br><br>
                    	<button class="btn btn-lg btn-success" value="Save my SMS settings">Save my SMS settings</button>
                   		</h4>
                    <?php
//                    	$result = isset($_GET['result']) ? $_GET['result'] : "";

						if($result == "success"){
                        	echo "<h4>SMS settings successfully saved</h4>";
                        }
						else if($result=="error"){
                        	echo "<h4>Error trying to save SMS settings</h4>";
                        }
                    ?>
                    </form>

        </center></section>
            </div>

        </div>
    </div>
</div>
</div>

<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>
                    
<script>
         $(function () {
         	$("#enablesms").click(function () {
            	if($(this).is(":checked")) {
                	$("#sms").show();
                } else {
                	$("#sms").hide();
                }
            });
         });
</script>


<!-- footer content -->
<?php $this->load->view("footer.php"); ?>
<!-- /footer content -->
</body>

</html>