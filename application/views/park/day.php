<?php
$this->load->view("header.php");
?>
<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php

        $this->load->view("navbar.php");
        ?>

        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Attaché Car Parking
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row">

                <div class="wrapper">
                    <!--Title section-->
                    <section class="top">
                        <header class="top">
                            <h1>
                                <?php
                                $day = $_GET['selectedDay'];
                                echo "Available spots for " . date('D d M', strtotime($day));
                                ?>
                            </h1>
                            <?php

                            //                        $process=$_GET['process'];
                            //                        $carpark=$_GET['carpark'];
                            //                        if($process=='assigned'){
                            //                            echo "<h3>Successfully assigned car park " . $carpark . "</h3>";
                            //                        }

                            ?>
                        </header>
                        <a href="index.php">Home</a>
                    </section>

                    <!--Boxes-->
                    <section class="boxes">
                        <!--Dislay Spot function-->
                        <?php

                        $allocations = $this->user_model->getAvailableSpots($day);
                        if (count($allocations) == 0) {
                            echo "<h3>Sorry, there are no car parks available today</h3>";
                        }

                        foreach ($allocations->result() as $row){
                        ?>
                        <div class="book boxfree" Tag="<?php echo $row->carpark ?>">
                        <h3><?php echo $row->carpark ?></h3>
                        <p>Space is free</p>
                        </div>
                <?php
                }
                ?>
                </section>
            </div>

        </div>
    </div>
</div>
</div>

<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>
<script>
    $(document).ready(function () {
        $('.book').click(function () {
            var div = this;
            $.ajax({
                url: "<?php echo base_url()?>ajax/allocateSpot",
                data: {
                    "carpark": this.getAttribute('Tag'),
                    "date": "<?php echo $day ?>"
                },
                dataType: "html",
                type: "post",
                success: function (data) {
                    
                    if(data == "0"){
                    	// alert("Sweet! The spot is mine!");
                        setTimeout(function() {
                            window.location.replace("<?php echo base_url() . "user/park"?>");
                        }, 3000);
                        new PNotify({title: 'Parking', text: "Sweet! The spot is mine!", type: 'success'});
                    }
                	else if(data == "1"){
                        setTimeout(function() {
                            window.location.replace("<?php echo base_url() . "user/park"?>");
                        }, 3000);
                        new PNotify({title: 'Parking', text: "Hmmm... Somehow allocated to multiple spots", type: 'warn'});
                	}
                	else if(data == "2") {
                        $(div).hide();
                	    alert("Sorry, this spot is no longer available");

                	}
                	else if(data=="4"){
                    	setTimeout(function() {
                            window.location.replace("<?php echo base_url() . "user/park"?>");
                        }, 3000);
                        new PNotify({title: 'Parking', text: "You already have a space on this day", type: 'warn'});
                    }
                	else {
                        $(div).hide();
						alert("Could not take spot at this time");
                	}
                }
            });
        });

    });
</script>

<!-- footer content -->
<?php $this->load->view("footer.php"); ?>
<!-- /footer content -->
</body>

</html>