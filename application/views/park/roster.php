<?php
$this->load->view("header.php");
?>
<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php

        $this->load->view("navbar.php");
        ?>

        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Attaché Parking Management
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>

            </div>
            <div class="clearfix"></div>
<h1>Lets get ready to Rotate! </h1>

            <div class="row">

                <div class="wrapper">
<form action="generateroster" method="post">
    <select class="form-control" style="max-width: 200px"  name="month">
        <?php
        $thisMonth = strtotime(date('Y')."-".date('m')."-01");
        $monthList = $this->user_model->getRosterMonths();
        $i = 0;
        foreach ($monthList->result() as $month){
            echo "<option value='".$month->id."'>";
            echo date_format(date_create($month->monthstart), "F Y");
            echo "</option>";
            $i++;
            if ($i >= 6)
                break;
        }
        ?>
    </select>
    <br>
<table class="table table-striped projects" id="example" style="max-width: 800px">
    <thead>
    <td>Space</td>
    <td>Status</td>
    <td>Allocation</td>
    <td>Notes / Properties</td>
    </thead>
<tbody>
<?php
$spaceList = $this->user_model->getAllSpotsForRotation();

$staff = $this->user_model->getUsersOnRotation();
if(isset($spaceList)) {
    foreach ($spaceList->result() as $spot) {
        echo "<tr>";
        echo "<td>Number: " . $spot->number."</td>";
        echo "<td> </td>";
        echo "<td>";
                ?>
                <select class="form-control input-sm reassign"  Tag="<?php echo $spot->number ?>" name="<?php echo $spot->number ?>">
                    <option value="0" >____Unallocated____</option>
                    <?php
                    if (isset($staff)) {
                        foreach ($staff->result() as $spotter) {
                            $person = $this->user_model->getUser($spotter->user);
                            $person = $person->row();
                            ?>
                            <option value="<?php echo $person->id ?>" ><?php echo $person->name ?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <?php
        echo "</td>";
        echo "<td>".$spot->notes."</td>";
        echo "</tr>";
    }
}
?>
<tr>
    <td colspan="4"></td>
</tr>
<tr>
    <td colspan="3"></td>
    <td><input type="submit"  class="btn btn-lg btn-success" id="generateRoster" value="Ok, Make magic happen"></input></td>
</tr>
</tbody>
</table>

</form>
                </div>


                </div>
            </div>
        </div>
    </div>



<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>


<!-- footer content -->
<?php $this->load->view("footer.php"); ?>
<!-- /footer content -->
</body>

</html>