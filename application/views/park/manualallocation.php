<?php
$this->load->view("header.php");
?>
<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php

        $this->load->view("navbar.php");
        ?>

        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
        				<?php
        				$space = $_GET['carpark'];
						$month = $_GET['month'];
						$year = $_GET['year'];
						$monthObj = (DateTime::createFromFormat('!m',$month));
						$monthName = $monthObj->format('F');
                        echo "Attaché Parking - Manual Allocations for Space# " . $space . " for " . $monthName . " " . $year;
                        ?>
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>

            </div>
            <div class="clearfix"></div>


            <div class="row">

                <div class="wrapper">
                    <!--Title-->
<table class="table table-striped projects" id="example" style="max-width: 800px">
    <thead>
    <td>Day</td>
    <td>Allocation</td>
    <td>Allocate</td>
    </thead>
<tbody>
<?php
$dayStart = $year . "-" . $month . "-01";
$days = date_format(date_create($dayStart), "t");
$dayEnd = $year . "-" . $month . "-" . $days;

$rosterList = $this->user_model->getRosterBySpace($dayStart,$dayEnd,$space);
$staff = $this->user_model->getUsers();

if(isset($rosterList)) {
    foreach ($rosterList->result() as $spot) {
        echo "<tr>";
    	$theDay = date('j', strtotime($spot->carparkdate));
        echo "<td>". $theDay ."</td>";
        echo "<td>";
        if ($spot->user != 0) {
            $owner = $this->user_model->getUser($spot->user)->row();
            echo $owner->name;
        }
        echo "</td>";
        echo "<td>";
        if ($spot->user != 0) {
            echo "<button class='btn btn-sm btn-warning remove' tagdate='".$spot->carparkdate."' tagspace='".$spot->carpark."'>un-assign</button>";
        }else {
            ?>
            <select class="form-control input-sm reassign" tagdate="<?php echo $spot->carparkdate ?>" tagspace="<?php echo $spot->carpark ?>">
                <option value="0">____Unallocated____</option>
                <?php
                if (isset($staff)) {
                    foreach ($staff->result() as $person) { ?>
                        <option
                                value="<?php echo $person->id ?>" <?php
                        if ($spot->user != 0) {
                            if ($spot->user == $person->id) echo 'selected';
                        }
                        ?> ><?php echo $person->name ?>
                        </option>
                    <?php }
                }
                ?>
            </select>
            <?php
        }
        echo "</td>";
        echo "</tr>";
    }
}
?>

</tbody>
</table>



                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>
<script>
    $(document).ready(function () {
        $('.reassign').change(function () {

            $.ajax({
                url: "<?php echo base_url()?>ajax/manuallyAllocateSpot",
                data: {
                    "staff": this.value,
                    "carpark": this.getAttribute('tagspace'),
                	"date":this.getAttribute('tagdate'),
                },
                dataType: "html",
                type: "post",
                success: function (data) {
                    if (data == "0") {
                        alert("Spot has been successfully allocated");
                    	window.location.reload();
                    }
                    else {
                        alert("Couldnt allocate");
                    }
                }
            });
        });
        $('.remove').click(function () {

            $.ajax({
                url: "<?php echo base_url()?>ajax/manuallyAllocateSpot",
                data: {
                    "staff": 0,
                    "carpark": this.getAttribute('tagspace'),
                	"date":this.getAttribute('tagdate'),
                },
                dataType: "html",
                type: "post",
                success: function (data) {
                    if (data == "0") {
                        alert("Spot has been successfully unallocated");
                        window.location.reload();
                    }
                    else {
                        alert("Couldnt unallocate - something went wrong");
                    }
                }
            });
        });

    });
</script>

<!-- footer content -->
<?php $this->load->view("footer.php"); ?>
<!-- /footer content -->
</body>

</html>