<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Attaché Car Parking</title>
</head>

<body>
    <!--Main section-->
    <section class="standard">
        <?php

            $host = 'localhost';
            $user = 'id7483243_carparkingdb';
            $pass = 'Unicorns4eva';
            $db = 'id7483243_carparking';

            $conn = mysqli_connect($host,$user,$pass,$db);
            if(!$conn){
                echo "Could not connect to database";
            }           
            else{
                $day=$_GET['selectedDay'];
                $user=$_GET['user'];
                $carpark=$_GET['carpark'];
                $process=$_GET['process'];

                $sql = "SELECT fullname FROM user WHERE userid='" . $user . "'";

                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result) == 1){
                    while($row = $result->fetch_assoc()){
                        if($process=="error"){
                            echo "<h3>Could not process the last request, perhaps try again</h3>";
                        }
                        echo "<h3>Are you sure you want to remove " . $row['fullname'] . " from spot " . $carpark . "?</h3>";
                        $url = 'day.php';
                        $url .= '?selectedDay=' . $day;
                        echo "<a href=\"" . $url . "\">";
                        echo "<button class=\"btn\">"; 
                        echo "<p>No</p>";
                        echo "</button></a>";

                        $url = 'makingspotavailable.php';
                        $url.= "?selectedDay=" . $day . "&amp;user=" . $user . "&amp;carpark=" . $carpark;
                        echo "<a href=\"" . $url . "\">";
                        echo "<button class=\"btn\">";
                        echo "<p>Yes</p>";
                        echo "</button></a>";
                    }
                }
                else{
                    echo "<h3>Something went wrong</h3>";
                }

                $conn -> close();
            }
        ?>
        
    </section>
</body>

</html>