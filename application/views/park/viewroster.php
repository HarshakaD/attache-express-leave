<?php
$this->load->view("header.php");
?>
<style>
    .table-scroll {
        position:relative;
        max-width:100%;
        margin:auto;
        overflow:hidden;
        border:1px solid #000;
    }
    .table-wrap {
        width:100%;
        overflow:auto;
    }
    .table-scroll table {
        width:100%;
        margin:auto;
        border-collapse:separate;
        border-spacing:0;
    }
    .table-scroll th, .table-scroll td {
        padding:5px 10px;
        border:1px solid #000;
        background:#fff;
        white-space:nowrap;
        vertical-align:top;
    }
    .table-scroll thead, .table-scroll tfoot {
        background:#f9f9f9;
    }
    .clone {
        position:absolute;
        top:0;
        left:0;
        pointer-events:none;
    }
    .clone th, .clone td {
        visibility:hidden
    }
    .clone td, .clone th {
        border-color:transparent
    }
    .clone tbody th {
        visibility:visible;
        color:red;
    }
    .clone .fixed-side {
        border:1px solid #000;
        background:#eee;
        visibility:visible;
    }
    .clone thead, .clone tfoot{background:transparent;}
</style>
<body class="nav-md" style="overflow-x: visible">

<div class="container body">


    <div class="main_container">

        <?php

        $this->load->view("navbar.php");
        ?>

        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Attaché Parking Roster
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>

            </div>
            <div class="clearfix"></div>
            <h1>Select a month to view </h1>

            <div class="row">

                <div class="wrapper" >
                    <form action="viewroster" method="post" id="myform">
                        <select class="form-control roster" style="max-width: 200px" name="month">
                            <option>Select a month to view</option>
                            <?php
                            if (!is_null($this->input->post('month')))
                                $showmonth = $this->input->post('month');
                            else
                                $showmonth = 0;

                            // for testin
//                            $showmonth = 1;

                            $thisMonth = strtotime(date('Y') . "-" . date('m') . "-01");
                            $monthList = $this->user_model->getRosterMonths(true);
                            $i = 0;
                            foreach ($monthList->result() as $month) {
                                echo "<option value='" . $month->id . "'";
                                if ($month->id == $showmonth) {
                                    echo " selected ";
                                    $startdate = $month->monthstart;
                                }
                                echo ">";
                                echo date_format(date_create($month->monthstart), "F Y");
                                echo "</option>";
                            }
                            ?>
                        </select>

<!--                        <P>-->
<!--                            display options:-->
<!--                            <br>-->
<!--                            by person ?-->
<!--                            person, then day, then spot.-->
<!--                            <br>-->
<!--                            By day?-->
<!--                            day, then spot, then person-->
<!--                            <br>-->
<!--                            by space?-->
<!--                            space, then day, then person-->
<!--                        </P>-->
<!---->
<!--                        Column-->
<!--                        <select>-->
<!--                            <option>Day</option>-->
<!--                            <option>Person</option>-->
<!--                            <option>Spot</option>-->
<!--                        </select>-->
<!--                        <br>-->
<!---->
<!--                        Row-->
<!--                        <select>-->
<!--                            <option>Day</option>-->
<!--                            <option>Person</option>-->
<!--                            <option>Spot</option>-->
<!--                        </select>-->
<!--                        <br>-->
<!---->
<!--                        Value-->
<!--                        <select>-->
<!--                            <option>Day</option>-->
<!--                            <option>Person</option>-->
<!--                            <option>Spot</option>-->
<!--                        </select>-->

                        <p>


                            <?php
                            if ($showmonth > 0){
                            $days = date_format(date_create($startdate), "t");
                            $enddate = date('Y-m-d', strtotime($startdate . '+1 month'));
                            $enddate = date('Y-m-d', strtotime($enddate . '-1 day'));

                            $roster = $this->user_model->getRosterByDate($startdate, $enddate);
                            $rosterArray = array();
                            $rosterArrayRow = array();

                            foreach ($roster->result() as $space) {
                                $rosterArray[$space->carparkdate][$space->carpark] = $space->user;
                                $rosterArrayRow[$space->carpark][$space->carparkdate] = $space->user;
                                //    $rosterArray[$space->user][$space->carparkdate] = $space->carpark;

                            }

                            ?>
                        <div id="table-scroll" class="table-scroll">
                            <div class="table-wrap">

                        <table class="table table-striped projects main-table" id="example" style="overflow-x: scroll">
                            <thead>
                            <tr>
                                <th class="fixed-side" scope="col"></th>
                                <?php
                                foreach ($rosterArray as $date => $data) {
                                    echo "<th scope=\"col\">" .  date_format(date_create($date), "D d") . "</th>";
                                }
                                ?>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            foreach ($rosterArrayRow as $spot => $rowdata) {
                                echo "<tr>";
                            	$month = date('n',strtotime($startdate));
                            	$year = date('Y',strtotime($startdate));
                            	$url = base_url() . "user/park/manualallocation?carpark=".$spot."&month=".$month."&year=".$year;
                                echo "<th class=\"fixed-side\"><a href=" .$url. ">Space# " . $spot . "</a></th>";
                                foreach ($rosterArray as $date => $data) {
                                    if($rowdata[$date] == 0)
                                        $name = "";
                                    else {
                                        $person = $this->user_model->getUser($rowdata[$date]);
                                        $name = $person->row()->name;
                                    }
                                    echo "<td>" . $name . "</td>";
                                }
                                echo "</tr>";
                            }
                            ?>
                            </tbody>
                        </table>

                            </div>
                        </div>
                                <?php
                        }
                        ?>

                    </form>
                </div>


            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>
<script >

    $(document).ready(function () {
        jQuery(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');

        $('.roster').change(function () {
            $('#myform').submit();
        });

    });
</script>

<!-- footer content -->
<?php $this->load->view("footer.php"); ?>
<!-- /footer content -->
</body>

</html>