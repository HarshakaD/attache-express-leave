<?php
// Left Nav menu
?>
<div class="col-md-3 left_col">
    <div class="left_col scroll-view" style="width: 230px;">

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul style="list-style-type: none;">
                    <li style="height: 1px !important;"><a>&nbsp;</a></li>
                </ul>
                <ul class="nav side-menu">
                    <?php
                    // if the license agreement needs signing, only show the eula page.
                    if (!$this->user_model->validAgreement()) {
                        ?>
                        <li style="height: 32px !important;" class="hidden-xs"><a>License Agreement</a></li>
                        <li style="height: 32px !important;"><a href="<?php echo base_url() ?>user/eula"><i
                                    class="fa fa-pencil"></i><span>Accept License</span></a>
                        </li>
                        <?php
//
//
                    } else {
                        $company = $this->user_model->getCompany();
                        $user = $this->user_model->getUser()->row();
                        ?>

                    <li style="height: 32px !important;" class="hidden-xs"><a>MY LEAVE</a></li>
                    <li style="height: 32px !important;"><a href="<?php echo base_url() ?>user/dashboard"><i
                                    class="fa fa-calendar-o"></i><span>My Leave</span></a></li>
                    <li style="height: 18px !important;" class="visible-xs-block"><a
                                href="<?php echo base_url() ?>user/dashboard">My Leave</a></li>
                    <li style="height: 8px !important; padding-top: 10px" class="visible-xs-block">
                        <div style="width: 90%; margin: auto" class="ln_solid"></div>
                    </li>

                    <li style="height: 32px !important;"><a href="<?php echo base_url() ?>user/request"><i
                                    class="fa fa-calendar-plus-o"></i><span>Request Leave</span></a></li>
                    <li style="height: 28px !important;" class="visible-xs-block"><a
                                href="<?php echo base_url() ?>user/request">Request<br>Leave</a></li>
                    <?php if ($this->session->userdata('usertype') <= '3') { ?>
                    <li style="height: 32px !important;" class="hidden-xs"><a
                                href="<?php echo base_url() ?>user/proxy"><i class="fa fa-user-plus"></i><span>Request on Behalf</span></a>
                    </li>
                    <?php } ?>
                    <li style="height: 8px !important; padding-top: 10px" class="visible-xs-block">
                        <div style="width: 90%; margin: auto" class="ln_solid"></div>
                    </li>
                    <?php
                        if ($this->session->userdata('usertype') <= $company->calendarAccessLevel) {
                    ?>
                    <li style="height: 32px !important;" class="hidden-xs"><a
                                href="<?php echo base_url() ?>user/calendar"><i class="fa fa-calendar"></i><span>Leave Calendar</span></a>
                    </li>
                    <?php
                    }


                   // parking for attache online
                   if ((ENVIRONMENT == 'development')||($company->id == 275) || ($company->id == 1 )){
                    ?>
                       <li style="height: 8px !important; padding-top: 10px" class="">
                           <div style="width: 90%; margin: auto" class="ln_solid"></div>
                       </li>
                    <li style="height: 32px !important;" class="hidden-xs"><a>PARKING</a></li>
                       <li style="height: 32px !important;"><a href="<?php echo base_url() ?>user/park"><i
                                       class="fa fa-car"></i><span>My Parking</span></a></li>
                       <li style="height: 18px !important;" class="visible-xs-block"><a
                                   href="<?php echo base_url() ?>user/park">My Parking</a></li>
                       <li style="height: 32px !important;"><a href="<?php echo base_url() ?>user/park/carparkimage"><i
                                       class="fa fa-map"></i><span>Carpark Map</span></a></li>
                       <li style="height: 18px !important;" class="visible-xs-block"><a
                                   href="<?php echo base_url() ?>user/park/carparkmap">Carpark Map</a></li>

                       <?php if ($this->session->userdata('usertype') <= '2') { ?>
                       <li style="height: 32px !important;"><a href="<?php echo base_url() ?>user/park/manage"><i
                                       class="fa fa-gear"></i><span>Permanent Allocations</span></a></li>
                       <li style="height: 32px !important;"><a href="<?php echo base_url() ?>user/park/viewroster"><i
                                       class="fa fa-calendar"></i><span>View Roster</span></a></li>
                       <li style="height: 32px !important;"><a href="<?php echo base_url() ?>user/park/roster"><i
                                       class="fa fa-calculator"></i><span>Generate Roster</span></a></li>
                       <?php } ?>
                    <li style="height: 8px !important; padding-top: 10px" class="visible-xs-block">
                        <div style="width: 90%; margin: auto" class="ln_solid"></div>
                    </li>
                    <?php
                    }


                    if ($company->timesheets && $user->hastimesheets) { ?>
                            <li style="height: 8px !important; padding-top: 10px" class="">
                                <div style="width: 90%; margin: auto" class="ln_solid"></div>
                            </li>
                            <li style="height: 32px !important;" class="hidden-xs"><a>MY TIMESHEETS</a></li>

                            <li style="height: 32px !important;" class=""><a
                                    href="<?php echo base_url() ?>user/mytimesheets"><i
                                        class="fa fa-clock-o"></i><span>My Timesheets</span></a></li>

                            <li style="height: 18px !important;" class="visible-xs-block"><a
                                    href="<?php echo base_url() ?>user/mytimesheets">My Timesheets</a></li>
                            <li style="height: 8px !important; padding-top: 10px" class="visible-xs-block">

                            <li style="height: 32px !important;" class="hidden-xs"><a
                                    href="<?php echo base_url() ?>user/timecalendar"><i
                                        class="fa fa-calendar"></i><span>Timesheet Calendar</span></a>
                            </li>
                        <?php } ?>
                        <?php if ($company->expenses) { ?>
                            <li style="height: 8px !important; padding-top: 10px" class="">
                                <div style="width: 90%; margin: auto" class="ln_solid"></div>
                            </li>
                            <li style="height: 32px !important;" class="hidden-xs"><a>MY Expenses</a></li>

                            <li style="height: 32px !important;" class=""><a
                                    href="<?php echo base_url() ?>user/myexpenses"><i
                                        class="fa fa-dollar"></i><span>Expense Claims</span></a></li>

                            <li style="height: 18px !important;" class="visible-xs-block"><a
                                    href="<?php echo base_url() ?>user/myexpenses">Expense Claims</a></li>
                            <li style="height: 8px !important; padding-top: 10px" class="visible-xs-block">

                        <?php } ?>

                        <li style="height: 8px !important; padding-top: 10px" class="">
                            <div style="width: 90%; margin: auto" class="ln_solid"></div>
                        </li>
                        <li style="height: 32px !important;" class="hidden-xs"><a>GENERAL</a></li>
                        <li style="height: 32px !important;" class="hidden-xs"><a
                                href="<?php echo base_url() ?>user/publicholidays"><i
                                    class="fa fa-calendar-times-o"></i><span>Public Holidays</span></a>
                        </li>
                        <li style="height: 32px !important;"><a href="https://mypay.attacheonline.com"
                                                                target="_blank"><i
                                    class="fa fa-external-link"></i><span>Launch MyPay</span></a></li>
                        <li style="height: 18px !important;" class="visible-xs-block"><a
                                href="https://mypay.attacheonline.com">My Pay</a></li>


                        <?php if ($this->session->userdata('usertype') <= '3') { ?>
                            <li style="height: 8px !important; padding-top: 10px" class="">
                                <div style="width: 90%; margin: auto" class="ln_solid"></div>
                            </li>
                            <li style="height: 32px !important;" class="hidden-xs"><a>MANAGE APPROVALS</a></li>

                            <li style="height: 32px !important;" class=""><a
                                    href="<?php echo base_url() ?>user/approval"><i
                                        class="fa fa-calendar-check-o"></i><span>Approve Leave</span>
                                    <?php
                                    $count = $this->user_model->getPendingLeaveByApprover();
                                    if ($count > 0) { ?>
                                        <span class="badge"
                                              style="float: right;    background: #1d92ef !important; border: 1px solid #1d92ef !important; color: #fff;"><?php echo $count ?></span>
                                    <?php } ?>
                                </a>
                            </li>
                            <li style="height: 28px !important;" class="visible-xs-block"><a
                                    href="<?php echo base_url() ?>user/approval">Approve<br>Leave</a></li>
                            <?php if ($company->timesheets) { ?>
                                <li style="height: 32px !important;" class=""><a
                                        href="<?php echo base_url() ?>user/timeapproval"><i
                                            class="fa fa-check-circle-o"></i><span>Approve Timesheets</span>
                                        <?php
                                        $count = $this->user_model->getPendingTimesheetsByApprover();
                                        if ($count > 0) { ?>
                                            <span class="badge"
                                                  style="float: right;    background: #1d92ef !important; border: 1px solid #1d92ef !important; color: #fff;"><?php echo $count ?></span>
                                        <?php } ?>
                                    </a></li>
                                <?php
                            }
                            if ($company->expenses) { ?>
                                    <li style="height: 32px !important;" class=""><a
                                            href="<?php echo base_url() ?>user/approveexpenses"><i
                                                class="fa fa-dollar"></i><span>Approve Expenses</span>
                                            <?php
                                            $count = $this->user_model->getPendingExpenseClaimsByApprover();
                                            if ($count > 0) { ?>
                                                <span class="badge"
                                                      style="float: right;    background: #1d92ef !important; border: 1px solid #1d92ef !important; color: #fff;"><?php echo $count ?></span>
                                            <?php } ?>
                                        </a>
                                    </li>

                            <?php } ?>

                            <li style="height: 32px !important;" class="hidden-xs"><a
                                    href="<?php echo base_url() ?>user/history"><i class="fa fa-file-text-o"></i><span>Approval History</span></a>
                            </li>

                        <?php } ?>


                        <?php if ($this->session->userdata('usertype') <= '2') { ?>
                            <li style="height: 8px !important; padding-top: 10px" class="hidden-xs">
                                <div style="width: 90%; margin: auto" class="ln_solid"></div>
                            </li>
                            <li style="height: 32px !important;" class="hidden-xs"><a>REVIEW & PROCESS</a></li>
                            <!--                            <li style="height: 32px !important;" class="hidden-xs"><a-->
                            <!--                                    href="--><?php //echo base_url() ?><!--user/proxy"><i class="fa fa-comment-o"></i><span>Create on Behalf</span></a>-->
                            <!--                            </li>-->
                            <li style="height: 32px !important;" class="hidden-xs"><a
                                    href="<?php echo base_url() ?>user/summary"><i
                                        class="fa fa-exclamation-triangle"></i><span>Check Pay Period</span></a></li>
                            <li style="height: 32px !important;" class="hidden-xs"><a
                                    href="<?php echo base_url() ?>user/download"><i class="fa fa-download"></i><span>Process for Payroll</span></a>
                            </li>


                            <li style="height: 8px !important; padding-top: 10px" class="hidden-xs">
                                <div style="width: 90%; margin: auto" class="ln_solid"></div>
                            </li>
                            <li style="height: 32px !important;" class="hidden-xs"><a>REPORTS</a></li>
                            <li style="height: 32px !important;" class="hidden-xs"><a
                                    href="<?php echo base_url() ?>user/audit"><i class="fa fa-gavel"></i><span>Audit Report</span></a>
                            </li>
                            <li style="height: 32px !important;" class="hidden-xs"><a
                                    href="<?php echo base_url() ?>user/liability"><i class="fa fa-sliders"></i><span>Leave Balance Report</span></a>
                            </li>
                            <li style="height: 32px !important;" class="hidden-xs"><a
                                    href="<?php echo base_url() ?>user/restorebatch"><i
                                        class="fa fa-archive"></i><span>Processed Entries</span></a></li>
                            <li style="height: 32px !important;" class="hidden-xs"><a
                                    href="<?php echo base_url() ?>user/report"><i class="fa fa-file-text"></i><span>Filtered Report</span></a>
                            </li>
                        <?php } ?>

                        <?php if ($this->session->userdata('usertype') == '1') { ?>
                            <li style="height: 8px !important; padding-top: 10px" class="hidden-xs">
                                <div style="width: 90%; margin: auto" class="ln_solid"></div>
                            </li>
                            <li style="height: 32px !important;" class="hidden-xs"><a>COMPANY ADMIN</a></li>
                            <li style="height: 32px !important;" class="hidden-xs"><a
                                    href="<?php echo base_url() ?>user/company"><i class="fa fa-gear"></i><span>Company Details</span></a>
                            </li>
                            <li style="height: 32px !important;" class="hidden-xs"><a
                                    href="<?php echo base_url() ?>user/users"><i class="fa fa-users"></i><span>Manage Users</span></a>
                            </li>
                            <?php if ($company->sevendayleave) { ?>
                                <li style="height: 32px !important;" class="hidden-xs"><a
                                        href="<?php echo base_url() ?>user/templates"><i class="fa fa-clone"></i><span>Templates</span></a>
                                </li>
                            <?php } ?>
                            <li style="height: 32px !important;" class="hidden-xs"><a
                                    href="<?php echo base_url() ?>user/upload"><i class="fa fa-cloud-upload"></i><span>Import Attaché Data</span></a>
                            </li>

                            <li style="height: 32px !important;" class="hidden-xs"><a
                                    href="<?php echo base_url() ?>user/managetypes"><i class="fa fa-square-o"></i><span>Leave Types</span></a>
                            </li>
                            <?php if ($company->timesheets) { ?>
                                <li style="height: 32px !important;" class="hidden-xs"><a
                                        href="<?php echo base_url() ?>user/managetimes"><i
                                            class="fa fa-circle-o"></i><span>Time Types</span></a>
                                </li>
                            <?php } ?>
                        <?php } ?>

                    <?php } ?>

                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">

    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <div class="navbar nav_title" style="border: 0;">
                <a href="<?php echo base_url() ?>" class="site_title">
                    <?php echo $this->session->userdata('companyname') ?>
                </a>
            </div>

            <ul class="nav navbar-nav navbar-right" style="padding-top: 16px;">

                <li class="">
                    <a href="<?php echo base_url() ?>logout">
                        <button type="button" class="btn btn-dark btn-sm"><i class="fa fa-sign-out"></i> Logout</button>
                    </a>
                </li>


                <?php
                $userlist = $this->user_model->getUserByEmail($this->session->userdata('email'));
                if ($userlist->num_rows() > 1) { // email is with more than one company
                    ?>
                    <li class="btn hidden-xs" style="margin-bottom:0px;padding:0px">
                        <select class="form-control" name="cc" id="cc"
                                style="padding-top:10px;padding-bottom:0px!important;font-size:0.9em!important;background-color: rgb(29, 146, 239);color: #FFFFFF; border: none;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif">
                            <option style="font-family:'Helvetica Neue',Helvetica,Arial,sans-serif!important" class=""
                                    value="<?php echo $this->session->userdata('id') ?>"
                            "selected">Change Company</option>
                            <?php // if get list of companies the email adddress belongs to
                            foreach ($userlist->result() as $usernav) {
                                $companynav = $this->user_model->getCompany($usernav->company);
                                ?>
                                <option style="font-family:'Helvetica Neue',Helvetica,Arial,sans-serif!important;"
                                        class=""
                                        value="<?php echo $usernav->id ?>"><?php echo $companynav->name ?></option>
                            <?php }
                            ?>
                        </select>
                    </li>
                    <script>
                        $(document).ready(function () {
                            $('#cc').change(function () {
                                $.ajax({
                                    url: "<?php echo base_url()?>ajax/userinfo",
                                    data: {
                                        "employee": $('#cc').val(),
                                        "set": true
                                    },
                                    dataType: "html",
                                    type: "post",
                                    success: function (data) {
                                        location.reload();
                                    },
                                    error: function (data) {
                                        alert(data);
                                    }
                                });
                            });
                            <?php if ($this->session->userdata('usertype') <= '3') { ?>

//                            $("#new").hide();
//                            $("#new").delay(3000).fadeIn("slow");
//                            for (var i = 3; i >= 1; i--){
//
//                                $("#new").fadeOut(200).fadeIn(200)
//
//                            }
//                            $("#new").delay(3000).fadeOut("slow");

                            <?php } ?>

                        });
                    </script>
                    <?php
                }
                ?>


                <li class="hidden-xs"><a href="<?php echo base_url() ?>user/confirm"> Change Password &nbsp;&nbsp;<i
                            class="fa fa-lock"></i></a>
                </li>

                <li class="hidden-xs"><a href="https://myattache.attachesoftware.com/x/pALD"
                <?php //echo base_url()."help" ?> target="_blank"> Help &nbsp;&nbsp;<i class="fa fa-info-circle"></i></a>
                </li>

            </ul>
        </nav>
    </div>

</div>
<!-- /top navigation -->
