<?php
include "header.php";

$company = $this->user_model->getCompany();
$days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

?>

<body class="nav-md" xmlns="http://www.w3.org/1999/html">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Manage Users</h3>
                    </div>


                    <div class="modal fade bs-user-modal" tabindex="-1" role="dialog" aria-hidden="true"
                         style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span
                                            aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">User Management</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal form-label-left" novalidate=""
                                          action="<?php echo base_url() ?>user/createuser" method="post">

                                        <div class="item form-group">
                                            <input type="hidden" name="company" id="company"
                                                   value="<?php echo $company->id ?>">
                                            <input type="hidden" name="id" id="id" value="">
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="name" class="form-control has-feedback-left"
                                                       id="name" placeholder="Name" required="required">
                                                <span class="fa fa-user form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>


                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empid">Employee
                                                ID
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="empid" class="form-control has-feedback-left"
                                                       id="empid" placeholder="">
                                                <span class="fa fa-info-circle form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="location">Location
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="location"
                                                       class="form-control has-feedback-left" id="location" value="">
                                                <span class="fa fa-globe form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>


                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">Country
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="country" id="country"
                                                        class="form-control has-feedback-left" required>
                                                    <option selected>Select Country</option>
                                                    <?php $countries = $this->user_model->getCountry();
                                                    foreach ($countries->result() as $country) {
                                                        echo "<option value='" . $country->id . "' ";
                                                        if ($country->id == $company->country) {
                                                            echo "selected";
                                                        }
                                                        echo ">" . $country->name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                                <span class="fa fa-globe form-control-feedback left"
                                                      aria-hidden="true"></span>

                                            </div>
                                        </div>


                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">State/Region
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select id="state" name="state" class="form-control has-feedback-left"
                                                        required="required">
                                                    <option>Select Country</option>
                                                </select>
                                                <span class="fa fa-globe form-control-feedback left"
                                                      aria-hidden="true"></span>

                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hours">Hours
                                                (standard day) <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="number" name="hours" class="form-control has-feedback-left"
                                                       id="hours" required>
                                                <span class="fa fa-calendar form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="approver">Default
                                                Approver</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <?php
                                                $approvers = $this->user_model->getApprovers();
                                                if (isset($approvers)) {
                                                    ?>
                                                    <select class="form-control has-feedback-left" name="approver"
                                                            id="approver" required>
                                                        <?php
                                                        foreach ($approvers->result() as $approver) {
                                                            ?>
                                                            <option
                                                                value="<?php echo $approver->id ?>" <?php //if($approver->id == $user->defaultapprover) echo 'selected' ?> ><?php echo $approver->name ?></option>

                                                        <?php }
                                                        ?>
                                                    </select>
                                                    <span class="fa fa-check form-control-feedback left"
                                                          aria-hidden="true"></span>

                                                    <?php
                                                } ?>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="email" id="email" name="email" required="required"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12">
                                                <span class="fa fa-at form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                            <div id="invalid" style="display: none"><i class="fa fa-ban"></i> Already
                                                Registered
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                   for="confirm email">Confirm Email <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="email" id="email2" name="confirm_email"
                                                       data-validate-linked="email" required="required"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12">
                                                <span class="fa fa-at form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usertype">User
                                                Type <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="usertype" id="usertype"
                                                        class="form-control has-feedback-left" required>
                                                    <?php $types = $this->user_model->getUserType();
                                                    foreach ($types->result() as $type)
                                                        echo "<option value=" . $type->id . ">" . $type->description . "</option>";
                                                    ?>
                                                </select>
                                                <span class="fa fa-suitcase form-control-feedback left"
                                                      aria-hidden="true"></span>

                                            </div>
                                        </div>

                                        <?php if ($company->timesheets) { ?>
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="timesheets">Timesheets Enabled
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select name="timesheets" id="timesheets"
                                                            class="form-control has-feedback-left" required>
                                                            <option value="0">No</option>
                                                            <option value="1">Yes</option>
                                                    </select>
                                                    <span class="fa fa-clock-o form-control-feedback left" aria-hidden="true"></span>

                                                </div>
                                            </div>

                                        <?php } ?>

                                        <div class="item form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                &nbsp;
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                   for="buttons"></label>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <button id="send" type="submit" class="btn btn-primary col-xs-5"> Save
                                                </button>
                                                <button type="button" class="btn btn-default col-xs-5"
                                                        style="float:right" data-dismiss="modal"> Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row top_tiles">
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-info-circle"></i>
                            </div>
                            <div class="count"><?php echo $company->vip; ?></div>

                            <h3>VIP</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-users"></i>
                            </div>
                            <div class="count"><?php echo $this->admin_model->getUserCount($company->id) ?></div>

                            <h3>Employees</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-users"></i>
                            </div>
                            <div class="count"><?php echo $this->admin_model->getLeaveCount($company->id) ?></div>

                            <h3>Leave Requests</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-check-square-o"></i>
                            </div>
                            <div class="count"><?php echo $this->admin_model->getStatus($company->status) ?></div>

                            <h3>Account Status</h3>
                        </div>
                    </div>

                </div>


                <div class="row">
                    <div class="col-md-12">

                        <div>

                            <?php
                            // for each team in get top teams


                            ?>
                            <div class="panel">

                                <div>
                                    <div class="panel-body">
                                        <p>
                                            <button
                                                onclick="document.getElementById('company').value='<?php echo $company->id ?>';"
                                                type="button" class="btn btn-primary newuser" data-toggle="modal"
                                                data-target=".bs-user-modal"><i class="fa fa-user"></i> Create New User
                                            </button>

                                            <!-- start project list -->
                                        <table class="table table-striped projects" id="example">
                                            <thead>
                                            <tr>
                                                <th>Employee</th>
                                                <th>Email</th>
                                                <th>Location</th>
                                                <th>Region,<br>Country</th>
                                                <th>Permission<br>Level</th>
                                                <th>Default<br>Approver</th>
                                                <?php
                                                if ($company->sevendayleave) {
                                                    echo '<th>Work Hours</th>';
                                                } else {
                                                    echo '<th>Hours/<br>Day</th>';
                                                }
                                                ?>
                                                <th>Leave<br>Balances</th>
                                                <!--<th>Sick</th>
                                                <th>Long<br>Service</th>
                                                <th>RDO</th>
                                                <th>Study</th>
                                                <th>Custom1</th>
                                                <th>Custom2</th>-->
                                                <th>As At</th>
                                                <th>Actions</th>
                                            </tr>


                                            </thead>
                                            <tbody>

                                            <?php
                                            // for each team in get top teams
                                            $format = 'd M Y';
                                            $staff = $this->user_model->getUsers($company->id);
                                            if (isset($staff))
                                                foreach ($staff->result() as $employee) {
                                                    ?>

                                                    <tr id="row<?php echo $employee->id ?>">
                                                        <td>
                                                            <a><?php echo $employee->name ?></a>
                                                            <br/>
                                                            <small>
                                                                EmployeeID: <?php echo $employee->employeeid ?></small>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $email = '<span style="color: red">Warning: Email address is missing</span>';
                                                            if (!is_null($employee->email) && $employee->email != '')
                                                                $email = substr($employee->email, 0, 10) . '...';
                                                            echo $email ?>
                                                        </td>
                                                        <?php if ($employee->status == 5) { ?>
                                                            <td> -</td>
                                                            <td> -</td>
                                                            <td> N/A</td>
                                                            <td> -</td>
                                                            <td> -</td>
                                                            <td> -</td>
                                                            <td> User Deleted</td>
                                                            <?php
                                                        } else {
                                                            $welcome = false;
                                                            ?>
                                                            <td>
                                                                <?php echo $employee->location ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                $region = '<span style="color: red">Warning: State & Country are missing</span>';
                                                                if (!is_null($employee->state) && $employee->state != '') {
                                                                    $regions = $this->user_model->getRegion($employee->state);
                                                                    if (isset($regions) && $regions != false) {
                                                                        if (!is_null($employee->country) && $employee->country != '') {
                                                                            $usercountry = $this->user_model->getCountry($employee->country);
                                                                            if (isset($usercountry) && $usercountry != false) {
                                                                                $region = $regions->row()->name . ',<br>';
                                                                                $region .= $usercountry->row()->name;
                                                                                $welcome = true;
                                                                            }

                                                                        }
                                                                    }
                                                                }
                                                                echo $region;

                                                                //                                                                echo $this->user_model->getCountry($employee->country)->row()->name
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php $types = $this->user_model->getUserType($employee->usertype);
                                                                $type = $types->row();
                                                                echo $type->description ?>
                                                            </td>
                                                            <td>
                                                                <?php $types = $this->user_model->getUser($employee->defaultapprover);
                                                                $type = $types->row();
                                                                echo $type->name ?>
                                                            </td>
                                                            <td style="vertical-align:middle">
                                                                <?php
                                                                if ($company->sevendayleave) {
                                                                    $workdays = $this->user_model->getUserWorkDays($employee->id);
                                                                    $workTxt = '';
                                                                    if ($workdays != false) {
                                                                        foreach ($days as $key => $day) {
                                                                            $d = 'd' . $key;
//                                                                            if ($workdays->$d != 0)

                                                                                $workTxt .= substr($day, 0, 3) . ': ' . $workdays->$d . '<br>';
                                                                        }

                                                                    }
                                                                    ?>
                                                                    <form
                                                                        action="<?php echo base_url() ?>user/edithours"
                                                                        method="post" name="frm">
                                                                        <input type="hidden" name="id"
                                                                               value="<?php echo $employee->id ?>">
                                                                        <?php if ($workTxt != '') { ?>
                                                                            <button type="submit"
                                                                                    class="btn btn-default btn-xs"
                                                                                    data-toggle="tooltip"
                                                                                    data-placement="left" title=""
                                                                                    data-html="true"
                                                                                    data-original-title="<?php echo $workTxt ?>"
                                                                                    style="width: 100%; max-width:150px">
                                                                                <i class="fa fa-info-circle"></i> Edit
                                                                                Hours
                                                                            </button>
<!--                                                                            <br>-->
<!--                                                                            <button type="submit"-->
<!--                                                                                    class="btn btn-primary btn-xs"-->
<!--                                                                                    style="width: 100%; max-width:150px">-->
<!--                                                                                Edit Hours-->
<!--                                                                            </button>-->
                                                                        <?php }
                                                                        else{
                                                                            ?>
                                                                            <button type="submit"
                                                                                    class="btn btn-danger btn-xs"
                                                                                    style="width: 100%; max-width:150px">
                                                                                Add Hours
                                                                            </button>
                                                                            <?php
                                                                        }?>

                                                                    </form>
                                                                    <?php
                                                                } else {
                                                                    if(!is_null($employee->hours))
                                                                    echo '<a>' . $employee->hours . '</a>';
                                                                }
                                                                ?>

                                                            </td>
                                                            <?php
                                                            $leaveTxt = 'Annual Leave: ' . $employee->leavebalance . ' hours <br>';
                                                            $leaveTxt .= 'Sick Leave: ' . $employee->personalbalance . ' hours <br>';
                                                            $leaveTxt .= 'Long Service: ' . $employee->longservicebalance . ' hours <br>';
                                                            if ($employee->timeoffbalance != 0)
                                                                $leaveTxt .= 'RDO Leave: ' . $employee->timeoffbalance . ' hours <br>';
                                                            if ($employee->flexbalance != 0)
                                                                $leaveTxt .= 'Flex-Time: ' . $employee->flexbalance . ' hours <br>';
                                                            if ($employee->studybalance != 0)
                                                                $leaveTxt .= 'Study Leave: ' . $employee->studybalance . ' hours <br>';
                                                            if ($employee->otherbalance != 0)
                                                                $leaveTxt .= 'Custom Leave 1: ' . $employee->otherbalance . ' hours <br>';
                                                            if ($employee->otherbalance2 != 0)
                                                                $leaveTxt .= 'Custom Leave 2: ' . $employee->otherbalance2 . ' hours <br>';
                                                            ?>
                                                            <td style="vertical-align:middle">
                                                                <button type="button" class="btn btn-default btn-xs"
                                                                        data-toggle="tooltip" data-placement="left"
                                                                        title=""
                                                                        data-html="true"
                                                                        data-original-title="<?php echo $leaveTxt ?>" ;>
                                                                    <i class="fa fa-info-circle"></i> Leave Balances
                                                                </button>
                                                                <br>
                                                            </td>

                                                            <td style="vertical-align:middle">
                                                                <a><?php echo date_format(date_create($employee->leaveupdated), $format) ?></a>
                                                            </td>
                                                            <td>
                                                                <?php if ($welcome && $employee->status == 1 && !is_null($employee->email) && $employee->email != '') { ?>
                                                                    <button id="welcome<?php echo $employee->id ?>" onclick="sendwelcome(<?php echo $employee->id ?>)"
                                                                            type="button"
                                                                            style="width: 100%; max-width:150px"
                                                                            class="btn btn-success btn-xs welcome"> Send
                                                                        Welcome Email
                                                                    </button>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <button tag="<?php echo $employee->id ?>"
                                                                        onclick="setmodal('<?php echo $employee->id ?>')"
                                                                        type="button" class="btn btn-primary btn-xs"
                                                                        data-toggle="modal" data-target=".bs-user-modal"
                                                                        style="width: 100%; max-width:150px"> Edit
                                                                </button>
                                                                <button tag="<?php echo $employee->id ?>"
                                                                        onclick="remtype('<?php echo $employee->id ?>')"
                                                                        type="button" class="btn btn-danger  btn-xs"
                                                                        style="width: 100%; max-width:150px"> Delete
                                                                </button>
                                                            </td>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tr>

                                                    <?php
                                                }
                                            ?>


                                            </tbody>
                                        </table>
                                        <!-- end project list -->

                                    </div>
                                    <button
                                        onclick="document.getElementById('company').value='<?php echo $company->id ?>';"
                                        type="button" class="btn btn-primary newuser" data-toggle="modal"
                                        data-target=".bs-user-modal" style="margin-left: 15px"><i class="fa fa-user"></i> Create New User
                                    </button>
                                </div>

                            </div>
                            <?php

                            ?>

                        </div>

                    </div>
                </div>


            </div>


        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="<?php echo base_url() ?>js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>

<script src="<?php echo base_url() ?>js/custom.js"></script>

<script src="<?php echo base_url() ?>js/validator/validator.js"></script>

<!-- PNotify -->
<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>

<!-- Datatables -->
<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>


<script>
    var asInitVals = new Array();
    $(document).ready(function () {

        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search:"
            },
            "bStateSave": false,
            "responsive": false,
            "scrollX": true,
            "iDisplayLength": 50,
            // "sPaginationType": "two_button" ,
            "dom": '<"clear">lfrtip'
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });


        $('#country').change(function () {
            $('#state').html("<option>Loading ... </option>");
            $.ajax({
                url: '<?php echo base_url() ?>ajax/getRegionsByCountry',
                type: 'post',
                data: {
                    country: this.value,
                    addnational: 'NO'
                },
                success: function (data) {
                    $('#state').html(data);
                }
            }).error(function () {
                alert('An error occurred');
            });
        });

        $('#state').html("<option>Loading ... </option>");
        $.ajax({
            url: '<?php echo base_url() ?>ajax/getRegionsByCountry',
            type: 'post',
            data: {
                country: $('#country').val(),
                addnational: 'NO'
            },
            success: function (data) {
                $('#state').html(data);
                $('#state').val('<?php echo $company->state ?>');
            }
        }).error(function () {
            //alert ('An error occurred');
        });

    });


    $('.modal').on('show.bs.modal', function () {
        $('.modal-body').css('height', $(window).height() * 0.75);
        $('#modalsrc').css('height', $(window).height() * 0.75);

    });

    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
        .on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
//            e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            return true;
        //this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);


    $('.newuser').click(function () {
        clearmodal();
    });

    function sendwelcome(id){
        $('#welcome'+id).attr("disabled", "disabled");
        $('#welcome'+id).html('Sending ...');
        $.ajax({
            url: '<?php echo base_url() ?>ajax/sendWelcome',
            type: 'post',
            data: {
                employee: id
            },
            success: function (data) {
                if(data == '') {
                    $('#welcome' + id).hide();
                    new PNotify({title: 'Welcome', text: 'Welcome Email Sent', type: 'success'});
                }
                else {
                    new PNotify({title: 'Error', text: 'Unable to send email. Check email address.', type: 'error'});
                    $('#welcome'+id).html('Send Welcome Email');
                    $('#welcome'+id).removeAttr("disabled");
                }
            }
            }).error(function () {
                new PNotify({title: 'Error', text: 'An error occurred', type: 'error'});
                $('#welcome'+id).html('Send Welcome Email');
                $('#welcome'+id).removeAttr("disabled");
            });

    };



    function clearmodal() {
        document.getElementById('id').value = '';
        document.getElementById('name').value = '';
        document.getElementById('empid').value = '';
        document.getElementById('email').value = '';
        document.getElementById('email2').value = '';
        document.getElementById('location').value = '';
        document.getElementById('approver').value = '';
        document.getElementById('country').value = '';
        document.getElementById('state').value = '';
        document.getElementById('hours').value = '';
//                document.getElementById('approver').value   = vals['approver'];
        document.getElementById('usertype').value = '';
    }

    function setmodal(id) {
        $.ajax({
            url: "<?php echo base_url()?>/ajax/userinfo",
            type: "POST",
            data: {
                employee: id
            },
            success: function (data) {
                var vals = jQuery.parseJSON(data);
                document.getElementById('id').value = vals['id'];
                document.getElementById('name').value = vals['name'];
                document.getElementById('empid').value = vals['employeeid'];
                document.getElementById('email').value = vals['email'];
                document.getElementById('email2').value = vals['email'];
                document.getElementById('location').value = vals['location'];
                document.getElementById('approver').value = vals['defaultapprover'];
                document.getElementById('country').value = vals['country'];
                document.getElementById('hours').value = vals['hours'];
//                document.getElementById('approver').value   = vals['approver'];
                document.getElementById('usertype').value = vals['usertype'];
                document.getElementById('timesheets').value = vals['hastimesheets'];

                $('#state').html("<option>Loading ... </option>");

                $.ajax({
                    url: '<?php echo base_url() ?>ajax/getRegionsByCountry',
                    type: 'post',
                    data: {
                        country: vals['country'],
                        addnational: 'NO'
                    },
                    success: function (data) {
                        $('#state').html(data);
                        $('#state').val(vals['state']);
                    }
                }).error(function () {
                    //alert ('An error occurred');
                });

            },
            error: function (data) {
                alert(data);
                return false
            }
        });

        return true;
    }

    $('#email')
        .on('blur', function () {
            $.ajax({
                url: "<?php echo base_url()?>/ajax/checkemail",
                type: "POST",
                data: {email: this.value},
                success: function (data, textStatus, jqXHR) {
                    if (data == "TRUE") {
                        $('#invalid').hide();
                        $('#send').prop('disabled', false);
                    }//data - response from server
                    else {
                        $('#invalid').show();
                        $('#send').prop('disabled', true);
                    }
                },
                error: function (data) {
                    alert(data);
                }
            });

        });


    function remtype(id) {
//            document.getElementById('did').value=id;
        if (confirm("Please confirm you wish to delete the user. This cannot be undone.")) {
            $.ajax({
                url: "<?php echo base_url()?>/ajax/deleteEmp",
                type: "POST",
                data: {emp: id},
                success: function (data) {
                    if (data == 'Deleted') {
                        document.getElementById("row" + id).style.display = 'none';
                        new PNotify({title: 'Delete Employee', text: data, type: 'success'});
                    }
                    else {
                        new PNotify({title: 'Delete Employee', text: data, type: 'error'});
                    }
                },
                error: function (data) {
                    new PNotify({title: 'Delete Employee', text: data, type: 'error'});
                }
            });
        }
    }

</script>

<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>

</html>
