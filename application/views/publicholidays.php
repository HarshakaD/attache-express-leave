<?php
include "header.php";
?>
<!-- style>
    html,
    body {
        margin:0;
        padding:0;
        height:100%;
    }
    #wrapper {
        min-height:100%;
        position:relative;
    }
    #header {
        background:#ededed;
        padding:10px;
    }
    .container {
        padding-bottom:100px; /* Height of the footer element */
    }
    footer {
        position:fixed;
        bottom:0;
        left:0;
    }
</style -->
<body class="nav-md">
<style type="text/css" class="init">

    tr.group,
    tr.group:hover {
        background-color: #ddd !important;
    }

</style>
<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Public Holidays
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>

            </div>
            <div class="clearfix"></div>


            <div class="row">
                <!-- form date pickers -->
                <!-- /form datepicker -->

                <!-- form input knob -->
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <table id="example" class="table responsive-utilities">
                                <thead>
                                <tr class="headings">
                                    <th>Country</th>
                                    <th>Region</th>
                                    <th>Date</th>
                                    <th>timestamp</th>
                                    <th>Month</th>
                                    <th class="last">Description</th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $format = 'D jS \of MM, Y';
                                $format = "l jS F Y";
                                $holidays = $this->user_model->getAllHolidays();

                                foreach ($holidays->result() as $holiday) {
                                    ?>
                                    <tr>
                                        <td><?php echo $holiday->country ?></td>
                                        <td><?php echo $holiday->state ?></td>
                                        <td><?php echo date_format(date_create($holiday->date), $format) ?></td>
                                        <td><?php echo date_format(date_create($holiday->date), "U") ?></td>
                                        <td><?php echo date_format(date_create($holiday->date), "F") ?></td>
                                        <td><?php echo $holiday->description ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /form input knob -->

            </div>
        </div>

        <!-- footer content -->

        <!-- /footer content -->

    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

        <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>
        <script src="<?php echo base_url()?>js/custom.js"></script>

<script>
    $(document).ready(function () {
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    var asInitVals = new Array();
    $(document).ready(function () {
        var table = $('#example').dataTable({
	    "oLanguage": {
		"sSearch": "Search:"
	    },
            "bStateSave": false,
            "responsive": false,
            "aoColumnDefs": [
                    { 
			"bVisible": false, 
			"aTargets": [3,4] 
		    },
                    {
                        "bSortable": false,
                        "aTargets": [5]
                    }, //disables sorting for column one
		    {
			"iDataSort":3,
			"aTargets": [2]
		    }
            ],
            "order": [[ 3, 'asc' ]],
            "iDisplayLength": 25,
            "sPaginationType": "full_numbers",
	    "dom": '<"clear">lfrtip',
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;

                api.column(4, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                        );

                        last = group;
                    }
                } );
            }
        } );

        // Order by the grouping
        $('#example tbody').on( 'click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
//                table.order( [ 2, 'desc' ] ).draw();
            }
            else {
  //              table.order( [ 2, 'asc' ] ).draw();
            }
        } );

        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });


</script>

<?php include "footer.php" ?>

</body>

</html>
