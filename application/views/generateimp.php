<?php
    header('Content-Type: application/download');
    header('Content-Disposition: attachment; filename="PAYTSHT.INP"');
//    header('Content-type: text/plain');

// create new "Batch" inthe processedleave table
    $batch = array(
        'company' => $this->session->userdata('company'),
        'status' => '2',
        'count' => '0',
        'dateprocessed' => date('Y-m-d h:i:s', time()),
        'processedby' => $this->session->userdata('id')
    );
    $batchID = $this->user_model->createBatch($batch);
    $processedcount = 0;

    $leavetypes = $this->user_model->getLeaveTypes();
//    $leaveresults = $this->user_model->getLeaveByCompany();
//    $hours = $this->session->userdata('hours');

    $format = 'd/m/Y';
    $datefilter = false;
    if (!is_null($this->input->post('todate'))){

        if (strpos($this->input->post('todate'), '/') > 1 ){
            $datefilter = true;

            $td = explode('/',$this->input->post('todate'));
            $tofilterd = $td[2]."-".$td[1]."-".$td[0];
            $tofilter = strtotime($tofilterd);

            $NewDate = new DateTime($tofilterd);
            $NewDate->add(new DateInterval('P1D'));
        }
    }


    $leaveresults = $this->user_model->getApprovedLeaveByCompany();
    $i = 1;
    $country = $this->input->post('country');
    $countryFilter = false;
    if ($country != "ALL")
        $countryFilter = true;


    $types = $this->input->post('types');
    $typesFilter = false;
    if ($types != "ALL")
        $typesFilter = true;

    $locations = $this->input->post('locations');
    $locationsFilter = false;
    if ($locations != "ALL")
        $locationsFilter = true;


    if (isset($leaveresults)){
        foreach ($leaveresults->result() as $leave) {
            $show = false;
            $createChild = false;

            $todate = strtotime($leave->todate);
            $fromdate = strtotime($leave->fromdate);
            $days =  $leave->days;

            if($datefilter){
                $show = false;
                if(!$leave->processed && $fromdate <= $tofilter){
                    if($leave->status > 2){
                        $this->user_model->updateleave($leave->id, array('processed' => TRUE, 'batch'=>$batchID));
                        $processedcount++;
                    }
                    else{
                        $show = true;
                    }

                    if($todate > $tofilter){
                        $todate = $tofilter;
                        // split
                        $createChild = true;
                        $child = array(
                            "user" => $leave->user,
                            "approver" => $leave->approver,
                            "fromdate" => $NewDate->format('Y-m-d'),
                            "todate" => $leave->todate,
                            "created" => $leave->created,
                            "approvedby" => $leave->approvedby,
                            "approvedon" => $leave->approvedon,
                            "status" => $leave->status,
                            "attachment" => $leave->attachment,
                            "leavetype" => $leave->leavetype,
                            "comments" => $leave->comments,
                            "parent" => $leave->id
                        );
                    }

                }

            }
            if ($show){
                $user = $this->user_model->getUser($leave->user)->row();
                if ($countryFilter){
                    if ($user->country != $country){
                        $show = false;
                    }
                }
                if ($typesFilter){
                    if($types == 'LEAVE' && !is_null($leave->timesheet)) {
                        $show = false;
                    }
                    elseif($types == 'TIME' && is_null($leave->timesheet)) {
                        $show = false;
                    }
                }
                if ($locationsFilter){
                    if($locations == 'BLANK'){
                        $l = ''.trim($user->location);
                        if($l != '') {
                            $show = false;
                        }
                    }
                    elseif ($user->location != $locations){
                        $show = false;
                    }
                }


            }
            if ($show){
                $days = $leave->days;
//                $user = $this->user_model->getUser($leave->user)->row();
                if(!is_null($leave->timesheet) && $leave->timesheet > 0) { // its a timesheet
                    $hours = $leave->hours;
                }
                elseif(is_null($leave->timesheet) && $days > 1){
                    $days = $this->user_model->getWorkingDays($fromdate, $todate, $leave->user)['days'];
                    $hours = $days*$user->hours;
                }
                else
                    $hours = $leave->hours;

                $leavecode = $this->user_model->getLeaveType($leave->leavetype)->row()->value;
                if($user->usertype < '5' && !is_null($leavecode) && $leavecode != '')
                    echo "T6,".$user->location.",".$user->employeeid.",,N,".$leavecode.",".$hours.",,,,".date($format, $fromdate).",".date($format, $todate).",,,,,,,,,,,,\r\n";

                $this->user_model->updateleave($leave->id, array('processed' => TRUE, 'batch'=>$batchID, 'dateprocessed' => date('Y-m-d', time())));
                $processedcount++;
                // update leave rocrd to process = true;
                if ($createChild){
                    $child["days"] = $leave->days - $days;
                    $child["hours"] = $leave->hours - $hours;
                    $this->user_model->addLeave($child);
                    $this->user_model->updateleave($leave->id, array('todate' => $tofilterd, 'days' => $days, 'hours'=> $hours));
                }
                $createChild = false;

            }

        }
        $batchdata = array(
//          'status' => '4',
            'count' => $processedcount
        );
        $this->user_model->updateBatch($batchID, $batchdata);

    }
?>





