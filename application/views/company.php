<?php
include "header.php";

$company = $this->user_model->getCompany($this->session->userdata('company'));

?>
<style>
    .info:hover {
        position: relative;
    }
    .info:hover:after {
        content: url(<?php echo base_url()?>images/APIKey.png); /* no need for qoutes */
        display: block;
        position: absolute;
        left: 300px; /* change this value to one that suits you */
        top: -300px; /* change this value to one that suits you */
        z-index: 99999;
    }
</style>
<body class="nav-md" xmlns="http://www.w3.org/1999/html">

    <div class="container body">


        <div class="main_container">

        <?php
        include "navbar.php";
        ?>

            <!-- page content -->
            <div class="right_col" role="main">

                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Company Details</h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row top_tiles" >
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-info-circle"></i>
                                </div>
                                <div class="count"><?php echo $company->vip; ?></div>

                                <h3>VIP</h3>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-users"></i>
                                </div>
                                <div class="count"><?php echo $this->admin_model->getUserCount($company->id) ?></div>

                                <h3>Employees</h3>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-users"></i>
                                </div>
                                <div class="count"><?php echo $this->admin_model->getLeaveCount($company->id) ?></div>

                                <h3>Leave Requests</h3>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-check-square-o"></i>
                                </div>
                                <div class="count"><?php echo $this->admin_model->getStatus($company->status) ?></div>

                                <h3>Account Status</h3>
                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-12">

                            <div >

                                <?php
                                // for each team in get top teams


                                    ?>
                                     <div class="panel">


                                    <div class="panel-body">
                                    <p>

                                        <form class="form-horizontal form-label-left" novalidate="" action="<?php echo base_url()?>user/updatecompany" method="post">

                                            <input type="hidden" name="company" value="<?php echo $company->id?>">

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Company Name <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input id="name" class="form-control has-feedback-left col-md-7 col-xs-12" data-validate-length-range="2" name="name" value="<?php echo $company->name ?>" required="required" type="text">
                                                    <span class="fa fa-building form-control-feedback left" aria-hidden="true"></span>
                                                </div>


                                            </div>


                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select class="form-control has-feedback-left" name="status" disabled>
                                                        <?php
                                                        // for each team in get top teams

                                                        $statustypes = $this->user_model->getStatusTypes();
                                                        if (isset($statustypes))
                                                            foreach($statustypes->result() as $type) {

                                                                ?>
                                                                <option value="<?php    echo $type->id ?>" <?php if ($type->id == $company->status) echo 'selected' ?>><?php echo $type->description ?></option>
                                                            <?php }?>
                                                    </select>
                                                    <span class="fa fa-check-square-o form-control-feedback left" aria-hidden="true"></span>

                                                </div>
                                            </div>
<?php if( $company->timesheets == 1){ // if the company has timesheets enabled, they can change the "Start Date" of each timesheet
    $days = array(
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
    );
    ?>
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Start Timesheets on </span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select class="form-control has-feedback-left" name="timesheetstart">
                                                        <?php
                                                        foreach ($days as $index=>$key){
                                                            echo '<option value="'.$index.'"';
                                                            if ($company->timesheetstart == $index) echo ' selected ';
                                                            echo '>'.$key.'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                    <span class="fa fa-calendar-check-o  form-control-feedback left" aria-hidden="true"></span>

                                                </div>
                                            </div>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="settings">Timesheet entry format</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select class="form-control has-feedback-left" name="decimal">
                                                        <option value="1" <?php if ($company->decimal) echo ' selected '; ?>>Decimal eg 7.75</option>
                                                        <option value="0" <?php if (!$company->decimal) echo ' selected '; ?>>Minutes eg 7:45</option>
                                                    </select>
                                                    <span class="fa fa-calculator  form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                            </div>
<?php } ?>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="note">
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    Note: The following values will be used as defaults when creating new employees
                                                </div>
                                            </div>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">Country <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select name="country" id="country" class="form-control has-feedback-left" required>
                                                        <option value="" selected>Select Country</option>
                                                        <?php $countries = $this->user_model->getCountry();
                                                        foreach ($countries->result() as $country){
                                                            echo "<option value='".$country->id."' ";
                                                            if ($country->id == $company->country){
                                                                echo "selected";
                                                            }
                                                            echo ">".$country->name."</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                    <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>

                                                </div>
                                            </div>


                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">State/Region <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select id="state" name="state" class="form-control has-feedback-left" required>
                                                        <option>Select Country</option>
                                                    </select>
                                                    <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>

                                                </div>
                                            </div>

                                            <!-- div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="logo">Logo (URL ONLY) <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="url" id="logo" name="logo" required="optional" placeholder="http://www.somewhere.com/logo.jpg" class="form-control col-md-7 col-xs-12" value="<?php //echo $company->logo?>" >
                                                </div>
                                            </div -->
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hours">Work Hours per Day <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="number" id="hours" name="hours" required="required"  class="form-control has-feedback-left col-md-7 col-xs-12" value="<?php echo $company->hours ?>" >
                                                    <span class="fa fa-clock-o form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                            </div>

                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usertype">Leave Calendar access level <span class="required"></span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select name="calendarAccessLevel" id="calendarAccessLevel" class="form-control has-feedback-left">
                                                        <?php $types = $this->user_model->getUserType();
                                                        foreach ($types->result() as $type) {
                                                            echo "<option value=" . $type->id . " ";
                                                            if ($type->id == $company->calendarAccessLevel) echo " selected ";
                                                            echo ">" . $type->description . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>

                                                </div>
                                            </div>

<?php if($company->syncdataset) { ?>
                                            <div class="item form-group ">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12 info" for="apikey">Attache Online API Key
                                                    <span class="fa fa-info-circle"></span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" id="apikey" name="apikey" class="form-control has-feedback-left col-md-7 col-xs-12" value="<?php echo $company->apikey ?>" >
                                                    <span class="fa fa-code form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                            </div>
<?php } ?>
                                            <div class="item form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    &nbsp;
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-3">
                                                    <button id="send" type="submit" class="btn btn-primary"><i class="fa fa-building"></i> Update Company Details</button>
                                                </div>
                                            </div>

                                            <div class="item form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    &nbsp;
                                                </div>
                                            </div>

                                        </form>
                                        <!-- start project list -->

                                        <!-- end project list -->

                                        </div>

                                    </div>
                                </div>
                                <?php

                                ?>

                            </div>

                        </div>
                    </div>


                </div>


            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url()?>js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>

    <script src="<?php echo base_url()?>js/custom.js"></script>

    <script src="<?php echo base_url()?>js/validator/validator.js"></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        $( document ).ready(function() {

        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

        // bind the validation to the form submit event
        //$('#send').click('submit');//.prop('disabled', true);

        $('form').submit(function (e) {
//            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                return true;
                //this.submit();
            return false;
        });


        $('#vfields').change(function () {
            $('form').toggleClass('mode2');
        }).prop('checked', false);

        $('#alerts').change(function () {
            validator.defaults.alerts = (this.checked) ? false : true;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);


        $('#country').change(function(){
            $('#state').html("<option>Loading ... </option>");
            $.ajax({
                url: '<?php echo base_url() ?>ajax/getRegionsByCountry',
                type: 'post',
                data: {
                    country: this.value,
                    addnational: 'NO'
                },
                success: function (data) {
                    $('#state').html(data);
                }
            }).error(function() {
                    alert ('An error occurred');
                });
        });

        $('#state').html("<option>Loading ... </option>");
        $.ajax({
            url: '<?php echo base_url() ?>ajax/getRegionsByCountry',
            type: 'post',
            data: {
                country: $('#country').val(),
                addnational: 'NO'
            },
            success: function (data) {
                $('#state').html(data);
                $('#state').val('<?php echo $company->state ?>');
            }
        }).error(function() {
                //alert ('An error occurred');
            });
        });
    </script>

    <!-- footer content -->
    <?php include "footer.php" ?>
    <!-- /footer content -->
</body>

</html>
