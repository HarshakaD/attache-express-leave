<?php
include "header.php";


$days = array( 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
$userid = $this->session->userdata('id');
$user = $this->user_model->getUser($userid)->row();
$workdays = $this->user_model->getUserWorkDays($userid);
$total = 0;

?>

<body class="nav-md">
<style type="text/css" class="init">

    tr.group,
    tr.group:hover {
        background-color: #ddd !important;
    }

</style>
<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
test
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>

            </div>
            <div class="clearfix"></div>


            <div class="row">
                <!-- form date pickers -->
                <!-- /form datepicker -->
<?php
$config = false;

$results = $this->user_model->getdatasetconfig();
if(isset($results) && $results->num_rows() > 0 ){
    $datasetconfig = $results->row();
    $config = true;
}

if($config){
?>
                <!-- form input knob -->
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            Server:  <input type="text" name="server" id="server" value="<?php echo $datasetconfig->server ?>"><br>
                            Port:    <input type="text" name="port" id="server" value="<?php echo $datasetconfig->port ?>"><br>
                            DataSet: <input type="text" name="dataset" id="dataset" value="<?php echo $datasetconfig->dataset ?>"><br>
                            UserID: <?php echo $datasetconfig->user ?>"><br>
                            Username:  <input type="text" id="user" name="user" value="supervisor"><br>
                            Password: <input type="password" id="pwd" name="password"><br>
                            <br>
                            <button class="btn btn-primary" id="getdataset">Get Dataset Config</button>

                            <button class="btn btn-default" id="getdataset2">Step 2</button>
                            <br>
                            <div id="stat">
Status
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /form input knob -->
<?php

    $url = 'http';
    if($datasetconfig->https)
        $url .= 's';
    $url .= '://';
    $url .= $datasetconfig->username;
    if(!is_null($datasetconfig->password))
        $url .= ':'.$datasetconfig->password;
    $url .= '@'.$datasetconfig->server;
    $url .= ':'.$datasetconfig->port;
    $url .= '/odata/';
    $url .= $datasetconfig->dataset;
    $url .= '/Employee';
}
?>

                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <div id="results">
                                <?php echo $url ?><br>
                                Results tobe displayed here
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- footer content -->

        <!-- /footer content -->

    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<script src="<?php echo base_url() ?>js/input_mask/jquery.inputmask.bundle.js"></script>

        <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url()?>js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url()?>js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url()?>js/icheck/icheck.min.js"></script>
        <script src="<?php echo base_url()?>js/custom.js"></script>

<!--<script>-->
<!--    // using jQuery & 'beforeSend' callback-->
<!--    $.ajax({-->
<!--        xhrFields: {-->
<!--            withCredentials: true-->
<!--        },-->
<!--        beforeSend: function (xhr) {-->
<!--            xhr.setRequestHeader('Authorization', 'Basic ' + btoa('myuser:mypswd'));-->
<!--        },-->
<!--        url: "http://www.example.org/protected-data.php"-->
<!--    });-->
<!---->
<!--    // using jQuery & 'headers' property-->
<!--    $.ajax({-->
<!--        xhrFields: {-->
<!--            withCredentials: true-->
<!--        },-->
<!--        headers: {-->
<!--            'Authorization': 'Basic ' + btoa('myuser:mypswd')-->
<!--        },-->
<!--        url: "http://www.example.org/protected-data.php"-->
<!--    });-->
<!---->
<!--    // using XMLHttpRequest-->
<!--    var xhr = new XMLHttpRequest();-->
<!--    xhr.open("GET", "http://www.example.org/protected-data.php", true);-->
<!--    xhr.withCredentials = true;-->
<!--    xhr.setRequestHeader("Authorization", 'Basic ' + btoa('myuser:mypswd'));-->
<!--    xhr.onload = function () {-->
<!--        console.log(xhr.responseText);-->
<!--    };-->
<!--    xhr.send();-->
<!--</script>-->

<script>
    var server = $('#server').val();
    var port = $('#port').val();


    var host = 'http://192.168.1.140';
    var host = 'https://192.168.1.16';
    var url = host+':6587/odata/sample_payroll_data/';
    var details = 'EmployeeLeaveTables';



    $("#getdataset").click(function() {

        var vals;
        var vals2;

        $('#stat').html($('#stat').html() + '<br>Step 1: Retrieving Employees ');

        $.ajax({
            headers: {
                'Authorization': 'Basic ' + btoa($('#user').val()+':'+$('#pwd').val())
            },
            url: url+'Employee',
            dataType: 'JSON',
            type: 'GET',
            error: function (data) {
                $('#stat').html($('#stat').html() + '<br>Error: Unable to connect <br>'+data);
            },
            complete: function (data) {
                vals = data.responseJSON.value;
//                alert(vals.length);
                var s = '<table><tr><th>name</th><th>loc</th><th>code</th><th>country</th><th>email</th><th>hours</th><th>leave</th></td>';
                for (i = 0; i < vals.length; i++) {
                    s += '<tr>';
                    s += '<td>' + vals[i].FullName + '</td>';
                    s += '<td>' + vals[i].Location + '</td>';
                    s += '<td>' + vals[i].Code + '</td>';
                    s += '<td>' + vals[i].Country + '</td>';
                    s += '<td>' + vals[i].Email + '</td>';
                    s += '<td>' + vals[i].HourPerDay + '</td>';

                    s += '<td id="' + vals[i].Code + '">';

                    s += '</td>';
                    s += '<tr>';
                }
                s += '</table>'

                $('#results').html(s);

                $('#stat').html($('#stat').html() + 'Complete.');
                $('#stat').html($('#stat').html() + '<br>Step 2: Retrieving Leave Balances ');
                $.ajax({
                    headers: {
                        'Authorization': 'Basic ' + btoa($('#user').val()+':'+$('#pwd').val())
                    },
                    url: url+details,
                    dataType: 'JSON',
                    type: 'GET',
//                    success: function (data1) {
//                    },
                    complete: function (data1) {
                        vals2 = data1.responseJSON.value;;
                        $('#stat').html($('#stat').html() + 'Complete.');
                        $('#stat').html($('#stat').html() + '<br>Step 3: Processing Data ');
                        $.ajax({
                            url: "<?php echo base_url()?>ajax/uploadjson",
                            dataType: "html",
                            type: "post",
                            data:{
                                employees: vals,
                                leavedata: vals2
                            },
//                            success: function () {
//                            },
                            complete: function (data3) {
                                $('#stat').html($('#stat').html() + 'Complete.');
                                $('#stat').html($('#stat').html() + '<BR><br>FINISHED. <br><BR>');
                                $('#stat').html($('#stat').html()+data3.responseText);
                            }
                        });

                    }
                });

            }
        });


    });







</script>
<?php include "footer.php" ?>

</body>

</html>
