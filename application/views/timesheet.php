<?php
include "header.php";

$edit = false;
$readonly = false;

if (isset($timesheetid) && $timesheetid != "") {
    $edit = true;

//    if(isset($readonly) && $readonly) $readonly == true;

    $timesheet = $this->user_model->getTimesheet($timesheetid)->row();
    $timeentries = $this->user_model->getTimesheetDetails($timesheetid);
    $leaveitems = $this->user_model->getLeaveByTimesheet($timesheetid);
    if (isset($leaveitems) && $leaveitems->num_rows() > 0)
        $leave = $leaveitems->row();
    else
        $leave = null;
    $this_user = $this->user_model->getUser($timesheet->user)->row();
} else {
    $this_user = $this->user_model->getUser($this->session->userdata('id'))->row();
}
$today = date_format(date_create(), 'd/m/Y');
$changeattachment = false;
$company = $this->user_model->getCompany($this->session->userdata('company'));


?>
<?php
if (isset($isapprover) && $isapprover) {
    $this_user = $this->user_model->getUser($timesheet->user)->row();

}
?>

<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <?php
        include "navbar.php";
        ?>

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Timesheet for <?php echo $this_user->name ?>
                    </h3>
                </div>
                <div class="title_right">
                    <h3>
                    </h3>
                </div>
                <div class="modal fade bs-world-modal" tabindex="-1" role="dialog" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                            aria-hidden="true">X</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">View Attachment</h4>
                            </div>
                            <div class="modal-body ">
                                <iframe id="modalsrc" src="" width="100%"></iframe>
                            </div>
                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row">
                <!-- form date pickers -->
                <div class="col-xs-12 col-lg-10">
                    <div class="x_panel" style="">
                        <div class="x_content">


                            <div id="leaverow">
                                <?php
                                if ($edit) {
                                    $week = $timesheet->week;
                                    $year = $timesheet->year;
                                } else {
                                    $handle = $this->input->post('handle');
                                    $year = substr($handle, 0, 4);
                                    $week = substr($handle, 4, 2);
                                }


                                $message = $this->session->userdata('message');
                                $this->session->set_userdata(array('message' => ""));
                                echo '<fieldset>
                                            <div class="control-group">
                                                <div class="controls"><h3 class="danger flash col-xs-12">' . $message . '</h3>
                                            </div>
                                          </fieldset>';
                                //if(isset($message) && $message !="") echo "<div class='error_message'>".$message."</div>"
                                ?>
                                <form class="form-horizontal form-label-left" novalidate="" id="my-form"
                                      action="<?php echo base_url() . 'user/addtime' ?>" method="post"
                                      enctype="multipart/form-data">

                                    <input type="hidden" name="year" value="<?php echo $year ?>">
                                    <input type="hidden" name="weekid"
                                           value="<?php echo str_pad($week, 2, '0', STR_PAD_LEFT) ?>">

                                    <input id="timesheetid" name="timesheetid" type="hidden"
                                           value="<?php if ($edit) echo $timesheet->id ?>">
                                    <input name="user" type="hidden" value="<?php echo $this_user->id ?>">

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">

                                        </label>
                                        <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="item form-group" style="font-weight:bolder">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <?php
                                        // for startdate to + 7 (days)
                                        // add another row.
                                        $counter = 0;

                                        $begin = new DateTime();
                                        $begin->setISODate($year, $week, $company->timesheetstart);
                                        $end = new DateTime();
                                        $end->setISODate($year, $week, $company->timesheetstart);
                                        $end = $end->modify('+7 days');    // need to add one more day

                                        $interval = new DateInterval('P1D');
                                        $daterange = new DatePeriod($begin, $interval, $end);
                                        $i = 0;
                                        $rows = 0;

                                        $types = $this->user_model->getLeaveTypes(null, null);
                                        $costcentres = $this->admin_model->getCostCentres($this->session->userdata('company'));
                                        //                                        $types1 = $this->user_model->getLeaveTypes(null, 1);
                                        $decimal = $company->decimal;
                                        ?>

                                        <div class="col-xs-12 ">
                                            <table class="table" id="mytable">
                                                <thead>
                                                <tr>
                                                    <th>Time Type</th>
                                                    <th>Cost Centre</th>
                                                    <?php
                                                    foreach ($daterange as $date) {
                                                        echo '<th style="text-align: right;margin-left: auto">' . date_format($date, 'D') . '<br>' . date_format($date, 'd/m') . '</th>';
                                                    }
                                                    ?>
                                                    <th style="text-align: right">Total</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>


                                                <?php

                                                $end = $end->modify('-1 days');    // need to add one more day
                                                $leaverequests = $this->user_model->getLeaveByDate($this_user->id, date_format($begin, 'Y-m-d'), date_format($end, 'Y-m-d'));
                                                $leavetype = '';

                                                $count = 0;

                                                foreach ($leaverequests->result() as $leaveitem){

                                                //    echo 'Id is '.$leaveitem->id.' and date is '.$leaveitem->fromdate.' to '.$leaveitem->todate.' of type '.$leaveitem->leavetype.' and hours '.$leaveitem->hours.'<br>';
                                                if ($leaveitem->leavetype != $leavetype) {
                                                if ($count > 0) {
                                                    for ($pointer; $pointer < 7; $pointer++) {
                                                        echo "<td></td>";
                                                    }
                                                    echo "<td></td></tr>";
                                                }
                                                $count += 1; // starting a new row
                                                $pointer = 0;
                                                ?>
                                                <tr>
                                                    <td><input type="text" class="form-control " disabled
                                                               value="<?php echo $this->user_model->getLeaveType($leaveitem->leavetype)->row()->description ?>">
                                                    </td>
                                                    <?php
                                                    }
                                                    $leavetype = $leaveitem->leavetype;

                                                    $localcount = 1;
                                                    foreach ($daterange as $date) {
                                                        if ($leaveitem->days == 1 && date_format($date, 'Y-m-d') == $leaveitem->fromdate) {
                                                            $pointer = $localcount;
                                                            if ($decimal) {
                                                                $hoursminutes = $leaveitem->hours;
                                                            } else {
                                                                $mins = ($leaveitem->hours - (int)$leaveitem->hours) * 60;
                                                                $hoursminutes = floor($leaveitem->hours) . ':' . sprintf("%02d", number_format($mins));
                                                            }
                                                            ?>
                                                            <td><input type="text" class="form-control" disabled
                                                                       style="max-width: 70px; margin-left: auto; padding-left: 2px; padding-right: 2px; text-align:right"
                                                                       value="<?php echo $hoursminutes ?>">
                                                            </td>
                                                            <?php
                                                            $localcount += 1;
                                                            continue 2;
                                                        } elseif ($leaveitem->days > 1 && $leaveitem->fromdate <= date_format($date, 'Y-m-d')) {
                                                            // set the right start and end dates for the period
//                                                                if($leaveitem->fromdate <= date_format($date, 'Y-m-d'))
                                                            $startDate = date_format($date, 'Y-m-d');
//                                                                else
//                                                                    $startDate = $leaveitem->fromdate;

                                                            if ($leaveitem->todate > date_format($end, 'Y-m-d'))
                                                                $endDate = date_format($end, 'Y-m-d');
                                                            else
                                                                $endDate = $leaveitem->todate;


                                                            if ($company->sevendayleave) {
                                                                $workingDays = $this->user_model->getWorkingHours($startDate, $endDate, $this_user->id);
                                                            } else {
                                                                $workingDays = $this->user_model->getWorkingDays($startDate, $endDate, $this_user->id);
                                                            }

                                                            $hours = $workingDays['hours'];
                                                            $days = $workingDays['days'];

                                                            $startDate = strtotime($startDate);
                                                            $endDate = strtotime($endDate);

                                                            $colspan = $endDate - $startDate;
                                                            $colspan = floor($colspan / (60 * 60 * 24)) + 1;
                                                            $pointer = $localcount + $colspan;

                                                            ?>
                                                            <td colspan="<?php echo $colspan ?>">
                                                                <?php
                                                                if ($hours > 0) {
                                                                    if ($decimal) {
                                                                        $hoursminutes = $hours;
                                                                    } else {
                                                                        $mins = ($hours - (int)$hours) * 60;
                                                                        $hoursminutes = floor($hours) . ':' . sprintf("%02d", number_format($mins));
                                                                    }
                                                                    ?>
                                                                    <input type="text" class="form-control" disabled
                                                                           style="<?php if ($colspan > 1) echo 'width: 100%'; else echo 'max-width: 70px;' ?>; margin-left: auto; padding-left: 2px; padding-right: 2px; text-align: right"
                                                                           value="<?php if ($days > 1) echo $days . ' days (' . $hoursminutes . ')'; else echo $hoursminutes ?>">
                                                                <?php } ?>
                                                            </td>
                                                            <?php
                                                            $localcount += 1;
                                                            continue 2;

                                                        } else {
                                                            if ($localcount > $pointer) {
                                                                echo '<td></td>';
                                                            }
                                                        }
                                                        $localcount += 1;
                                                    }
                                                    }
                                                    if ($count > 0) {
                                                        for ($pointer; $pointer < 7; $pointer++) {
                                                            echo "<td></td>";
                                                        }
                                                        echo "<td></td></tr>";
                                                    }


                                                if ($edit) {


                                                        // for each leave type
                                                        foreach ($types->result() as $type1) {
                                                            $showrow = false;
                                                            $rowdata = array(0, 0, 0, 0, 0, 0, 0, 0);
                                                            foreach ($timeentries->result() as $timeentry) {
                                                                if ($type1->id == $timeentry->type) {
                                                                    if ($timeentry->hours + $timeentry->minutes > 0) {
                                                                        if ($decimal) {
                                                                            $lstr = $timeentry->hours . "." . floor($timeentry->minutes / 60 * 100);
                                                                        } else {
                                                                            $lstr = $timeentry->hours . ":";
                                                                            if ($timeentry->minutes < 10)
                                                                                $lstr .= '0';
                                                                            $lstr .= $timeentry->minutes;
                                                                        }
                                                                        $rowdata[$timeentry->day] = $lstr;
                                                                        $showrow = true;
                                                                    }
                                                                }
                                                            }
                                                            if ($showrow) {
                                                            ?>
                                                            <tr id="row<?php echo $rows ?>">
                                                                <td>
                                                                    <?php
                                                                    if (1) {
                                                                        ?>
                                                                        <select class="form-control"
                                                                                name="<?php echo 't' . $rows ?>"
                                                                                id="<?php echo 't' . $rows ?>"
                                                                                style="display: inline; min-width:130px">
                                                                            <option value="0">None</option>
                                                                            <?php
                                                                            $t = false;
                                                                            foreach ($types->result() as $categories) {
                                                                                echo "<option value='" . $categories->id . "'";
                                                                                if ($type1->id == $categories->id) echo ' selected ';
                                                                                echo ">" . $categories->description . "</option>";
                                                                            }
                                                                            ?>
                                                                        </select>


                                                                        <?php
                                                                        // get all the entries that relate to this type category
                                                                        // add a row with that type
                                                                        // put the values into the right 'days'
                                                                    }
                                                                    ?>

                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    if (isset($costcentres)) {
                                                                        ?>
                                                                        <select class="form-control"
                                                                                name="<?php echo 'c' . $rows ?>"
                                                                                id="<?php echo 'c' . $rows ?>"
                                                                                style="display: inline; min-width:130px">
                                                                            <option value="0">None</option>
                                                                            <?php
                                                                            foreach ($costcentres->result() as $centre) {
                                                                                echo "<option value='" . $centre->id . "'";
                                                                                echo ">" . $centre->costcentrecode . " - " . $centre->description . "</option>";
                                                                            }
                                                                            ?>
                                                                        </select>

                                                                        <?php
                                                                    } ?>
                                                                </td>

                                                                <?php
                                                                foreach ($daterange as $date) {
                                                                    echo '<td>';
                                                                    ?>
                                                                    <input type="text" class="form-control timesheet"
                                                                           id="<?php echo 'r' . $rows . 'd' . $date->format('N') ?>"
                                                                           name="<?php echo 'r' . $rows . 'd' . $date->format('N') ?>"
                                                                           style="max-width: 70px; margin-left: auto; padding-left: 2px; padding-right: 2px"
                                                                           value="<?php if ($rowdata[$date->format('N')] != 0) echo $rowdata[$date->format('N')] ?>">
                                                                    <?php
                                                                    echo '</td>';
                                                                }
                                                                ?>
                                                                <td id="rtotal<?php echo $rows ?>"
                                                                    style="vertical-align: middle;text-align: right">0
                                                                </td>
                                                                <td><a class="close-link"
                                                                       onclick="remrow(<?php echo $rows ?>)"><i
                                                                                class="fa fa-close"></i></a></td>
                                                            </tr>
                                                        <?php
                                                        $rows++;

                                                        }// end showrow
                                                    }
                                                // add a row
                                                // and add the times to it
                                                // else, just add a blank row
                                                //$timeEntry = $this->user_model->getTimeEntryByDay($timesheet->id, $date->format('N'));
                                                } else {
                                                    ?>
                                                    <tr id="row<?php echo $rows ?>">
                                                        <td>
                                                            <?php
                                                            if (isset($types)) {
                                                                ?>
                                                                <select class="form-control"
                                                                        name="<?php echo 't' . $rows ?>"
                                                                        id="<?php echo 't' . $rows ?>"
                                                                        style="display: inline; min-width:130px">
                                                                    <option value="0">None</option>
                                                                    <?php
                                                                    foreach ($types->result() as $type) {
                                                                        echo "<option value='" . $type->id . "'";
                                                                        echo ">" . $type->description . "</option>";
                                                                    }
                                                                    ?>
                                                                </select>

                                                                <?php
                                                            } ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            if (isset($costcentres)) {
                                                                ?>
                                                                <select class="form-control"
                                                                        name="<?php echo 'c' . $rows ?>"
                                                                        id="<?php echo 'c' . $rows ?>"
                                                                        style="display: inline; min-width:130px">
                                                                    <option value="0">None</option>
                                                                    <?php
                                                                    foreach ($costcentres->result() as $centre) {
                                                                        echo "<option value='" . $centre->id . "'";
                                                                        echo ">" . $centre->costcentrecode . " - " . $centre->description . "</option>";
                                                                    }
                                                                    ?>
                                                                </select>

                                                                <?php
                                                            } ?>
                                                        </td>
                                                        <?php
                                                        foreach ($daterange as $date) {
                                                            echo '<td>';
                                                            ?>
                                                            <input type="text" class="form-control timesheet"
                                                                   id="<?php echo 'r' . $rows . 'd' . $date->format('N') ?>"
                                                                   name="<?php echo 'r' . $rows . 'd' . $date->format('N') ?>"
                                                                   style="max-width: 70px; margin-left: auto; padding-left:2px; padding-right: 2px">
                                                            <?php
                                                            echo '</td>';
                                                        }
                                                        ?>
                                                        <td id="rtotal<?php echo $rows ?>"
                                                            style="vertical-align: middle;text-align: right">0
                                                        </td>
                                                        <td><a class="close-link" onclick="remrow(<?php echo $rows ?>)"><i
                                                                        class="fa fa-close"></i></a></td>
                                                    </tr>
                                                    <?php
                                                    $rows++;
                                                }
                                                ?>

                                                <tr style="text-align: right; font-weight: bold">
                                                    <td id=""></td>
                                                    <td></td>
                                                    <?php
                                                    foreach ($daterange as $date) {
                                                        ?>
                                                        <td id="ctotal<?php echo $date->format('N') ?>"
                                                            style="vertical-align: middle;text-align: right; padding-right: 14px">
                                                            0
                                                        </td>
                                                        <?php
                                                    }
                                                    ?>
                                                    <td id="total" style="text-align: right; font-weight: bold">0</td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="9">
                                                        <button type="button" name="addRow" id="addRow"
                                                                class="btn btn-default"
                                                                style="width: 100%; max-width:150px">Add Row
                                                        </button>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                            <hr style="width: 70%">
                                            <input type="hidden" name="rows" id="rows" value="<?php echo $rows ?>">
                                            <input type="hidden" name="start" id="start"
                                                   value="<?php echo $company->timesheetstart ?>">


                                        </div>

                                        <hr>
                                        <br>
                                        <?php if (!$edit || $leave->approver != $this->session->userdata('id')) { ?>
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                    Approver <span class="required">*</span>
                                                </label>
                                                <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                                    <?php $approvers = $this->user_model->getApprovers();
                                                    if (isset($approvers)) {
                                                        ?>
                                                        <select class="form-control" name="approver" id="approver">
                                                            <?php
                                                            foreach ($approvers->result() as $approver) {
                                                                if ($approver->id != $this_user->id) {
                                                                    ?>
                                                                    <option
                                                                            value="<?php echo $approver->id ?>" <?php
                                                                    if ($approver->id == $this_user->defaultapprover) echo 'selected' ?>><?php echo $approver->name ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                        <?php
                                                    } ?>
                                                </div>
                                            </div>
                                        <?php } ?>


                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                Comments
                                            </label>
                                            <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="message" class="form-control" name="comments"><?php
                                                    if ($edit && isset($leave) && !is_null($leave->comments) && $leave->comments != "") {
                                                        echo $leave->comments;
                                                    }
                                                    ?></textarea>
                                            </div>
                                        </div>


                                        <?php if ($edit && isset($leave) && !is_null($leave->attachment) && $leave->attachment != '') {
                                            $changeattachment = true; ?>
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                    Current Attachment
                                                </label>
                                                <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                                    <button type="button" class="btn-link btn-primary btn-xs"
                                                            data-toggle="modal" data-target=".bs-world-modal"
                                                            onclick="document.getElementById('modalsrc').src ='<?php echo base_url() . $leave->attachment ?>'">
                                                        <i class="fa fa-file"></i> <?php echo $leave->attachment ?>
                                                    </button>
                                                </div>
                                            </div>

                                        <?php } else {
                                            ?>
                                            <?php
                                        }
                                        ?>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                <?php if ($changeattachment) echo 'Change ' ?>Attachment if required
                                            </label>
                                            <div class="item form-group col-md-6 col-sm-6 col-xs-12">
                                                <input type="file" class="form-control" id="attachment" name="userfile"
                                                       style="border: 1px solid #DDE2E8; line-height: 0px"/>
                                            </div>
                                        </div>

                                        <div class="form-group" id="buttons">
                                            <div class="col-xs-12 col-md-offset-3">
                                                <P></P>
                                                <?php
                                                if (isset($isapprover) && $isapprover) {
                                                    ?>
                                                    <button type="submit" name="btnApprove"
                                                            class="btn btn-success "
                                                            style="width: 100%; max-width:150px">Approve
                                                    </button>
                                                <?php } else {

                                                    ?>
                                                    <button id="btnSubmit" name="btnSubmit" type="submit"
                                                            class="btn btn-primary"
                                                            style="width: 100%; max-width:150px">Submit
                                                    </button>
                                                    <button id="btnSave" name="btnSave" type="submit"
                                                            class="btn btn-default"
                                                            style="width: 100%; max-width:150px; color:#1D92EF; font-weight: 600">
                                                        Save As Draft
                                                    </button>
                                                    <?php
                                                } ?>

                                                <a type="link" class="btn btn-default"
                                                   href="<?php echo base_url() . 'user/mytimesheets' ?>"
                                                   style="width: 100%; max-width:150px">Cancel
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </form>


                            </div>


                        </div>

                    </div>
                </div>
                <!-- /form datepicker -->
            </div>


        </div>

    </div>
    <!-- /page content -->
</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url() ?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url() ?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>js/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>js/custom.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url() ?>js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/datepicker/daterangepicker.js"></script>
<script src="<?php echo base_url() ?>js/validator/validator.js"></script>


<!-- datepicker -->
<script type="text/javascript">

    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
        .on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
//            e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            return true;
        //this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);


    ///////////////////////////////////////////////

</script>
<!-- datepicker -->

<!-- /datepicker -->

<!-- /input mask -->
<!-- Datatables -->
<script src="<?php echo base_url() ?>js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>js/datatables/tools/js/dataTables.tableTools.js"></script>
<script src="<?php echo base_url() ?>js/input_mask/jquery.inputmask.bundle.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.core.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.buttons.js"></script>
<script src="<?php echo base_url() ?>js/notify/pnotify.nonblock.js"></script>

<script src="<?php echo base_url() ?>js/dropzone/dropzone.js"></script>


<script>
    $(document).ready(function () {
        recalc();

        $('#my-form').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        var t = $('#buttons').height();
        $("#dz-form").css({top: '-' + (190 + t) + 'px'});

        <?php if($decimal){ ?>
        $('.timesheet').inputmask({
            mask: "9.(9)|(99)",
            oncomplete: function () {
                recalc();
            },
            onincomplete: function () {
                // need to add a 0 or two to the end of the field
                var cellval = $(this).val();
                $(this).val(cellval.replace("_", "0"));
                recalc();
            },
            onCleared: function () {
                recalc();
            },
            greedy: false
            ,
            rightAlign: true

        });
        <?php
        }
        else{
        ?>
        $('.timesheet').inputmask({
            mask: "9:(09)|(19)|(29)|(39)|(49)|(59)",
            oncomplete: function () {
                recalc();
            },
            onincomplete: function () {
                // need to add a 0 or two to the end of the field
                var cellval = $(this).val();
                $(this).val(cellval.replace("_", "0"));
                recalc();
            },
            onCleared: function () {
                recalc();
            },
            greedy: false
            ,
            rightAlign: true

        });
        <?php
        }?>


    });


    $('.modal').on('show.bs.modal', function () {
        $('.modal-body').css('height', $(window).height() * 0.75);
        $('#modalsrc').css('height', $(window).height() * 0.75);

    });


    $('#type').change(function () {
        var notes = this.options[this.selectedIndex].getAttribute('Tag');
        if (notes != '')
            new PNotify({title: 'Leave Policy', text: notes, type: 'warning'});

    });

    function remrow(i) {
        var row = document.getElementById('row' + i);
        row.parentNode.removeChild(row)
        recalc();
    }
    ;

    function recalc() {
        var total = 0;
        var rowtotal = 0;
        var coltotals = [0, 0, 0, 0, 0, 0, 0, 0];
        var rows = $('#rows').val();


//        var table = document.getElementById("mytable");
        var table = document.getElementById('mytable').getElementsByTagName('tbody')[0];
        ;
        for (var i = 0, row; row = table.rows[i]; i++) {
            //iterate through rows
            //rows would be accessed using the "row" variable assigned in the for loop
            var rowid = row.id;
            var rowindex = rowid.substr(3);
            var rowtotal = 0;
            if (rowindex != '')
                for (colindex = 1; colindex <= 7; colindex++) {
                    var cellhours = 0;
                    var cellmins = 0;

                    var cellval = $('#r' + rowindex + 'd' + colindex).val();
                    cellval = cellval.replace("_", "0");
                    $('#r' + rowindex + 'd' + colindex).val(cellval);

                    if (cellval == '') {
                        cellval = 0;
                        cellhours = 0;
                        cellmins = 0;
                    }
                    else if (cellval.indexOf(':') > 0) {
                        cellhours = Number(cellval.substr(0, cellval.indexOf(':')));
                        cellmins = Number(cellval.substr(cellval.indexOf(':') + 1, cellval.length - cellval.indexOf(':')));
                    } else {
                        cellhours = Number(cellval);
                        cellmins = 0;
//                    $('#r'+rowindex+'d'+colindex).val(cellhours+':00')
                    }

                    coltotals[colindex] += cellhours + cellmins / 60;
                    rowtotal += cellhours + cellmins / 60;
                }
            var mins = Math.round((rowtotal - Math.floor(rowtotal)) * 60);
            var tstr = '' + Math.floor(rowtotal) + ':';
            if (mins < 10)
                tstr += '0';

            <?php if ($decimal){?>
            tstr = rowtotal;
            <?php }
            else{ ?>
            tstr += mins;
            <?php } ?>
            $('#rtotal' + rowindex).html(tstr);
            total += rowtotal;
        }

///////////////////////////////////////////////

        for (colindex = 1; colindex <= 7; colindex++) {
            var mins = Math.round((coltotals[colindex] - Math.floor(coltotals[colindex])) * 60);
            var tstr = '' + Math.floor(coltotals[colindex]) + ':';
            if (mins < 10)
                tstr += '0';
//            tstr += mins;
            <?php if ($decimal){?>
            tstr = coltotals[colindex];
            <?php }
            else{ ?>
            tstr += mins;
            <?php } ?>
            $('#ctotal' + colindex).html(tstr);
        }

        // update the row total
        // update the column total
        // update the total
        <?php
        if ($decimal){
        ?>
//            var mins = Math.round((total - Math.floor(total)));
//            var mins = Math.trunc((total - Math.trunc(total))*10);
        var tstr = total;
        <?php
        }
        else{
        ?>
        var mins = Math.round((total - Math.floor(total)) * 60);
        var tstr = '' + Math.floor(total) + ':';

        if (mins < 10)
            tstr += '0';
        tstr += mins;

        <?php
        }
        ?>

        $('#total').html(tstr);
        if (total == 0)
            $('#btnSubmit').attr("disabled", "disabled");
        else
            $('#btnSubmit').removeAttr("disabled");
    }


    var rows;

    function newrow() {
        var rows = $('#rows').val();
        var tableRef = document.getElementById('mytable').getElementsByTagName('tbody')[0];

// Insert a row in the table at the last row
        var newRow = tableRef.insertRow(tableRef.rows.length - 1);
        newRow.id = 'row' + rows;

        var newCell = newRow.insertCell(-1);

        var c0 = '       <select class="form-control" name="t' + rows + '"  \
                                id="t' + rows + '" \
                                style="display: inline; min-width:130px">   \
                                <option value="0">None</option> \
                        <?php
            foreach ($types->result() as $type) {
                echo '<option value="' . $type->id . '" >' . $type->description . '</option> \ ';
            }
            ?>
            < / select > ';

        var c1 = '       <select class="form-control" name="c' + rows + '"  \
                                id="c' + rows + '" \
                                style="display: inline; min-width:130px">   \
                                <option value="0">None</option> \
                        <?php
            foreach ($costcentres->result() as $centre) {
                echo '<option value="' . $centre->id . '" >' . $centre->costcentrecode . ' - ' . $centre->description . '</option> \ ';
            }
            ?>
            < / select > ';

//        var newText  = document.createTextNode(c0);
        newCell.innerHTML = c0;

        var newCell = newRow.insertCell(-1);
        newCell.innerHTML = c1;

        <?php
        $counter = 0;
        foreach ($daterange as $date) {
        $counter++;
        ?>
        var c<?php echo $counter ?> = '<input type="text" class="form-control timesheet"  id="r' + rows + 'd<?php echo $date->format('N') ?>" name="r' + rows + 'd<?php echo $date->format('N') ?>" style="max-width: 70px; padding-left: 2px; padding-right: 2px; margin-left: auto"> ';
        var newCell = newRow.insertCell(-1);
        newCell.innerHTML = c<?php echo $counter ?>;

        <?php if($decimal){ ?>
        $('#r' + rows + 'd<?php echo $date->format('N') ?>').inputmask({
            mask: "9.(9)|(99)",
            oncomplete: function () {
                recalc();
            },
            onincomplete: function () {
                // need to add a 0 or two to the end of the field
                var cellval = $(this).val();
                $(this).val(cellval.replace("_", "0"));
                recalc();
            },
            onCleared: function () {
                recalc();
            },
            greedy: false
            ,
            rightAlign: true

        });
        <?php
        }
        else{
        ?>
        $('#r' + rows + 'd<?php echo $date->format('N') ?>').inputmask({
            mask: "9:(09)|(19)|(29)|(39)|(49)|(59)",
            oncomplete: function () {
                recalc();
            },
            onincomplete: function () {
                // need to add a 0 or two to the end of the field
                var cellval = $(this).val();
                $(this).val(cellval.replace("_", "0"));
                recalc();
            },
            onCleared: function () {
                recalc();
            },
            greedy: false
            ,
            rightAlign: true

        });
        <?php
        }
        }
        ?>
        var ct = '<td>0</td>';
        var cx = '<td><a class="close-link" onclick="remrow(' + rows + ')" ><i class="fa fa-close"></i></a></td>';

        var newCell = newRow.insertCell(-1);
        newCell.innerHTML = ct;
        newCell.id = "rtotal" + rows;
        newCell.style = "vertical-align: middle; text-align:right";

        var newCell = newRow.insertCell(-1);
        newCell.innerHTML = cx;
        rows++;
        $('#rows').val(rows);


    }

    $('#addRow').click(function () {
        newrow();
        recalc();

    });

    <?php
    if(!$readonly){
    ?>

    $(".input").prop('disabled', true);
    <?php
    }

    ?>

</script>

<!-- footer content -->
<?php include "footer.php" ?>
<!-- /footer content -->
</body>
</html>
