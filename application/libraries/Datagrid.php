<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class datagrid {
    
    function datagrid()
    {
        $CI = & get_instance();
        log_message('Debug', 'datagrid class is loaded.');
    }
 
    function load($param=NULL)
    {
        include_once APPPATH.'/third_party/datagrid/phpmydatagrid.class.php';
         
        if ($param == NULL)
        {
            $param = '"en-GB-x","A4","","",10,10,10,10,6,3';         
        }
         
        return new mPDF($param);
    }
}